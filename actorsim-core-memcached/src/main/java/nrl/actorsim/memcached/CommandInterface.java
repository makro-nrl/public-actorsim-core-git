package nrl.actorsim.memcached;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.LoggerFactory;

import java.io.Serializable;
import java.util.function.Consumer;

import static nrl.actorsim.memcached.CommandInterface.Result.*;

public class CommandInterface implements Serializable {
    final static org.slf4j.Logger logger = LoggerFactory.getLogger(CommandInterface.class);

    public Integer id;
    public Result result = Result.UNKNOWN;

    public boolean __Command__ = true; // a "fake" member to align with the python side
    public int maxsize;  // helps determine unset integer values

    private static final ObjectMapper mapper = new ObjectMapper();

    @JsonIgnore
    private ProcessListener listener;

    @JsonIgnore
    public Consumer<CommandInterface> executable;

    @JsonIgnore
    public Interruptable interruptable;

    // ====================================================
    // region<Result>

    public void setResult(Result result) {
        this.result = result;
    }

    public void setResultToStopped() {
        this.result = STOPPED;
    }

    public void setResultToExecuting() {
        this.result = EXECUTING;
    }

    public void setResultToSuccess() {
        this.result = SUCCESS;
    }

    public void setResultToFailed() {
        this.result = FAIL;
    }

    public void setResultToPaused() {
        this.result = PAUSED;
    }

    public boolean isFinishedExecuting() {
        return result == SUCCESS
                || result == Result.FAIL;
    }

    public boolean isExecuting() {
        return result == EXECUTING;
    }

    // endregion
    // ====================================================

    // ====================================================
    // region<Execute and Listen>

    public boolean isExecutable() {
        return executable != null;
    }

    public void add(Consumer<CommandInterface> executable) {
        this.executable = executable;
    }

    public void addListener(ProcessListener listener) {
        this.listener = listener;
    }

    public ProcessListener getListener() {
        if (listener == null) {
            return ProcessListener.NULL_PROCESS_LISTENER;
        }
        return listener;
    }

    public boolean isInterruptable() {
        return interruptable != null;
    }

    public void add(Interruptable<? extends CommandInterface> interruptable) {
        this.interruptable = interruptable;
    }

    // endregion
    // ====================================================


    // ====================================================
    // region<JSON processing>

    public static CommandInterface fromJSON(String json, Class<? extends CommandInterface> commandClass) {
        logger.debug("Attempting to parse '{}'", json);
        try {
            return mapper.readValue(json, commandClass);
        } catch (JsonProcessingException e) {
            logger.error("Error parsing '{}'", json, e);
        }
        return null;
    }

    public String toJSON() {
        try {
            String json = mapper.writeValueAsString(this);
            return json;
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        return "NOT PARSED";
    }

    // endregion
    // ====================================================


    public enum Result {
        UNKNOWN,
        ENQUEUE_SUCCESS,
        ENQUEUE_FAIL,
        EXECUTING,
        PAUSED,
        SUCCESS,
        FAIL,
        STOPPED;
    }


}
