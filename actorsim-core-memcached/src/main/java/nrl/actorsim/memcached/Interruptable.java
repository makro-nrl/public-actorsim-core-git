package nrl.actorsim.memcached;

public interface Interruptable<T extends CommandInterface> {
    void onPause(T command);
    void onResume(T command);
    void onStop(T command);
}
