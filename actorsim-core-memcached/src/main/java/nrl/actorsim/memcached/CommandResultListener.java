package nrl.actorsim.memcached;


import nrl.actorsim.utils.WorkerThread;

public class CommandResultListener implements ProcessListener {
    private final CommandInterface command;
    private final WorkerThread worker;

    public CommandResultListener(CommandInterface command, WorkerThread worker) {
        this.command = command;
        this.worker = worker;
    }

    @Override
    public void onStarted() {
        command.setResultToExecuting();
        worker.notifyWorkerThereIsWork();
    }

    @Override
    public void onFinishedSuccessfully() {
        command.setResultToSuccess();
        worker.notifyWorkerThereIsWork();
    }

    @Override
    public void onError() {
        command.setResultToFailed();
        worker.notifyWorkerThereIsWork();
    }
}
