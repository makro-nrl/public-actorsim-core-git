package nrl.actorsim.memcached;

import nrl.actorsim.utils.WorkerThread;
import org.slf4j.LoggerFactory;

import java.time.Duration;

public class MemcachedOptions extends WorkerThread.Options {
    final static org.slf4j.Logger logger = LoggerFactory.getLogger(MemcachedOptions.class);
    public Class<? extends CommandInterface> commandClass;
    String commandKey;
    String resultKey;
    Duration wakeUpTimeout = Duration.ofSeconds(2);
    Duration initTimeout = Duration.ofSeconds(1);

    private MemcachedOptions() {
    }

    public static Builder builder() {
        return new Builder(new MemcachedOptions());
    }


    public static class Builder extends WorkerThread.Options.Builder<MemcachedOptions, Builder> {
        protected Builder(MemcachedOptions instance) {
            super(instance);
        }

        public Builder commandClass(Class<? extends CommandInterface> commandClass) {
            optionsInstance.commandClass = commandClass;
            return getThis();
        }

        public Builder commandKey(String commandKey) {
            optionsInstance.commandKey = commandKey;
            return getThis();
        }

        public Builder resultKey(String resultKey) {
            optionsInstance.resultKey = resultKey;
            return getThis();
        }

        public Builder wakeUpTimeout(Duration timeout) {
            optionsInstance.wakeUpTimeout = timeout;
            return getThis();
        }

        public Builder initTimeout(Duration timeout) {
            optionsInstance.initTimeout = timeout;
            return getThis();
        }

        @Override
        protected void validate() {
            super.validate();
            if (optionsInstance.commandClass == null
                    || optionsInstance.commandKey == null
                    || optionsInstance.resultKey == null) {
                String message = "The MemcachedServer requires a command class, command key, and result key to instantiate.";
                logger.error(message);
                throw new IllegalArgumentException(message);
            }
        }
    }
}
