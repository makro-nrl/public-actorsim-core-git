package nrl.actorsim.memcached;

import nrl.actorsim.utils.WorkerThread;

/**
 * A worker that produces commands for a MemcachedClient,
 * and listens for results from a consumer.
 *
 * The keys for producing and consuming are set in MemcachedOptions.
 */
public class MemcachedCommandProducer extends WorkerThread {

    public MemcachedCommandProducer(MemcachedOptions options) {
        super(options);
    }
}
