package nrl.actorsim.memcached;


public interface ProcessListener {
    NullProcessListener NULL_PROCESS_LISTENER = new NullProcessListener();
    default  void onStarted() {
    }
    default void onFinishedSuccessfully() {
    }
    default void onError() {
    }
}
