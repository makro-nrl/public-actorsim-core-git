package nrl.actorsim.memcached;

import net.spy.memcached.MemcachedClient;
import net.spy.memcached.MemcachedNode;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.net.InetSocketAddress;

public class MemcachedConnector {
    final static org.slf4j.Logger logger = LoggerFactory.getLogger(MemcachedConnector.class);

    private MemcachedClient memcache;
    final MemcachedOptions options;

    public MemcachedConnector(MemcachedOptions options) {
        this.options = options;
    }

    public boolean deleteKey(String key) {
        if(checkMemcacheServer()) {
            memcache.delete(key);
            return true;
        }
        return false;
    }

    public Object readKey(String key) {
        if (checkMemcacheServer()) {
            return memcache.get(key);
        }
        return null;
    }

    @SuppressWarnings("UnusedReturnValue")
    public boolean setKey(String key, int expriationTime, String value) {
        if (checkMemcacheServer()) {
            memcache.set(key, expriationTime, value);
            return true;
        }
        return false;
    }

    boolean checkMemcacheServer() {
        boolean activeNodeFound = false;
        initMemcacheClientIfNeeded();
        if (memcache != null) {
            for (MemcachedNode node : memcache.getNodeLocator().getAll()) {
                if (node.isActive()) {
                    activeNodeFound = true;
                }
            }
        }
        if (! activeNodeFound) {
            logger.error("Failed to connect to Memcached server.  Did you forget to start memcached?");
        }
        return activeNodeFound;
    }

    protected void initMemcacheClientIfNeeded()  {
        if (memcache == null) {
            try {
                memcache = new MemcachedClient(new InetSocketAddress("127.0.0.1", 11211));
            } catch (IOException e) {
                logger.error("Could not initialize MemcachedClient because", e);
            }
        }
    }

    boolean initMemcacheKeysNeeded = true;
    public void initMemcacheAndKeysIfNeeded() {
        if (initMemcacheKeysNeeded) {
            initMemcacheClientIfNeeded();
            if (deleteKey(options.commandKey)
                    && deleteKey(options.resultKey)) {
                initMemcacheKeysNeeded = false;
            }
        }
    }



    public CommandInterface readCommand() {
        Object commandRaw = readKey(options.commandKey);
        CommandInterface command = null;
        if (commandRaw != null) {
            logger.debug("Recieved command json: {}", commandRaw);
            deleteKey(options.commandKey);
            command =  CommandInterface.fromJSON(commandRaw.toString(), options.commandClass);
        }
        return command;
    }

    public void sendResult(CommandInterface command) {
        logger.info("Sending result {}", command);
        String jsonResult = command.toJSON();
        int neverExpires = 0;
        setKey(options.resultKey, neverExpires, jsonResult);
    }

    public void sendCommand(CommandInterface command) {
        String jsonCommand = command.toJSON();
        int neverExpires = 0;
        logger.info("Sending command {} with json {}", command, jsonCommand);
        setKey(options.commandKey, neverExpires, jsonCommand);
    }

    public CommandInterface readResult() {
        Object resultRaw = readKey(options.resultKey);
        CommandInterface result = null;
        if (resultRaw != null) {
            logger.debug("Recieved result json: {}", resultRaw);
            deleteKey(options.resultKey);
            result =  CommandInterface.fromJSON(resultRaw.toString(), options.commandClass);
        }
        return result;
    }


}
