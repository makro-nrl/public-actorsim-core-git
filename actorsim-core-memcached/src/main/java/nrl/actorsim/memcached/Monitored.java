package nrl.actorsim.memcached;

public interface Monitored {
    void onUpdate(CommandInterface command);
}
