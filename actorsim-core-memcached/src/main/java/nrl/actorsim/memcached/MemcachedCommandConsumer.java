package nrl.actorsim.memcached;

import ch.qos.logback.classic.Level;
import nrl.actorsim.utils.WorkerThread;
import org.slf4j.LoggerFactory;

import java.util.function.Consumer;

/**
 * A worker that listens for commands from a MemcachedClient,
 * consumes them, and executes them, producing a result.
 *
 * The keys for producing and consuming are set in MemcachedOptions.
 */
public class MemcachedCommandConsumer extends WorkerThread {
    final static org.slf4j.Logger logger = LoggerFactory.getLogger(MemcachedCommandConsumer.class);
    final MemcachedConnector memcachedConnector;

    boolean needsInit = true;
    boolean printCommandWaitStart = true;

    Consumer<? super CommandInterface> commandConsumer;

    public MemcachedCommandConsumer(MemcachedOptions options) {
        super(options);
        memcachedConnector = new MemcachedConnector(options);
    }

    MemcachedOptions getOptions() {
        return (MemcachedOptions) options;
    }

    public void initAndStart(Consumer<? super CommandInterface> commandConsumer) {
        this.commandConsumer = commandConsumer;
        start();
        setWakeUpCall(getOptions().initTimeout);

        //get rid of annoying messages from MemcachedClient's WorkerThread!
        ((ch.qos.logback.classic.Logger) LoggerFactory.getLogger(WorkerThread.class)).setLevel(Level.INFO);
    }

    public void testInitAndStart()  {
        start();
        setWakeUpCall(getOptions().initTimeout);
    }

    @Override
    public WorkResult performWork() {
        memcachedConnector.initMemcacheAndKeysIfNeeded();
        checkForNewCommandAndExecute();
        setWakeUpCall(getOptions().wakeUpTimeout);
        return WorkResult.CONTINUE;
    }

    private void checkForNewCommandAndExecute() {
        if (printCommandWaitStart) {
            logger.debug("Waiting for new command...");
            printCommandWaitStart = false;
        }
        CommandInterface command = memcachedConnector.readCommand();
        if (command != null) {
            logger.info("Processing command {}", command);
            commandConsumer.accept(command);
            memcachedConnector.sendResult(command);
            printCommandWaitStart = true;
        }
    }

}
