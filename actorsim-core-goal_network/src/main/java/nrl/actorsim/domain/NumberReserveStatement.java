package nrl.actorsim.domain;

import java.util.List;

/**
 * A statement that holds the value of a StateVariable
 * so that it cannot be used by another action.
 * For example, suppose there is a StateVariable in
 * FactMemory representing a quantity of fuel:
 * <code>
 *   [start, end] fuel(robot1) == 10
 * </code>
 * then a ReserveStatement for consuming 2 units of fuel
 * would be:
 * <code>
 *   [t1] reserve(fuel(robot1), 2)
 * </code>
 * where t1 == start and which would result in:
 * <code>
 *   [start, end] fuel(robot1) == 8 [2 reserved at start]
 * </code>
 *
 * ActorSim uses a ReserveStatement so that it can be held
 * within a goal network.
 *
 */
public class NumberReserveStatement extends TransitionStatement<Number> {

    public NumberReserveStatement(StateVariableTemplate svt, List<WorldObject> bindings, Number by) {
        super(svt, bindings, 0, -by.doubleValue());
    }
}
