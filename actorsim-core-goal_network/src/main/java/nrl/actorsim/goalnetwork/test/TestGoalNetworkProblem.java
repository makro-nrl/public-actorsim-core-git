package nrl.actorsim.goalnetwork.test;

import nrl.actorsim.domain.PlanningDomain;
import nrl.actorsim.domain.PlanningProblem;

public class TestGoalNetworkProblem extends PlanningProblem {
    public TestGoalNetworkProblem(PlanningDomain domain) {
        super(new Options.Builder()
                .domain(domain)
                .name("TestGoalNetworkProblem")
                .allowNewObjects()
                .build());
    }
}
