package nrl.actorsim.goalnetwork.test;

import nrl.actorsim.domain.*;
import nrl.actorsim.domain.test.TestItemExistsDomain;
import nrl.actorsim.goalnetwork.*;

import static nrl.actorsim.domain.test.TestItemExistsDomain.*;

public class TestGoalNetworkDomain extends GoalNetworkDomain {

    public TestGoalNetworkDomain(String name) {
        super(PlanningDomain.Options.builder()
                .name(name)
                .closedWorldAssumption()
                .hideTypesWhenPossible()
                .build());
        add(EXISTS);
        add(ITEM);
    }

    public static TestGoalNetworkDomain getABCNetworkDomain() {
        TestGoalNetworkDomain domain = new TestGoalNetworkDomain("AbcNetworkDomain");
        Method method = TestGoalNetworkDomain.createMethod_A_to_b_c();
        domain.add(method);
        return domain;
    }

    public static GoalNetworkNode<Statement> existsNode(WorldObject obj) {
        PredicateStatement s = existsStatement(obj);
        return new GoalNetworkNode<>(s);
    }

    @SuppressWarnings("unused")
    public static GoalNetworkNode<Statement> existsNode(Statement s) {
        return new GoalNetworkNode<>(s);
    }

    /**
     * A
     * .  .
     * b -> c
     */
    @SuppressWarnings({"rawtypes", "unchecked"})
    public static GoalNetwork createGoalNetwork_3_2OrderedPrimitive() {
        GoalNetworkNode nodeA = existsNode(TestItemExistsDomain.A);
        GoalNetworkNode nodeB = existsNode(TestItemExistsDomain.B);
        nodeB.setPrimitive();
        GoalNetworkNode nodeC = existsNode(TestItemExistsDomain.C);
        nodeC.setPrimitive();
        GoalNetwork network = new GoalNetwork();
        network.addSubsOrdered(nodeA, nodeB, nodeC);
        return network;
    }

    public static Method createMethod_A_to_b_c() {
        Method method = new Method();
        method.addDecomposition(createGoalNetwork_3_2OrderedPrimitive());
        return method;
    }

    public GoalNetworkProblem buildABCNetworkProblem() {
        GoalNetworkProblem.Options options = new GoalNetworkProblem.Options.Builder()
                .domain(this)
                .name("AbcNetworkProblem")
                .allowNewObjects()
                .build();
        GoalNetworkProblem problem = new GoalNetworkProblem(options);
        problem.setNetwork(createGoalNetwork_3_2OrderedPrimitive());
        return problem;
    }


}
