package nrl.actorsim.goalnetwork.test;


import nrl.actorsim.goalnetwork.SimpleEdge;
import nrl.actorsim.goalnetwork.SimpleNetwork;
import nrl.actorsim.goalnetwork.SimpleNode;

public class SimpleNetworkBuilder {

    public static SimpleNode<String> a;
    public static SimpleNode<String> b;
    public static SimpleNode<String> c;
    public static SimpleNode<String> d;
    public static SimpleNode<String> e;
    public static SimpleNode<String> f;
    public static SimpleNode<String> g;
    public static SimpleNode<String> h;
    public static SimpleNode<String> i;
    public static SimpleNode<String> j;
    public static SimpleNode<String> k;
    public static SimpleNode<String> l;

    static {
        initNodes();
    }

    private static void initNodes() {
        a = new SimpleNode<>("A");
        b = new SimpleNode<>("B");
        c = new SimpleNode<>("C");
        d = new SimpleNode<>("D");
        e = new SimpleNode<>("E");
        f = new SimpleNode<>("F");
        g = new SimpleNode<>("G");
        h = new SimpleNode<>("H");
        i = new SimpleNode<>("I");
        j = new SimpleNode<>("J");
        k = new SimpleNode<>("K");
        l = new SimpleNode<>("L");
    }

    /**
     * A simple, non-numeric HGN that decomposes into the following goals
     * <p>
     * C -----> B -----> A
     * . .
     * .    .
     * D ---> E
     * <p>
     * where B is a parent root, and D and C are the unconstrained nodes.
     */
    public static SimpleNetwork<String, SimpleNode<String>, SimpleEdge>
    createSimpleNetwork_5_Ord() {
        initNodes();
        SimpleNetwork<String, SimpleNode<String>, SimpleEdge>
                network = new SimpleNetwork<>(SimpleEdge.class);

        network.add(a);
        network.add(b, a);

        network.addSubsOrdered(b, d, e);
        network.add(c, b);

        return network;
    }

    /**
     * A simple, non-numeric HGN that decomposes into the following goals
     * <p>
     * A  ---> B
     * . .
     * .    .
     * C ---> D
     * <p>
     * where A is a parent root, A occurs before B, and C is unconstrained.
     */
    public static SimpleNetwork<String, SimpleNode<String>, SimpleEdge>
    createSimpleNetwork_4_Ord() {
        initNodes();
        SimpleNetwork<String, SimpleNode<String>, SimpleEdge>
                network = new SimpleNetwork<>(SimpleEdge.class);

        network.add(a);
        network.add(a, b);
        network.addSubsOrdered(a, c, d);

        return network;
    }

    /**
     * A simple, non-numeric GN that decomposes into the following goals
     * <p>
     * A --> B
     * . .
     * C   D
     * <p>
     * where A is a parent root, A is before B, and the unconstrained
     * nodes are C and D (because C and D are unordered).
     */
    public static SimpleNetwork<String, SimpleNode<String>, SimpleEdge>
    createSimpleNetwork_4_Unord() {
        initNodes();
        SimpleNetwork<String, SimpleNode<String>, SimpleEdge>
                network = new SimpleNetwork<>(SimpleEdge.class);

        network.add(a);
        network.add(a, b);
        network.addSubsUnordered(a, c, d);

        return network;
    }

    // @formatter:off
    /**
     * A more sophisticated, non-numeric goal network
     * <p>
     *          A               D
     *         .  .            .  .
     *       .     .          .     .
     *      B --1-> C        I --3-> J
     *     ..      . .      . .
     *    .  .   .    .    .    .
     *    G  H  E -2-> F  K -4-> L
     * <p>
     * Explicit
     * 1. b < c
     * 2. e < f
     * 3. i < j
     * 4. k < l
     * <p>
     * Implicit because of subgoals
     * (b, c).during(a)
     * (e, f).during(c) [.during(a)]
     * (g, h).during(b) [.during(a)]
     * (i, j).during(d)
     * (k, l).during(i) [.during(d)]
     */
    // @formatter:on
    public static SimpleNetwork<String, SimpleNode<String>, SimpleEdge>
    createSimpleNetwork_12_Ord() {
        initNodes();
        SimpleNetwork<String, SimpleNode<String>, SimpleEdge>
                network = new SimpleNetwork<>(SimpleEdge.class);

        network.add(a);
        network.addSubsOrdered(a, b, c);
        network.add(d);
        network.addSubsOrdered(c, e, f);
        network.addSubsUnordered(b, g, h);
        network.addSubsOrdered(d, i, j);
        network.addSubsOrdered(i, k, l);

        return network;
    }

}
