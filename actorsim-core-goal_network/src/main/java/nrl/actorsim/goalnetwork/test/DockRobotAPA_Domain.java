package nrl.actorsim.goalnetwork.test;

import nrl.actorsim.domain.*;

import static nrl.actorsim.domain.WorldObject.NULL_WORLD_OBJECT;
import static nrl.actorsim.domain.WorldType.BOOLEAN;
import static nrl.actorsim.domain.WorldType.NIL;

/**
 * Objects, relations, and variables for the dock-robot domain in APA.
 */
public class DockRobotAPA_Domain extends PlanningDomain {
    public static final WorldType ROBOT = new WorldType("robot");
    public static final WorldObject robot2 = new WorldObject(ROBOT, "r2");
    public static final WorldObject robot1 = new WorldObject(ROBOT, "r1");

    public static final WorldType DOCK = new WorldType("dock");
    public static final WorldObject dock3 = new WorldObject(DOCK, "d3");
    public static final WorldObject dock2 = new WorldObject(DOCK, "d2");
    public static final WorldObject dock1 = new WorldObject(DOCK, "d1");

    public static final WorldType CONTAINER = new WorldType("container");
    public static final WorldObject container3 = new WorldObject(CONTAINER, "c3");
    public static final WorldObject container2 = new WorldObject(CONTAINER, "c2");
    public static final WorldObject container1 = new WorldObject(CONTAINER, "c1");

    public static final WorldType PILE_TYPE = new WorldType("pile");
    public static final WorldObject pile3 = new WorldObject(PILE_TYPE, "p3");
    public static final WorldObject pile2 = new WorldObject(PILE_TYPE, "p2");
    public static final WorldObject pile1 = new WorldObject(PILE_TYPE, "p1");

    public static final StateRelationTemplate ADJACENT = StateRelationTemplate.builder()
            .name("adjacent")
            .argTypes(DOCK.asArgument("start"), DOCK.asArgument("end"))
            .build();
    public static final StateRelationTemplate PILE_AT = StateRelationTemplate.builder()
            .name("pile-at").argTypes(PILE_TYPE, DOCK).build();

    public static final StateVariableTemplate OCCUPIED = StateVariableTemplate.builder()
            .name("occupied").argTypes(DOCK).valueTypes(BOOLEAN).build();
    public static final StateVariableTemplate LOC = StateVariableTemplate.builder()
            .name("loc").argTypes(ROBOT).valueTypes(DOCK).build();
    public static final StateVariableTemplate POS = StateVariableTemplate.builder()
            .name("pos").argTypes(CONTAINER).valueTypes(NIL, DOCK, ROBOT).build();
    public static final StateVariableTemplate CARGO = StateVariableTemplate.builder()
            .name("cargo").argTypes(ROBOT).valueTypes(NIL, CONTAINER).build();
    public static final StateVariableTemplate TOP = StateVariableTemplate.builder()
            .name("top").argTypes(PILE_TYPE).valueTypes(NIL, CONTAINER).build();
    public static final StateVariableTemplate ON_PILE = StateVariableTemplate.builder()
            .name("on-pile").argTypes(CONTAINER).valueTypes(NIL, PILE_TYPE).build();

    public DockRobotAPA_Domain() {
        super(PlanningDomain.Options.builder()
                .name("DockRobotDomain").closedWorldAssumption().build());
        add(ROBOT, DOCK, CONTAINER, PILE_TYPE);
        add(ADJACENT, PILE_AT);
        add(OCCUPIED, LOC, POS, CARGO, TOP, ON_PILE);
    }

    @SuppressWarnings("UnusedReturnValue")
    public PlanningProblem makeExample_2_3() {
        PlanningProblem.Options options = new PlanningProblem.Options.Builder()
                .domain(this)
                .name("Example_2_3")
                .build();
        PlanningProblem problem = new PlanningProblem(options);
        addExample_2_3(problem);
        return problem;
    }

    public void addExample_2_3(PlanningProblem problem) {
        StateAssignment init = problem.getNewState();
        init.add(ADJACENT.copier().build(dock1, dock2));
        init.add(ADJACENT.copier().build(dock1, dock3));
        init.add(ADJACENT.copier().build(dock2, dock1));
        init.add(ADJACENT.copier().build(dock2, dock3));
        init.add(ADJACENT.copier().build(dock3, dock1));
        init.add(ADJACENT.copier().build(dock3, dock2));
        init.add(PILE_AT.copier().build(pile1, dock1));
        init.add(PILE_AT.copier().build(pile2, dock2));
        init.add(PILE_AT.copier().build(pile3, dock2));

        init.add(new PersistenceStatement<>(CARGO, robot1, NULL_WORLD_OBJECT));
        init.add(new PersistenceStatement<>(CARGO, robot2, NULL_WORLD_OBJECT));

        init.add(new PersistenceStatement<>(LOC, robot1, dock1));
        init.add(new PersistenceStatement<>(LOC, robot2, dock2));

        init.add(new PersistenceStatement<>(OCCUPIED, dock1, WorldObject.TRUE));
        init.add(new PersistenceStatement<>(OCCUPIED, dock2, WorldObject.TRUE));
        init.add(new PersistenceStatement<>(OCCUPIED, dock3, WorldObject.FALSE));

        init.add(new PersistenceStatement<>(ON_PILE, container1, pile1));
        init.add(new PersistenceStatement<>(ON_PILE, container2, pile1));
        init.add(new PersistenceStatement<>(ON_PILE, container3, pile2));

        init.add(new PersistenceStatement<>(POS, container1, container2));
        init.add(new PersistenceStatement<>(POS, container2, NULL_WORLD_OBJECT));
        init.add(new PersistenceStatement<>(POS, container3, NULL_WORLD_OBJECT));

        init.add(new PersistenceStatement<>(TOP, pile1, container1));
        init.add(new PersistenceStatement<>(TOP, pile2, container3));
        init.add(new PersistenceStatement<>(TOP, pile3, NULL_WORLD_OBJECT));
        problem.setInit(init);
    }
}
