package nrl.actorsim.goalnetwork.test;


import nrl.actorsim.domain.Statement;
import nrl.actorsim.domain.test.TestItemExistsDomain;
import nrl.actorsim.domain.WorldObject;
import nrl.actorsim.goalnetwork.GoalNetworkBase;
import nrl.actorsim.goalnetwork.GoalNetworkNode;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@SuppressWarnings({"DanglingJavadoc", "UnusedReturnValue"})
public class TestGoalNetworkBuilder<T extends Statement,
        V extends GoalNetworkNode<T>,
        N extends GoalNetworkBase<T, V>> {

    public V a;
    public V b;
    public V c;
    public V d;
    public V e;
    public V f;
    public V g;
    public V h;
    public V i;
    public V j;
    public V k;
    public V l;
    public V m;
    public V n;
    public V o;
    public V p;
    public V q;
    public V r;
    public V s;
    public V t;
    public V u;
    public V v;
    public V w;
    public V x;
    public V y;
    public V z;

    public N buildNetwork(Class<N> networkClass) {
        try {
            return networkClass.newInstance();
        } catch (InstantiationException | IllegalAccessException e) {
            e.printStackTrace();
        }
        return null;
    }

    private void initNodes(N network) {
        Map<String, V> nodeMap = new HashMap<>();
        for (Map.Entry<String, WorldObject> pair : TestItemExistsDomain.itemMap.entrySet()) {
            Statement s = TestItemExistsDomain.existsStatement(pair.getValue());
            V node = network.buildNode(s);
            nodeMap.put(pair.getKey(), node);
        }
        a = nodeMap.get("A");
        b = nodeMap.get("B");
        c = nodeMap.get("C");
        d = nodeMap.get("D");
        e = nodeMap.get("E");
        f = nodeMap.get("F");
        g = nodeMap.get("G");
        h = nodeMap.get("H");
        i = nodeMap.get("I");
        j = nodeMap.get("J");
        k = nodeMap.get("K");
        l = nodeMap.get("L");
        m = nodeMap.get("M");
        n = nodeMap.get("N");
        o = nodeMap.get("O");
        p = nodeMap.get("P");
        q = nodeMap.get("Q");
        r = nodeMap.get("R");
        s = nodeMap.get("S");
        t = nodeMap.get("T");
        u = nodeMap.get("U");
        v = nodeMap.get("V");
        w = nodeMap.get("W");
        x = nodeMap.get("X");
        y = nodeMap.get("Y");
        z = nodeMap.get("Z");
    }

    /**
     * An empty network that allows using the nodes independently.
     *
     * @param networkClass the class of network to build
     */
    public N build_empty(Class<N> networkClass) {
        N network = buildNetwork(networkClass);
        initNodes(network);
        return network;
    }

    /**
     * A network with a single goal, A.
     *
     * @param networkClass the class of network to build
     */
    public N build_1(Class<N> networkClass) {
        N network = buildNetwork(networkClass);
        initNodes(network);

        network.add(a);
        return network;
    }

    /**
     * A simple, non-numeric HGN that decomposes into the following goals
     * <p>
     * C -----> B -----> A
     *         . .
     *        .   .
     *       D --> E
     * <p>
     * where B is a parent root, and D and C are the unconstrained nodes.
     *
     * @param networkClass the class of network to build
     */
    public N build_5_Ord(Class<N> networkClass) {
        N network = buildNetwork(networkClass);
        initNodes(network);

        network.add(a);
        network.add(b, a);

        network.addSubsOrdered(b, d, e);
        network.add(c, b);

        return network;
    }

    // @formatter:off
    /**
     * A more sophisticated, non-numeric goal network
     * <p>
     *          A               D
     *         .  .            .  .
     *       .     .          .     .
     *      B --1-> C        I --3-> J
     *     ..      . .      . .
     *    .  .   .    .    .    .
     *    G  H  E -2-> F  K -4-> L
     * <p>
     * Explicit
     * 1. b < c
     * 2. e < f
     * 3. i < j
     * 4. k < l
     * <p>
     * Implicit because of subgoals
     * (b, c).during(a)
     * (e, f).during(c) [.during(a)]
     * (g, h).during(b) [.during(a)]
     * (i, j).during(d)
     * (k, l).during(i) [.during(d)]
     */
    // @formatter:on
    public N build_12_Ord_NoSharing(Class<N> networkClass) {
        N network = buildNetwork(networkClass);
        initNodes(network);

        network.add(a);
        network.addSubsOrdered(a, b, c);
        network.add(d);
        network.addSubsOrdered(c, e, f);
        network.addSubsUnordered(b, g, h);
        network.addSubsOrdered(d, i, j);
        network.addSubsOrdered(i, k, l);

        return network;
    }

    // @formatter:off
    /**
     * A variant of 12_Ord that shares a node
     * within the same network.
     *
     * <p>
     *          A                 D
     *         .  .              .  .
     *       .     .            .     .
     *      B --1-> C          I --3-> J
     *     ..      ...         ..
     *    .  .   .  .  .      .  .
     *    .  . .   .    .    .    .
     *    G   H   E -2-> F  K -4-> L
     * <p>
     * Explicit
     * 1. b < c
     * 2. e < f
     * 3. i < j
     * 4. k < l
     * <p>
     * Implicit because of subgoals
     * (b, c).during(a)
     * (e, f).during(c) [.during(a)]
     * (g, h).during(b) [.during(a)]
     * (i, j).during(d)
     * (k, l).during(i) [.during(d)]
     */
    // @formatter:on
    public N build_12_Ord_WithinSharing(Class<N> networkClass) {
        N network = buildNetwork(networkClass);
        initNodes(network);

        network.add(a);
        network.addSubsOrdered(a, b, c);
        network.add(d);
        network.addSubsUnordered(c, h); // this is the new constraint
        network.addSubsOrdered(c, e, f);
        network.addSubsUnordered(b, g, h);
        network.addSubsOrdered(d, i, j);
        network.addSubsOrdered(i, k, l);

        return network;
    }

    // @formatter:off
    /**
     * A more sophisticated, non-numeric goal network
     * that creates independent networks for A and D
     * and shares between the two networks.
     *
     * <p>
     *          A                D
     *          ..              ...
     *         .  .    ......... .  .
     *       .     .  .         .     .
     *      B --1-> C          I --3-> J
     *     ..      ...         ..
     *    .  .   .  .  .      .  .
     *    .  . .   .    .    .    .
     *    G   H   E -2-> F  K -4-> L
     * <p>
     * Explicit
     * 1. b < c
     * 2. e < f
     * 3. i < j
     * 4. k < l
     * <p>
     * Implicit because of subgoals
     * (b, c).during(a)
     * (e, f).during(c) [.during(a)]
     * (g, h).during(b) [.during(a)]
     * (i, j).during(d)
     * (k, l).during(i) [.during(d)]
     */
    // @formatter:on
    public List<N> build_12_Ord_BetweenSharing(Class<N> networkClass) {
        N networkA = buildNetwork(networkClass);
        N networkD = buildNetwork(networkClass);
        initNodes(networkA);

        networkA.add(a);
        networkA.addSubsOrdered(a, b, c);
        networkA.addSubsUnordered(b, g, h);
        networkA.addSubsUnordered(c, h);
        networkA.addSubsOrdered(c, e, f);

        networkD.add(d);
        networkD.addSubsUnordered(d, c); // this is the new constraint
        networkD.addSubsOrdered(d, i, j);
        networkD.addSubsOrdered(i, k, l);

        List<N> list = new ArrayList<>();
        list.add(networkA);
        list.add(networkD);
        return list;
    }

    /**
     * A simple, non-numeric HGN that decomposes into the following goals
     *
     *                     A  ---> B
     *                    . .
     *                  .    .
     *                 C ---> D
     *
     *  where A is a parent root, A occurs before B, and C is unconstrained.
     */
//    public static
//    SimpleNetwork<String, SimpleNode<String>, SimpleEdge>
//    createSimpleNetwork_4_Ord() {
//        initNodes();
//        SimpleNetwork<String, SimpleNode<String>, SimpleEdge>
//                network = new SimpleNetwork<>(SimpleEdge.class);
//
//        network.add(a);
//        network.add(a, b);
//        network.addSubsOrdered(a, c, d);
//
//        return network;
//    }
//
    /**
     * A simple, non-numeric GN that decomposes into the following goals
     *
     *                     A --> B
     *                    . .
     *                   C   D
     *
     *  where A is a parent root, A is before B, and the unconstrained
     *  nodes are C and D (because C and D are unordered).
     */
//    public static
//    SimpleNetwork<String, SimpleNode<String>, SimpleEdge>
//    createSimpleNetwork_4_Unord() {
//        initNodes();
//        SimpleNetwork<String, SimpleNode<String>, SimpleEdge>
//                network = new SimpleNetwork<>(SimpleEdge.class);
//
//        network.add(a);
//        network.add(a, b);
//        network.addSubsUnordered(a, c, d);
//
//        return network;
//    }

    /**
     * A more sophisticated, non-numeric goal network
     *
     *                    A              D
     *                  .  .            .  .
     *                .     .          .     .
     *               B --1-> C        I --3-> J
     *              ..      . .      . .
     *             .  .   .    .    .    .
     *             G  H  E -2-> F  K -4-> L
     *
     *     Explicit
     *  1. b < c
     *  2. e < f
     *  3. i < j
     *  4. k < l
     *
     *  Implicit because of subgoals
     *  (b, c).during(a)
     *  (e, f).during(c) [.during(a)]
     *  (g, h).during(b) [.during(a)]
     *  (i, j).during(d)
     *  (k, l).during(i) [.during(d)]
     */
//    public static
//    SimpleNetwork<String, SimpleNode<String>, SimpleEdge>
//    createSimpleNetwork_12_Ord() {
//        initNodes();
//        SimpleNetwork<String, SimpleNode<String>, SimpleEdge>
//                network = new SimpleNetwork<>(SimpleEdge.class);
//
//        network.add(a);
//        network.addSubsOrdered(a, b, c);
//        network.add(d);
//        network.addSubsOrdered(c, e, f);
//        network.addSubsUnordered(b, g, h);
//        network.addSubsOrdered(d, i, j);
//        network.addSubsOrdered(i, k, l);
//
//        return network;
//    }


    /**
     * A simple, non-numeric HGN that decomposes into the following goals
     *
     *           A
     *         /   \
     *        /     \
     *       B       E
     *      / \    / | \
     *     C   D  F  G  H
     *
     * where each subgoal must be completed before its parent
     * and where C < D, F < G, and F, H.  So G and H are unordered
     * with respect to each other.
     *
     * This example is loosely based on the paper:
     * Neil Yorke-Smith. 2005. Exploiting the Structure of Hierarchial
     * Plans in Temporal Constraint Propagation.  In Proc. AAAI.
     */


    /*
     * A simple, non-numeric HGN that decomposes into the following goals
     *
     *           A
     *         /   \
     *        /     \
     *       B*     C
     *             /  \
     *           D*   E*
     *
     * where asterisked goals are primitive.
     *
     * This network can also be written N = ({A, B, C, D, E}, {AB, AC, CD, CE},
     * where LM is shorthand for (L, M) indicating that L preceeds M.
     *
     */


}
