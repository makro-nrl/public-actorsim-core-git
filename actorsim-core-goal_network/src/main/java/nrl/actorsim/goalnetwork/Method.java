package nrl.actorsim.goalnetwork;


import nrl.actorsim.domain.Statement;
import org.slf4j.LoggerFactory;

public class Method {
    protected final static org.slf4j.Logger logger = LoggerFactory.getLogger(Method.class);
    protected GoalNetwork decomposition;  //changes network

    public Method() {

    }

    public Method(GoalNetwork decomposition) {
        this.addDecomposition(decomposition);
    }

    /**
     * Adds the decomposition network to this method.
     * Only one network is allowed, so calling this will replace any
     * previous decomposition.
     *
     * @param decomposition the network to add
     * @return this to allow chaining.
     */
    @SuppressWarnings("UnusedReturnValue")
    public Method addDecomposition(GoalNetwork decomposition) {
        this.decomposition = decomposition;
        return this;
    }

    //=====================================================
    // Decomposition related
    //=====================================================

    /**
     * Return an instance of this method.
     * If the method is applicable to the provided network,
     * then the method is also linked with it.
     * Otherwise, the instance is not linked.
     *
     * If linked successfully, Instance.isValid() returns true;
     *
     * @param network the network to associate
     * @return the Method.Instance
     */
    public Instance instance(GoalNetwork network) {
        if (this.isApplicable(network)) {
            return new Instance(network);
        }
        return new Instance();
    }

    /***
     * Determines if this method can decompose part(s) of the network
     * by ensuring that every node in the network matching the
     * proposed decomposition has no children.
     *
     * @param network the network to check
     * @return whether this method can decompose network
     */
    public boolean isApplicable(GoalNetwork network) {
        boolean canBeDecomposed = true;
        for (GoalNetworkNode<Statement> proposed : decomposition.getNodes()) {
            if (network.contains(proposed)
                    && network.hasChildren(proposed)) {
                canBeDecomposed = false;
                break;
            }
        }
        return canBeDecomposed;
    }

    class Instance {
        private final boolean isValid;
        final GoalNetwork network;

        private Instance() {
            network = null;
            this.isValid = false;
        }

        private Instance(GoalNetwork network) {
            this.network = network;
            this.isValid = true;
        }

        /***
         * Destructively decomposes the network.
         */
        public boolean apply() {
            if (network != null
                    && isApplicable(network)) {
                network.addAll(decomposition);
                return true;
            }
            return false;
        }

        public boolean isValid() {
            return isValid;
        }
    }


}
