package nrl.actorsim.goalnetwork;

import nrl.actorsim.domain.PlanningDomain;

import java.util.ArrayList;
import java.util.List;

public class GoalNetworkDomain extends PlanningDomain {
    protected final List<Method> methodTemplates = new ArrayList<>();

    public GoalNetworkDomain(Options options) {
        super(options);
    }

    @Override
    public GoalNetworkProblem buildEmptyProblem() {
        GoalNetworkProblem.Options options = new GoalNetworkProblem.Options.Builder()
                .domain(this)
                .name("EmptyProblem")
                .allowNewObjects()
                .build();
        return new GoalNetworkProblem(options);
    }

    public void add(Method method) {
        methodTemplates.add(method);
    }
}
