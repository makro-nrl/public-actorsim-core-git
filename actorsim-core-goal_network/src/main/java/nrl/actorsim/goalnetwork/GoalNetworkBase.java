package nrl.actorsim.goalnetwork;

import nrl.actorsim.domain.*;
import nrl.actorsim.memory.History;

import java.time.Instant;
import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

/**
 * An abstract class that allows consolidating the work
 * of creating nodes for GoalNetworks or GoalLifecycleNetworks.
 *
 * @param <T> The type stored in the node
 * @param <V> The type of the node
 */
public abstract class GoalNetworkBase<T extends Statement, V extends GoalNetworkNode<T>>
        extends SimpleNetwork<T, V, SimpleEdge>
{
    public static AtomicInteger NEXT_ID = new AtomicInteger();

    final int id;
    History<V> history;
    private Map<Instant, Method.Instance> appliedMethods;

    protected <N extends GoalNetworkBase<T, V>>
    GoalNetworkBase(Class<N> cls) {
        super(SimpleEdge.class);
        id = NEXT_ID.getAndIncrement();
        setPathName("gn" + id);
    }

    public <N extends GoalNetworkBase<T, V>>
    N buildNetwork(Class<N> networkClass) {
        try {
            return networkClass.newInstance();
        } catch (InstantiationException | IllegalAccessException e) {
            e.printStackTrace();
        }
        return null;
    }

    History<V> getHistory() {
        return history;
    }

    public Set<V> checkComplete(StateAssignment state) {
        Set<V> released = new HashSet<>();
        for (GoalNetworkNode<T> unconstrained : this.getUnconstrained()) {
            if (unconstrained.releaseIfEntailedBy(state)) {
                //noinspection unchecked
                released.add((V) unconstrained);
            }
        }
        return released;
    }

    public boolean isPrimitive(V node) {
        List<V> matching
                = graph.vertexSet()
                .stream()
                .filter(node::equals)
                .collect(Collectors.toList());
        if (matching.size() == 1) {
            return matching.get(0).isPrimitive();
        }
        return false;
    }

    public abstract V buildNode(Statement stored);

    public abstract <V2 extends GoalNetworkNode<T>> Class<V2> getNodeClass();

}
