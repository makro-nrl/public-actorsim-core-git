package nrl.actorsim.goalnetwork;


/**
 * A GoalNetwork that allows more sophisticated temporal constraints
 * and supports goal sharing between networks.
 */
public class
TemporalGoalNetwork extends GoalNetwork {
    public TemporalGoalNetwork() {
        super();
    }
}
