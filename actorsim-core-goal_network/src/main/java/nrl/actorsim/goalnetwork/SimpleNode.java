package nrl.actorsim.goalnetwork;

import nrl.actorsim.utils.ChangeNotifier;
import nrl.actorsim.utils.ChangeNotifierImpl;
import nrl.actorsim.utils.Named;
import org.jetbrains.annotations.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;

/**
 * Wraps an object for store within a JGraphT graph.
 *
 * JGraphT relies on a consistent implementation of both equals()
 * *and* hashCode(), and our implementation of equals() relies on compareTo().
 * Failing to synchronize these methods in subclasses results in
 * incorrect behavior in the graph library or algorithms that use it!
 * For details, see:
 * <ul>
 *     <li>https://github.com/jgrapht/jgrapht/wiki/Users%3A-EqualsAndHashCode</li>
 *     <li>https://www.ibm.com/developerworks/java/library/j-jtp05273/index.html</li>
 * </ul>
 *
 * @param <T> The type this node stores
 */
public class SimpleNode<T>
        extends ChangeNotifierImpl
        implements Named, Comparable<SimpleNode<T>> {
    protected final static Logger logger = LoggerFactory.getLogger(SimpleNode.class);

    final protected UUID uuid;
    final protected T stored;

    //Only supports one network membership, so no sharing
    protected SimpleNetwork<T, ? extends SimpleNode<T>, SimpleEdge> network;
    boolean releaseInProgress = false;


    // ====================================================
    // region<Constructors, Initializers, Object Overrides>

    public SimpleNode(T value) {
        uuid = UUID.randomUUID();
        stored = value;

        if (stored instanceof ChangeNotifier) {
            ((ChangeNotifier) stored).addChangeListener(this::notifyOfInternalChange);
        }
    }

    /**
     * Method of subclasses to initialize the node, if needed.
     */
    @SuppressWarnings("EmptyMethod")
    protected void init() {
    }

    @Override
    public String toString() {
        return stored.toString();
    }

    // endregion
    // ====================================================

    // ====================================================
    // region<Accessors>

    @Override
    public String getShortName() {
        if (stored instanceof Named) {
            return ((Named) stored).getShortName();
        } else {
            return stored.toString();
        }
    }

    public UUID getId() {
        return uuid;
    }

    public T getStored() {
        return stored;
    }

    /**
     * A convenience function to return the stored content
     * after casting.
     *
     * This method may throw a ClassCastException if used on
     * the right side of assignment where implicit value of X
     * is the incorrect class.
     *
     * @param <X> The (usually implicit) type of content to return
     * @return
     */
    public
    <X extends T>
    X content() throws ClassCastException {
        //noinspection unchecked
        return (X) stored;
    }

    @SuppressWarnings({"rawtypes", "unchecked"})
    protected <N extends SimpleNetwork>
    N getNetwork() {
        return (N) network;
    }

    public <V extends SimpleNode<T>, B extends Cloner<V, B>>
    Cloner<V, B> cloner() {
        return new Cloner<>(this);
    }

    // endregion
    // ====================================================

    // ====================================================
    // region<GoalNetwork>

    /**
     * Adds this node as a member of the network.
     *
     * @param network to add
     */
    boolean attach(SimpleNetwork<T, ? extends SimpleNode<T>, SimpleEdge> network) {
        if (this.network == null) {
            this.network = network;
            return true;
        }
        logger.error("Node is already attached; it must be released first.");
        return false;
    }

    public boolean isAttached() {
        return this.network != null;
    }

    public boolean isAttachedTo(SimpleNetwork<?, ?, ?> network) {
        return this.network == network;
    }

    /**
     * A mechanism for subclasses of SimpleNode to prohibit release.
     * For example, a goal lifecycle node cannot be released until FINISHED.
     *
     * @return whether the node can be released
     */
    boolean canBeReleased() {
        return true;
    }

    /**
     * Release this goalNode from membership of the network.
     *
     * @param network to release from
     */
    void releaseFrom(SimpleNetwork<T, ? extends SimpleNode<T>, ? extends SimpleEdge> network) {
        if (isMemberOf(network)) {
            this.network = null;
            wasReleased();
        } else {
            logger.error("Requested clearing of an unaffiliated network; skipping request");
        }
    }

    /**
     * A method for subclasses to perform special housekeeping when released.
     */
    protected void wasReleased() {
        releaseInProgress = true;
    }

    public boolean isMemberOf(SimpleNetwork<T, ? extends SimpleNode<T>, ? extends SimpleEdge> network) {
        return this.network != null
                && this.network == network;
    }

    public boolean isMemberOf(int networkId) {
        return this.network != null
                && this.network.networkId == networkId;
    }

    /**
     * Returns the comparison between this object and other.
     *
     * JGraphT relies on a consistent implementation of both equals()
     * *and* hashCode(), and our implementation of equals() relies on compareTo().
     * Failing to synchronize these methods in subclasses results in
     * incorrect behavior in the graph library or algorithms that use it!
     * For details, see:
     * <ul>
     *     <li>https://github.com/jgrapht/jgrapht/wiki/Users%3A-EqualsAndHashCode</li>
     *     <li>https://www.ibm.com/developerworks/java/library/j-jtp05273/index.html</li>
     * </ul>
     *
     * @param other the other class to compare against
     * @return negative, zero, or positive integers for less-than, equals, or
     *         greater-than comparison results
     */
    @Override
    public int compareTo(@NotNull SimpleNode<T> other) {
        if (this == other
                || this.uuid.equals(other.uuid)) {
            return 0;
        } else if (this.stored instanceof Comparable
                && other.stored instanceof Comparable) {
            //noinspection unchecked,rawtypes
            return ((Comparable) this.stored).compareTo(other.stored);
        }
        return this.stored.toString().compareTo(other.stored.toString());
    }

    public int compareToId(@NotNull SimpleNode<T> other) {
        return this.uuid.compareTo(other.uuid);
    }

    public boolean hasDifferentId(@NotNull SimpleNode<T> other) {
        return compareToId(other) != 0;
    }

    public boolean hasSameId(@NotNull SimpleNode<T> other) {
        return compareToId(other) == 0;
    }

    /**
     * Returns whether this object equals other.
     *
     * JGraphT relies on a consistent implementation of both equals()
     * *and* hashCode(), and our implementation of equals() relies on compareTo().
     * Failing to synchronize these methods in subclasses results in
     * incorrect behavior in the graph library or algorithms that use it!
     * For details, see:
     * <ul>
     *     <li>https://github.com/jgrapht/jgrapht/wiki/Users%3A-EqualsAndHashCode</li>
     *     <li>https://www.ibm.com/developerworks/java/library/j-jtp05273/index.html</li>
     * </ul>
     *
     * @param other the object to test
     * @return whether this equals other
     */
    @SuppressWarnings({"unchecked", "rawtypes"})
    @Override
    public boolean equals(Object other) {
        if (other instanceof SimpleNode) {
            SimpleNode otherNode = (SimpleNode) other;
            return compareTo(otherNode) == 0;
        }
        return super.equals(other);
    }

    /**
     * Returns the hashCode for this object.
     *
     * JGraphT relies on a consistent implementation of both equals()
     * *and* hashCode(), and our implementation of equals() relies on compareTo().
     * Failing to synchronize these methods in subclasses results in
     * incorrect behavior in the graph library or algorithms that use it!
     * For details, see:
     * <ul>
     *     <li>https://github.com/jgrapht/jgrapht/wiki/Users%3A-EqualsAndHashCode</li>
     *     <li>https://www.ibm.com/developerworks/java/library/j-jtp05273/index.html</li>
     * </ul>
     *
     * @return the hash code for this object.
     */
    @Override
    public int hashCode() {
        return stored.hashCode();
    }

    /**
     * Get the parents of this node
     *
     * @return the parents of this node; using an array of a raw type to support subclass covariance
     */
    @SuppressWarnings({"unchecked", "rawtypes"})
    public SimpleNode[] getParents() {
        return ((SimpleNetwork<T, SimpleNode<T>, SimpleEdge>) network)
                .parentsOf(this).toArray(new SimpleNode[0]);
    }

    /**
     * Get the parents of this node
     *
     * @return the parents of this node; using an array of a raw type to support subclass covariance
     */
    @SuppressWarnings({"unchecked", "rawtypes"})
    public SimpleNode[] getChildren() {
        return ((SimpleNetwork<T, SimpleNode<T>, SimpleEdge>) network)
                .childrenOf(this).toArray(new SimpleNode[0]);
    }

    // endregion
    // ====================================================

    // ====================================================
    // region<Parent and Subgoal Related>

    /**
     * @return the subgoals of this node; using an array of a raw type to support subclass covariance
     */
    @SuppressWarnings({"unchecked", "rawtypes"})
    public SimpleNode[] getSubgoals() {
        return ((SimpleNetwork<T, SimpleNode<T>, SimpleEdge>) network)
                .childrenOf(this).toArray(new SimpleNode[0]);
    }

    @SuppressWarnings("unchecked")
    public int subgoalCount() {
        if (network != null) {
            return ((SimpleNetwork<T, SimpleNode<T>, SimpleEdge>) network)
                    .childrenOf(this).size();
        }
        return 0;
    }

    // endregion
    // ====================================================

    // ====================================================
    // region<Builder and Cloner>

    public class Cloner<V extends SimpleNode<T>, B extends Cloner<V, B>> {
        final T stored;
        protected SimpleNetwork<T, ? extends SimpleNode<T>, SimpleEdge> network;

        protected Cloner(SimpleNode<T> other) {
            this.stored = other.stored;
        }

        protected B getThis() {
            //noinspection unchecked
            return (B) this;
        }

        public Cloner<V, B> copyNetwork(SimpleNode<T> other) {
            this.network = other.network;
            return getThis();
        }

        public V build() {
            //noinspection unchecked
            V instance = (V) new SimpleNode<>(stored);
            instance.network = network;
            return instance;
        }
    }

    // endregion
    // ====================================================

    // ====================================================
    // region<Constraint Checking>
    public boolean isUnconstrained() {
        return getNetwork().isUnconstrained(this);
    }

    // endregion
    // ====================================================

}
