package nrl.actorsim.goalnetwork;

import nrl.actorsim.domain.Statement;

/**
 * An edge type that allows more sophisticated temporal orderings
 * between nodes.
 */
public class TemporalGoalNetworkNode extends GoalNetworkNode<Statement> {
    public TemporalGoalNetworkNode(Statement statement) {
        super(statement);
    }

    public TemporalGoalNetworkNode(Statement statement, boolean isPrimitive) {
        super(statement, isPrimitive);
    }

    public TemporalGoalNetworkNode(TemporalGoalNetworkNode other) {
        super(other);
    }
}
