package nrl.actorsim.goalnetwork;

import nrl.actorsim.chronicle.AllenIntervalOperator;
import nrl.actorsim.utils.Named;
import org.jgrapht.graph.DefaultEdge;

/**
 * A SimpleEdge points from a source to a target, where the source is the
 * the predecessor to the target; i.e., it occurs BEFORE the successor.
 * By default, This class only stores the {@link AllenIntervalOperator}.BEFORE
 * ordering.
 * <p>
 * By default, there is no special relationship other than ordering.
 * A parent-child relationship can also be added with targetIsParent(),
 * which will change the relationship to child.BEFORE(parent).
 */
public class SimpleEdge extends DefaultEdge implements Named {

    private AllenIntervalOperator ordering = AllenIntervalOperator.BEFORE;
    private boolean targetIsParent = false;

    public SimpleEdge() {

    }

    @Override
    public String toString() {
        String sName = getSource().toString();
        String tName = getTarget().toString();
        String oString = ordering.toString();
        String parent = "";
        if (targetIsParent()) {
            parent = "(PARENT)";
        }
        return String.format("%s ==%s==>%s %s", sName, oString, parent, tName);
    }

    @Override
    public String getShortName() {
        String tName = getTarget() instanceof Named ?
                ((Named) getTarget()).getShortName() :
                getTarget().toString();
        String sName = getSource() instanceof Named ?
                ((Named) getSource()).getShortName() :
                getSource().toString();
        String oString = ordering.getShortName();
        return String.format("%s %s %s", sName, oString, tName);
    }

    public String getShortNameChild() {
        String sName = getSource() instanceof Named ?
                ((Named) getSource()).getShortName() :
                getSource().toString();
        String oString = ordering.getShortName();
        return String.format("%s%s", oString, sName);
    }

    public String getShortNameParent() {
        String tName = getTarget() instanceof Named ?
                ((Named) getTarget()).getShortName() :
                getTarget().toString();
        String oString = ordering.getShortName();
        return String.format("%s%s", oString, tName);
    }

    public boolean targetIsParent() {
        return targetIsParent;
    }

    public void setTargetAsParent() {
        targetIsParent = true;
        this.ordering = AllenIntervalOperator.DURING;
    }
}
