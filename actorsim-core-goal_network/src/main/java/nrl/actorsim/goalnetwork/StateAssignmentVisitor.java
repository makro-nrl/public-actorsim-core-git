package nrl.actorsim.goalnetwork;


import nrl.actorsim.domain.StateAssignment;
import nrl.actorsim.domain.Statement;

/**
 * A visitor that checks each node against the state
 */
public class StateAssignmentVisitor<V extends GoalNetworkNode<?>>
        extends GoalNetworkVisitor {
    protected final StateAssignment assignment;

    StateAssignmentVisitor(StateAssignment assignment) {
        this.assignment = assignment;
    }

    @Override
    public void visit(GoalNetworkNode<Statement> node) {
        node.checkComplete(assignment);
    }
}
