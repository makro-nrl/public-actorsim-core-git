package nrl.actorsim.goalnetwork;


import nrl.actorsim.domain.Statement;
import nrl.actorsim.memory.History;

/**
 * A GoalNetwork is a {@link SimpleNetwork} that links network
 * nodes to goal {@link Statement}s.
 */
public class GoalNetwork extends GoalNetworkBase<Statement,
        GoalNetworkNode<Statement>> {
    public GoalNetwork() {
        super(GoalNetwork.class);
        history = new History<>();
    }

    @Override
    public GoalNetworkNode<Statement> buildNode(Statement s) {
        return new GoalNetworkNode<>(s);
    }

    @SuppressWarnings({"rawtypes", "unchecked"})
    @Override
    public Class<GoalNetworkNode> getNodeClass() {
        return GoalNetworkNode.class;
    }

    public boolean decomposeWith(Method method) {
        return method.instance(this).apply();
    }

}
