package nrl.actorsim.goalnetwork;

import nrl.actorsim.domain.PlanningProblem;

public class GoalNetworkProblem extends PlanningProblem {
    GoalNetwork network;

    public GoalNetworkProblem(Options options) {
        super(options);
        setNetwork(new GoalNetwork());
    }

    public GoalNetwork getNetwork() {
        return network;
    }

    public void setNetwork(GoalNetwork network) {
        this.network = network;
    }
}
