package nrl.actorsim.goalnetwork;

import nrl.actorsim.domain.*;

import java.util.*;

public class GoalNetworkNode<T extends Statement> extends SimpleNode<T> {
    final static org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(GoalNetworkNode.class);

    protected boolean isPrimitive;
    protected boolean isComplete;
    final BindableImpl bindable;


    public GoalNetworkNode(T statement) {
        super(statement);
        bindable = BindableImpl.createWrapper(statement);
    }

    public GoalNetworkNode(T statement, boolean isPrimitive) {
        this(statement);
        this.isPrimitive = isPrimitive;
    }

    public GoalNetworkNode(GoalNetworkNode<T> other) {
        super(other.stored);
        bindable = BindableImpl.createWrapper(other.stored);
        this.isPrimitive = other.isPrimitive;
        this.isComplete = other.isComplete;
    }

    @SuppressWarnings("unchecked")
    public Cloner<? extends GoalNetworkNode<T>, ?> cloner() {
        return new Cloner<>(this);
    }

    @Override
    boolean canBeReleased() {
        return super.canBeReleased()
                && isComplete();
    }

    public boolean isPrimitive() {
        return isPrimitive;
    }

    @SuppressWarnings("UnusedReturnValue")
    public GoalNetworkNode<T> setPrimitive() {
        isPrimitive = true;
        return this;
    }

    public boolean isComplete() {
        return isComplete;
    }

    public boolean isNotComplete() {
        return ! isComplete;
    }

    @SuppressWarnings("UnusedReturnValue")
    public GoalNetworkNode<T> setComplete() {
        isComplete = true;
        return this;
    }

    public void checkComplete(StateAssignment state) {
        if (stored.isEntailedBy(state)) {
            setComplete();
        }
    }

    /**
     * Attempts to release this node from its network.
     *
     * @return whether the node was released
     */
    public boolean release() {
        //noinspection unchecked - we know it is of this type
        return getNetwork().release(this);
    }

    /**
     * Checks whether this node is entailed by the provided state.
     * If so, then sets this node to complete and attempts release.
     *
     * @param state the state to check against
     * @return whether the node was released
     */
    public boolean releaseIfEntailedBy(StateAssignment state) {
        Statement s = getStored();
        logger.debug("Checking whether state entails statement.");
        if (s.isEntailedBy(state)) {
            setComplete();
            return release();
        }
        return false;
    }

    // ====================================================
    // Parent / Subgoal Related
    // ====================================================
    @SuppressWarnings({"unchecked", "rawtypes"})
    public GoalNetworkNode[] getParents() {
        return ((GoalNetwork) getNetwork())
                .parentsOf((GoalNetworkNode<Statement>) this)
                .toArray(new GoalNetworkNode[0]);
    }

    @SuppressWarnings({"unchecked", "rawtypes"})
    @Override
    public GoalNetworkNode[] getSubgoals() {
        return ((GoalNetwork) getNetwork())
                .childrenOf((GoalNetworkNode<Statement>) this)
                .toArray(new GoalNetworkNode[0]);
    }

    public class Cloner<V extends GoalNetworkNode<T>, B extends Cloner<V, B>>
            extends SimpleNode<T>.Cloner<V, B> {
        private final boolean isPrimitive;
        private boolean isComplete;

        protected Cloner(GoalNetworkNode<T> other) {
            super(other);
            this.isPrimitive = other.isPrimitive;
        }

        @SuppressWarnings("unused")
        protected B copyComplete(GoalNetworkNode<T> other) {
            this.isComplete = other.isComplete;
            return getThis();
        }

        @Override
        public V build() {
            //noinspection unchecked
            V instance = (V) new GoalNetworkNode<>(stored);
            instance.isPrimitive = isPrimitive;
            instance.isComplete = isComplete;
            return instance;
        }
    }

}
