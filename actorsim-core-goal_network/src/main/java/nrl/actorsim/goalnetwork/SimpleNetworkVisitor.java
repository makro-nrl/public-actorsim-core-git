package nrl.actorsim.goalnetwork;

public interface SimpleNetworkVisitor<V extends SimpleNode<?>> {
    void visit(V node);
}
