package nrl.actorsim.goalnetwork;

import nrl.actorsim.chronicle.AllenIntervalOperator;
import nrl.actorsim.domain.BindableContainer;
import nrl.actorsim.domain.BindableImpl;
import org.apache.commons.lang3.NotImplementedException;
import org.jgrapht.Graph;
import org.jgrapht.graph.DirectedAcyclicGraph;
import org.slf4j.LoggerFactory;

import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.Predicate;
import java.util.stream.Collectors;


/**
 * A SimpleNetwork is a Directed Acyclic Graph that holds a set of nodes,
 * their orderings (e.g., BEFORE, DURING), and their parent-child relationships.
 * <p>
 * Nodes can be ordered by (1) directly adding an edge in the graph or
 * (2) creating a parent-child relationship between two nodes, which adds
 * specific edges to capture the ordering of the relationship.
 * A node is unconstrained if it has no predecessors.
 * A node is last if no successors follow it.
 * <p>
 * An edge (x, y) indicates a strict ordering x < y, which is translated as
 * x.BEFORE(y) in Allen's logic (see {@link AllenIntervalOperator}) and is
 * equivalent in timepoint temporal logic to x.start < x.end < y.start < y.end.
 * <p>
 * Methods with the "add" prefix add missing nodes to the network.
 * The method add(node) adds the node to the network without adding edges.
 * <p>
 * BEFORE orderings are created using the add(predecessor, successors).
 * For example, add(a, {b, c, d}) creates edges {(a, b), (a, c), (a, d)}.
 * <p>
 * For nodes in a parent-child relationship, all children must complete
 * during a parent, creating a DURING ordering, i.e., child.DURING(parent).
 * This is equivalent to parent.CONTAINS(child), but we use the DURING
 * ordering for consistency with BEFORE orderings.
 * <p>
 * Parent-child orderings may be created using addSubs(parent, children).
 * For example, addSubs(a, {b, c, d}) creates the edges
 * (b, c, d).DURING(a) with _no_ ordering over b, c, or d.  However,
 * addSubsOrdered(a, {b, c, d}) creates the edges (b, c, d).DURING(a) as well
 * as pairwise explicit edges {(b, c), (c, d)}, where (b, d) is transitive.
 * <p>
 * Limitations of the {@link SimpleNetwork} include:
 * - nodes must be unique,
 * - within network sharing is supported but between network sharing is not; and
 * - only BEFORE and DURING constraints are supported.
 * A {@link TemporalGoalNetwork} overcomes these limitations by allowing sharing
 * and more sophisticated temporal constraints.
 */
public class SimpleNetwork<T, V extends SimpleNode<T>, E extends SimpleEdge>
    extends BindableImpl implements BindableContainer
{
    protected final static org.slf4j.Logger logger = LoggerFactory.getLogger(SimpleNetwork.class);
    protected static final AtomicInteger NEXT_NETWORK_ID = new AtomicInteger(0);

    protected final int networkId;
    protected final Graph<V, E> graph;

    Map<UUID, Integer> localIdMap = new TreeMap<>();
    AtomicInteger nextNodeId = new AtomicInteger(0);

    public SimpleNetwork(Class<E> edgeClass) {
        graph = new DirectedAcyclicGraph<>(edgeClass);
        networkId = NEXT_NETWORK_ID.getAndIncrement();
    }

    public String shortId() {
        return "" + networkId;
    }

    @Override
    public String toString() {
        return toDetailedString("");
    }

    public String toDetailedStringNodeIds() {
        return toDetailedStringUsingNodeIds("", true);
    }

    public String toDetailedString(String prefix) {
        return toDetailedString(prefix, false);
    }

    public String toDetailedStringUsingNodeIds(String prefix, boolean includeNodeDetails) {
        Set<V> parentRoots = getParentRoots();
        Set<V> unconstrained = getUnconstrained();

        String header = String.format("%sGoalNetwork (id:%s) nodes:%s, unconstrained:%s parentRoots:%s",
                prefix, networkId, graph.vertexSet().size(), unconstrained.size(), parentRoots.size());
        StringBuilder s = new StringBuilder(header);

        if (includeNodeDetails) {
            s.append("\n" + header);
            Map<UUID, String> idDetailsMap = new TreeMap<>();

            String prefix2 = String.format("%s| ", prefix);
            for (V root : parentRoots) {
                collectChildSummary(s, root, prefix);
            }
        }
        return s.toString();
    }

    private void collectChildSummary(StringBuilder summary, V node, String prefix) {
        addSummary(summary, node, prefix, "");
        //noinspection unchecked
        V[] children = (V[]) node.getChildren();
        for (V child : children) {
            collectChildSummary(summary, child, prefix + "  ");
        }
    }

    private void addSummary(StringBuilder summary, V node, String prefix, String contents) {
        UUID uuid = node.uuid;
        if (localIdMap.containsKey(uuid)) {
            int localId = localIdMap.get(uuid);
            String shortUUID = uuid.toString();
            shortUUID = shortUUID.substring(shortUUID.length() - 7);
            summary.append(String.format("\n%s|-%5d(%s) -- %s %s",
                    prefix, localId, shortUUID, node, contents));
        } else {
            summary.append(String.format("\n%s|-%5d(%s) -- %s %s",
                    prefix, -1, "UNK", node, contents));
        }
    }

    public String toDetailedString(String prefix, boolean includeNodeDetails) {
        Set<V> parentRoots = getParentRoots();
        Set<V> unconstrained = getUnconstrained();

        StringBuilder s = new StringBuilder(String.format("%sGoalNetwork (id:%s) nodes:%s, unconstrained:%s parentRoots:%s",
                prefix, networkId, graph.vertexSet().size(), unconstrained.size(), parentRoots.size()));

        if (includeNodeDetails) {
            String prefix2 = String.format("%s| ", prefix);

            for (V node : graph.vertexSet()) {
                String details = collectNodeDetails(node, prefix2);
                s.append(details);
            }
        }
        return s.toString();
    }

    private String collectNodeDetails(V node, String prefix) {
        StringBuilder parentString = new StringBuilder();
        StringBuilder subString = new StringBuilder();
        StringBuilder timeString = new StringBuilder();
        Set<E> successors = graph.edgeSet().stream()
                .filter(e -> graph.getEdgeSource(e).equals(node))
                .collect(Collectors.toSet());
        for (E edge : successors) {
            V target = graph.getEdgeTarget(edge);
            if (edge.targetIsParent()) {
                parentString.append(String.format(".P:%s", target.getShortName()));
                timeString.append(String.format(".D%s", target.getShortName()));
            } else {
                timeString.append(String.format(".B:%s", target.getShortName()));
            }
        }
        subString.append(timeString);

        Set<E> parentEdges = graph.edgeSet().stream()
                .filter(e -> graph.getEdgeTarget(e).equals(node))

                .filter(SimpleEdge::targetIsParent)
                .collect(Collectors.toSet());

        for (E parentEdge : parentEdges) {
            subString.append(String.format("%s", parentEdge.getShortNameChild()));
        }

        String unconstrainedString = " ";
        if (isUnconstrained(node)) {
            unconstrainedString = "u";
        }

        String parentRootString = " ";
        if (isParent(node)) {
            parentRootString = "p";
        }

        String shortId = node.getShortName();
        return String.format("\n%s%s%s(%s): %s%s -- %s",
                prefix, parentRootString, unconstrainedString, shortId,
                parentString, subString, node);
    }

    public void debug() {
        String networkString = toDetailedString("", true);
        for (String line : networkString.split("\n")) {
            logger.debug(line);
        }
    }

    public int size() {
        return graph.vertexSet().size();
    }

    public boolean contains(V node) {
        return graph.containsVertex(node);
    }

    public void releaseAll() {
        throw new NotImplementedException("Not Yet Implemented");
    }

    /***
     * Adds the node this network if it is _not_ attached, returning a
     * SKIPPED_DUPLICATE status if the node is already attached to this network
     * or an ERROR_NODE_ATTACHED_TO_DIFFERENT_NETWORK if the node is attached
     * to another network.
     *
     * @param node the node to add
     * @return the add status of the attempt to add the node
     */
    @SuppressWarnings("unchecked")
    public AddStatus add(V node) {
        if (graph.containsVertex(node)) {
            String msg = String.format("%s:%s Network already contains node, skipping the add", graph, node);
            logger.debug(msg);
            return AddStatus.SKIPPED_DUPLICATE;
        } else if (node.isAttached()
                && !node.isAttachedTo(this)) {
            String msg = String.format("%s:%s Node is attached to another network, returning an error status", graph, node);
            logger.error(msg);
            return AddStatus.nodeAttachedToDifferentNetwork(msg);
        }
        if (node.attach((SimpleNetwork<T, V, SimpleEdge>) this)) {
            graph.addVertex(node);
            int localId = nextNodeId.getAndIncrement();
            localIdMap.put(node.uuid, localId);
            String msg = String.format("%s:%s Added node with localId %d", graph, node, localId);
            logger.debug(msg);
            return AddStatus.ADDED;
        } else {
            String msg = String.format("%s:%s Node could not be attached, returning an error status", graph, node);
            logger.error(msg);
            return AddStatus.error(msg);
        }
    }

    /***
     * Adds predecessor and successors to the network with an ordering
     * for each successor predecessor.BEFORE(successor).
     * @param predecessor the node that will execute first
     * @param successors the nodes that will follow the predecessor
     */
    @SafeVarargs
    public final void add(V predecessor, V... successors) {
        if (add(predecessor).noError()) {
            for (V successor : successors) {
                if (add(successor).noError()) {
                    addBeforeEdge(predecessor, successor);
                }
            }
        }
    }

    public void addAll(SimpleNetwork<T, V, E> decomposition) {
        Map<V, V> nodes = new HashMap<>(); //java.util.Set lacks get(),
        for (V nodeToAdd : decomposition.getNodes()) {
            @SuppressWarnings("unchecked")
            V instance = (V) nodeToAdd.cloner().build();
            add(instance);
            nodes.put(instance, instance);
        }
        for (E edge : decomposition.getEdges()) {
            V decompSource = decomposition.graph.getEdgeSource(edge);
            V decompTarget = decomposition.graph.getEdgeTarget(edge);
            V newSource = nodes.get(decompSource);
            V newTarget = nodes.get(decompTarget);
            if (edge.targetIsParent()) {
                addSubsOrdered(newTarget, newSource);
            } else {
                addBeforeEdge(newSource, newTarget);
            }
        }
    }

    /***
     * Adds an unconstrained nodes children to the network and constrains
     * them to execute DURING the parent.
     *
     * @param parent the parent node, which will execute last
     * @param children the children, which precede the parent
     */
    @SafeVarargs
    public final void addSubsUnordered(V parent, V... children) {
        addSubs_impl(parent, Arrays.asList(children), false);
    }

    /***
     * Adds an children nodes to the network, constrains them to execute
     * DURING the parent, and constrains them to be ordered
     *
     * @param parent the parent node, which will execute last
     * @param children the children, which precede the parent
     */
    @SafeVarargs
    public final void addSubsOrdered(V parent, V... children) {
        addSubs_impl(parent, Arrays.asList(children), true);
    }

    void addSubs_impl(V parent,
                      List<V> children,
                      boolean childrenAreOrdered) {
        if (add(parent).noError()) {
            V lastChild = null;
            for (V child : children) {
                AddStatus status = add(child);
                if (status.noError()) {
                    E childEdge = addBeforeEdge(child, parent);
                    childEdge.setTargetAsParent();
                    if (childrenAreOrdered &&
                            lastChild != null) {
                        addBeforeEdge(lastChild, child);
                    }
                } else {
                    String msg = String.format("Child node %s had status %s", child, status);
                    logger.error(msg);
                    throw new UnsupportedOperationException(msg);
                }
                lastChild = child;
            }
        }
    }

    public E addBeforeEdge(V predecessor, V successor) {
        return graph.addEdge(predecessor, successor);
    }

    /**
     * Returns a Set of all nodes in the network.
     *
     * @return a Set of all nodes in the network
     */
    public Set<V> getNodes() {
        return graph.vertexSet();
    }

    /**
     * Returns a Set of all edges in the network.
     *
     * @return a Set of all edges in the network
     */
    public Set<E> getEdges() {
        return graph.edgeSet();
    }

    /**
     * Attempts release (removal) of the node.
     * At a minimum, a node must be unconstrained to be released.
     * Subclasses can further restrict when nodes can be released.
     *
     * @param node the node to attempt releasing
     * @return whether the goal was released
     */
    public boolean release(V node) {
        if (isUnconstrained(node)) {
            if (node.canBeReleased()) {
                logger.debug("{}:{} is releasing", graph, node);
                graph.removeVertex(node);
                node.releaseFrom(this);
                localIdMap.remove(node.uuid);
                return true;
            } else {
                logger.debug("{}:{} declares it cannot be released; ignoring request to release", graph, node);
            }
        } else {
            logger.debug("{}:{} is constrained; ignoring request to release", graph, node);
        }
        return false;
    }

    /**
     * Checks whether node has children, which means it is constrained.
     *
     * @param node the node to check
     * @return whether the node has children
     */
    boolean hasChildren(V node) {
        return isConstrained(node);
    }

    /***
     * An unconstrained goalNode has no predecessors.
     *
     * @return whether the node is unconstrained
     */
    boolean isConstrained(V node) {
        return graph.inDegreeOf(node) > 0;
    }

    /***
     * An unconstrained goalNode has no predecessors.
     *
     * @return whether the node is unconstrained
     */
    boolean isUnconstrained(V node) {
        return graph.inDegreeOf(node) == 0;
    }

    public Set<V> getUnconstrained() {
        return graph.vertexSet().stream()
                .filter(this::isUnconstrained)
                .collect(Collectors.toSet());
    }

    public Set<V> getUnconstrainedChildren() {
        return graph.vertexSet().stream()
                .filter(this::isUnconstrained)
                .filter(this::hasParent)
                .collect(Collectors.toSet());
    }

    public Set<V> getUnconstrainedRoots() {
        return graph.vertexSet().stream()
                .filter(this::isUnconstrained)
                .filter(this::hasNoParent)
                .collect(Collectors.toSet());
    }

    public Set<V> getParentRoots() {
        return graph.edgeSet().stream()
                .filter(SimpleEdge::targetIsParent)
                .filter(e -> hasNoParent(graph.getEdgeTarget(e)))
                .map(graph::getEdgeTarget)
                .collect(Collectors.toCollection(TreeSet::new));
    }

    public Set<V> getDanglingRoots() {
        return graph.vertexSet().stream()
                .filter(this::hasNoTarget)
                .collect(Collectors.toCollection(TreeSet::new));
    }

    public Set<V> getViewRoots() {
        Set<V> viewRoots = getParentRoots();
        Set<V> danglingRoots = getDanglingRoots();
        Set<V> unconstrainedRoots = getUnconstrainedRoots();
        viewRoots.addAll(danglingRoots);
        viewRoots.addAll(unconstrainedRoots);
        return viewRoots;
    }

    public boolean isParent(V node) {
        Set<E> incomingParentEdges = graph.incomingEdgesOf(node).stream()
                .filter(SimpleEdge::targetIsParent)
                .collect(Collectors.toSet());
        return incomingParentEdges.size() == 0;
    }

    public boolean hasParent(V node) {
        Set<E> outgoingParentEdges = graph.outgoingEdgesOf(node).stream()
                .filter(SimpleEdge::targetIsParent)
                .collect(Collectors.toSet());
        return outgoingParentEdges.size() > 0;
    }

    public boolean hasNoParent(V node) {
        Set<E> outgoingParentEdges = graph.outgoingEdgesOf(node).stream()
                .filter(SimpleEdge::targetIsParent)
                .collect(Collectors.toSet());
        return outgoingParentEdges.size() == 0;
    }

    public boolean hasNoTarget(V node) {
        return graph.outgoingEdgesOf(node).size() == 0;
    }

    public Set<V> successorsOf(V child) {
        return graph.outgoingEdgesOf(child).stream()
                .map(graph::getEdgeTarget)
                .collect(Collectors.toCollection(TreeSet::new));
    }

    public Set<V> parentsOf(V child) {
        return graph.outgoingEdgesOf(child).stream()
                .filter(SimpleEdge::targetIsParent)
                .map(graph::getEdgeTarget)
                .collect(Collectors.toCollection(TreeSet::new));
    }

    public Set<V> childrenOf(V parent) {
        return graph.incomingEdgesOf(parent).stream()
                .filter(SimpleEdge::targetIsParent)
                .map(graph::getEdgeSource)
                .collect(Collectors.toCollection(TreeSet::new));
    }

    public Set<V> find(Predicate<V> predicate) {
        return graph.vertexSet().stream()
                .filter(predicate)
                .collect(Collectors.toCollection(TreeSet::new));
    }

    public List<V> find(V node) {
        Set<V> matches = find(nodeToCheck -> nodeToCheck.equals(node));
        return new ArrayList<>(matches);
    }

    public boolean contains(Predicate<V> predicate) {
        return graph.vertexSet().stream().anyMatch(predicate);
    }

    public boolean isBefore(V predecessor, V successor) {
        return graph.containsEdge(predecessor, successor);
    }

    public boolean isDuring(V child, V parent) {
        return childrenOf(parent).contains(child);
    }

    public boolean parentOf(V parent, V child) {
        return childrenOf(parent).contains(child);
    }

    public boolean childOf(V child, V parent) {
        return childrenOf(parent).contains(child);
    }

    public void removeTransitiveEdges() {
        DirectedAcyclicGraph<V, E> dag = (DirectedAcyclicGraph<V, E>) graph;
        List<V> nodes = new ArrayList<V>(graph.vertexSet());
        for (int i = 0; i < nodes.size(); i++) {
            for (int j = i+1; j < nodes.size(); j++) {

                if (dag.containsEdge(nodes.get(i), nodes.get(j))) {
                    dag.removeEdge(nodes.get(i), nodes.get(j));

                    if(!dag.getDescendants(nodes.get(i)).contains(nodes.get(j))) {
                        dag.addEdge(nodes.get(i), nodes.get(j));
                    }
                }

                if (dag.containsEdge(nodes.get(j), nodes.get(i))) {
                    dag.removeEdge(nodes.get(j), nodes.get(i));

                    if(!dag.getDescendants(nodes.get(j)).contains(nodes.get(i))) {
                        dag.addEdge(nodes.get(j), nodes.get(i));
                    }
                }
            }
        }
    }

    /**
     * Return the target of the provided edge.
     * <p>
     * Recall that the target will be the successor node.
     * If edge.targetIsParent() is true, then the target is also a parent.
     *
     * @param edge the requested edge
     * @return the target of the edge
     */
    public V getTarget(E edge) {
        return graph.getEdgeTarget(edge);
    }

    /**
     * Return the source of the provided edge.
     * <p>
     * Recall that the target will be the predecessor node.
     * If edge.targetIsParent() is true, then the source is also a subgoal.
     *
     * @param edge the requested edge
     * @return the source of the edge
     */
    public V getSource(E edge) {
        return graph.getEdgeSource(edge);
    }

    public void visitAll(SimpleNetworkVisitor<V> visitor) {
        graph.vertexSet().forEach(visitor::visit);
    }


    //////////////////////////////////////////////////////////
    // Visitors
    //////////////////////////////////////////////////////////

    public enum AddStatus {
        ADDED,
        SKIPPED_DUPLICATE,
        ERROR_NODE_ATTACHED_TO_DIFFERENT_NETWORK,
        ERROR;

        private String errorMessage;

        @SuppressWarnings("SameReturnValue")
        public static AddStatus added() {
            return ADDED;
        }

        public static AddStatus error(String msg) {
            return ERROR
                    .setErrorMessage(msg);
        }

        public static AddStatus nodeAttachedToDifferentNetwork(String msg) {
            return ERROR_NODE_ATTACHED_TO_DIFFERENT_NETWORK
                    .setErrorMessage(msg);
        }

        public AddStatus setErrorMessage(String errorMessage) {
            this.errorMessage = errorMessage;
            return this;
        }

        public boolean isErrorStatus() {
            return errorMessage != null;
        }

        public boolean noError() {
            return errorMessage == null;
        }
    }

}
