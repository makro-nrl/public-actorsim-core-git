package nrl.actorsim.goalnetwork;

public class NodeBase<T> {
    final T stored;

    protected NodeBase(T value) {
        stored = value;
    }

    public static void main(String[] args) {
        System.out.println("Test!");

    }

    public <V extends NodeBase<T>, B extends Builder<V, B>>
    Builder<V, B> builder() {
        return new Builder<>();
    }

    NodeBase<T> instance(T value) {
        return new NodeBase<>(value);
    }

    public class Builder<V extends NodeBase<T>, B extends Builder<V, B>> {
        T stored;

        protected Builder() {
        }

        public Builder<V, B> builder() {
            return new Builder<>();
        }

        private Builder<V, B> getThis() {
            return this;
        }

        public Builder<V, B> store(T value) {
            this.stored = value;
            return getThis();
        }

        @SuppressWarnings("unchecked")
        V build() {
            return (V) new NodeBase<>(stored);
        }

    }
}
