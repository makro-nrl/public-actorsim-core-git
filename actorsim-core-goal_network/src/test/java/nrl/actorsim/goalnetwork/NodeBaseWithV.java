package nrl.actorsim.goalnetwork;

@SuppressWarnings("unchecked")
public class NodeBaseWithV<T, V extends NodeBaseWithV<T, V>> {
    final T stored;

    protected NodeBaseWithV(T value) {
        stored = value;
    }

    public static void main(String[] args) {
        System.out.println("Test!");

    }

    public <B extends Builder<B>>
    Builder<B> builder() {
        return new Builder<>();
    }

    V instance(T value) {
        return (V) new NodeBaseWithV<T, V>(value);
    }

    public class Builder<B extends Builder<B>> {
        T stored;

        protected Builder() {
        }

        public Builder<B> builder() {
            return new Builder<>();
        }

        private Builder<B> getThis() {
            return this;
        }

        public Builder<B> store(T value) {
            this.stored = value;
            return getThis();
        }

        V build() {
            return (V) new NodeBaseWithV<T, V>(stored);
        }

    }
}
