package nrl.actorsim.goalnetwork;

import nrl.actorsim.domain.PlanningProblem;
import nrl.actorsim.domain.WorldObject;
import nrl.actorsim.goalnetwork.test.DockRobotAPA_Domain;
import org.testng.annotations.Test;

public class TestDockRobotDomain {

    @Test
    public void testDRD_AutomaticallyAddObjectsDuringInstantiationOfProblem() {
        DockRobotAPA_Domain domain = new DockRobotAPA_Domain();
        PlanningProblem problem = domain.makeExample_2_3();
        WorldObject dock1 = problem.getObject("d1");
        assert dock1.getPathName().equals("d1");
    }
}
