package nrl.actorsim.goalnetwork;

import nrl.actorsim.domain.Statement;
import nrl.actorsim.goalnetwork.test.TestGoalNetworkBuilder;
import org.testng.SkipException;
import org.testng.annotations.Test;

public class TestTemporalGoalNetworks {

    @Test
    void testTGNs_12_Ord_NoSharingSucceedsForTGN() {
        TestGoalNetworkBuilder<Statement,
                GoalNetworkNode<Statement>,
                TemporalGoalNetwork> builder = new TestGoalNetworkBuilder<>();
        GoalNetwork network = builder.build_12_Ord_NoSharing(TemporalGoalNetwork.class);
        assert network.size() == 12;
    }

    @Test
    void testTGNs_12_Ord_WithinSharingSucceedsForTGN() {
        TestGoalNetworkBuilder<Statement,
                GoalNetworkNode<Statement>,
                TemporalGoalNetwork> builder = new TestGoalNetworkBuilder<>();
        GoalNetwork network = builder.build_12_Ord_WithinSharing(TemporalGoalNetwork.class);
        assert network.size() == 12;
    }

    @Test
    void testTGNs_12_Ord_BetweenSharingSucceedsForTGN() {
        throw new SkipException("TGN sharing is not yet implemented.");
        //TestGoalNetworkBuilder<Statement,
        //        GoalNetworkNode<Statement>,
        //        TemporalGoalNetwork> builder = new TestGoalNetworkBuilder<>();
        //builder.build_12_Ord_BetweenSharing(TemporalGoalNetwork.class);
        //assert true;
    }

}
