package nrl.actorsim.goalnetwork;

import nrl.actorsim.domain.Statement;


@SuppressWarnings("unchecked")
public class NodeSub<T extends Statement> extends NodeBase<T> {
    protected final boolean isImportant;

    protected NodeSub(T value, boolean important) {
        super(value);
        this.isImportant = important;
    }

    @Override
    NodeSub<T> instance(T value) {
        return new NodeSub<>(value, isImportant);
    }

    public class Builder<V extends NodeSub<T>, B extends Builder<V, B>>
            extends NodeBase<T>.Builder<V, B> {
        protected boolean important;

        public void setImportant() {
            this.important = true;
        }

        V build() {
            return (V) new NodeSub<>(stored, important);
        }
    }
//
//
//    public static
//    <T2 extends Statement,
//            V extends NodeSubWithV<T2>,
//            B extends NodeSubWithV.Cloner<T2, V, ?>>
//    NodeSubWithV.Cloner<T2, ? extends NodeSubWithV<T2>, ? extends Cloner<T2, ?, ?>>
//    newBuilder(Class<NodeSubWithV<T2>> cls) {
//        return new Cloner<T2, NodeSubWithV<T2>, Cloner<T2, ?, ?>>();
//    }
//
//    public static class Cloner<
//            T2 extends Statement,
//            V extends NodeSubWithV<T2>,
//            B extends NodeBaseWithV.Cloner<T2, V, B>>
//            extends NodeBaseWithV.Cloner<T2, V, Cloner<T2, V, B>> {
//        protected boolean important;
//
//        protected Cloner() {
//        }
//
//        public void setImportant() {
//            this.important = true;
//        }
//
//
//    }
}
