package nrl.actorsim.goalnetwork;

import nrl.actorsim.domain.Statement;

@SuppressWarnings("unchecked")
public class NodeSubWithV<T extends Statement, V extends NodeSubWithV<T, V>>
        extends NodeBaseWithV<T, V> {
    protected final boolean isImportant;

    protected NodeSubWithV(T value, boolean important) {
        super(value);
        this.isImportant = important;
    }

    @Override
    V instance(T value) {
        return (V) new NodeSubWithV<T, V>(value, isImportant);
    }

    public class Builder<B extends Builder<B>> extends NodeBaseWithV<T, V>.Builder<B> {
        protected boolean important;

        public void setImportant() {
            this.important = true;
        }

        V build() {
            return (V) new NodeSubWithV<T, V>(stored, important);
        }
    }
//
//
//    public static
//    <T2 extends Statement,
//            V extends NodeSubWithV<T2>,
//            B extends NodeSubWithV.Cloner<T2, V, ?>>
//    NodeSubWithV.Cloner<T2, ? extends NodeSubWithV<T2>, ? extends Cloner<T2, ?, ?>>
//    newBuilder(Class<NodeSubWithV<T2>> cls) {
//        return new Cloner<T2, NodeSubWithV<T2>, Cloner<T2, ?, ?>>();
//    }
//
//    public static class Cloner<
//            T2 extends Statement,
//            V extends NodeSubWithV<T2>,
//            B extends NodeBaseWithV.Cloner<T2, V, B>>
//            extends NodeBaseWithV.Cloner<T2, V, Cloner<T2, V, B>> {
//        protected boolean important;
//
//        protected Cloner() {
//        }
//
//        public void setImportant() {
//            this.important = true;
//        }
//
//
//    }
}
