package nrl.actorsim.goalnetwork;

import nrl.actorsim.goalnetwork.SimpleNetwork.AddStatus;
import nrl.actorsim.goalnetwork.test.SimpleNetworkBuilder;
import org.testng.annotations.Test;

import java.util.Set;

import static nrl.actorsim.goalnetwork.test.SimpleNetworkBuilder.*;

public class TestSimpleNetworks {
    @Test
    void testSimpleNetwork_4_Ord() {
        SimpleNetwork<String, SimpleNode<String>, SimpleEdge> network =
                SimpleNetworkBuilder.createSimpleNetwork_4_Ord();
        network.debug();
        assert (network.size() == 4);
        assert (network.getUnconstrained().size() == 1);

        Set<SimpleNode<String>> parentRoots = network.getParentRoots();
        assert parentRoots.size() == 1;
        assert parentRoots.contains(SimpleNetworkBuilder.a);
    }

    @Test
    void testSimpleNetwork_4_Unord() {
        SimpleNetwork<String, SimpleNode<String>, SimpleEdge> network =
                SimpleNetworkBuilder.createSimpleNetwork_4_Unord();
        network.debug();
        assert (network.size() == 4);
        assert (network.getUnconstrained().size() == 2);

        Set<SimpleNode<String>> parentRoots = network.getParentRoots();
        assert parentRoots.size() == 1;
        assert parentRoots.contains(SimpleNetworkBuilder.a);
    }

    @Test
    void testSimpleNetwork_5_Ord() {
        SimpleNetwork<String, SimpleNode<String>, SimpleEdge> network =
                SimpleNetworkBuilder.createSimpleNetwork_5_Ord();
        network.debug();
        assert (network.size() == 5);
        assert (network.getUnconstrained().size() == 2);

        Set<SimpleNode<String>> parentRoots = network.getParentRoots();
        assert parentRoots.size() == 1;
        assert parentRoots.contains(SimpleNetworkBuilder.b);
    }

    @Test
    void testSimpleNetwork_5_Ord_ReleaseCorrectly() {
        SimpleNetwork<String, SimpleNode<String>, SimpleEdge> network =
                SimpleNetworkBuilder.createSimpleNetwork_5_Ord();
        network.debug();
        assert network.release(d);
        assert network.release(e);
        assert network.release(c);
        assert network.release(b);
        assert network.release(a);
    }

    @Test
    void testSimpleNetwork_5_Ord_ReleaseIncorrectlyDetected() {
        SimpleNetwork<String, SimpleNode<String>, SimpleEdge> network =
                SimpleNetworkBuilder.createSimpleNetwork_5_Ord();
        network.debug();
        assert !network.release(e);
        assert !network.release(b);
        assert !network.release(a);

        assert network.release(d);
        assert !network.release(b);
        assert !network.release(a);

        assert network.release(e);
        assert !network.release(b);
        assert !network.release(a);

        assert network.release(c);
        assert !network.release(a);

        assert network.release(b);
        assert network.release(a);
    }

    @Test
    void testSimpleNetwork_ComplexOrderedNetwork() {
        SimpleNetwork<String, SimpleNode<String>, SimpleEdge> network =
                SimpleNetworkBuilder.createSimpleNetwork_12_Ord();
        network.debug();
        assert network.size() == 12;
        assert network.getUnconstrained().size() == 4;

        Set<SimpleNode<String>> parentRoots = network.getParentRoots();
        assert parentRoots.size() == 2;
        assert parentRoots.contains(SimpleNetworkBuilder.a);
        assert parentRoots.contains(d);
    }

    @Test
    void testSimpleNetwork_AddingDuplicateNodeIsHarmless() {
        SimpleNetwork<String, SimpleNode<String>, SimpleEdge> network =
                SimpleNetworkBuilder.createSimpleNetwork_5_Ord();
        network.debug();
        SimpleNode<String> duplicate = new SimpleNode<>("A");
        assert network.add(duplicate) == AddStatus.SKIPPED_DUPLICATE;
        assert network.size() == 5;

        duplicate = new SimpleNode<>("B");
        assert network.add(duplicate) == AddStatus.SKIPPED_DUPLICATE;
        assert network.size() == 5;

        duplicate = new SimpleNode<>("C");
        assert network.add(duplicate) == AddStatus.SKIPPED_DUPLICATE;
        assert network.size() == 5;

        duplicate = new SimpleNode<>("D");
        assert network.add(duplicate) == AddStatus.SKIPPED_DUPLICATE;
        assert network.size() == 5;

        duplicate = new SimpleNode<>("E");
        assert network.add(duplicate) == AddStatus.SKIPPED_DUPLICATE;
        assert network.size() == 5;
    }

}
