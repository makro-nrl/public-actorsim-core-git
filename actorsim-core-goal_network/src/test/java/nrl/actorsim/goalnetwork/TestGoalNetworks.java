package nrl.actorsim.goalnetwork;

import nrl.actorsim.domain.Statement;
import nrl.actorsim.goalnetwork.test.TestGoalNetworkBuilder;
import org.testng.annotations.Test;

import java.util.Set;

public class TestGoalNetworks {
    @Test
    void testGNs_Network_5_Ord() {
        TestGoalNetworkBuilder<Statement,
                GoalNetworkNode<Statement>,
                GoalNetwork> builder = new TestGoalNetworkBuilder<>();
        GoalNetwork network = builder.build_5_Ord(GoalNetwork.class);
        network.debug();
        assert (network.size() == 5);
        assert (network.getUnconstrained().size() == 2);

        Set<GoalNetworkNode<Statement>> parentRoots = network.getParentRoots();
        assert parentRoots.size() == 1;
        assert parentRoots.contains(builder.b);
    }

    @Test
    void testGNs_5_Ord_ReleaseCorrectly() {
        TestGoalNetworkBuilder<Statement,
                GoalNetworkNode<Statement>,
                GoalNetwork> builder = new TestGoalNetworkBuilder<>();
        GoalNetwork network = builder.build_5_Ord(GoalNetwork.class);
        network.debug();
        assert !network.release(builder.d);
        builder.d.setComplete();
        assert network.release(builder.d);


        assert !network.release(builder.e);
        builder.e.setComplete();
        assert network.release(builder.e);

        assert !network.release(builder.c);
        builder.c.setComplete();
        assert network.release(builder.c);

        assert !network.release(builder.b);
        builder.b.setComplete();
        assert network.release(builder.b);

        assert !network.release(builder.a);
        builder.a.setComplete();
        assert network.release(builder.a);
    }

    @Test
    void testGNs_5_Ord_ReleaseIncorrectlyDetected() {
        TestGoalNetworkBuilder<Statement,
                GoalNetworkNode<Statement>,
                GoalNetwork> builder = new TestGoalNetworkBuilder<>();
        GoalNetwork network = builder.build_5_Ord(GoalNetwork.class);
        network.debug();
        assert !network.release(builder.e);
        assert !network.release(builder.b);
        assert !network.release(builder.a);

        builder.d.setComplete();
        assert network.release(builder.d);
        assert !network.release(builder.b);
        assert !network.release(builder.a);

        builder.e.setComplete();
        assert network.release(builder.e);
        assert !network.release(builder.b);
        assert !network.release(builder.a);

        builder.c.setComplete();
        assert network.release(builder.c);
        assert !network.release(builder.a);

        builder.b.setComplete();
        builder.a.setComplete();
        assert network.release(builder.b);
        assert network.release(builder.a);
    }

    @Test
    void testGNs_12_Ord_NoSharingSucceedsForGoalNetwork() {
        TestGoalNetworkBuilder<Statement,
                GoalNetworkNode<Statement>,
                GoalNetwork> builder = new TestGoalNetworkBuilder<>();
        GoalNetwork network = builder.build_12_Ord_NoSharing(GoalNetwork.class);
        assert network.size() == 12;
    }

    @Test
    void testGNs_12_Ord_WithinSharingSucceedsForGoalNetwork() {
        TestGoalNetworkBuilder<Statement,
                GoalNetworkNode<Statement>,
                GoalNetwork> builder = new TestGoalNetworkBuilder<>();
        GoalNetwork network = builder.build_12_Ord_WithinSharing(GoalNetwork.class);
        assert network.size() == 12;
    }

    @Test (expectedExceptions = {UnsupportedOperationException.class})
    void testGNs_12_Ord_BetweenSharingFailsForGoalNetwork() {
        TestGoalNetworkBuilder<Statement,
                GoalNetworkNode<Statement>,
                GoalNetwork> builder = new TestGoalNetworkBuilder<>();
        builder.build_12_Ord_BetweenSharing(GoalNetwork.class);
        assert true;
    }
}
