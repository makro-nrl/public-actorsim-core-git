package nrl.actorsim.goalnetwork;

import nrl.actorsim.domain.StateAssignment;
import nrl.actorsim.domain.Statement;
import nrl.actorsim.goalnetwork.SimpleNetwork.AddStatus;
import nrl.actorsim.goalnetwork.test.TestGoalNetworkDomain;
import org.testng.annotations.Test;

import java.util.Set;

import static nrl.actorsim.goalnetwork.test.TestGoalNetworkDomain.*;
import static nrl.actorsim.domain.test.TestItemExistsDomain.*;

public class TestStatementGoalNetworks {

    @Test
    void testSGNs_AddingDuplicateNodeIsHarmless() {
        GoalNetworkNode<Statement> existsNodeA = existsNode(A);
        GoalNetworkNode<Statement> existsNodeB = existsNode(B);
        GoalNetworkNode<Statement> existsNodeC = existsNode(C);

        GoalNetwork network = createGoalNetwork_3_2OrderedPrimitive();
        network.debug();

        assert network.add(existsNodeA) == AddStatus.SKIPPED_DUPLICATE;
        assert network.size() == 3;

        assert network.add(existsNodeB) == AddStatus.SKIPPED_DUPLICATE;
        assert network.size() == 3;

        assert network.add(existsNodeC) == AddStatus.SKIPPED_DUPLICATE;
        assert network.size() == 3;
    }


    @Test
    public void testSGNs_AbcNetworkMethodApplication() {
        //           A
        //         .  .
        //        b -> c
        GoalNetwork network = new GoalNetwork();
        GoalNetworkNode<Statement> existsNodeA = existsNode(A);
        GoalNetworkNode<Statement> existsNodeB = existsNode(B);
        GoalNetworkNode<Statement> existsNodeC = existsNode(C);

        network.add(existsNodeA);
        assert network.contains(existsNodeA);

        Method method = createMethod_A_to_b_c();
        Method.Instance instance = method.instance(network);
        instance.apply();
        assert network.size() == 3;
        assert network.contains(existsNodeA);
        assert network.contains(existsNodeB);
        assert network.contains(existsNodeC);

        assert network.isBefore(existsNodeB, existsNodeC);
        assert network.isDuring(existsNodeB, existsNodeA);
        assert network.isDuring(existsNodeC, existsNodeA);
        assert network.isUnconstrained(existsNodeB);
        assert network.isPrimitive(existsNodeB);
        assert network.isPrimitive(existsNodeC);
    }

    @Test
    public void testSGNs_AbcNetworkWithState() {
        //           A
        //         .  .
        //        b -> c
        TestGoalNetworkDomain domain = getABCNetworkDomain();
        GoalNetworkProblem problem = domain.buildABCNetworkProblem();
        GoalNetwork network = problem.getNetwork();

        StateAssignment state = problem.getNewState();
        Set<GoalNetworkNode<Statement>> released = network.checkComplete(state);
        assert released.size() == 0;

        GoalNetworkNode<Statement> existsNodeA = existsNode(A);
        GoalNetworkNode<Statement> existsNodeB = existsNode(B);
        GoalNetworkNode<Statement> existsNodeC = existsNode(C);

        state.add(existsNodeB.getStored());
        released = network.checkComplete(state);
        assert network.size() == 2
                && released.size() == 1
                && released.contains(existsNodeB);

        state.add(existsNodeC.getStored());
        released = network.checkComplete(state);
        assert network.size() == 1
                && released.size() == 1
                && released.contains(existsNodeC);

        state.add(existsNodeA.getStored());
        released = network.checkComplete(state);
        assert network.size() == 0
                && released.size() == 1
                && released.contains(existsNodeA);
    }
}
