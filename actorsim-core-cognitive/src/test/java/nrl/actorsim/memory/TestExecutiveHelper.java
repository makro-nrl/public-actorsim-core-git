package nrl.actorsim.memory;

import nrl.actorsim.cognitive.*;
import nrl.actorsim.cognitive.test.*;
import nrl.actorsim.domain.*;

import java.util.*;

public class TestExecutiveHelper {
    final static org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(TestExecutiveHelper.class);

    Options options;

    PlanningDomain domain;
    PlanningProblem problem;
    Executive executive;
    ExecutiveState state;
    WorkingMemory memory;
    GoalMemorySimple goalMemory;
    InitialObjects init;
    Strategies strategies;

    TestExecutiveHelper() {
        this(new Options());
    }

    TestExecutiveHelper(Options options) {
        this.options = options;
        domain = loadDomain();
        problem = domain.getProblem();
        executive = Executive.getInstance(domain);
        state = executive.getState();
        memory = executive.getWorkingMemory();
        init = new InitialObjects(domain, executive);
        goalMemory = executive.getGoalMemory();
        strategies = new Strategies(executive);
    }


    public static PlanningDomain loadDomain() {
        PlanningDomain.Options domainOptions = new PlanningDomain.Options();
        return new PlanningDomain(domainOptions);
    }

    static class Options {
    }


    static class InitialObjects extends InitObjectsHelper<PlanningDomain, Executive> {
        TestExecutiveHelper parent;
        List<Statement> initStatements;

        protected InitialObjects(PlanningDomain domain, Executive executive) {
            super(domain, executive);
        }
    }
}