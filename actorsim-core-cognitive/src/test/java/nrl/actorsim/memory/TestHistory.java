package nrl.actorsim.memory;

import org.testng.annotations.Test;

import java.time.Instant;

public class TestHistory {

    @Test
    public void testHistoryDirectly() throws InterruptedException {
        History<Integer> history = new History<>();
        Instant realTimeOne = Instant.now();
        double simTimeOne = 1;
        history.inserted(1, simTimeOne);
        history.inserted(2, simTimeOne);
        history.inserted(3, simTimeOne);

        assert history.eventsAt(simTimeOne).size() == 3;

        Thread.sleep(1);
        Instant realTimeTwo = Instant.now();
        double simTimeTwo = 2;
        history.deleted(2, simTimeTwo);
        assert history.eventsAt(simTimeOne).size() == 3;
        assert history.eventsAt(simTimeTwo).size() == 1;

        Thread.sleep(1);
        Instant realTimeThree = Instant.now();
        double simTimeThree = 3;
        history.updated(1, simTimeThree);
        history.updated(3, simTimeThree);
        assert history.eventsAt(simTimeOne).size() == 3;
        assert history.eventsAt(simTimeTwo).size() == 1;
        assert history.eventsAt(simTimeThree).size() == 2;

        Thread.sleep(1);
        Instant realTimeFour = Instant.now();
        double simTimeFour = 4;

        assert history.eventsAtOrSince(simTimeOne).size() == 6;
        assert history.eventsAtOrSince(realTimeOne).size() == 6;
        assert history.eventsAtOrSince(simTimeTwo).size() == 3;
        assert history.eventsAtOrSince(realTimeTwo).size() == 3;
        assert history.eventsAtOrSince(simTimeThree).size() == 2;
        assert history.eventsAtOrSince(realTimeThree).size() == 2;
        assert history.eventsAtOrSince(simTimeFour).size() == 0;
        assert history.eventsAtOrSince(realTimeFour).size() == 0;

        assert history.insertedAtOrSince(simTimeOne).size() == 3;
        assert history.insertedAtOrSince(simTimeTwo).size() == 0;

        assert history.eventsBefore(simTimeThree).size() == 4;
        assert history.eventsBefore(realTimeThree).size() == 4;
    }

}
