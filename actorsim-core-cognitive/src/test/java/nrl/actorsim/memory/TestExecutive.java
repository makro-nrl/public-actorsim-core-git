package nrl.actorsim.memory;

import nrl.actorsim.domain.*;
import org.testng.annotations.Test;

import static nrl.actorsim.domain.WorldType.BOOLEAN;
import static nrl.actorsim.domain.test.TestItemExistsDomain.A;
import static nrl.actorsim.domain.test.TestItemExistsDomain.ITEM;

public class TestExecutive {

    @Test
    public void testDefaultAddsNewUnknownObjectToMemory() {
        TestExecutiveHelper t = new TestExecutiveHelper();

        StateVariable EXISTS_ITEM = StateVariable.builder()
            .name("exists").argTypes(ITEM).valueTypes(BOOLEAN).build();

        WorldObject itemA = A;
        StateVariable existsA = EXISTS_ITEM.copier().build(itemA);
        BooleanPersistenceStatement statement1 = new BooleanPersistenceStatement(existsA);

        t.memory.assertMissing(A);
        t.memory.assertMissing(statement1);

        t.state.add(statement1);
        t.executive.notifyWorkerAndWaitOnCycle();

        t.memory.assertContains(A);
        t.memory.assertContains(statement1);
    }


    @Test
    public void testFailForAddingUnknownObject() {
        TestExecutiveHelper t = new TestExecutiveHelper();

        StateVariable EXISTS_ITEM = StateVariable.builder()
            .name("exists").argTypes(ITEM).valueTypes(BOOLEAN).build();

        WorldObject itemA = A;
        StateVariable existsA = EXISTS_ITEM.copier().build(itemA);
        BooleanPersistenceStatement statement1 = new BooleanPersistenceStatement(existsA);

        t.memory.assertMissing(A);
        t.memory.assertMissing(statement1);

        t.state.getOptions().disableAddingUnknownObjectsInStatements();

        t.state.add(statement1);
        t.executive.notifyWorkerAndWaitOnCycle();

        t.memory.assertMissing(A);
        t.memory.assertMissing(statement1);
    }



}
