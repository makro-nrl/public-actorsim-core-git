package nrl.actorsim.domain;

import nrl.actorsim.cognitive.ExecutiveBase;
import nrl.actorsim.goalnetwork.GoalLifecycleNode;
import nrl.actorsim.goalnetwork.test.StrategyThreadHelper;
import nrl.actorsim.memory.GoalMemorySimple;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class InitObjectsHelper<D extends PlanningDomain, E extends ExecutiveBase<D>> {
    final protected D domain;
    final protected PlanningProblem problem;
    final protected E executive;

    protected InitObjectsHelper(D domain, E executive) {
        this.domain = domain;
        problem = domain.getProblem();
        this.executive = executive;
    }

    public D getDomain() {
        return domain;
    }

    public List<Statement> createInitStatements() {
        return Collections.emptyList();
    }

    public void insertInitialState() {
        insertInitialState(Collections.emptyList());
    }

    public void insertInitialState(String... excludedNames) {
        insertInitialState(Arrays.asList(excludedNames));
    }

    public void insertInitialState(List<String> excludedNames) {
        insertObjects();
        List<Statement> initStatements = createInitStatements();
        for (Statement statement : initStatements) {
            boolean include = true;
            String statementString = statement.toString();
            for (String excluded : excludedNames) {
                if (statementString.contains(excluded)) {
                    include = false;
                    break;
                }
            }
            if (include) {
                executive.add(statement);
            }
        }
    }

    private void insertObjects() {
        if (problem != null) {
            for (WorldObject object : problem.getObjects()) {
                executive.add(object);
            }
        }
    }

    public void insertAndSelect(GoalLifecycleNode goal) throws InterruptedException {
        GoalMemorySimple goalMemory = executive.getGoalMemory();
        StrategyThreadHelper.formulate(goal, goalMemory);
        StrategyThreadHelper.select(goalMemory.findFirst(goal));
    }
}

