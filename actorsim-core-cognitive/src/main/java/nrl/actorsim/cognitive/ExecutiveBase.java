package nrl.actorsim.cognitive;

import nrl.actorsim.domain.PlanningDomain;
import nrl.actorsim.domain.Statement;
import nrl.actorsim.domain.WorldObject;
import nrl.actorsim.goalnetwork.GoalLifecycleNode;
import nrl.actorsim.goalrefinement.lifecycle.StrategyGroup;
import nrl.actorsim.memory.GoalMemoryOptions;
import nrl.actorsim.memory.GoalMemorySimple;
import nrl.actorsim.memory.WorkingMemory;
import nrl.actorsim.planner.PlannerWorkerBase;
import nrl.actorsim.utils.NamedImpl;

import java.time.*;
import java.util.*;
import java.util.concurrent.*;
import java.util.function.Predicate;

public class ExecutiveBase<D extends PlanningDomain> extends NamedImpl {
    final static private org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(ExecutiveBase.class);

    protected WorkingMemory memory;

    protected ExecutiveWorker worker;
    protected ExecutiveState state;

    protected PlannerWorkerBase planner;
    protected D domain;

    // ====================================================
    // region<Constructors, Object, and Generic Accessors>
    protected ExecutiveBase(String executiveBaseName, D domain) {
        init(executiveBaseName, domain);
    }

    public void setPlanner(PlannerWorkerBase<?> planner) {
        this.planner = planner;
    }

    public void start() {
        if (planner != null) {
            planner.start();
        }
        if (worker != null) {
            worker.start();
        }
    }

    public void startShutdown() {
        if (planner != null) {
            planner.startShutdown();
        }
        if (worker != null) {
            worker.startShutdown();
        }
    }

    private void init(String executiveBaseName, D domain) {
        shortName = executiveBaseName + "Exec";
        GoalMemoryOptions goalMemoryOptions = GoalMemoryOptions.builder()
                .name(executiveBaseName + "Goals")
                .build();
        WorkingMemory.Options memoryOptions = WorkingMemory.Options.builder()
                .goalMemoryOptions(goalMemoryOptions)
                .build();
        memory = new WorkingMemory(memoryOptions);
        logger.info("Initialized options to {}", memoryOptions);

        memory.getGoalMemory().addChangeListener(this::notifyWorker);

        this.domain = domain;

        ExecutiveState.Options stateOptions = new ExecutiveState.Options();
        state = new ExecutiveState(this, stateOptions);

        RankStrategiesInterface ranker = new RankStrategiesByGoalOrdering(memory);
        CognitiveCycleWorker.Options workerOptions = CognitiveCycleWorker.Options.builder()
                .workingMemory(memory)
                .memoryUpdateLock(state)
                .ranker(ranker)
                .shortName(executiveBaseName + "Worker")
                .build();
        worker = new ExecutiveWorker(workerOptions);

		logger.info("{}: Initialized", shortName);
    }

    public WorkingMemory getWorkingMemory() {
        return memory;
    }

    public GoalMemorySimple getGoalMemory() {
        return getWorkingMemory().getGoalMemory();
    }

    @SuppressWarnings("unused")
    public void addStrategies(StrategyGroup strategies) {
        strategies.addStrategies(memory);
    }

    // endregion
    // ====================================================

    // ====================================================
    // region<Executive Worker>
    public class ExecutiveWorker extends CognitiveCycleWorker {
        public ExecutiveWorker(CognitiveCycleWorker.Options workerOptions) {
            super(workerOptions);
        }

        @Override
        protected void updateWorkingMemory() {
            state.updateMemoryFromCurrentState();
            state.resetIfRequestedAfterUpdate();
        }
    }

    @SuppressWarnings("unused")
    public ExecutiveWorker getWorker() {
        return worker;
    }

    /**
     * Cycles the processing thread in a loop waiting until
     * the predicate is true or the timeout has exceeded.
     *
     * Primarily used by unit testing to wait until something
     * occurs in memory.
     * @param predicate the predicate to wait on
     * @param timeout the timeout value
     */
    public void waitUntil(Duration timeout, Predicate<GoalLifecycleNode> predicate)
            throws InterruptedException {
        Instant start = Instant.now();
        String threadName = Thread.currentThread().getName();
        do {
            logger.debug("{}: checking memory for matching goal", threadName);
            Set<GoalLifecycleNode> matching = getGoalMemory().find(predicate);
            if (!matching.isEmpty()) {
                break;
            }
            logger.debug("{}: Sleeping for 1 second", threadName);
            //noinspection BusyWait
            Thread.sleep(1000);
            logger.debug("{}: calling notifyWorkerAndWaitOnCycle()", threadName);
            notifyWorkerAndWaitOnCycle();
        } while(withinTimeout(start, timeout));
    }

    private boolean withinTimeout(Instant start, Duration timeout) {
        Instant now = Instant.now();
        Duration diff = Duration.between(start, now);
        return diff.minus(timeout).isNegative();
    }

    public void notifyWorker() {
        worker.increaseJobCount();
        worker.notifyWorkerThereIsWork();
    }

    public void notifyWorkerAndWaitOnCycle(Duration timeout) {
        boolean notify = true;
        //noinspection ConstantConditions
        notifyAndWaitImpl(notify, timeout);
    }

    public void notifyWorkerAndWaitOnCycle() {
        boolean notify = true;
        Duration timeout = Duration.ofSeconds(0);
        //noinspection ConstantConditions
        notifyAndWaitImpl(notify, timeout);
    }

    @SuppressWarnings("unused")
    public void waitOnCycleWithoutNotify() {
        boolean notify = false;
        Duration timeout = Duration.ofSeconds(0);
        //noinspection ConstantConditions
        notifyAndWaitImpl(notify, timeout);
    }

    private void notifyAndWaitImpl(boolean notify, Duration duration) {
        CountDownLatch latch = new CountDownLatch(1);
        try {
            worker.addCycleLatch(latch);
            if (notify) {
                worker.increaseJobCount();
                worker.notifyWorkerThereIsWork();
            }
            if (latch.getCount() > 0) {
                if (duration.isZero()) {
                    latch.await();
                } else {
                    //noinspection ResultOfMethodCallIgnored
                    latch.await(duration.getSeconds(), TimeUnit.SECONDS);
                }

            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    @SuppressWarnings("unused")
    public void enableStopOnBreakPointForWorker() {
        worker.enableStopOnBreakpoint();
    }

    @SuppressWarnings("unused")
    public void disableStopOnBreakPointForWorker() {
        worker.disableStopOnBreakpoint();
    }

    @SuppressWarnings("unused")
    public void requestProgression(GoalLifecycleNode node, Runnable runnable) {
        worker.requestProgression(node, runnable);
    }

    // endregion
    // ====================================================


    // ====================================================
    // region<Planner Related>

    public void setDomain(D domain) {
        this.domain = domain;
    }

    public ExecutiveState getState() {
        return state;
    }

    public PlannerWorkerBase getPlanner() {
        return planner;
    }

    public <ReturnType extends PlanningDomain> ReturnType getDomain() {
        //noinspection unchecked
        return (ReturnType) domain;
    }

    public void add(Statement statement) {
        state.add(statement);
    }

    public void add(WorldObject object) {
        state.add(object);
    }

    // endregion
    // ====================================================

    // ====================================================
    // region<Test Assertions>

    public void assertContainsAfterStateUpdate(Statement statement) {
        state.add(statement);
        Instant start = Instant.now();
        Duration timeout = Duration.ofSeconds(5);
        notifyWorkerAndWaitOnCycle(timeout);
        Instant end = Instant.now();
        Duration diff = Duration.between(start, end);
        logger.info("Waited {} seconds for cycle to complete", diff);
        memory.assertContains(statement);
    }

    // endregion
    // ====================================================
}



