package nrl.actorsim.cognitive;

import nrl.actorsim.domain.*;
import nrl.actorsim.memory.WorkingMemory;

import java.util.*;

/**
 * Provides a linkage between the states in the simulator or platform
 * to the data structures used by ActorSim.
 */
public class ExecutiveState {
    final static org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(ExecutiveState.class);

    private final ExecutiveBase<?> executive;

    @SuppressWarnings({"FieldCanBeLocal", "unused"})
    private final Options options;

    final Set<WorldObject> observed;
    final Set<Statement> observedStatements;
    private double simulationTime;

    public ExecutiveState(ExecutiveBase<?> executive, Options options) {
        this.executive = executive;
        this.options = options;
        this.observed = new HashSet<>();
        this.observedStatements = new HashSet<>();
    }

    @SuppressWarnings("unused")
    void setSimulationTime(double time) {
        synchronized (this) {
            simulationTime = time;
        }
    }

    @SuppressWarnings("unused")
    public double getSimulationTime() {
        return simulationTime;
    }

    public void add(WorldObject object) {
        synchronized (this) {
            observed.add(object);
        }
    }

    public void add(Statement statement) {
        synchronized (this) {
            observedStatements.add(statement);
        }
    }

    void updateMemoryFromCurrentState() {
        executive.getWorkingMemory().setSimulationTime(simulationTime);
        for(WorldObject object : observed) {
            insertObject(object);
        }
        for(Statement statement : observedStatements) {
            for (Binding binding : statement.findBindings()) {
                if (binding.getTarget() instanceof WorldObject) {
                    insertObject(binding.getTarget().content());
                }
            }
            executive.getWorkingMemory().updateOrCloneThenAdd(statement, simulationTime);
        }
    }

    public void resetIfRequestedAfterUpdate() {
        if (options.resetAfterUpdate) {
            observed.clear();
            observedStatements.clear();
        }
    }

    private void insertObject(WorldObject object) {
        WorkingMemory memory = executive.getWorkingMemory();
        if (options.addUnknownObjects) {
            //NB: no need to check whether the object is missing from memory
            memory.updateOrCloneThenAdd(object, simulationTime);
        } else {
            if (memory.contains(object)) {
                memory.updateOrCloneThenAdd(object, simulationTime);
            } else {
                String message = String.format("The object %s is missing from memory and cannot be added", object);
                logger.error(message);
                throw new IllegalArgumentException(message);
            }
        }
    }

    public Options getOptions() {
        return options;
    }

    public static class Options {
        boolean addUnknownObjects = true;
        boolean resetAfterUpdate = true;

        public void enableAddingUnknownObjectsInStatements() {
            addUnknownObjects = true;
        }

        public void disableAddingUnknownObjectsInStatements() {
            addUnknownObjects = false;
        }
    }
}
