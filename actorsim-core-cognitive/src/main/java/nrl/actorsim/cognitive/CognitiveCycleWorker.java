package nrl.actorsim.cognitive;

import nrl.actorsim.chronicle.GoalOrdering;
import nrl.actorsim.goalnetwork.GoalLifecycleNode;
import nrl.actorsim.goalrefinement.strategies.ApplyStrategyDetails;
import nrl.actorsim.goalrefinement.strategies.StrategyTemplateClassSpecific;
import nrl.actorsim.goalrefinement.strategies.RequestedProgressionStrategy;
import nrl.actorsim.goalrefinement.strategies.StrategyThread;
import nrl.actorsim.memory.*;
import nrl.actorsim.utils.Named;

import java.util.*;
import java.util.concurrent.CountDownLatch;


/**
 * An abstract, threaded class that runs the cognitive cycle on the working memory.
 * <p>
 *
 * The worker runs a loop that performs the following in order:
 * <code>
 * while(notShuttingDown()) {
 *   synchronized(workerLock) {  //an optional lock that clients may use
 *       workerLock.wait();  //use notifyWorkerThereIsWork() to release
 *   }
 *
 *   synchronized(memoryLock) {  //an optional lock that clients may use
 *     updateWorkingMemory();
 *   }
 *   insertApplicableStrategies()
 *   rankApplicableStrategies()
 *   filterApplicableStrategies()
 *   applyStrategies()
 * }
 * </code>
 * <p>
 * Subclasses may override the behavior to specialize any of these methods,
 * but the only required method is updateWorkingMemory().
 * <p>
 * So that subclasses can synchronize with the worker thread, the <code>workerLock</code>
 * and <code>memoryLock</code> objects can be provided via the Options.Builder.
 * If either lock is missing then Options.Builder provides default lock objects.
 */
@SuppressWarnings("unused")
public abstract class CognitiveCycleWorker extends Thread implements StrategyThread, Named {
    final static org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(CognitiveCycleWorker.class);
    private boolean isShuttingDown;
    private final Options options;

    private final Object workerLock;
    private final Object memoryLock;

    private final List<CountDownLatch> cycleLatches;
    private int completedCycles;

    protected final List<RequestedProgressionStrategy> requestedProgressions
            = Collections.synchronizedList(new ArrayList<>());

    public CognitiveCycleWorker(Options options) {
        super();
        this.options = options;
        this.setName("Thread-" + getShortName());

        if (options.tmpWorkerLock == null) {
            workerLock = new Object();
        } else {
            workerLock = options.tmpWorkerLock;
        }

        if (options.tmpMemoryLock == null) {
            memoryLock = new Object();
        } else {
            memoryLock = options.tmpMemoryLock;
        }

        cycleLatches = Collections.synchronizedList(new ArrayList<>());
    }

    public boolean isShuttingDown() {
        return isShuttingDown;
    }

    public boolean notShuttingDown() {
        return ! isShuttingDown;
    }

    public void startShutdown() {
        isShuttingDown = true;
        notifyWorkerThereIsWork();
    }

    @Override
    public String getShortName() {
        return options.shortName;
    }

    Integer jobCount = 0;

    public void increaseJobCount() {
        synchronized (jobCount) {
            jobCount++;
        }
    }

    public void notifyWorkerThereIsWork() {
        synchronized (workerLock) {
            workerLock.notify();
        }
    }

    @Override
    public void run() {
        while (notShuttingDown()) {
            try {
                logger.debug("=========================================================================");
                logger.debug("");
                logger.debug("=========================================================================");
                logger.debug("");
                logger.debug("=========================================================================");
                logger.debug("");
                logger.debug("=========================================================================");
                logger.debug("");

                boolean waitForWork = true;

                synchronized (jobCount) {
                    if (jobCount > 0) {
                        jobCount--;
                        waitForWork = false;
                        logger.debug("==================");
                        logger.debug("{}: Jobs remain, continuing to work...", options.shortName);
                        logger.debug("==================");
                    }
                }
                if (waitForWork) {
                    synchronized (workerLock) {
                        logger.debug("==================");
                        logger.debug("{}: Waiting on changes...", options.shortName);
                        logger.debug("==================");
                        workerLock.wait();
                    }
                }

                if (isShuttingDown()) {
                    continue;
                }

                synchronized (memoryLock) {
                    logger.debug("=========================================================================");
                    logger.debug("{}: notified of new changes, updating memory...", options.shortName);
                    logger.debug("=========================================================================");
                    updateWorkingMemory();
                    int totalApplied = 0;
                    int iteration = 0;
                    boolean finalIteration;
                    boolean onlyApplyEndOfCycleStrategies = false;
                    options.strategyRanker.updateAtStartOfCognitiveCycle(options.memory);

                    List<ApplyStrategyDetails> filtered = Collections.emptyList();
                    while (totalApplied < options.memory.getOptions().getApplyDetails().maxApplications) {
                        logger.debug("---------------------------------------------------------");
                        logger.debug("Iteration:{} - Determining and applying strategies", iteration);
                        List<ApplyStrategyDetails> applicable = new ArrayList<>();

                        if (options.allowRequestedProgressions) {
                            insertRequestedProgressions(applicable);
                        }

                        if (options.reuseFilteredStrategies) {
                            insertPreviouslyFilteredStrategies(applicable, filtered);
                        }

                        finalIteration = insertApplicableStrategies(applicable, onlyApplyEndOfCycleStrategies);
                        options.strategyRanker.updateDuringEachCognitiveCycleIteration(options.memory);
                        rankApplicableStrategies(applicable);
                        filtered = filterApplicableStrategies(applicable);
                        int appliedThisCycle = applyStrategies(applicable);
                        totalApplied += appliedThisCycle;

                        if (finalIteration
                                || onlyApplyEndOfCycleStrategies) {
                            logger.debug("Iteration:{} - The cycle's end strategies were applied, stopping this cycle", iteration);
                            break;
                        }

                        if (appliedThisCycle == 0) {
                            logger.debug("Iteration:{} - No more strategies could be applied", iteration);
                            onlyApplyEndOfCycleStrategies = true;
                        }

                        iteration++;
                    }
                }
            } catch (InterruptedException ex) {
                if (isShuttingDown()) {
                    logger.info("{}: shutting down worker thread", options.shortName);
                } else {
                    logger.warn("{}: event processor caught exception {}", options.shortName, ex.getCause());
                }
            } catch (Exception e) {
                logger.error("{}: exception thrown during the process method.  Continuing...", options.shortName, e);
           } finally {
                completedCycles++;
                notifyCycleLatches();
            }
        }
    }

    public void addCycleLatch(CountDownLatch latch) {
        synchronized (cycleLatches) {
            cycleLatches.add(latch);
        }
    }

    protected void notifyCycleLatches() {
        synchronized (cycleLatches) {
            for (Iterator<CountDownLatch> it = cycleLatches.iterator(); it.hasNext(); ) {
                CountDownLatch latch = it.next();
                latch.countDown();
                if (latch.getCount() == 0) {
                    it.remove();
                }
            }
        }
    }

    public int getCompletedCycles() {
        return completedCycles;
    }

    protected void printApplicableStrategiesAtDebug(List<ApplyStrategyDetails> applicable) {
        ApplyStrategyDetails.LogCompressor logCompressor = new ApplyStrategyDetails.LogCompressor();
        for (ApplyStrategyDetails details : applicable) {
            logCompressor.logAtDebug(logger, details);
        }
        logCompressor.printAndClear(logger);
    }

    protected void insertRequestedProgressions(List<ApplyStrategyDetails> applicable) {
        synchronized (requestedProgressions) {
            Iterator<RequestedProgressionStrategy> iter = requestedProgressions.iterator();
            while (iter.hasNext()) {
                RequestedProgressionStrategy requested = iter.next();
                logger.debug("Inserting requested progression: {}", requested);
                applicable.add(new ApplyStrategyDetails(requested, requested.node));
                iter.remove();
            }
        }
    }

    protected boolean insertApplicableStrategies(List<ApplyStrategyDetails> applicable, boolean onlyApplyEndOfCycleStrategies) {
        logger.debug("----- Determining applicable strategies.");
        boolean finalIteration = options.memory.insertApplicableStrategies(applicable, onlyApplyEndOfCycleStrategies);
        logger.debug("Found {} applicable strategies finalIteration:{}", applicable.size(), finalIteration);
        if (logger.isDebugEnabled()) {
            printApplicableStrategiesAtDebug(applicable);
        }
        return finalIteration;
    }

    protected void insertPreviouslyFilteredStrategies(List<ApplyStrategyDetails> applicable, List<ApplyStrategyDetails> previouslyFiltered) {
        if (previouslyFiltered.size() > 0) {
            logger.debug("Inserting {} previously filtered strategies", previouslyFiltered.size());
            applicable.addAll(previouslyFiltered);
            if (logger.isDebugEnabled()) {
                printApplicableStrategiesAtDebug(applicable);
            }
        }
    }

    protected void rankApplicableStrategies(List<ApplyStrategyDetails> applicable) {
        applicable.sort(options.strategyRanker);
        if (logger.isDebugEnabled()) {
            logger.debug("----- Strategies after sorting: ");
            printApplicableStrategiesAtDebug(applicable);
        }
    }

    protected List<ApplyStrategyDetails> filterApplicableStrategies(List<ApplyStrategyDetails> applicable) {
        List<ApplyStrategyDetails> filtered = new ArrayList<>();
        GoalMemorySimple goalMemory = options.memory.getGoalMemory();
        Map<String, Set<String>> precendeceMap = goalMemory.getGoalOrderingPrecedenceMap();
        Set<String> successors = new TreeSet<>();
        Iterator<ApplyStrategyDetails> iter = applicable.iterator();
        while (iter.hasNext()) {
            ApplyStrategyDetails details = iter.next();
            if (details.strategy instanceof StrategyTemplateClassSpecific) {
                Class<? extends GoalLifecycleNode> lhsClass = ((StrategyTemplateClassSpecific) details.strategy).getMatchingClass();
                String lhsKey = GoalOrdering.getKey(lhsClass);
                if (successors.contains(lhsKey)) {
                    iter.remove();
                    filtered.add(details);
                } else {
                    if (precendeceMap.containsKey(lhsKey)) {
                        successors.addAll(precendeceMap.get(lhsKey));
                    }
                }
            }
        }

        if (logger.isDebugEnabled()) {
            logger.debug("----- Strategies after filtering: ");
            printApplicableStrategiesAtDebug(applicable);
        }
        return filtered;
    }

    protected int applyStrategies(List<ApplyStrategyDetails> applicable) {
        logger.debug("----- Applying {} strategies.", applicable.size());
        int totalApplied = 0;
        int suppressedCount = 0;
        Set<String> suppressedStrategyNames = new TreeSet<>();
        for (ApplyStrategyDetails applyDetails : applicable) {
            try {
                if (applyDetails.strategy.logApply()) {
                    logger.debug("\\\\\\\\\\ START attempt {}", applyDetails.toStringApplicable());
                } else {
                    suppressedStrategyNames.add(applyDetails.strategy.getShortName());
                    suppressedCount++;
                }
                applyDetails.strategy.attemptApply(options.memory, applyDetails);
                totalApplied += applyDetails.attemptedApplications;
                if (applyDetails.strategy.logApply()) {
                    logger.debug("///// END attempt #{} of {}", applyDetails.attemptedApplications, applyDetails.toStringApplicable());
                }
            } catch (Exception e) {
                logger.error("{}: exception thrown while attempting to apply strategy.  Continuing...", options.shortName, e);
            }
        }
        if (suppressedCount > 0) {
            logger.debug("    (Suppressed {} apply details for: {})", suppressedCount, suppressedStrategyNames);
        }
        return totalApplied;
    }

    protected abstract void updateWorkingMemory();

    public void enableStopOnBreakpoint() {
        options.stopOnBreakPoint = true;
    }

    public void disableStopOnBreakpoint() {
        options.stopOnBreakPoint = false;
    }


    public void requestProgression(GoalLifecycleNode node, Runnable runnable) {
        if (options.allowRequestedProgressions) {
            RequestedProgressionStrategy strategy = new RequestedProgressionStrategy(node, runnable);
            synchronized (requestedProgressions) {
                requestedProgressions.add(strategy);
            }
            notifyWorkerThereIsWork();
        } else {
            String msg = "Requested progressions are disabled";
            logger.error(msg);
            throw new UnsupportedOperationException(msg);
        }
    }


    public static class Options {
        final boolean reuseFilteredStrategies = true;
        private String shortName;

        Object tmpWorkerLock;
        Object tmpMemoryLock;
        WorkingMemory memory;
        RankStrategiesInterface strategyRanker;
        private boolean stopOnBreakPoint = false;  // a debugging helper

        public boolean allowRequestedProgressions = true;

        private Options() {
        }

        public static Builder builder() {
            return new Builder();
        }

        public static class Builder {
            Options instance;

            Builder() {
                instance = new Options();
            }

            public Builder shortName(String shortName) {
                instance.shortName = shortName;
                return this;
            }

            public Builder workerLock(Object workerLock) {
                instance.tmpWorkerLock = workerLock;
                return this;
            }

            public Builder memoryUpdateLock(Object memoryLock) {
                instance.tmpMemoryLock = memoryLock;
                return this;
            }

            public Builder workingMemory(WorkingMemory memory) {
                instance.memory = memory;
                return this;
            }

            public Builder ranker(RankStrategiesInterface ranker) {
                instance.strategyRanker = ranker;
                return this;
            }

            public Options build() {
                return instance;
            }
        }
    }


}
