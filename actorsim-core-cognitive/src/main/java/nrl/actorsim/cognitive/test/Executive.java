package nrl.actorsim.cognitive.test;

import nrl.actorsim.cognitive.ExecutiveBase;
import nrl.actorsim.domain.PlanningDomain;

public class Executive extends ExecutiveBase<PlanningDomain> {
    final static org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(Executive.class);

    private static Executive instance;

    public static void reset() {
        instance = null;
    }

    @SuppressWarnings("unused")
    public static Executive getInstance() {
        if (instance == null) {
            PlanningDomain.Options domainOptions = PlanningDomain.Options.builder()
                .name("test")
                .closedWorldAssumption()
                .build();
            getInstance(new PlanningDomain(domainOptions));
        }
        return instance;
    }

    public static Executive getInstance(PlanningDomain domain) {
        instance = new Executive(domain);
        instance.start();
        return instance;
    }

    private Executive(PlanningDomain domain) {
        super("TestExec", domain);
    }
}
