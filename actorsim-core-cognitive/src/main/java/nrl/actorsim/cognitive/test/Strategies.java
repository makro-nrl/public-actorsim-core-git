package nrl.actorsim.cognitive.test;

import nrl.actorsim.goalrefinement.lifecycle.StrategyGroup;
import nrl.actorsim.domain.PlanningDomain;
import nrl.actorsim.domain.StateVariable;
import nrl.actorsim.planner.PlanRequestWithMemory;

import java.util.Map;
import java.util.TreeMap;

public class Strategies extends StrategyGroup {
    final static org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(Strategies.class);

    final PlanningDomain domain;
    private final Executive executive;
    private final Map<StateVariable, PlanRequestWithMemory> expanding
            = new TreeMap<>();

    public Strategies(Executive executive) {
        super();
        this.executive = executive;
        this.domain = executive.getDomain();
    }
}
