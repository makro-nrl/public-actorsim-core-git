package nrl.actorsim.cognitive;

import nrl.actorsim.chronicle.GoalOrdering;
import nrl.actorsim.goalnetwork.GoalLifecycleNode;
import nrl.actorsim.goalrefinement.strategies.ApplyStrategyDetails;
import nrl.actorsim.goalrefinement.strategies.StrategyTemplate;
import nrl.actorsim.goalrefinement.strategies.StrategyTemplateClassSpecific;
import nrl.actorsim.memory.GoalMemorySimple;
import nrl.actorsim.memory.WorkingMemory;

import java.util.Map;
import java.util.Set;

public class RankStrategiesByGoalOrdering implements RankStrategiesInterface {
    final private GoalMemorySimple goalMemory;

    public RankStrategiesByGoalOrdering(WorkingMemory memory) {
        goalMemory = memory.getGoalMemory();
    }

    @Override
    public int compare(ApplyStrategyDetails lhsDetails, ApplyStrategyDetails rhsDetails) {
        Map<String, Set<String>> precedenceMap = goalMemory.getGoalOrderingPrecedenceMap();
        StrategyTemplate lhsStrategy = lhsDetails.strategy;
        StrategyTemplate rhsStrategy = rhsDetails.strategy;
        if (lhsStrategy instanceof StrategyTemplateClassSpecific
                && rhsStrategy instanceof StrategyTemplateClassSpecific) {
            Class<? extends GoalLifecycleNode> lhsClass = ((StrategyTemplateClassSpecific) lhsStrategy).getMatchingClass();
            String lhsKey = GoalOrdering.getKey(lhsClass);
            Class<? extends GoalLifecycleNode> rhsClass = ((StrategyTemplateClassSpecific) rhsStrategy).getMatchingClass();
            String rhsKey = GoalOrdering.getKey(rhsClass);

            if (goalMemory.getGoalOrderingPrecedenceMap().containsKey(lhsKey)) {
                if (precedenceMap.get(lhsKey).contains(rhsKey)) {
                    // lhs < rhs; return < 0
                    return -1;
                }
            }
            if (precedenceMap.containsKey(rhsKey)) {
                if (goalMemory.getGoalOrderingPrecedenceMap().get(rhsKey).contains(lhsKey)) {
                    // lhs > rhs, return > 0
                    return 1;
                }
            }
            // there is no ordering specified, so assume lhs == rhs, return 0
            return 0;
        } else if (lhsStrategy instanceof StrategyTemplateClassSpecific) {
            // lhs expresses a preference for being earlier, so assume lhs < rhs and return < 0
            return -1;
        } else if (rhsStrategy instanceof StrategyTemplateClassSpecific) {
            // rhs has expressed a preference for being earlier, so assume lhs > rhs and return > 0
            return 1;
        } else {
            // neither lhs nor rhs have expressed a preference, so make them equal
            return 0;
        }
    }
}
