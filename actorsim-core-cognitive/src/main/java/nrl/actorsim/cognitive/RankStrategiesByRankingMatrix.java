package nrl.actorsim.cognitive;

import nrl.actorsim.goalrefinement.strategies.ApplyStrategyDetails;
import nrl.actorsim.memory.WorkingMemory;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;


@SuppressWarnings("rawtypes")
public class RankStrategiesByRankingMatrix implements RankStrategiesInterface {
    final List<Class> itemsToRank;
    final protected WorkingMemory memory;
    final List<RankingFunction> rankingFunctions = new ArrayList<>();
    List<RankingFunction> orderedRankingFunctions = new ArrayList<>();

    public RankStrategiesByRankingMatrix(WorkingMemory memory, List<Class> itemsToRank) {
        this.memory = memory;
        this.itemsToRank = itemsToRank;
    }

    public void add(RankingFunction function) {
        rankingFunctions.add(function);
    }

    public void addAll(Collection<RankingFunction> functions) {
        rankingFunctions.addAll(functions);
    }

    public List<RankingFunction> getRankingFunctions() {
        return rankingFunctions;
    }

    @Override
    public int compare(ApplyStrategyDetails lhsDetails, ApplyStrategyDetails rhsDetails) {
        //eventually should return an ordering using the orderedRankingFunctions member
        return 0;
    }

    @Override
    public void updateDuringEachCognitiveCycleIteration(WorkingMemory memory) {
        for (RankingFunction function : rankingFunctions) {
            function.updateDuringEachCognitiveCycleIteration(memory);
        }
        //eventually should update ranking matrix and create a new
        // orderedRankingFunctions if needed
    }


    @Override
    public void updateAtStartOfCognitiveCycle(WorkingMemory memory) {
        for (RankingFunction function : rankingFunctions) {
            function.updateAtStartOfCognitiveCycle(memory);
        }
        //possibly update ranking matrix and create a
        // new orderedRankingFunctions if needed (probably overkill here)
    }
}
