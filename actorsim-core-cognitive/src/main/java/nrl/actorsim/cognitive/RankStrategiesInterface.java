package nrl.actorsim.cognitive;

import nrl.actorsim.goalrefinement.strategies.ApplyStrategyDetails;
import nrl.actorsim.memory.WorkingMemory;

import java.util.Comparator;

public interface RankStrategiesInterface extends Comparator<ApplyStrategyDetails> {
    default void updateAtStartOfCognitiveCycle(WorkingMemory memory) {

    }

    default void updateDuringEachCognitiveCycleIteration(WorkingMemory memory) {

    }

}
