# ActorSim Core Library

This repository contains the core library for ActorSim.

For complete details on installing and using this library, see 
[src/site/README.md](src/site/README.md).
