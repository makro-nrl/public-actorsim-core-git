package nrl.actorsim.strategies;

import nrl.actorsim.goalrefinement.lifecycle.StrategyName;
import nrl.actorsim.goalrefinement.lifecycle.StrategyGroup;
import nrl.actorsim.domain.*;
import nrl.actorsim.goalnetwork.GoalLifecycleNode;
import nrl.actorsim.goalrefinement.strategies.*;
import nrl.actorsim.goals.RequestGoal;
import nrl.actorsim.memory.WorkingMemory;
import nrl.actorsim.planner.PlannerWorkerBase;
import nrl.actorsim.planner.PlanRequestWithMemory;

import java.util.*;
import java.util.function.Predicate;

import static nrl.actorsim.goalrefinement.lifecycle.GoalMode.*;
import static nrl.actorsim.goalrefinement.lifecycle.StrategyName.*;

public class PlanRequestStrategies<G extends RequestGoal> extends StrategyGroup {
    final static org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(PlanRequestStrategies.class);

    public PlanRequestStrategies(G goal, PlannerWorkerBase<PlanRequestWithMemory> planner) {
        this(goal, planner, FORMULATE, SELECT, EXPAND, COMMIT, DISPATCH, EVALUATE, FINISH);
    }

    public PlanRequestStrategies(G goal, PlannerWorkerBase<PlanRequestWithMemory> planner, StrategyName... strategies) {
        super();
        List<StrategyName> strategiesList = Arrays.asList(strategies);

        if(strategiesList.contains(FORMULATE)) {
            add(new FormulateOnConditionStrategy<>(goal));
        }
        if(strategiesList.contains(SELECT)) {
            add(new AlwaysSelectStrategy(goal));
        }
        if(strategiesList.contains(EXPAND)) {
            add(new Expand<>(goal, planner));
        }
        if (strategiesList.contains(COMMIT)) {
            add(new Commit<>(goal));
        }
        if (strategiesList.contains(DISPATCH)) {
            add(new Dispatch<>(goal));
        }
        if (strategiesList.contains(FINISH)) {
            add(new AlwaysFinishStrategy(goal));
        }
    }

    public static class Expand<G extends RequestGoal> extends StrategyTemplateFilterByGoalClassAndMode {
        PlannerWorkerBase<PlanRequestWithMemory> planner;

        public Expand(G goal, PlannerWorkerBase<PlanRequestWithMemory> planner) {
            super(goal.getClass(), SELECTED, EXPANDING);
            this.planner = planner;
        }

        @Override
        public boolean matches(GoalLifecycleNode goalNode) {
            if (super.matches(goalNode)) {
                boolean hasValidPlannder = (planner != null);
                boolean needsRequest = ((RequestGoal) goalNode).needsRequest();
                boolean hasRequest = ((RequestGoal) goalNode).hasRequest();
                return hasValidPlannder && (needsRequest | hasRequest);
            }
            return false;
        }

        @Override
        public void attemptApply(WorkingMemory memory, ApplyStrategyDetails applyDetails) {
            G goal = applyDetails.getGoal();
            if (matches(goal)
                    && goal.needsRequest()) {
                goal.expanding();
                if (reusedRequestFromExistingGoal(memory, goal)) {
                    logger.debug("Reused existing request");
                } else {
                    createAndEnqueueRequest(memory, goal);
                }
            } else if (goal.requestFinished()) {
                goal.expand();
            }
        }

        private boolean reusedRequestFromExistingGoal(WorkingMemory memory, G goal) {
            if (planner.getPlannerOptions().getPlanForAllGoalsInMemory()) {
                StateVariable sv = goal.getStored().getTarget();
                G node = memory.getGoalMemory().findFirst((Predicate<G>) goalToTest -> {
                    StateVariable testTarget = goalToTest.getStored().getTarget();
                    return goalToTest.hasDifferentId(goal)
                            && testTarget.nameEquals(sv);
                });
                if (node != null
                        && node.hasRequest()) {
                    goal.setRequest(node.getRequest());
                    return true;
                }
            }
            return false;
        }

        private void createAndEnqueueRequest(WorkingMemory memory, G goal) {
            PlanRequestWithMemory request = planner.getRequestWithMemory(memory);
            if(planner.getPlannerOptions().getPlanIndividualGoals()) {
                Statement statement = goal.getStored();
                request.add(statement);
            }
            request.generateDomainAndProblem();
            goal.setRequest(request);
            planner.enqueue(request);
        }

    }

    public static class Commit<G extends RequestGoal> extends StrategyTemplateFilterByGoalClassAndMode {
        public Commit(G goal) {
            super(goal.getClass(), EXPANDED);
        }

        @Override
        public boolean matches(GoalLifecycleNode goalNode) {
            return super.matches(goalNode)
                    && ((RequestGoal)goalNode).requestFinished();
        }

        @Override
        public void attemptApply(WorkingMemory memory, ApplyStrategyDetails applyDetails) {
            G goal = applyDetails.getGoal();
            if(goal.insertPlanStepNodes()) {
                goal.commit();
            }
        }
    }

    public static class Dispatch<G extends RequestGoal> extends StrategyTemplateFilterByGoalClassAndMode {
        public Dispatch(G goal) {
            super(goal.getClass(), COMMITTED);
        }

        @Override
        public boolean matches(GoalLifecycleNode goalNode) {
            return super.matches(goalNode)
                    && ((RequestGoal)goalNode).requestFinished();
        }

        @Override
        public void attemptApply(WorkingMemory memory, ApplyStrategyDetails applyDetails) {
            G goal = applyDetails.getGoal();
            for (GoalLifecycleNode child : goal.getSubgoals()) {
                if (child.getStored() instanceof ExecuteStatement) {
                    child.formulate(goal.getGoalMemory());
                }
            }
            goal.dispatch();
        }
    }
}
