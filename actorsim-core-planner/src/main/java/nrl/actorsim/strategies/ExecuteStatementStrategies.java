package nrl.actorsim.strategies;

import nrl.actorsim.domain.ExecuteStatement;
import nrl.actorsim.goalnetwork.GoalLifecycleNode;
import nrl.actorsim.goals.ExecuteStatementGoal;
import nrl.actorsim.goalrefinement.strategies.*;
import nrl.actorsim.goalrefinement.lifecycle.StrategyGroup;
import nrl.actorsim.goalrefinement.lifecycle.StrategyName;
import nrl.actorsim.memory.WorkingMemory;

import java.util.*;

import static nrl.actorsim.domain.ExecuteStatement.State.COMPLETED;
import static nrl.actorsim.goalrefinement.lifecycle.GoalMode.*;
import static nrl.actorsim.goalrefinement.lifecycle.StrategyName.*;

public class ExecuteStatementStrategies extends StrategyGroup {
    final static org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(PlanRequestStrategies.class);

    public ExecuteStatementStrategies() {
        this(SELECT, EXPAND, COMMIT, DISPATCH, EVALUATE, FINISH, DROP);
    }

    public ExecuteStatementStrategies(StrategyName... strategies) {
        super();
        List<StrategyName> strategiesList = Arrays.asList(strategies);

        if(strategiesList.contains(SELECT)) {
            add(new AlwaysSelectStrategy(ExecuteStatementGoal.class));
        }
        if(strategiesList.contains(EXPAND)) {
            add(new AlwaysExpandStrategy(ExecuteStatementGoal.class));
        }
        if (strategiesList.contains(COMMIT)) {
            add(new AlwaysCommitStrategy(ExecuteStatementGoal.class));
        }
        if (strategiesList.contains(DISPATCH)) {
            add(new Dispatch<>(ExecuteStatementGoal.class));
            add(new RespondToExecutionStatusChanges());
        }
        if (strategiesList.contains(DROP)) {
            add(new DropFinishedGoalsStrategy(ExecuteStatementGoal.class) {
                @Override
                public void checkPreconditions(WorkingMemory memory, ApplyStrategyDetailsCollection applyCollection) {
                    int starting = applyCollection.size();
                    super.checkPreconditions(memory, applyCollection);
                    if (applyCollection.size() > starting) {
                        logger.debug("Found new goals!");
                    }
                }

                @Override
                public void attemptApply(WorkingMemory memory, ApplyStrategyDetails applyDetails) {
                    super.attemptApply(memory, applyDetails);
                }
            });
        }
    }

    public static class RespondToExecutionStatusChanges
            extends StrategyTemplateFilterByGoalClassAndMode {
        public RespondToExecutionStatusChanges() {
            super(ExecuteStatementGoal.class, ANY);
        }

        @Override
        public boolean matches(GoalLifecycleNode goalNode) {
            return super.matches(goalNode)
                    && ((ExecuteStatementGoal) goalNode).statementChangeOccurred();
        }

        @Override
        public void checkPreconditions(WorkingMemory memory, ApplyStrategyDetailsCollection applyCollection) {
            super.checkPreconditions(memory, applyCollection);
            if (applyCollection.applicableGoals.size() > 0) {
                logger.debug("Found goals!");
            }
        }

        @Override
        public void attemptApply(WorkingMemory memory, ApplyStrategyDetails applyDetails) {
            ExecuteStatementGoal goal = applyDetails.getGoal();
            ExecuteStatement statement = goal.content();
            if (goal.statementChangeOccurred()) {
                goal.clearStatementChangeFlag();
                if (statement.stateEquals(COMPLETED)) {
                    if (! goal.modeMatches(FINISHED)) {
                        goal.finish();
                    }
                }
            }
        }
    }

    public static class Dispatch<G extends ExecuteStatementGoal>
            extends StrategyTemplateFilterByGoalClassAndMode {
        public Dispatch(Class<G> cls) {
            super(cls, COMMITTED);
        }

        @Override
        public boolean matches(GoalLifecycleNode goalNode) {
            return super.matches(goalNode)
                    && goalNode.isUnconstrained();
        }

        @Override
        public void checkPreconditions(WorkingMemory memory, ApplyStrategyDetailsCollection applyCollection) {
            super.checkPreconditions(memory, applyCollection);
            if (applyCollection.applicableGoals.size() > 0) {
                logger.debug("Found goals!");
            }
        }

        @Override
        public void attemptApply(WorkingMemory memory, ApplyStrategyDetails applyDetails) {
            G goal = applyDetails.getGoal();
            if (matches(goal)) {
                goal.dispatch();
            }
        }
    }

}
