package nrl.actorsim.goals;

import nrl.actorsim.domain.ExecuteStatement;
import nrl.actorsim.goalnetwork.GoalLifecycleNode;
import nrl.actorsim.goalrefinement.lifecycle.StrategyStatus;

import java.time.Duration;

import static nrl.actorsim.domain.ExecuteStatement.State.COMPLETED;
import static nrl.actorsim.domain.ExecuteStatement.State.EXECUTING;

public class ExecuteStatementGoal extends GoalLifecycleNode {
    final ExecuteStatement executeStatement;

    private boolean statementChangeOccurred = false;

    public ExecuteStatementGoal(ExecuteStatement statement) {
        super(statement);
        executeStatement = statement;
        addThisAsStatementChangeListener();
        setAsPrimitive();
    }

    private void addThisAsStatementChangeListener() {
        executeStatement.addChangeListener(
                () -> statementChangeOccurred = true
        );
    }

    @Override
    public boolean isComplete() {
        return executeStatement.stateEquals(COMPLETED);
    }

    @Override
    public GoalLifecycleNode setComplete() {
        if (! executeStatement.stateEquals(COMPLETED)) {
            Duration timeout = Duration.ofSeconds(10);
            executeStatement.attemptTransitionToAndNotify(COMPLETED, timeout);
        }
        return (GoalLifecycleNode) super.setCompleteSafely();
    }

    public boolean statementChangeOccurred() {
        return statementChangeOccurred;
    }

    public void clearStatementChangeFlag() {
        statementChangeOccurred = false;
    }

    // ====================================================
    // region<Strategy Overrides>


    @Override
    protected StrategyStatus dispatchInSubclass() {
        Duration timeout = Duration.ofSeconds(10);
        executeStatement.attemptTransitionToAndNotify(EXECUTING, timeout);
        return StrategyStatus.getSuccessResult();
    }

    @Override
    protected StrategyStatus finishInSubclass() {
        setComplete();
        return StrategyStatus.getSuccessResult();
    }


    // endregion
    // ====================================================
}
