package nrl.actorsim.goals;

import nrl.actorsim.domain.ExecuteStatement;
import nrl.actorsim.domain.Statement;
import nrl.actorsim.goalnetwork.GoalLifecycleNode;
import nrl.actorsim.goalrefinement.goals.StateVariableGoal;
import nrl.actorsim.planner.PlanStep;
import nrl.actorsim.planner.PlanRequestWithMemory;

import java.util.ArrayList;
import java.util.List;

public class RequestGoal extends StateVariableGoal {
    protected PlanRequestWithMemory request;

    public RequestGoal(Statement statement, CompleteWhen completeWhen) {
        super(statement, completeWhen);
    }

    public RequestGoal(Statement statement,
                       CompleteWhen completeWhen,
                       Statement formulateCondition) {
        super(statement, completeWhen, formulateCondition);
    }

    public PlanRequestWithMemory getRequest() {
        return request;
    }

    public void setRequest(PlanRequestWithMemory request) {
        this.request = request;
        request.add(this);
    }

    /**
     * Return whether this goal needs to be planned via
     * a planning request.  By default, this method relies
     * only on lack of a request.  Subclasses can
     * specialize this behavior.
     *
     * @return whether this goal needs a request created
     */
    public boolean needsRequest() {
        return hasNoRequest();
    }

    public boolean hasRequest() {
        return request != null;
    }

    private boolean hasNoRequest() {
        return request == null;
    }

    public boolean requestFinished() {
        if (request == null) {
            return false;
        }
        return request.hasPlan();
    }

    public boolean insertPlanStepNodes() {
        if (request.convertUsingTemporalOrdering()) {
            return insertPlanStepNodesUsingTemporalOrdering_impl();
        } else if (request.convertUsingTotalOrdering()){
            return insertPlanStepNodesUsingTotalOrdering_impl();
        }
        String msg = "The conversion process must specify a valid ConversionType.";
        logger.error(msg);
        return false;
    }

    public boolean insertPlanStepNodesUsingTotalOrdering_impl() {
        if (!request.hasPlan()) {
            return false;
        }

        boolean insertedAtLeastOneStep = false;

        RequestGoal mainGoal = request.getMainGoal();
        if (mainGoal.getSubgoals().length == 0) {
            List<PlanStep> planSteps = request.getPlanSteps();
            ExecuteStatementGoal previousNode = null;
            for (PlanStep step : planSteps) {
                ExecuteStatementGoal stepNode = getGoal(step);
                goalMemory.addSubgoal(this, stepNode);
                insertedAtLeastOneStep = true;

                if (previousNode != null) {
                    goalMemory.addSubgoalOrdering(previousNode, stepNode);
                }
                previousNode = stepNode;
            }
        }
        if (request.isShared()
            && this != mainGoal) {
            for (GoalLifecycleNode child : mainGoal.getSubgoals()) {
                goalMemory.addSubgoal(this, child);
                insertedAtLeastOneStep = true;
            }
        }
        return insertedAtLeastOneStep;
    }

    private ExecuteStatementGoal getGoal(PlanStep step) {
        return new ExecuteStatementGoal(new ExecuteStatement(step));
    }

    public boolean insertPlanStepNodesUsingTemporalOrdering_impl() {
        if (!request.hasPlan()) {
            return false;
        }

        boolean insertedAtLeastOneStep = false;

        RequestGoal mainGoal = request.getMainGoal();
        if (mainGoal.getSubgoals().length == 0) {
            List<PlanStep> planSteps = request.getPlanSteps();

            double currentStart = -1;
            List<ExecuteStatementGoal> predecessors = new ArrayList<>();
            List<ExecuteStatementGoal> successors = new ArrayList<>();

            for (PlanStep step : planSteps) {
                ExecuteStatementGoal stepNode = getGoal(step);
                goalMemory.addSubgoal(this, stepNode);
                insertedAtLeastOneStep = true;

                double stepStart = step.getEstimatedStart();
                if (stepStart != currentStart) {
                    goalMemory.addSubgoalOrderings(predecessors, successors);
                    predecessors.clear();
                    predecessors.addAll(successors);

                    successors.clear();
                    currentStart = stepStart;
                }
                successors.add(stepNode);
            }
            //catch the last set
            goalMemory.addSubgoalOrderings(predecessors, successors);
        }
        if (request.isShared()
            && this != mainGoal) {
            for (GoalLifecycleNode child : mainGoal.getSubgoals()) {
                goalMemory.addSubgoal(this, child);
                insertedAtLeastOneStep = true;
            }
        }
        return insertedAtLeastOneStep;
    }

}
