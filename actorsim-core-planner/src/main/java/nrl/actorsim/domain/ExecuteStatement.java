package nrl.actorsim.domain;

import nrl.actorsim.planner.PlanStep;
import nrl.actorsim.utils.Utils;

import java.time.Duration;
import java.time.Instant;

import static nrl.actorsim.domain.ExecuteStatement.FailureReason.*;
import static nrl.actorsim.domain.ExecuteStatement.Outcome.*;
import static nrl.actorsim.domain.ExecuteStatement.State.*;
import static nrl.actorsim.domain.WorldObject.ACTION;
import static nrl.actorsim.domain.WorldType.NUMBER;

/**
 * Defines a transition statement of the form
 * <code>
 * [start, end] executed(action) == State.INACTIVE -> State.COMPLETED
 * </code>
 * where:
 * <ul>
 *     <li> <code>executed</code> is an instance of the StateVariable
 *          {@link ExecuteStatement.EXECUTED}.
 *     <li> <code>action</code> is an {@link Action}
 *          (i.e., an instance of {@link Operator}), and
 *     <li> {@link State} is an enumeration defined within this class to indicate the execution state.
 * </ul>
 * This form requires the <code>action</code> to be a WorldObject because
 * it is an argument to the StateVariable <code>executed</code>.
 * At first glance, this might seem to create problems because the action
 * itself does not exist in the world.  In practice, this is not really an issue
 * because we simply wrap the action with {@link Action.AsWorldObject}.
 * A benefit of this approach is that the ExecuteStatement
 * links an {@link Action}
 * as a node within a {@link nrl.actorsim.goalnetwork.GoalNetwork}.
 *
 * An {@link ExecuteStatement} proceeds through a set of State changes:
 * <ul>
 *     <li> INACTIVE: the executive has not yet activated this node.
 *          Once active, the node transitions to a WAITING state.
 *     <li> WAITING (aka PENDING): the executive is waiting until
 *          one or more conditions are met to execute the node.
 *          The WAITING state is useful, for example, to track nodes
 *          scheduled for future execution that have not yet started.
 *          It can also be used to indicate a suspended goal.
 *          Once started, a node transitions to EXECUTING.
 *          Abnormal conditions result in Outcome.SKIPPED and
 *          transitioning to State.COMPLETED.
 *     <li> EXECUTING (aka RUNNING): the executive is actively executing
 *          this node.  Nominal execution proceeds to COMPLETING,
 *          while failures proceed to FAILING.
 *     <li> COMPLETING: the node is no longer executing and the executive
 *          will be processing for final condition checking.
 *     <li> FAILING: execution of the node has failed and the executive
 *          will be processing the node.
 *     <li> ITERATION_ENDED: the node has completed execution, either
 *          through COMPLETING or FAILING, and the executive will
 *          be processing the node for another iteration.  A node
 *          might iterate if it is a maintenance goal, it until its
 *          repeatCondition is entailed, or it is waiting on a suspended parent.
 *          A repeated node returns to WAITING.
 *     <li> COMPLETED: the node has completed execution and should
 *          contain an outcome and, upon a FAILED outcome, a FailureReason.
 * </ul>
 * A set of Conditions control the progression through States.
 * Conditions match zero-parameter method names and return
 * a boolean for whether the condition is entailed by the current state
 * or by WorkingMemory if using the cognitive module.
 * This base class provides default responses for each condition for
 * error free execution.
 * Subclasses override these default conditions for specialized behavior.
 *
 * In the table below, we show the Outcomes for each condition when it is false.
 * Conditions are checked from top to bottom, except that a condition
 * which is empty for a state is not used in that state.
 * When the last (bottom-most) condition listed for a state is passed,
 * the state transitions to the next column, where Completed circles to Inactive.
 * A continue (V) indicates that the condition must fail before the
 * next condition can be checked; for example, a node in Inactive state
 * remains Inactive if parentSuspended() is true and, when parentSuspended()
 * is false then parentNotExecuting() can be checked.
 * A halt (o) indicates that the condition is checked but the node will halt
 * in the current state until this condition is passed; for example a node
 * in Waiting will remain in waiting until startCondition() is true.
 * <pre>
 *                      Inact Wait  Exec  Completing  Fail  IterEnd  Completed
 * parentSuspended()    V     V     INT
 * parentNotExecuting() SKIP  SKIP  INT
 * exitCondition()            SKIP  INT
 * startCondition()           o
 * preCondition()             FAIL
 * invariantCondition()             FAIL
 * endCondition()                   o
 * postCondition()                        FAIL
 * completionCondition()                  o           o
 * repeatCondition()                                        o
 * parentWaiting()                                                   o
 *</pre>
 * The outcome of execution is held in an Outcome object, which can take
 * on the following values:
 * <ul>
 *     <li> SUCCESS: if the node transitions from COMPLETING
 *          without failures or condition violations
 *     <li> FAILED: if the execution fails after reaching the EXECUTING
 *          state. FailureReason indicates the violated condition.
 *     <li> INTERRUPTED: if the execution stops after reaching EXECUTING
 *          but before reaching COMPLETING
 *     <li> SKIPPED: if the execution was stopped prior to transitioning
 *          to EXECUTING.
 * </ul>
 *
 * The state machine of this statement emulates the execution semantics
 * of several execution systems in the literature.

 * The {@link ExecuteStatement} is similar to the program step
 * described in APA pages 98ff.  In APA, the program step can indicate
 * execution state using RUNNING, DONE, or FAILED.  We augment
 * the transition RUNNING to include INACTIVE, WAITING, and EXECUTING.
 * The FAILED condition is an outcome that can be read at COMPLETED.
 *
 * Its main structure follows from the lifecycle of the PLEXIL executive
 * (cf. http://plexil.sourceforge.net/wiki/index.php/Node_State_Transition_Diagrams)
 * with several simplifications:
 * <ul>
 *   <li> it does not distinguish between different node types.  All
 *        transitions and conditions are provided for all node types.
 *        Subclasses may specialize these transitions or conditions.
 *   <li> it uses a slightly simplified, and unified, transition diagram.
 *        Specifically, the skipCondition, normally in waiting, is changed
 *        to check for an exitCondition.
 *</ul>
 *
 * Niemueller et al. (2019) created a transition graph for execution states
 * that tracks the execution of gaols for a robotics platform.  The transitions
 * of that lifecycle correspond to states within the lifecycle of an {@link ExecuteStatement}.
 *
 * Harland et al. provide a goal lifecycle with operational semantics (2014)
 * that clarifies and extends the goal lifecycle for Jadex (Braubach et al., 2005).
 * Harland et al. later extended their lifecycle to include aborting,
 * suspending, and resuming goals (2017).
 * We augmented our lifecycle to support their lifecycle concerns.
 */
@SuppressWarnings("JavadocReference")
public class ExecuteStatement extends TransitionStatement<ExecuteStatement.State> {
    public static final StateVariable EXECUTED =
            StateVariable.builder()
                    .name("executed")
                    .argTypes(ACTION)
                    .valueTypes(NUMBER.asArgument("returnValue"))
                    .build();
    private static final Duration DEFAULT_TIMEOUT = Duration.ofSeconds(10);

    private PlanStep step;
    private State state;
    private Outcome outcome;
    private FailureReason failureReason;

    private StateTransition transition;

    // ====================================================
    // region<Construction and Object Overrides>

    public ExecuteStatement(PlanStep step) {
        super(EXECUTED.copier().build(), INACTIVE, COMPLETED);
        bindArgs(step.action.asWorldObject());
        this.step = step;
        Instant now = Instant.now();
        Instant estimatedStart = now.plusSeconds((long)step.getEstimatedStart());
        this.estimatedInterval.setStart(estimatedStart);
        Instant estimatedEnd = estimatedStart.plusSeconds(step.getDurationInSeconds());
        this.estimatedInterval.setEnd(estimatedEnd);
        init();
    }

    public ExecuteStatement(Action action) {
        super(EXECUTED.copier().build(), INACTIVE, COMPLETED);
        bindArgs(action.asWorldObject());
        init();
    }

    protected void init() {
        state_inactive();
    }

    public boolean matches(String action) {
        return step.toString().compareTo(action) == 0;
    }

    @Override
    public String toString() {
        return super.toString()
                + Utils.VALUE_TYPE_START
                + Utils.VALUE_TYPE_START
                + state
                + Utils.VALUE_TYPE_END
                + Utils.VALUE_TYPE_END;
    }

    // endregion
    // ====================================================

    // ====================================================
    // region<Accessors>

    public Action getAction() {
        WorldObject object = getBinding("action");
        if (object instanceof Action.AsWorldObject) {
            return ((Action.AsWorldObject)object).getItem();
        }
        return Action.NULL_ACTION;
    }

    public PlanStep getPlanStep() {
        return step;
    }

    public boolean stateEquals(State value) {
        return state == value;
    }

    // endregion
    // ====================================================

    // ====================================================
    // region<Outcome>

    public enum Outcome {
        UNDEFINED,
        SKIPPED,
        INTERRUPTED,
        FAILED,
        SUCCESS
    }

//    private void setOutcome(Outcome outcome, FailureReason reason) {
//        this.failureReason = reason;
//        this.outcome = outcome;
//    }

    private void setOutcome(Outcome outcome) {
        this.outcome = outcome;
    }

    public enum FailureReason {
        UNDEFINED,

        PARENT_EXITED,
        PARENT_SUSPENDED,

        PRE_CONDITION_VIOLATED,
        INVARIANT_VIOLATED,
        POST_CONDITION_VIOLATED,
        EXIT_VIOLATED,
    }

    // endregion
    // ====================================================

    // ====================================================
    // region<State>

    public enum State {
        INACTIVE,
        WAITING,
        EXECUTING,
        COMPLETING,
        FAILING,
        ITERATION_ENDED,
        COMPLETED
    }

    private void setState(State state) {
        this.state = state;
    }

    public boolean transitionAllowedTo(State newState) {
        return true;
    }

    protected void state_inactive() {
        transitionTo(INACTIVE, this::inactive);
    }

    protected void state_waiting() {
        transitionTo(WAITING, this::waiting);
    }

    protected void state_executing() {
        transitionTo(EXECUTING, this::executing);
    }

    protected void state_completing() {
        transitionTo(COMPLETING, this::completing);
    }

    protected void state_failing(FailureReason reason) {
        failureReason = reason;
        transitionTo(FAILING, this::failing);
    }

    protected void state_iterationEnded() {
        transitionTo(ITERATION_ENDED, this::iterationEnded);
    }

    public void state_completed() {
        transitionTo(COMPLETED, this::completed);
    }

    // endregion
    // ====================================================

    // ====================================================
    // region<State Transitions>

    @FunctionalInterface
    interface StateTransition {
        /**
         * The transition function to call.
         * @return
         */
        void execute();
    }

    /**
     * Attempts to transition to newState and returns
     * the current state.
     * @param newState the state to attempt
     * @param transition the transition function() for this state
     * @return the current state of this statement
     */
    private State transitionTo(State newState, StateTransition transition) {
        if (transitionAllowedTo(newState)) {
            setState(newState);
            this.transition = transition;
        }
        return state;
    }

    public void attemptTransitionToAndNotify(State newState) {
        attemptTransitionToAndNotify(newState, DEFAULT_TIMEOUT);
    }

    /**
     * Attempts to transition to newState and, if changed,
     * notifies any listeners as to status.
     */
    public void attemptTransitionToAndNotify(State newState, Duration timeout) {
        Instant start = Instant.now();
        State startingState = this.state;
        while (state != newState) {
            transition.execute();
            Instant now = Instant.now();
            Duration elapsed = Duration.between(start, now);
            if (elapsed.compareTo(timeout) > 0) {
                break;
            }
        }
        if (this.state != startingState) {
            notifyOfInternalChange();
        }
    }

    /**
     * The inactive transition.
     */
    protected void inactive() {
        if (parentSuspended()) {
            return;
        } else if (parentNotExecuting()) {
            setOutcome(SKIPPED);
            state_completed();
        } else {
            state_waiting();
        }
    }

    /**
     * The waiting transition.
     */
    protected void waiting() {
        if (parentSuspended()) {
            return;
        } else if(parentNotExecuting()
                || exitCondition()) {
            setOutcome(SKIPPED);
            state_completed();
            return;
        }
        if (startCondition()) {
            if (preCondition()) {
                state_executing();
            } else {
                setOutcome(FAILED);
                state_failing(FailureReason.PRE_CONDITION_VIOLATED);
            }
        }
    }

    /**
     * The executing transition.
     */
    protected void executing() {
        if (parentSuspended()) {
            setOutcome(INTERRUPTED);
            state_failing(PARENT_SUSPENDED);
        } else if (parentNotExecuting()) {
            setOutcome(INTERRUPTED);
            state_failing(PARENT_EXITED);
        } else if(exitCondition()) {
            setOutcome(INTERRUPTED);
            state_failing(EXIT_VIOLATED);
        } else if(!invariantCondition()) {
            setOutcome(FAILED);
            state_failing(INVARIANT_VIOLATED);
        } else if(endCondition()) {
            state_completing();
        }
    }

    /**
     * The completing transition.
     */
    protected void completing() {
        if(postCondition()) {
            if(completionCondition()) {
                setOutcome(SUCCESS);
                state_iterationEnded();
            }
        } else {
            setOutcome(FAILED);
            state_failing(POST_CONDITION_VIOLATED);
        }
    }

    protected void failing() {
        if (completionCondition()) {
            if (outcome == FAILED) {
                state_completed();
            } else {
                state_iterationEnded();
            }
        }
    }

    protected void iterationEnded() {
        if ((outcome != FAILED)
            && repeatCondition()) {
            repeat();
        } else {
            state_completed();
        }
    }

    protected void completed() {
        if(parentWaiting()) {
            state_inactive();
        }
    }

    public void repeat() {
        state_waiting();
    }

    // endregion
    // ====================================================

    // ====================================================
    // region<Parent Conditions>

    protected boolean parentNotExecuting() {
        return false;
    }

    protected boolean parentSuspended() {
        return false;
    }

    /**
     * Checks whether the parent is in a waiting state.
     * If there is no parent, this should return false.
     *
     * The default value is false.
     * @return whether the parent is in a waiting state
     */
    protected boolean parentWaiting() {
        return false;
    }

    // endregion
    // ====================================================

    // ====================================================
    // region<Conditions>

    /**
     * Returns true if this nodes exit condition
     * is entailed.
     *
     * The default behavior is false.
     *
     * @return whether this node's exit condition is entailed
     */
    protected boolean exitCondition() {
        return false;
    }


    /**
     * Checks the start condition of this node.
     * A start condition is useful for a node
     * that is scheduled to execute in the future
     * but the time to execute has not yet arrived.
     *
     * The default value is true.
     *
     * @return
     */
    protected boolean startCondition() {
        return true;
    }


    /**
     * Checks the pre condition of this node.
     *
     * The default value of this is true.
     *
     * @return whether the pre condition holds
     */
    protected boolean preCondition() {
        return true;
    }

    /**
     * Checks that the invariant condition of this node
     * holds true.
     *
     * The default value of this is true.
     *
     * @return whether the invariant condition holds
     */
    protected boolean invariantCondition() {
        return true;
    }

    /**
     * Checks the end condition of this node.
     *
     * The default value of this is true.
     *
     * @return whether the end condition holds
     */
    protected boolean endCondition() {
        return true;
    }

    /**
     * Checks the post condition of this node.
     *
     * The default value of this is true.
     * @return whether the post condition is true
     */
    protected boolean postCondition() {
        return true;
    }

    /**
     * Checks the completion condition of this node.
     * This condition can include whether an update
     * was completed, a command was completed,
     * whether all children of this node are completed,
     * etc.
     *
     * The default value is true.
     * @return
     */
    protected boolean completionCondition() {
        return true;
    }

    /**
     * Returns whether this node should repeat
     *
     * By default, this is false.
     *
     * @return whether this node should repeat.
     */
    public boolean repeatCondition() {
        return false;
    }

    // endregion
    // ====================================================
}
