package nrl.actorsim.planner;

import nrl.actorsim.domain.PlanningDomain;
import nrl.actorsim.memory.WorkingMemory;
import nrl.actorsim.utils.WorkerThread;
import org.apache.commons.cli.AbstractOption;
import org.apache.commons.cli.BooleanOption;
import org.apache.commons.cli.SingleArgumentOption;

import java.time.Duration;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

import static org.apache.commons.cli.BooleanOption.Status.DISABLED;

abstract public class PlannerWorkerBase<R extends PlanRequest> extends WorkerThread {
    final static org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(PlannerWorkerBase.class);

    protected final PlannerOptions plannerOptions;
    protected final Queue<R> requestQueue = new LinkedList<>();
    protected R currentRequest;

    protected final Object isWorkingLock = new Object();
    protected Boolean isWorking;

    protected PlannerWorkerBase(PlannerOptions options) {
        super(options);
        this.plannerOptions = options;
        setIsWorking(false);
    }

    // ====================================================
    // region<Request Entry>
    public PlanRequestWithMemory getRequestWithMemory(WorkingMemory memory) {
        return new PlanRequestWithMemory(memory, plannerOptions.domain);
    }

    public void enqueue(R request) {
        synchronized (requestQueue) {
            requestQueue.add(request);
        }
        notifyWorkerThereIsWork();
    }

    public void waitUntilQueueEmpty() {
        while (hasMoreWork()) {
            notifyWorkerAndWaitOnCycle();
        }
    }

    public void waitUntilAllWorkDone() {
        while(isWorking() || hasMoreWork()) {
            notifyWorkerAndWaitOnCycle();
        }
    }

    public R getCurrentRequest() {
        return currentRequest;
    }

    private boolean hasMoreWork() {
        boolean sizeGreaterThanZero;
        synchronized (requestQueue) {
            sizeGreaterThanZero = requestQueue.size() > 0;
        }
        return sizeGreaterThanZero;
    }

    // endregion
    // ====================================================

    // ====================================================
    // region<Worker>

    @Override
    public WorkResult performWork() {
        setIsWorking(true);
        if (hasMoreWork()) {
            popNextRequest();
            runPlanner();
        }

        WorkResult result = WorkResult.CONTINUE;
        if (hasMoreWork()) {
            result = WorkResult.MORE_WORK;
        }
        setIsWorking(false);
        return result;
    }

    private void popNextRequest() {
        synchronized (requestQueue) {
            R request = requestQueue.poll(); //returns null on empty
            if (request != null) {
                currentRequest = request;
            }
        }
    }

    /**
     * A hook for planner-specific code to run the planner
     * and parse the results.
     */
    abstract protected void runPlanner();


    // ====================================================
    // region<IsWorking status>
    private void setIsWorking(boolean value) {
        synchronized (isWorkingLock) {
            this.isWorking = value;
        }
    }

    private boolean isWorking() {
        boolean working;
        synchronized (isWorkingLock) {
            working = isWorking;
        }
        return working;
    }

    // endregion
    // ====================================================


    public PlannerOptions getPlannerOptions() {
        return plannerOptions;
    }

    public static class PlannerOptions extends WorkerThread.Options {

        public static Builder<? extends PlannerOptions, ? extends Builder<?,?>> builder() {
            return new Builder<>(new PlannerOptions());
        }

        protected PlannerOptions() {

        }

        public static class Builder<T extends PlannerOptions, B extends Builder<T, B>>
                extends WorkerThread.Options.Builder<T, B> {

            protected Builder(T instance) {
                super(instance);
                this.optionsInstance = instance;
            }

            public B domain(PlanningDomain domain) {
                getOptionsInstance().domain = domain;
                return getThis();
            }

            public B enableCapturePlannerOutput() {
                getOptionsInstance().capturePlanOutput.enable();
                return getThis();
            }

            public B disableCapturePlannerOutput() {
                getOptionsInstance().capturePlanOutput.disable();
                return getThis();
            }

            public B setPlannerTimeOut(Duration duration) {
                getOptionsInstance().plannerTimeoutInSeconds.setValue(duration.getSeconds());
                return getThis();
            }

            protected void validate() {
                super.validate();
                if (getOptionsInstance().domain == null) {
                    String message = "PlannerWorkerBase requires a domain instantiate.";
                    logger.error(message);
                    throw new IllegalArgumentException(message);
                }
            }
        }

        public <D extends PlanningDomain>
        D getDomain() {
            return (D) domain;
        }

        public PlanningDomain domain;

        protected boolean planIndividualGoals = true;

        public void planForIndividualGoals() {
            planIndividualGoals = true;
        }

        public void planForAllGoalsInMemory() {
            planIndividualGoals = false;
        }


        public boolean getPlanIndividualGoals() {
            return planIndividualGoals;
        }

        public boolean getPlanForAllGoalsInMemory() {
            return ! planIndividualGoals;
        }

        List<AbstractOption> plannerOptions = new ArrayList<>();
        public BooleanOption capturePlanOutput
                = new BooleanOption(
                plannerOptions, "capturePlanOutput", DISABLED,
                "Whether to include the full planner output in the request", DISABLED);

        public SingleArgumentOption plannerTimeoutInSeconds
                = new SingleArgumentOption(
                plannerOptions,"plannerTimeoutInSeconds", "90",
                "The planner timeout in seconds. [default=90]", "90");
    }

}
