package nrl.actorsim.planner;


import nrl.actorsim.domain.Action;

import java.time.Duration;

public class PlanStep {
    String step;
    double estimatedStart;
    Duration duration = Duration.ZERO;
    public Action action;

    private PlanStep() {
    }

    public static Builder builder() {
        return new Builder();
    }

    @Override
    public String toString() {
        return "'" + step + "'";
    }

    public double getEstimatedStart() {
        return estimatedStart;
    }

    public long getDurationInSeconds() {
        return duration.toMillis() / 1000;
    }

    public static class Builder {
        private PlanStep instance;

        Builder() {
            instance = new PlanStep();
        }

        public Builder step(String step) {
            instance.step = step;
            return this;
        }

        public Builder start(double start) {
            instance.estimatedStart = start;
            return this;
        }

        public Builder duration(Duration duration) {
            instance.duration = duration;
            return this;
        }

        public Builder action(Action action) {
            instance.action = action;
            return this;
        }

        public PlanStep build() {
            return instance;
        }
    }
}

