package nrl.actorsim.planner;

import nrl.actorsim.goals.RequestGoal;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class PlanRequest {
    final static org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(PlanRequest.class);

    StringBuffer domainBuffer = new StringBuffer();
    StringBuffer problemBuffer = new StringBuffer();
    StringBuffer domainDebugBuffer = new StringBuffer();
    StringBuffer problemDebugBuffer = new StringBuffer();
    StringBuffer planDebugBuffer = new StringBuffer();

    List<String> plan = new ArrayList<>();
    List<PlanStep> planSteps = new ArrayList<>();
    List<String> plannerOutput;

    List<RequestGoal> requestGoalBases  = new ArrayList<>();

    enum ConversionType {
        TOTAL_ORDERING,
        TEMPORAL_ORDERING
    }

    ConversionType conversionType = ConversionType.TEMPORAL_ORDERING;

    // ====================================================
    // region<Constructors, Object Overrides, Accessors>

    public PlanRequest() {
        reset();
    }

    public void reset() {
        domainBuffer.setLength(0);
		problemBuffer.setLength(0);
		domainDebugBuffer.setLength(0);
		problemDebugBuffer.setLength(0);
        planDebugBuffer.setLength(0);
    }

    @Override
    public String toString() {
        return String.format("Request domain:%s problem:%s ", domainBuffer.length(), problemBuffer.length());
    }

    public boolean isShared() {
        return requestGoalBases.size() > 1;
    }

    public RequestGoal getMainGoal() {
        return requestGoalBases.get(0);
    }

    public void add(RequestGoal goal) {
        requestGoalBases.add(goal);
    }

    @SuppressWarnings("unused")
    public void convertUsing(ConversionType conversionType) {
        this.conversionType = conversionType;
    }

    // endregion
    // ====================================================

    // ====================================================
    // region<Generate Problem or Domain>

    @SuppressWarnings("unused")
    public void generateDomainAndProblem() {
		appendComment(domainDebugBuffer, "---------- Start of domain debug");
		generateDomain();
		appendComment(domainDebugBuffer, "---------- End of domain debug");

		appendComment(problemDebugBuffer, "---------- Start of problem debug");
    	generateProblem();
		appendComment(problemDebugBuffer, "---------- End of problem debug");
    }

    public void generateDomain() {
        logger.warn("Not creating any content for domain file!");
    }

    public void generateProblem() {
        logger.warn("Not creating any content for problem file!");
    }

    // endregion
    // ====================================================

    // ====================================================
    // region<Read from Files>

    public void readDomain(Path domainPath) throws IOException {
		appendComment(domainDebugBuffer, "---------- Start of domain debug");
		appendComment(domainDebugBuffer, "---------- Loaded from file: " + domainPath);
        List<String> lines = Files.readAllLines(domainPath);
        lines.stream().forEachOrdered(e -> append(domainBuffer, e));
		appendComment(domainDebugBuffer, "---------- End of domain debug");
    }

    public void readProblem(Path problemPath) throws IOException {
		appendComment(problemDebugBuffer, "---------- Start of problem debug");
		appendComment(problemDebugBuffer, "---------- Loaded from file: " + problemPath);
        List<String> lines = Files.readAllLines(problemPath);
        lines.stream().forEachOrdered(e -> append(problemBuffer, e));
		appendComment(problemDebugBuffer, "---------- End of problem debug");
    }

    public void readPlan(Path solutionPath) throws IOException {
        appendComment(planDebugBuffer, "---------- Start of problem debug");
        appendComment(planDebugBuffer, "---------- Loaded from file: " + solutionPath);
        List<String> lines = Files.readAllLines(solutionPath);
        lines.stream().forEachOrdered(this::addPlanLine);
        appendComment(planDebugBuffer, "---------- End of problem debug");
    }

    // endregion
    // ====================================================

    // ====================================================
    // region<Planner Output>

    public void enableCapturePlannerOutput() {
        plannerOutput = new ArrayList<>();
    }

    public boolean capturePlannerOutput() {
        return plannerOutput != null;
    }

    public void addOutput(List<String> outputLines) {
        if (plannerOutput == null) {
            enableCapturePlannerOutput();
        }
        plannerOutput.addAll(outputLines);
    }

    public void resetPlan() {
        plan.clear();
        planSteps.clear();
    }

    public String report() {
         return String.format("planLength:%s", plan.size());
    }

    public void addPlanLine(String line) {
        plan.add(line);
    }

    public List<String> getPlan() {
        return Collections.unmodifiableList(plan);
    }

    public int getPlanLength() {
        return getPlan().size();
    }

    public boolean hasPlan() {
        return plan.size() > 0;
    }

    // endregion
    // ====================================================

    // ====================================================
    // region<Append variants for constructing planner content>

    @SuppressWarnings("unused")
    protected void appendToDomain(String line) {
        append(domainBuffer, line);
    }

    @SuppressWarnings("unused")
    protected void commentToDomain(String line) {
        appendComment(domainBuffer, line);
    }

    public void appendToProblem(String line) {
        append(problemBuffer, line);
    }

    @SuppressWarnings("unused")
    protected void commentToProblem(String line) {
        appendComment(problemBuffer, line);
    }

    protected void appendComment(StringBuffer buffer, String line) {
    	append(buffer, ";;  " + line, true);
    }

    protected void append(StringBuffer buffer, String line) {
    	append(buffer, line, true);
    }

    @SuppressWarnings("unused")
    protected void appendWithoutNewline(StringBuffer buffer, String line) {
    	append(buffer, line, false);
    }

    protected void append(StringBuffer buffer, String line, boolean useLineSeparator) {
    	buffer.append(line);
    	if (useLineSeparator) {
    		buffer.append(System.lineSeparator());
    	}
    }

    // endregion
    // ====================================================


    public boolean convertUsingTemporalOrdering() {
        return conversionType == ConversionType.TEMPORAL_ORDERING;
    }

    public boolean convertUsingTotalOrdering() {
        return conversionType == ConversionType.TOTAL_ORDERING;
    }



    public List<PlanStep> getPlanSteps() {
        if (!hasPlan()) {
            return Collections.emptyList();
        }
        convertPlanToPlanSteps();
        return planSteps;
    }

    protected void convertPlanToPlanSteps() {
        planSteps = Collections.emptyList();
    }


}
