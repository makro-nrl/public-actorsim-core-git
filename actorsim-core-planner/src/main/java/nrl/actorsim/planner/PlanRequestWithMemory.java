package nrl.actorsim.planner;

import nrl.actorsim.domain.*;
import nrl.actorsim.memory.WorkingMemory;
import org.slf4j.*;

import java.util.*;

@SuppressWarnings("unused")
public class PlanRequestWithMemory extends PlanRequest {
    final static Logger logger = LoggerFactory.getLogger(PlanRequestWithMemory.class);
    protected final WorkingMemory memory;
    protected final PlanningDomain domain;
    protected final List<Statement> considered = new ArrayList<>();

    /**
     * A collection of goal statements that will be used in the request.
     * If empty, then memory is used.
     */
    protected Collection<Statement> requestGoals;


    // ====================================================
    // region<Constructors, Object Overrides, Accessors>

    public PlanRequestWithMemory(WorkingMemory memory, PlanningDomain domain) {
        this.memory = memory;
        this.domain = domain;
    }

    @Override
    public String toString() {
        StringBuilder goalString = new StringBuilder("[");
        if (requestGoals != null) {
            for (Statement goal : requestGoals) {
                goalString.append(goal.toString());
            }
        }
        goalString.append("]");

        StringBuilder consideredString = new StringBuilder("[");
        for(Statement goal : considered) {
            consideredString.append(goal.toString());
        }
        consideredString.append("]");


        return String.format("Request domain:%s problem:%s ", domainBuffer.length(), problemBuffer.length())
                + String.format(" requested:%s", goalString)
                + String.format(" considered:%s", consideredString);
    }

    public
    <D extends PlanningDomain> D getDomain() {
        //noinspection unchecked
        return (D) domain;
    }



    // endregion
    // ====================================================

    // ====================================================
    // region<Statements>

    public List<Statement> getConsidered() {
        return Collections.unmodifiableList(considered);
    }

    public void add(Statement goalStatement) {
        if (requestGoals == null) {
            requestGoals = new ArrayList<>();
        }
        requestGoals.add(goalStatement);
    }

    // endregion
    // ====================================================

}
