package nrl.actorsim.domain;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.Set;

@SuppressWarnings("unused")
public class TestWorldObjects {


    public static final WorldType LOCATION = new WorldType("location");
    ////////////////////////////////////////////////////////////
    //
    // Objects for simple tree networks
    //
    ////////////////////////////////////////////////////////////
    public static StateVariableTemplate AT = StateVariableTemplate.builder()
            .name("at")
            .argTypes(WorldType.OBJECT)
            .valueTypes(TestWorldObjects.LOCATION)
            .build();
    public static WorldObject A;
    public static WorldObject B;
    public static WorldObject C;
    public static WorldObject D;
    public static WorldObject E;

    public static WorldType ROOM;

    static {
        resetTypeManager();
    }

    @BeforeMethod
    public static void resetTypeManager() {
        TypeManager.resetInstanceForTesting();
        WorldObject.initializeStaticTypes();
        A = new WorldObject(LOCATION, "A");
        B = new WorldObject(LOCATION, "B");
        C = new WorldObject(LOCATION, "C");
        D = new WorldObject(LOCATION, "D");
        E = new WorldObject(LOCATION, "E");

        ROOM = new WorldType("room", LOCATION);

    }

    @Test
    public void testTypeEquals() {
        assert A.equalsOrInheritsFromType(LOCATION);
        //noinspection EqualsWithItself because we want to ensure it works!
        assert ROOM.equals(ROOM);
        assert ROOM.equalsOrInheritsFromType(LOCATION);
    }


    @Test
    public void testSuperTypeIsTransitive() {
        WorldObject ITEM = new WorldObject("item").addSuperType("object");
        WorldObject FOOD = new WorldObject("food").addSuperType("item");
        WorldObject BREAD = new WorldObject("bread").addSuperType("food");

        assert BREAD.equalsOrInheritsFromType(ITEM);
    }

    @Test
    public void testOutOfOrderSuperTypeIsTransitive() {
        WorldObject BREAD = new WorldObject("bread").addSuperType("food");
        WorldObject FOOD = new WorldObject("food");
        FOOD.addSuperType("item");
        WorldObject ITEM = new WorldObject("item").addSuperType("Object");
        Set<WorldType> itemSupers = TypeManager.getSuperTypes(ITEM);
        Set<WorldType> breadSupers = TypeManager.getSuperTypes(BREAD);
        TypeManager instance = TypeManager.getInstance();

        assert BREAD.equalsOrInheritsFromType(ITEM);
    }
}
