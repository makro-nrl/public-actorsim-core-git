package nrl.actorsim.domain;

import nrl.actorsim.domain.test.TestItemExistsDomain;
import org.testng.Assert;
import org.testng.annotations.Test;

import static nrl.actorsim.domain.WorldType.BOOLEAN;
import static nrl.actorsim.domain.test.TestItemExistsDomain.*;

public class TestStateProperties {

    @Test(expectedExceptions = {IllegalArgumentException.class})
    public void testStateVariableMissingName() {
        StateVariable sv = StateVariable.builder().build();
        assert sv.isBindable;
    }

    @Test(expectedExceptions = {IllegalArgumentException.class})
    public void testStateVariableNullBindings() {
        StateVariable sv = StateVariable.builder()
                .name("test").removeBindings().build();
        assert sv.isBindable;
    }

    @Test(expectedExceptions = IllegalArgumentException.class)
    public void testStateVariableWithEmptyArgs() {
        StateVariable.builder().name("test").build();
    }

    @Test
    public void testStateVariable() {
        StateVariable sv = StateVariable.builder()
                .name("test").argTypes(TestItemExistsDomain.ITEM).build();
        assert sv.name.equals("test");
        assert sv.isBindable;
        assert sv.isAssignable;
        assert sv.getBindingsAtThisNodeOnly().size() == 0;
    }

    @Test
    public void testWrongBindingTypeThrows() {
        //Utils.enableDebugLogging();
        WorldObject tmpA = TestWorldObjects.A;
        //following should throw because
        //   tmpA.type == LOCATION but existsStatement expects ITEM
        Statement s = TestItemExistsDomain.existsStatement(tmpA);
        WorldObject boundObject = s.getBinding("item");
        assert boundObject == WorldObject.NULL_WORLD_OBJECT;
    }

    @Test
    public void testPersistenceStatementCompareTo() {
        Statement existsA = TestItemExistsDomain.existsStatement(A);
        Statement existsB = TestItemExistsDomain.existsStatement(B);
        int statementCompare = existsA.compareTo(existsB);
        assert statementCompare < 0;
    }

    @Test
    public void testPersistenceStatementCopiesBindingsCorrectly() {
        StateVariable EXISTS_ITEM = StateVariable.builder()
            .name("exists").argTypes(ITEM).valueTypes(BOOLEAN).build();

        WorldObject itemA = A;
        StateVariable tmpSv = EXISTS_ITEM.copier().build(itemA);
        BooleanPersistenceStatement statement1 = new BooleanPersistenceStatement(tmpSv);
        Bindings bindingsInTarget = statement1.getTarget().getBindingsAtThisNodeOnly();
        Assert.assertEquals(bindingsInTarget.size(), 0, "Bindings should be held at statement, not the target!");

        Bindings bindingsAtStatement = statement1.getBindingsAtThisNodeOnly();
        Assert.assertEquals(bindingsAtStatement.size(), 1, "Bindings should be held at statement, but its missing!");
        WorldObject firstBinding = bindingsAtStatement.get(0).getTarget().content();
        Assert.assertEquals(firstBinding, itemA);

        Bindings bindingsFoundForTarget = statement1.getTarget().findBindings();
        Assert.assertEquals(bindingsFoundForTarget.size(), 1, "Bindings should be available for target but are missing!");
        WorldObject foundBinding = bindingsFoundForTarget.get(0).getTarget().content();
        Assert.assertEquals(foundBinding, itemA);
    }

    @Test
    public void testPersistenceStatementValueType() {
        StateVariable sv = StateVariable.builder()
                .name("at")
                .argTypes(TestItemExistsDomain.ITEM)
                .valueTypes(LOCATION)
                .build();

        sv.persistenceStatement(WorldObject.class, A).assign(loc1);
        boolean exceptionThrown = false;
        try {
            //should fail because we specified the value type as a location
            sv.persistenceStatement(Boolean.class).assign(true);
        } catch (IllegalArgumentException e) {
            exceptionThrown = true;
        }
        assert exceptionThrown;

        sv = StateVariable.builder()
                .name("exists")
                .argTypes(TestItemExistsDomain.ITEM)
                .valueTypes(BOOLEAN)
                .build();

        //should succeed because BOOLEAN accepts java.lang.Boolean
        sv.persistenceStatement(Boolean.class).assign(true);
        exceptionThrown = false;
        try {
            //should fail because we specified the value type as a boolean
            sv.persistenceStatement(WorldObject.class).assign(loc1);
        } catch (IllegalArgumentException e) {
            exceptionThrown = true;
        }
        assert exceptionThrown;

        sv = StateVariable.builder()
                .name("inventory-of")
                .argTypes(TestItemExistsDomain.ITEM)
                .valueTypes(WorldType.NUMBER)
                .build();

        //should succeed because NUMBER accepts java.lang.Integer
        sv.persistenceStatement(Integer.class).assign(1);
        exceptionThrown = false;
        try {
            //should fail because we specified the value type as an integer
            sv.persistenceStatement(Boolean.class).assign(true);
        } catch (IllegalArgumentException e) {
            exceptionThrown = true;
        }
        assert exceptionThrown;
    }
}
