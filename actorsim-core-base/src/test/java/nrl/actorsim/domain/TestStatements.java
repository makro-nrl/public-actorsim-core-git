package nrl.actorsim.domain;

import org.testng.Assert;
import org.testng.annotations.Test;

import static nrl.actorsim.domain.test.TestItemExistsDomain.*;

public class TestStatements {

    @Test
    public void testNumericPersistanceStatement() {
        NumericPersistenceStatement statement = AVAILABLE_FUEL.numericPersistenceStatement(r1);
        statement.assign(20);
        Assert.assertEquals(statement.getValue(), 20);
        Assert.assertNotEquals(statement, NumericPersistenceStatement.NULL_STATEMENT);
    }
}
