package nrl.actorsim.domain;

import nrl.actorsim.domain.test.TestItemExistsDomain;
import org.testng.Assert;
import org.testng.annotations.Test;

import static nrl.actorsim.domain.WorldObject.NULL_WORLD_OBJECT;
import static nrl.actorsim.domain.WorldType.BOOLEAN;
import static nrl.actorsim.domain.test.TestItemExistsDomain.*;

public class TestBindings {
    @Test
    public void testStateRelationInstantiation() {
        WorldArgument argFrom = new WorldArgument(LOCATION, "from");
        WorldArgument argTo = new WorldArgument(LOCATION, "to");

        StateRelationTemplate CONNECTED = StateRelationTemplate.builder()
            .name("connected")
            .argTypes(argFrom, argTo)
            .valueTypes(BOOLEAN)
            .bindingConstraint(BindingConstraint.notEqual(argFrom, argTo))
            .build();

        StateRelation sr = CONNECTED.asStateRelation();
        WorldObject loc1 = LOCATION.instance("loc1");
        WorldObject loc2 = LOCATION.instance("loc2");
        assert sr.bind("from", loc1).isSuccess();
        assert sr.bind("to", loc1) == BindResult.FAILURE_VIOLATES_BINDING_CONSTRAINT;
        assert sr.bind("to", loc2).isSuccess();
    }

    @Test
    public void testStateVariableInstantiation() {
        WorldArgument argFrom = new WorldArgument(LOCATION, "from");
        WorldArgument argTo = new WorldArgument(LOCATION, "to");

        StateVariableTemplate AT = StateVariableTemplate.builder()
            .name("at")
            .addArgument(ITEM.asArgument("item"))
            .valueTypes(LOCATION.asArgument("loc"))
            .build();

        StateVariable itemAtFrom = AT.stateVariableBuilder()
                .specializeValueTypes(argFrom)
                .buildWithoutBindings();
        Statement preItemAtFrom = itemAtFrom.persistenceStatement(LOCATION.getClass());
        String preName = preItemAtFrom.getPathName();

        StateVariable itemAtTo = AT.stateVariableBuilder()
                .specializeValueTypes(argTo)
                .buildWithoutBindings();
        Statement effItemAtTo = itemAtTo.persistenceStatement(LOCATION.getClass());
        String effName = effItemAtTo.getPathName();


        String prePath = preItemAtFrom.toStringPathDetails();
        String effPath = effItemAtTo.toStringPathDetails();

        assert prePath.equals(String.format("~%s\n  .at", preName));
        assert effPath.equals(String.format("~%s\n  .at", effName));
    }

    @Test
    public void testStateVariableBinding() {
        WorldArgument argFrom = new WorldArgument(LOCATION, "from");
        WorldArgument argTo = new WorldArgument(LOCATION, "to");

        WorldObject item = A;
        StateVariable itemAtFrom = AT.stateVariableBuilder()
                .specializeValueTypes(argFrom)
                .build(item);

        StateVariable itemAtTo = AT.stateVariableBuilder()
                .specializeValueTypes(argTo)
                .build(item);

        WorldObject boundItemAtFrom = itemAtFrom.getBinding("item");
        WorldObject boundItemAtTo = itemAtTo.getBinding("item");

        boundItemAtFrom.toString();

        assert boundItemAtFrom != NULL_WORLD_OBJECT;
        assert boundItemAtTo != NULL_WORLD_OBJECT;
        assert boundItemAtFrom == boundItemAtTo;
    }

    @Test
    public void bindForPersistneceStatement() {
        //expects two args but only gets one; should fail!
        Statement statement = DELIVERY_NEEDED.persistenceStatement(A, loc1);
        Assert.assertEquals("A", statement.getBinding("item").toString());
        Assert.assertEquals("loc1", statement.getBinding("location").toString());
    }

    @Test(expectedExceptions = IllegalArgumentException.class)
    public void bindForPersistneceStatement_TooFewArguments() {
        //expects two args but only gets one; should fail!
        DELIVERY_NEEDED.persistenceStatement(A);
    }

    @Test(expectedExceptions = IllegalArgumentException.class)
    public void testSpecializeStateVariableArgs() {
        WorldArgument argFrom = LOCATION.asArgument("from");
        WorldArgument argPackage = PACKAGE.asArgument("item");

        StateVariable packageAtFrom = AT.stateVariableBuilder()
                .specializeValueTypes(argFrom)
                .specializeArgs(argPackage)
                .build();

        WorldObject item = A;
        assert packageAtFrom.bind(argPackage, item).isFailure();

        WorldObject p1 = PACKAGE.instance("p1");
        assert packageAtFrom.bind(argPackage, p1).isSuccess();

        //as a final check, ensure binding an item as a package fails
        Binding test = Binding.equal(argPackage, item);
    }

    @Test
    public void testStatementBinding() {
        WorldArgument argFrom = new WorldArgument(LOCATION, "from");
        WorldArgument argTo = new WorldArgument(LOCATION, "to");

        WorldObject item = A;

        StateVariable itemAtFrom = AT.stateVariableBuilder()
                .specializeValueTypes(argFrom)
                .build();
        Statement preItemAtFrom = itemAtFrom.persistenceStatement(LOCATION.getClass());
        assert preItemAtFrom.bindArgs(item) == BindResult.SUCCESS;

        StateVariable itemAtTo = AT.stateVariableBuilder()
                .specializeValueTypes(argTo)
                .build();
        Statement effItemAtTo = itemAtTo.persistenceStatement(LOCATION.getClass());
        assert effItemAtTo.bindArgs(item) == BindResult.SUCCESS;

        //the initial SVs should not be bound b/c they should have been copied
        assert itemAtFrom.getBinding("item") == NULL_WORLD_OBJECT;
        assert itemAtTo.getBinding("item") == NULL_WORLD_OBJECT;

        //target binding lists should be empty b/c statements have the bindings
        assert preItemAtFrom.getTarget().getBindingsAtThisNodeOnly().size() == 0;
        assert effItemAtTo.getTarget().getBindingsAtThisNodeOnly().size() == 0;

        //target binding lists should "find" those of the parent
        assert preItemAtFrom.getTarget().findBindings().size() == 1;
        assert effItemAtTo.getTarget().findBindings().size() == 1;

        //bindings of pre/eff targets should match statement bindings
        WorldObject boundPreTargetItem = preItemAtFrom.getTarget().getBinding("item");
        WorldObject boundEffTargetItem = effItemAtTo.getTarget().getBinding("item");
        WorldObject boundPreItem = preItemAtFrom.getBinding("item");
        WorldObject boundEffItem = effItemAtTo.getBinding("item");

        assert boundPreTargetItem != NULL_WORLD_OBJECT;
        assert boundPreItem != NULL_WORLD_OBJECT;
        assert boundPreTargetItem == boundPreItem;

        assert boundEffTargetItem != NULL_WORLD_OBJECT;
        assert boundEffItem != NULL_WORLD_OBJECT;
        assert boundEffTargetItem == boundEffItem;

        assert boundPreTargetItem == boundEffTargetItem;
    }

    @Test
    public void testOperatorBinding() {
        Operator move = new Operator("move");

        WorldArgument argFrom = move.addArgument(new WorldArgument(LOCATION, "from"));
        WorldArgument argTo = move.addArgument(new WorldArgument(LOCATION, "to"));
        WorldArgument argItem = move.addArgument(new WorldArgument(ITEM, "item"));
        Binding bindingFromNeqTo = move.addBinding(Binding.notEqual(argFrom, argTo));
        move.addBinding(bindingFromNeqTo);

        StateVariable itemAtFrom = AT.stateVariableBuilder()
                .specializeValueTypes(argFrom)
                .buildWithoutBindings();
        Statement preItemAtFrom = move.addPrecondition(itemAtFrom.persistenceStatement(LOCATION.getClass()));
        String preName = preItemAtFrom.getPathName();

        StateVariable itemAtTo = AT.stateVariableBuilder()
                .specializeValueTypes(argTo)
                .buildWithoutBindings();
        Statement effItemAtTo =  move.addEffect(itemAtTo.persistenceStatement(LOCATION.getClass()));
        String effName = effItemAtTo.getPathName();

        String movePath = move.toStringPathDetails();
        String expectedMovePath = String.format("~move\n  .from\n  .to\n  .item\n  .%s\n  .%s", preName, effName);
        String prePath = preItemAtFrom.toStringPathDetails();
        String effPath = effItemAtTo.toStringPathDetails();

        move.pathParent.toString();

        assert movePath.equals(expectedMovePath);
        assert prePath.equals(String.format("~move.%s\n  .at\n  ^move", preName));
        assert effPath.equals(String.format("~move.%s\n  .at\n  ^move", effName));
    }

    @Test
    public void testNamedPathDownCasting() {
        Operator moveOp = TestItemExistsDomain.move;
        for (Statement precondition : moveOp.getPreconditions()) {
            //noinspection UnnecessaryLocalVariable
            NamedPath path = precondition;

            //test the direct transformer method
            Statement testStatement = path.accept(NamedPathVisitor.TRANSFORMER);
            assert testStatement != null;

            //test the indirect content() method
            testStatement = path.content();
            assert testStatement != null;

            try {
                //test an incorrect cast
                StateVariable testSV = path.content();
                assert false; //should not reach this code
            } catch (ClassCastException e) {
            }
        }
    }

    @Test
    public void testActionInstantiation() {
        Operator moveOp = TestItemExistsDomain.move;
        Action move = moveOp.instance();
        String moveName = move.getPathName();
        String preName = "";
        for (Statement precondition : move.getPreconditions()) {
            preName = precondition.getPathName();
        }
        String effName = "";
        for (Statement effect : move.getEffects()) {
            effName = effect.getPathName();
        }

        String movePath = move.toStringPathDetails();
        String expectedMovePath = String.format("~%s\n  .from\n  .to\n  .item\n  .%s\n  .%s", moveName, preName, effName);
        assert movePath.equals(expectedMovePath);
    }

    @Test
    public void testActionBinding() {
        Action move = TestItemExistsDomain.move.instance();
        move.bind("item", A);
        move.bind("from", loc1);
        move.bind("to", loc2);

        assert move.getBinding("item") == A;
        assert move.getBinding("from") == loc1;
        assert move.getBinding("to") == loc2;

        for (Statement precondition : move.getPreconditions()) {
            WorldObject from = precondition.getBinding("from");
            assert from == loc1;

            StateVariable sv = precondition.getTarget();
            WorldObject item = sv.getBinding("item");
            assert item == A;
        }

        for (Statement effect : move.getPreconditions()) {
            WorldObject from = effect.getBinding("to");
            assert from == loc2;

            StateVariable sv = effect.getTarget();
            WorldObject item = sv.getBinding("item");
            assert item == A;
        }
    }
}
