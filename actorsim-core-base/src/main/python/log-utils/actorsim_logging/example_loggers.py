from LogManager import LogManager

malmo_root_logger = LogManager.get_logger('actorsim.malmo')
malmo_root_logger.setLevel(LogManager.DEBUG)

class MalmoLoggers():
    """A convenience class for accessing the common loggers in the MalmoConnector."""

    malmo_root_logger = LogManager.get_logger('actorsim.malmo')

    #character-related loggers
    character_logger = LogManager.get_logger('actorsim.malmo.character')
    obs_logger = LogManager.get_logger('actorsim.malmo.character.obs')
    calc_logger = LogManager.get_logger('actorsim.malmo.character.calc')
    command_logger = LogManager.get_logger('actorsim.malmo.character.command')
    memory_logger = LogManager.get_logger('actorsim.malmo.character.memory')
    inventory_logger = LogManager.get_logger('actorsim.malmo.inventory')

    #action / dbase loggers
    action_logger = LogManager.get_logger('actorsim.malmo.action')
    kqml_logger = LogManager.get_logger("companions.kqml")

    #planning loggers
    planner_logger = LogManager.get_logger('actorsim.planner')
    jshop2_logger = LogManager.get_logger("actorsim.planner.jshop2")
    malmo_planner_logger = LogManager.get_logger("actorsim.malmo.planner")
    bindings_logger = LogManager.get_logger("actorsim.malmo.planner.bindings")
    trace_logger = LogManager.get_logger("actorsim.malmo.trace_manager")
    type_logger = LogManager.get_logger("actorsim.malmo.type_manager")

    # detailed logging_tmp switches
    log_observation_blocks = False
    log_observation_entities = False
    log_mission_spec = False
    log_last_command = False

    @staticmethod
    def set_all_non_root_loggers(level: int):
        MalmoLoggers.character_logger.setLevel(level)
        MalmoLoggers.obs_logger.setLevel(level)
        MalmoLoggers.calc_logger.setLevel(level)
        MalmoLoggers.command_logger.setLevel(level)
        MalmoLoggers.memory_logger.setLevel(level)
        MalmoLoggers.inventory_logger.setLevel(level)

        MalmoLoggers.action_logger.setLevel(level)
        MalmoLoggers.kqml_logger.setLevel(level)

        MalmoLoggers.planner_logger.setLevel(level)
        MalmoLoggers.jshop2_logger.setLevel(level)
        MalmoLoggers.malmo_planner_logger.setLevel(level)
        MalmoLoggers.bindings_logger.setLevel(level)
        MalmoLoggers.trace_logger.setLevel(level)
        MalmoLoggers.type_logger.setLevel(level)
