
import logging
from typing import Dict

base_loggers = {
    'root': logging.INFO,
}

#DEFAULT_FORMAT = '%(asctime)s - %(name)s - %(levelname)s - %(message)s'

QUIET_FORMAT = "%(levelname)5s %(message)s"
TERSE_FORMAT = "[%(threadName)s] %(filename)s:%(lineno)3s %(funcName)s().%(levelname)s  %(message)s"
FULL_FORMAT = "%(filename)s:%(lineno)3s %(funcName)s().%(levelname)s  %(message)s  [log:%(name)s]"

#use the following format for clickable file links (but very wide logging_tmp formats..)
LINKABLE_FORMAT = "%(pathname)s:%(lineno)3s %(name)s:%(funcName)s()]:%(levelname)7s:%(message)s"

DEFAULT_FORMAT = TERSE_FORMAT

class TraceLogger(logging.getLoggerClass()):
    TRACE = 5
    TRACE_V = 4 #verbose
    TRACE_VV = 3 #very verbose

    def __init__(self, name, level=logging.NOTSET):
        super().__init__(name, level)

    def trace(self, msg, *args, **kwargs):
        """
        Log 'msg % args' with severity 'TraceLogger.TRACE'.

        To pass exception information, use the keyword argument exc_info with
        a true value, e.g.

        logger.debug("Houston, we have a %s", "thorny problem", exc_info=1)
        """
        if self.isEnabledFor(TraceLogger.TRACE):
            self._log(TraceLogger.TRACE, msg, args, **kwargs)

    def trace_v(self, msg, *args, **kwargs):
        """
        Log 'msg % args' with severity 'TraceLogger.TRACE_V' (verbose tracing).

        To pass exception information, use the keyword argument exc_info with
        a true value, e.g.

        logger.debug("Houston, we have a %s", "thorny problem", exc_info=1)
        """
        if self.isEnabledFor(TraceLogger.TRACE_V):
            self._log(TraceLogger.TRACE_V, msg, args, **kwargs)

    def trace_vv(self, msg, *args, **kwargs):
        """
        Log 'msg % args' with severity 'TraceLogger.TRACE_VV' (very verbose tracing).

        To pass exception information, use the keyword argument exc_info with
        a true value, e.g.

        logger.debug("Houston, we have a %s", "thorny problem", exc_info=1)
        """
        if self.isEnabledFor(TraceLogger.TRACE_VV):
            self._log(TraceLogger.TRACE_VV, msg, args, **kwargs)

    def log_lines(self, lines, level: int = logging.INFO, *args, **kwargs):
        if self.isEnabledFor(level):
            for line in lines:
                self._log(level, line, args, **kwargs)

    def debug_lines(self, lines):
        self.log_lines(lines, logging.DEBUG)

logging.addLevelName(TraceLogger.TRACE, "TRACE")
logging.addLevelName(TraceLogger.TRACE_V, "TRACE_V")
logging.addLevelName(TraceLogger.TRACE_VV, "TRACE_VV")
logging.setLoggerClass(TraceLogger)

class MultilineFormatter(logging.Formatter):
    def format(self, record: logging.LogRecord):
        output = ""
        if record.msg.find("\n") < 0:
            output += super().format(record)
        else:
            orig_msg = record.msg
            endl = ""
            for line in orig_msg.splitlines():
                record.msg = line
                output += endl + super().format(record)
                endl = "\n"
            record.msg = orig_msg

        record.message = output
        return output


class LogManager:
    """
    A convenience class to hide the complexity of initializing and obtaining loggers.
    All methods in the class are static, so this class is just a container -- no instance is needed.

    The default output is to the console handler at the DEBUG level via init_with_console_handler().
    If you need a different output handler, add a method init_with_XXX_handler().

    *** Examples of using this class are in 'test/test_logging.py' ***

    Briefly, to obtain and use a logger accepting its current (possibly inherited) level, you simply call:

      logger = LogManager.get_logger('phyg.module.poe')
      logger.info("...")
      logger.debug("...")
      etc.

    To obtain and use a logger while explicitly setting its level to INFO (evermore), simply call:
      logger = LogManager.get_logger_setting_level('phyg.module.raven', logging_tmp.INFO)
      logger.info("...")
      logger.debug("...")
      etc.

    """

    """Convenience members linking to standard and custom logging_tmp levels.
       Eliminates needing to import logging_tmp for these variables. """
    CRITICAL = logging.CRITICAL
    FATAL = CRITICAL
    ERROR = logging.ERROR
    WARNING = logging.WARNING
    WARN = WARNING
    INFO = logging.INFO
    DEBUG = logging.DEBUG
    TRACE = TraceLogger.TRACE
    NOTSET = logging.NOTSET

    _logger = None

    base_loggers_created = False
    format_str = DEFAULT_FORMAT
    formatter = None
    handlers = dict()  #type: typing.Dict[str, logging.Handler]


    def __init__(self):
        pass

    @staticmethod
    def enable_debugging():
        LogManager.__init()
        LogManager._logger.setLevel(logging.DEBUG)
        LogManager.add_console_handler()

    @staticmethod
    def disable_debugging():
        LogManager.__init()
        LogManager._logger.setLevel(logging.INFO)
        LogManager.add_console_handler()

    #########################################
    #
    # INITIALIZERS
    #
    #########################################

    @staticmethod
    def init_with_console_handler(level: int = logging.INFO, format_str = DEFAULT_FORMAT):
        LogManager.__init(format_str)
        LogManager.add_console_handler(level)

    @staticmethod
    def __init():
        if LogManager._logger is None:
            LogManager._logger = logging.getLogger('phyg.utils.LogManager')
        if LogManager.formatter is None:
            LogManager.set_format(LogManager.format_str)


    #########################################
    #
    # FORMATTERS
    #
    #########################################

    @classmethod
    def set_format(cls: 'LogManager', format_str: str = DEFAULT_FORMAT):
        cls.format_str = format_str
        # LogManager.formatter = logging_tmp.Formatter(LogManager.format_str)
        cls.formatter = MultilineFormatter(LogManager.format_str)
        cls._logger.info("Set format string to '%s' and formatter to %s",
                                   LogManager.format_str, LogManager.formatter)

    #########################################
    #
    # HANDLERS
    #
    #########################################

    @staticmethod
    def add_console_handler(level: int = TraceLogger.TRACE):
        """Creates a console handler at level or updates the existing console handler to that level if it already exists."""
        handler_name = 'console'
        if handler_name not in LogManager.handlers:
            LogManager._logger.debug("Creating '%s' handler", handler_name)
            ch = logging.StreamHandler()
            ch.setLevel(level)
            ch.setFormatter(LogManager.formatter)
            LogManager.handlers[handler_name] = ch
        else:
            ch = LogManager.handlers['console']
            ch.setLevel(level)

    @staticmethod
    def get_console_handler():
        LogManager.__define_console_handler_if_missing()
        ch = LogManager.handlers['console']
        return ch

    @staticmethod
    def __define_default_console_handler_if_no_handlers(level: int = TraceLogger.TRACE):
        if len(LogManager.handlers) == 0:
            LogManager.__init()
            LogManager.add_console_handler(level)

    @staticmethod
    def __define_console_handler_if_missing(level: int = TraceLogger.TRACE):
        if 'console' not in LogManager.handlers:
            LogManager.add_console_handler(level)

    @staticmethod
    def __add_handlers(logger: logging.Logger):
        """Adds all known handlers to the logger."""
        LogManager._logger.debug("Adding %s handler(s) for %s", len(LogManager.handlers), logger.name)
        for (handler_name, handler) in LogManager.handlers.items():
            LogManager._logger.debug("  adding '%s' handler", handler_name)
            logger.addHandler(handler)
        LogManager._logger.debug("finished adding handlers for %s", logger.name)

    #########################################
    #
    # BASE LOGGERS
    #
    #########################################

    @staticmethod
    def set_root_level(level: int = logging.INFO):
        root = logging.getLogger()
        root.setLevel(level)



    @staticmethod
    def __define_base_loggers_if_needed(root_level: int = logging.INFO):
        """Define root and base loggers for phyg system"""
        if LogManager.base_loggers_created == False:
            LogManager.__define_root_logger(root_level)
            for (logger_name, level) in base_loggers.items():
                if logger_name == 'root':
                    LogManager.set_root_level(level)
                else:
                    LogManager.__define_base_logger('phyg', level)
            LogManager.base_loggers_created = True

    @staticmethod
    def __define_root_logger(level: int = logging.INFO) -> logging.Logger:
        logger = logging.getLogger()
        logger.setLevel(level)
        LogManager.__add_handlers(logger)

        return logger

    @staticmethod
    def __define_base_logger(name: str = None, level: int = logging.INFO) -> logging.Logger:
        logger = logging.getLogger(name)
        logger.setLevel(level)
        LogManager._logger.debug("defined base logger - %s", name)
        return logger


    #########################################
    #
    # LOGGER ACCESS METHODS
    #
    #########################################

    @staticmethod
    def get_root_logger() -> TraceLogger:
        """Returns a the root logger from the standard library logging_tmp, ensuring that a minimum console handler is created"""
        LogManager.__define_default_console_handler_if_no_handlers()
        LogManager.__define_root_logger()
        logger = logging.getLogger()
        return logger


    @staticmethod
    def get_logger(name: str) -> TraceLogger:
        """Returns a logger from the standard library logging_tmp, ensuring that parent loggers are defined
        to the level and a minimum console handler is created"""
        LogManager.__define_default_console_handler_if_no_handlers()
        LogManager.__define_base_loggers_if_needed()
        logger = logging.getLogger(name)
        return logger

    @staticmethod
    def get_logger_setting_level(name: str, level: int) -> TraceLogger:
        """Returns a logger from the standard library logging_tmp, ensuring that parent loggers are initialized
        to the level and connected to any existing handlers"""
        logger = LogManager.get_logger(name)
        logger.setLevel(level)
        return logger

