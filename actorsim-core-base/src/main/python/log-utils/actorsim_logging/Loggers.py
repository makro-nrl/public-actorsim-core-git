import logging

from actorsim_logging.LogManager import LogManager, QUIET_FORMAT

root_logger = LogManager.get_logger('actorsim')
root_logger.setLevel(LogManager.DEBUG)


class Loggers():
    """A convenience class for accessing the common loggers in the MalmoConnector."""
    root_name = 'actorsim'

    root_logger = LogManager.get_logger(root_name)

    # config loggers
    config_logger = LogManager.get_logger(root_name + '.config.txt')

    @classmethod
    def set_all_non_root_loggers(cls, level: int):
        attributes = [i for i in cls.__dict__.keys() if i[:1] != '_']
        attributes.extend([i for i in Loggers.__dict__.keys() if i[:1] != '_'])

        for attribute in attributes:
            if attribute.endswith("_logger") \
                    and attribute != "root_logger":
                logger = getattr(cls, attribute)
                logger.setLevel(level)
                level_name = logging.getLevelName(level)
                root_logger.info("Set to {} logger {}".format(level_name, logger))

