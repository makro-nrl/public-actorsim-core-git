
import argparse
import importlib
import os
from typing import List

import sys as _sys

from actorsim_logging.LogManager import LogManager
from actorsim_experiments.experiment_loggers import ExperimentLoggers

logger = ExperimentLoggers.experiment_root_logger


def parse_args_adding_unknown(parser, args, namespace):
    known, unknown_args = parser.parse_known_args(args, namespace)

    unknown_options = dict()
    key = None
    for arg in unknown_args:
        arg = arg.strip()
        if arg == "":
            continue
        if arg.startswith('--'):
            eq_start = arg.find("=")
            if eq_start > 0:
                key = make_valid_key(arg[2:eq_start])
                value = arg[(eq_start+1):]
                unknown_options[key] = value
            else:
                # key without an equals
                key = make_valid_key(arg[2:])  # type: str
        else:
            key_value = key
            arg_value = arg
            if key is None:
                key_value = arg
                arg_value = None
            unknown_options[key_value] = arg_value

    for (key, value) in unknown_options.items():
        converted = convert_value(value)
        setattr(namespace, key, converted)

def make_valid_key(key):
    return key.replace("-", "_")

def convert_value(value):
    new_value = value
    if new_value is None:
        new_value = ""

    try:
        # try to eval as int
        if new_value.startswith("int("):
            return eval(new_value)
    except:
        pass

    try:
        # try to eval as float
        if new_value.startswith("float("):
            return eval(new_value)
    except:
        pass

    try:
        # try to eval as string
        if new_value.startswith("str("):
            return eval(new_value)
    except:
        pass

    try:
        new_value = int(value)
        return new_value
    except:
        pass

    try:
        new_value = float(value)
        return new_value
    except:
        pass

    try:
        if new_value.lower() == "false":
            return False
    except:
        pass

    try:
        if new_value.lower() == "true":
            return True
    except:
        pass

    try:
        # try to parse as an array
        if new_value.startswith("["):
            return eval(new_value)
    except:
        pass

    return new_value

class LoadFromFile(argparse.Action):
    def __call__ (self, parser, namespace, values, option_string=None):

        with values as f:
            lines = f.readlines()

        trimmed_lines = str()
        for line in lines:  # type: str
            trimmed = line
            comment_start = line.find("#")
            if comment_start >= 0:
                trimmed = trimmed[:comment_start] + "\n"
            if trimmed != "":
                trimmed_lines += " " + trimmed

        if len(trimmed_lines) > 0:
            extra_args = trimmed_lines.split("\n")
            parse_args_adding_unknown(parser, extra_args, namespace)



class ExperimentBase:
    def __init__(self, special_args = None):
        self.use_relative_path = True
        self.experiments_root_path = None
        self.filter_path = None  # type: str
        self.config_paths = []  # type: List[os.path]
        self.special_config_paths = []  # type: List[os.path]
        self.file_paths = []  # type: List[os.path]
        self.directory_paths = []  # type: List[os.path]

        # self.args is a local namespace to collect arguments from defaults and files
        if special_args is None:
            self.args = argparse.Namespace()
        else:
            self.args = argparse.Namespace(**special_args)

        self.argparser = argparse.ArgumentParser()
        self.argparser.add_argument('--file', type=open, action=LoadFromFile)
        self.argparser.add_argument('--info', '-i', default=False, help='turn on info statements', action='store_true')
        self.argparser.add_argument('--debug', '-d', default=False, help='turn on debugging', action='store_true')
        self.argparser.add_argument('--trace', '-t', default=False, help='turn on tracing', action='store_true')
        self.argparser.add_argument('--max-runs', type=int, default=1, help='the maximum number of runs - default is 1')
        self.argparser.add_argument('--run-start', type=int, default=1, help='the starting run number - default is 1')
        self.argparser.add_argument('--mkdir', default=False, help='make variation directories if missing', action='store_true')
        self.argparser.add_argument('--env', type=str, default=None, help='the name of the environment to load')
        self.argparser.add_argument('--testing-environment', default=False, help='whether we are in a testing environment', action='store_true')
        self.argparser.add_argument('--env-requires-valid-display', default=False, help='whether the environment requires a valid DISPLAY variable', action='store_true')
        self.argparser.add_argument('--env-requires-nonroot-display', default=False, help='whether the environment requires DISPLAY != ":0"', action='store_true')

        args_in = None
        if str(_sys.argv[0]).find("unit"):
            args_in = _sys.argv[1:]
            #args_in.append("--testing-environment")
        parse_args_adding_unknown(self.argparser, args_in, self.args)
        args = self.args

        if self.args.info:
            ExperimentLoggers.set_all_non_root_loggers(LogManager.INFO)
        if self.args.debug:
            ExperimentLoggers.set_all_non_root_loggers(LogManager.DEBUG)
        if self.args.trace:
            ExperimentLoggers.set_all_non_root_loggers(LogManager.TRACE)

    def add_argument_if_missing(self, key, value):
        valid_key = make_valid_key(key)
        if not hasattr(self.args, valid_key):
            setattr(self.args, valid_key, value)

    def add_or_override_argument(self, key, value):
        valid_key = make_valid_key(key)
        setattr(self.args, valid_key, value)

    def print_args(self, header="Current arguments", log_func=logger.info, prefix="  ", filter_func = None, exclude_private=True):
        log_func("{}".format(header))
        attribute_names = dir(self.args)
        for attribute_name in attribute_names:
            if filter_func is not None:
                if not filter_func(attribute_name):
                    continue
            if exclude_private:
                if attribute_name.startswith("_"):
                    continue
            log_func("{}{}={}".format(prefix, attribute_name, getattr(self.args, attribute_name)))

    def trim_path(self, path):
        trimmed = path
        if self.use_relative_path:
            trimmed = path.replace(self.experiments_root_path, "")
            if trimmed.startswith("/"):
                trimmed = trimmed[1:]
        return trimmed

    def trim_path(self, path):
        trimmed = path
        if self.use_relative_path:
            trimmed = path.replace(self.experiments_root_path, "")
            if trimmed.startswith("/"):
                trimmed = trimmed[1:]
        return trimmed

    def log_path(self, path):
        trimmed = path
        if self.use_relative_path:
            trimmed = path.replace(self.experiments_root_path, "[root]")
        return trimmed

    def _collect_directories_and_files(self, starting_path):
        if not os.path.exists(starting_path):
            logger.error("starting path does not exist: {}".format(starting_path))
        for root_path, dirs, files  in os.walk(starting_path):
            dir_path = self.trim_path(root_path)

            if dir_path.find(".git") >= 0 \
                    or dir_path.find("__pycache__") >= 0:
                logger.trace("  skipping {}".format(dir_path))
                dirs.clear()
                continue

            if self.filter_path is not None:
                filter_path = self.filter_path
                logger.trace("testing root '{}'".format(dir_path))
                logger.trace("with filter  '{}'".format(filter_path))
                find_value = self.filter_path.find(dir_path)
                logger.trace("  find_value: {}".format(find_value))
                if find_value >= 0:
                    logger.trace("  processing {}".format(dir_path))
                else:
                    logger.trace("  skipping path {}".format(dir_path))
                    continue

            if dir_path != "":
                self.directory_paths.append(dir_path)
            if self.use_relative_path:
                logger.debug("  relative directory: {}".format(dir_path))
            else:
                logger.debug("  directory: {}".format(dir_path))


            files = files
            for file in files:
                full_path = os.path.join(root_path, file)
                log_path = self.log_path(full_path)
                logger.trace("full_path: {}".format(full_path))
                self.file_paths.append(full_path)
                printed_file = False

                if full_path.endswith("~"):
                    logger.debug("  Skipping temp file: {}".format(log_path))
                    continue

                (head, tail) = os.path.split(full_path)
                if tail.startswith("config"):
                    logger.debug("    Found config file: {}".format(log_path))
                    printed_file = True
                    self.config_paths.append(full_path)

                if tail.endswith(".config"):
                    logger.debug("    Found special config file: {}".format(log_path))
                    printed_file = True
                    self.special_config_paths.append(full_path)

                if self._process_file(full_path):
                    printed_file = True

                if not printed_file:
                    logger.debug("    file: {}".format(log_path))

    def _process_file(self, path):
        printed_file = False
        return printed_file

    def read_config(self, path):
        logger.info("Reading config file: {}".format(self.trim_path(path)))
        file_arg = ['--file', path ]
        self.argparser.parse_args(args=file_arg, namespace=self.args)

    def load_tmp_module(self, name, path):
        spec = importlib.util.spec_from_file_location(name, path)
        module = importlib.util.module_from_spec(spec)
        spec.loader.exec_module(module)
        return module

    def find_file(self, file_name) -> os.path:
        for file_path in self.file_paths:
            (head, tail) = os.path.split(file_path)
            if tail == file_name:
                return file_path
        logger.debug("Could not find file: {}".format(file_name))
        return None


