import inspect
import os
from collections import OrderedDict
from inspect import Signature

from actorsim_experiments.experiment_base import ExperimentBase, logger, LoadFromFile
from actorsim_experiments.run import Run


class Sample(ExperimentBase):
    """The base class for a sample of runs for a single algorithm or approach.

       Usually, a sample will be the leaf directory directly above a set of runNNN directories.
    """

    def __init__(self, path: os.path, base: ExperimentBase = None, special_args=None):
        ExperimentBase.__init__(self, special_args=special_args)
        self.base = base
        if self.base is None:
            from actorsim_experiments.directory_base import DirectoryBase
            self.base = DirectoryBase()
            self.base.set_path(path)
        self.experiments_root_path = self.base.experiments_root_path
        self.use_relative_path = self.base.use_relative_path

        if self.use_relative_path:
            self.sample_path = os.path.join(self.base.experiments_root_path, path)
        else:
            self.sample_path = path
        if not os.path.exists(self.sample_path):
            if self.args.mkdir:
                logger.info("Creating sample dir: {}".format(self))
                os.makedirs(self.sample_path)
            else:
                msg = "Sample dir does not exist: {}".format(self)
                logger.warning(msg)


        self.runs = OrderedDict()
        self.missing_runs = OrderedDict()  # type: Dict[str, Run]

    def __repr__(self):
        if self.use_relative_path:
            return self.sample_path.replace(self.base.experiments_root_path, "[root]")
        else:
            return self.sample_path

    def get_run_strings(self):
        created_str = "created: "
        sep = ""
        for created_run in self.runs.keys():
            created_str += sep + created_run
        missing_str = "missing: "
        sep = ""
        for missing_run in self.missing_runs.keys():
            missing_str += sep + missing_run
            sep = ", "
        return created_str, missing_str

    def load(self):
        logger.info("Loading sample: {}".format(self))
        self.filter_path = self.sample_path
        self._collect_directories_and_files(self.base.experiments_root_path)
        self.load_config()
        self.load_runs()

    def count_directories(self, path: str):
        return path.count("/")

    def load_config(self, default_funcs = []):
        args = self.args  # convenient for debugging
        for default_func in default_funcs:
            logger.info("Loading defaults from {}".format(default_func))
            sig= inspect.Signature.from_callable(default_func)
            for param in sig.parameters.values():  # type: inspect.Parameter
                logger.trace("  capturing param:{} default-value:{}".format(param.name, param.default))
                if param.default is not inspect.Parameter.empty:
                    self.add_argument_if_missing(param.name, param.default)


        logger.info("Checking {} '.config' files matching whole directory names".format(len(self.special_config_paths)))
        special_config_list = sorted(self.special_config_paths, key=self.count_directories)
        for config_path in special_config_list:
            logger.trace("  checking {}".format(config_path))
            head, tail = os.path.split(config_path)
            config_filename = tail.replace(".config", "")
            if config_path.find("/") > 0:
                (head, tail) = config_filename.split()
                config_filename = tail
            if self.sample_path.find(config_filename) > 0:
                self.read_config(config_path)

        logger.info("Loading {} config files that start with 'config' in top-down directory order".format(len(self.config_paths)))
        sorted_list = sorted(self.config_paths, key=self.count_directories)
        for config_path in sorted_list:
            self.read_config(config_path)


    def load_runs(self):
        full_path = self.sample_path
        if self.base.use_relative_path:
            full_path = os.path.join(self.experiments_root_path, self.sample_path)

        for run_num in range(1, self.args.max_runs + 1):
            run_name = "run{:0>3}".format(run_num)
            logger.debug("  Checking {}".format(run_name))
            run_dir = os.path.join(full_path, run_name)
            run = Run(self, run_dir)
            if run.is_missing():
                self.missing_runs[run_name] = run
            else:
                logger.debug("  run_name {} found".format(run_name))

    def confirm_param(self, param, value):
        if hasattr(self.args, param):
            converted = LoadFromFile.convert_value(value)
            if self.args.__getattribute__(param) == converted:
                return True
        return False

    def confirm_param_missing(self, param):
        if not hasattr(self.args, param):
            return True
        return False

    def get_run_path(self, run):
        return os.path.join(self.sample_path, run)


