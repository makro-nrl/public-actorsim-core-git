from actorsim_experiments.directory_base import DirectoryBase
from actorsim_experiments.run import Run
from actorsim_experiments.sample import Sample


class Procedure:
    """An interface for running a particular kind of procedure.
       Calling execute should instantiate the run.environment
       and execute a procedure on that environment.
    """

    def __init__(self, run: Run, default_funcs = []):
        self.run = run  # type: Run
        self.sample = run.sample  # type: Sample
        self.base = run.sample.base  # type: DirectoryBase
        self.default_funcs = default_funcs


    def execute(self):
        pass