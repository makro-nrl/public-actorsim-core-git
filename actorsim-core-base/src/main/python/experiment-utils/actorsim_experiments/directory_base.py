import os
import shutil
from inspect import Signature
from typing import List, Dict

from actorsim_experiments.experiment_base import ExperimentBase, logger
from actorsim_experiments.run import Run
from actorsim_experiments.sample import Sample

class DirectoryBase(ExperimentBase):
    """Hold details relevant to running and reporting for an entire experiments directory.

    An experiment directory consists of one or more samples stored in the following directory structure:

    experiments_base_dir  (MUST start with 'experiments')
      [   (can be nested multiple times)
        /aaai2020traces[_param=value]  (param-value pairs distinguish specific parameters (e.g., /seed=1000)
          config.txt.txt    (optional: for parameters of all experiments_dir_tracegen (e.g., seed, num_runs, run_start)
      ]
          /algName[_param=value] (param-value pairs distinguish algorithm instantiations (e.g., _learnRate=0.99, _learnRate=0.97)
          configAlgorithm.txt - complete algorithm parameters used
          configRun.txt - complete run parameters not already specified
            [  (may be repeated)
              /runNNN
                  runDetails
                  logging files
                  models
            ]

    """
    def __init__(self):
        ExperimentBase.__init__(self)
        self.base_path = None
        self.experiments_dir = None
        self.reset()

    def reset(self):
        self.samples = dict()  # type: Dict[Set(str)]
        self.variations_paths = []

    def set_path(self, path: os.path):
        """Loads base details for an experiment directory from a given path."""
        logger.info("Loading experiment dir based on path: {}".format(path))

        parent = path
        while self.experiments_dir is None:
            (head, tail) = os.path.split(parent)  # type (os.path, str)

            logger.debug("checking directory {}".format(tail))

            if tail.startswith("experiments"):
                self.experiments_dir = tail
                self.base_path = head
                self.experiments_root_path = parent

            parent = head

        if self.experiments_root_path != path:
            self.filter_path = path

        logger.info("base_path: {}".format(self.base_path))
        logger.info("experiments_root: {}".format(self.experiments_dir))
        logger.info("experiments_root_path: {}".format(self.experiments_root_path))
        logger.info("filter_path: {}".format(self.filter_path))

    def completely_load_directory(self):
        logger.debug("Loading directory {}".format(self.filter_path))
        self.reset()
        self._collect_directories_and_files(self.experiments_root_path)
        self._collect_samples()

    def _process_file(self, full_path):
        printed_file = False
        (head, tail) = os.path.split(full_path)
        if tail.startswith("variations"):
            logger.debug("    Found variations file: {}".format(tail))
            printed_file = True
            self.variations_paths.append(full_path)
        return printed_file

    def _collect_samples(self):
        self._load_variations()
        self._collect_samples_not_within_variations()

    def _load_variations(self):
        for variation_path in self.variations_paths:
            full_path = variation_path
            if self.use_relative_path:
                full_path = os.path.join(self.experiments_root_path, variation_path)
            self._load_variation_impl(full_path)

    def _load_variation_impl(self, variation_path):
        module = self.load_tmp_module(name="variationHelper", path=variation_path)
        sample_name = getattr(module, "sample_name")
        vars = self._get_sample_variables(sample_name, module)
        logger.info("Loading variations for {}".format(sample_name))

        sample_names = self._replace_variables(sample_name, vars)
        for (sample_name, substitutions) in sample_names.items():
            (head, tail) = os.path.split(variation_path)
            sample_path = os.path.join(head, sample_name)
            self.load_sample(sample_path, special_args=substitutions)

    def _get_sample_variables(self, sample_name: str, module):
        variables = dict()
        tokens = sample_name.split("{")
        for token in tokens:
            end = token.find("}")
            if end > 0:
                variable = token[:end]
                if hasattr(module, variable):
                    variables[variable] = getattr(module, variable)
                else:
                    logger.warning("Variable {} appears to be missing from your variations file.".format(variable))
        return variables

    def _replace_variables(self, sample_name, vars: Dict):
        sample_names = dict()
        var_names = list(vars.keys())
        substitutions = dict()
        self._replace_variables_impl(sample_name, var_names, vars, sample_names, 0, substitutions)
        return sample_names

    def _replace_variables_impl(self, sample_name: str, var_names, vars, sample_names: Dict[str, Dict[str, str]], i, subtitutions: Dict[str, str]):
        if i == len(var_names):
            sample_names[sample_name] = dict(subtitutions)
            return
        var_name = var_names[i]
        var_list = vars[var_name]
        if not isinstance(var_list, (list, tuple)):
            var_list = [var_list]
        for value in var_list:
            key = "{" + var_name + "}"
            new_subtitutions = dict(subtitutions)
            replaced_sample_name = sample_name.replace(key, str(value))
            new_subtitutions[var_name] = str(value)
            self._replace_variables_impl(replaced_sample_name, var_names, vars, sample_names, i+1, new_subtitutions)

    def _collect_samples_not_within_variations(self):
        for dir_path in self.directory_paths:
            (head, tail) = os.path.split(dir_path)
            sample_name = None

            if tail.startswith("run"):
                if not head in self.samples:
                    sample_name = head

            self.load_sample(sample_name)

    def load_sample(self, sample_path, special_args = None):
        if sample_path is not None\
                and sample_path not in self.samples:
            sample = Sample(sample_path, base=self, special_args=special_args)
            sample.load()
            self.samples[sample_path] = sample

    def sample_summary(self):
        """Find all sample directories, load related config.txt files for them and summarize sample status"""
        logger.info("Sample dirs:")
        for (sample_dir, sample) in self.samples.items():
            created_str, missing_str = sample.get_run_strings()
            logger.info("  {}: \n\t{}\n\t{}".format(sample_dir, created_str, missing_str))

    def calculate_missing_runs(self):
        """Returns the paths of missing runs."""
        missing_runs = []  # type: List[Run]
        for (sample_dir, sample) in self.samples.items():
            created_str, missing_str = sample.get_run_strings()
            logger.debug("  {} has runs {} but missing {}".format(sample, created_str, missing_str))
            for (run_path, run) in sample.missing_runs.items():
                missing_runs.append(run)
        return missing_runs

    def delete_all_samples(self):
        for (sample_dir, sample) in self.samples.items():
            if os.path.exists(sample_dir):
                shutil.rmtree(sample_dir, ignore_errors=True)


