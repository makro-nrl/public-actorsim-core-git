import os

import time

from actorsim_experiments.experiment_base import logger


class Run:
    STARTED_FILENAME = "started.txt"
    COMPLETED_FILENAME = "completed.txt"
    ARGS_USED = "args_used.txt"

    def __init__(self, sample: 'Sample', name: str):
        self.environment_start_time_out_in_seconds = 60
        self.sample = sample
        self.name = name
        self.path = os.path.join(self.sample.sample_path, self.name)
        self.started_path = os.path.join(self.path, Run.STARTED_FILENAME)
        self.completed_path = os.path.join(self.path, Run.COMPLETED_FILENAME)
        self.args_used_path = os.path.join(self.path, Run.ARGS_USED)
        self.env_module = None
        self.procedure_module = None

    def __repr__(self):
        if self.sample.use_relative_path:
            return self.path.replace(self.sample.base.experiments_root_path, "[root]")
        else:
            return self.path

    def __str__(self):
        return self.__repr__()

    def is_missing(self) -> bool:
        return not os.path.exists(self.path)

    def is_started(self) -> bool:
        return os.path.exists(self.started_path)

    def is_completed(self) -> bool:
        return os.path.exists(self.completed_path)

    def ensure_path(self):
        if self.is_missing():
            os.makedirs(self.path)

    def create_start_file(self) -> bool:
        """Creates the start file for this run.  If the start file already exists, returns False."""
        if not self.is_started():
            self.ensure_path()
            with open(self.started_path, mode='w') as file:
                file.write("started run")
            return True
        return False

    def create_completed_file(self):
        """Creates the start file for this run.  If the start file already exists, returns False."""
        self.ensure_path()
        with open(self.completed_path, mode='w') as file:
            file.write("started run")

    def write_config_details(self):
        args = self.sample.args
        attributes = dir(args)
        with open(self.args_used_path, mode='w') as file:
            for key in attributes:
                if key.startswith("_"):
                    continue
                value = getattr(args, key)
                line = "--{}={}\n".format(key,value)
                file.write(line)

    def execute(self):
        if self.prepare_procedure():
            self.prepare_env()

            args = self.sample.args
            if not args.testing_environment:
                if hasattr(self.sample.args, "test_variables"):
                    msg = "You have specified testing variables but are no longer running in a test environment"
                    logger.error(msg)
                    raise Exception(msg)
                display = os.getenv("DISPLAY")
                if args.env_requires_nonroot_display:
                    if display == ":0":
                        msg = "The environment requires a non-root display (something other than DISPLAY=:0)"
                        logger.error(msg)
                        raise Exception(msg)
                if args.env_requires_valid_display:
                    if display == "":
                        msg = "The environment requires a valid DISPLAY variable, which is currently empty."
                        logger.error(msg)
                        raise Exception(msg)


            self.procedure.execute()
            self.close()
            self.create_completed_file()

    def close(self):
        if self.env_module is not None:
            self.env_module.close(self)

    def prepare_procedure(self) -> bool:
        logger.info("Preparing procedure for {}".format(self))
        procedure_name = getattr(self.sample.args, "procedure")
        if procedure_name is not None:
            logger.debug("Attempting to prepare {} procedure".format(procedure_name))
            procedure_path = self.sample.find_file(procedure_name + ".py")
            if procedure_path is not None:
                if os.path.exists(procedure_path):
                    self.procedure_module = self.sample.load_tmp_module(name="procedureHelper", path=procedure_path)
                    logger.info("Procedure {} module loaded.".format(procedure_name))
                    for attribute in dir(self.procedure_module):
                        if attribute.endswith("_Procedure"):
                            procedure_class = getattr(self.procedure_module, attribute)
                            self.procedure = procedure_class(self)
                    logger.info("Procedure {} module loaded.".format(procedure_name))
                    return True
        logger.debug("Could not load procedure.  Did you specify one in a config file or a variations file?".format())
        return False

    def load_procedure(self) -> bool:
        return False

    def prepare_env(self) -> bool:
        logger.info("Preparing environment for {}".format(self))
        args = self.sample.args
        env_name = getattr(args, "env")
        if env_name is not None:
            logger.debug("Attempting to prepare {} environment".format(env_name))
            env_path = self.sample.find_file(env_name + ".py")
            if env_path is not None:
                if os.path.exists(env_path):
                    self.env_module = self.sample.load_tmp_module(name="envHelper", path=env_path)
                    logger.info("Environment {} module loaded.".format(env_name))
                    self.env = self.env_module.prepare(self)
                    logger.info("Environment {} module loaded.  Use start_env() to start the environment.".format(env_name))
                    return True
        else:
            logger.debug("Could not load an environment.  Did you specify one in a config file?")
        return False

    def start_env(self) -> bool:
        self.env_module.start(self)

