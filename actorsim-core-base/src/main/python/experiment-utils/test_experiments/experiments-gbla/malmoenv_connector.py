import os
import signal
import subprocess

import datetime
import psutil
from character import CharacterState

from mission import build_mission_spec

import malmoenv
from actorsim_experiments.run import Run
from malmo_wrappers import wrap_malmo

def prepare(run: Run):
    args = run.sample.args
    run.sample.add_or_override_argument("env-requires-valid-display", True)

    run.sample.add_argument_if_missing("malmo-server", "127.0.0.1")
    run.sample.add_argument_if_missing("malmo-port", 10000)
    run.sample.add_argument_if_missing("malmo-resync", 0)
    run.sample.add_argument_if_missing("malmo-episode", 0)
    run.sample.add_argument_if_missing("malmo-replaceable", False)
    run.sample.add_argument_if_missing("malmo-install-dir", "/home/mroberts/virtualenv/gbla/Malmo")
    run.sample.add_argument_if_missing("malmo-show-image", True)
    run.sample.add_argument_if_missing("malmo-rendering-off", False)
    run.sample.add_argument_if_missing("malmo-video-height", 480)
    run.sample.add_argument_if_missing("malmo-video-width", 640)
    run.sample.add_argument_if_missing("malmo-time-in-ms", 5000)

    character = CharacterState()
    character.set_task(args.task)
    character.set_action_selection(args.action_selection)
    task_descriptor = character.get_task_descriptor()
    mission_spec = build_mission_spec(task_descriptor, args)
    character.main_goal.print_subgoals()

    env = malmoenv.Env(reshape=True)
    env.init(mission_spec,
             args.malmo_port,
             server=args.malmo_server,
             episode=args.malmo_episode,
             resync=args.malmo_resync)

    run.sample.print_args(header="Prepared environment with arguments:", prefix="  ", filter_func=lambda name : name.startswith("malmo"))

    env = wrap_malmo(env, character)
    from baselines import bench, logger
    logger.configure(dir=run.path)
    env = bench.Monitor(env, logger.get_dir())
    return env

def start(run: Run):
    launch_minecraft(run)
    process = run.sample.args.malmo_server_process  # type: subprocess.Popen
    minecraft_loading = True
    within_timeout = True
    start_time = datetime.datetime.now()
    from actorsim_experiments.experiment_base import logger
    logger.info("Waiting for malmo to load...")
    while minecraft_loading and within_timeout:
        from actorsim_experiments.experiment_base import logger
        for line in process.stdout:
            logger.info("malmo-line: {}".format(line))
            if str(line).find("CLIENT enter state: DORMANT") > 0:
                minecraft_loading = False
                break
            end_time = datetime.datetime.now()
            delta_time = end_time - start_time
            within_timeout = delta_time.total_seconds() < run.environment_start_time_out_in_seconds
            if not within_timeout:
                break
        if not minecraft_loading:
            logger.info("Malmo loaded")


def close(run: Run):
    server_process = getattr(run.sample.args, "malmo_server_process", None)  # type: subprocess.Popen
    if server_process is not None:
        pid = server_process.pid
        kill_child_processes(pid)

def kill_child_processes(parent_pid, sig=signal.SIGTERM):
    try:
      parent = psutil.Process(parent_pid)
    except psutil.NoSuchProcess:
      return
    children = parent.children(recursive=True)
    for process in children:
      process.send_signal(sig)

#copied from malmo-git/MalmoEnv/malmoenv/bootstrap.py
def launch_minecraft(run: Run):
    args = run.sample.args
    port = args.malmo_port
    installdir=args.malmo_install_dir
    replaceable=args.malmo_replaceable

    launch_script = './launchClient.sh'
    if os.name == 'nt':
        launch_script = 'launchClient.bat'
    cwd = os.getcwd()
    os.chdir(installdir)
    os.chdir("Minecraft")
    try:
        cmd = [launch_script, '-port', str(port), '-env']
        if replaceable:
            cmd.append('-replaceable')

        process = subprocess.Popen(cmd, stderr=subprocess.PIPE, stdout=subprocess.PIPE)
        setattr(run.sample.args, "malmo_server_process", process)
    finally:
        os.chdir(cwd)


