import os

from actorsim_experiments.procedure import Procedure
from actorsim_experiments.run import Run
from baselines.deepq import deepq



class DQN_Procedure(Procedure):
    def __init__(self, run: Run):
        Procedure.__init__(self, run, [deepq.learn])

    def execute(self):
        self.sample.load_config(self.default_funcs)
        self.run.create_start_file()

        self.run.prepare_env()
        self.run.start_env()

        os.chdir(self.run.path)
        self.run.write_config_details()

        args = self.sample.args  # convenient local var
        self.model = deepq.learn(
            self.run.env,
            args.network,
            convs=args.convs,
            hiddens=args.hiddens,
            dueling=args.dueling,
            lr=args.lr,
            total_timesteps=args.total_timesteps,
            buffer_size=args.buffer_size,
            exploration_fraction=args.exploration_fraction,
            exploration_final_eps=args.exploration_final_eps,
            train_freq=args.train_freq,
            learning_starts=args.learning_starts,
            target_network_update_freq=args.target_network_update_freq,
            gamma=args.gamma
        )

        # "conv_only",
        #  convs=[(32, 8, 4), (64, 4, 2), (64, 3, 1)],
        #  hiddens=[256],
        #  dueling=True,
        #  lr=1e-4,
        #  total_timesteps=int(1e7),
        #  buffer_size=10000,
        #  exploration_fraction=0.1,
        #  exploration_final_eps=0.01,
        #  train_freq=4,
        #  learning_starts=10000,
        #  target_network_update_freq=1000,
        #  gamma=0.99,

        self.model.save('{}_model.pkl'.format(args.task))
        self.run.env.close()

