import os

import sys

from LogManager import LogManager
from actorsim_experiments.sample import Sample
from actorsim_experiments.directory_base import DirectoryBase
from actorsim_experiments.experiment_loggers import ExperimentLoggers

logger = ExperimentLoggers.experiment_root_logger


def test_load_experiment_dir():
    ExperimentLoggers.set_all_non_root_loggers(LogManager.TRACE)
    path = os.path.join(os.getcwd(), 'experiments_dir_tracegen/aaai2020traces/seed=1000/carnivore-default')  # type os.path

    base = DirectoryBase()
    base.set_path(path)
    base.completely_load_directory()

    sample = Sample(path, base)
    sample.load()


def test_load_carnivore_only():
    ExperimentLoggers.set_all_non_root_loggers(LogManager.INFO)
    path = os.path.join(os.getcwd(), 'experiments_dir_tracegen/aaai2020traces/seed=1000/carnivore-default')  # type os.path
    base = DirectoryBase()
    base.set_path(path)

    sample = Sample(path, base)
    sample.load()
    assert(sample.confirm_param("carnivore-only", True))
    assert(sample.confirm_param_missing("herbivore-only"))
    assert(sample.confirm_param_missing("omnivore-only"))

def test_load_herbivore_only():
    ExperimentLoggers.set_all_non_root_loggers(LogManager.INFO)
    path = os.path.join(os.getcwd(), 'experiments_dir_tracegen/aaai2020traces/seed=1000/herbivore-default')  # type os.path
    base = DirectoryBase()
    base.set_path(path)

    sample = Sample(path, base)
    sample.load()
    assert(sample.confirm_param("herbivore-only", True))
    assert(sample.confirm_param("herbivore-special", "overridden"))
    assert(sample.confirm_param_missing("carnivore-only"))
    assert(sample.confirm_param_missing("omnivore-only"))


def test_load_single_sample_missing_runs():
    ExperimentLoggers.set_all_non_root_loggers(LogManager.INFO)
    path = os.path.join(os.getcwd(), 'experiments_dir_tracegen/aaai2020traces/seed=1000/carnivore-default')  # type os.path
    sample = Sample(path)
    sample.load()
    assert(len(sample.missing_runs) == 9)

    path = os.path.join(os.getcwd(), 'experiments_dir_tracegen/aaai2020traces/seed=1000/herbivore-default')  # type os.path
    sample = Sample(path)
    sample.load()
    assert(len(sample.missing_runs) == 3)


def test_load_empty_sample_missing_runs():
    ExperimentLoggers.set_all_non_root_loggers(LogManager.INFO)
    path = os.path.join(os.getcwd(), 'experiments_dir_tracegen/aaai2020traces/seed=1000/omnivore-default')  # type os.path
    sample = Sample(path)
    sample.load()
    assert(len(sample.missing_runs) == 5)


def test_missing_experiments():
    ExperimentLoggers.set_all_non_root_loggers(LogManager.INFO)
    path = os.path.join(os.getcwd(), 'experiments_dir_tracegen/aaai2020traces/seed=1000/carnivore-default')  # type os.path

    base = DirectoryBase()
    base.set_path(path)
    base.completely_load_directory()
    missing_runs = base.calculate_missing_runs()
    assert(len(missing_runs) == 17)

def test_comment_config():
    ExperimentLoggers.set_all_non_root_loggers(LogManager.INFO)
    path = os.path.join(os.getcwd(), 'experiments-gbla')  # type: os.path
    base = DirectoryBase()
    base.set_path(path)
    base.completely_load_directory()

def test_subdir_only_load():
    ExperimentLoggers.set_all_non_root_loggers(LogManager.INFO)
    path = os.path.join(os.getcwd(), 'experiments_dir_tracegen/aaai2020traces')  # type os.path

    base = DirectoryBase()
    base.set_path(path)
    base.completely_load_directory()
    missing_runs = base.calculate_missing_runs()
    assert(len(missing_runs) == 17)

def test_full_subdir_load():
    ExperimentLoggers.set_all_non_root_loggers(LogManager.INFO)
    path = os.path.join(os.getcwd(), 'experiments_dir_tracegen')  # type os.path

    base = DirectoryBase()
    base.set_path(path)
    base.completely_load_directory()
    missing_runs = base.calculate_missing_runs()
    assert(len(missing_runs) == 20)

def test_prepare_env_fails():
    ExperimentLoggers.set_all_non_root_loggers(LogManager.INFO)
    path = os.path.join(os.getcwd(), 'experiments-gbla/fail_env_load')  # type os.path

    base = DirectoryBase()
    base.set_path(path)
    base.completely_load_directory()
    missing_runs = base.calculate_missing_runs()
    assert(len(missing_runs) > 0)
    prepare_success = missing_runs[0].prepare_env()
    assert(not prepare_success)

def test_prepare_env_succeeds():
    ExperimentLoggers.set_all_non_root_loggers(LogManager.DEBUG)
    path = os.path.join(os.getcwd(), 'experiments-gbla/initial-runs')  # type os.path

    base = DirectoryBase()
    base.set_path(path)
    base.completely_load_directory()
    missing_runs = base.calculate_missing_runs()
    assert(len(missing_runs) > 0)
    prepare_success = missing_runs[0].prepare_env()
    assert(prepare_success)

def test_load_malmo_succeeds():
    ExperimentLoggers.set_all_non_root_loggers(LogManager.DEBUG)
    path = os.path.join(os.getcwd(), 'experiments-gbla/initial-runs')  # type os.path

    base = DirectoryBase()
    base.set_path(path)
    base.completely_load_directory()
    missing_runs = base.calculate_missing_runs()
    assert(len(missing_runs) > 0)
    run0 = missing_runs[0]
    prepare_success = run0.prepare_env()
    assert(prepare_success)
    try:
        run0.start_env()
    finally:
        run0.close()

def test_load_malmo_different_port():
    ExperimentLoggers.set_all_non_root_loggers(LogManager.DEBUG)
    path = os.path.join(os.getcwd(), 'experiments-gbla/initial-runs')  # type os.path

    sys.argv.append("--malmo-port=10002")

    base = DirectoryBase()
    base.set_path(path)
    base.completely_load_directory()
    missing_runs = base.calculate_missing_runs()
    assert(len(missing_runs) > 0)
    run0 = missing_runs[0]
    prepare_success = run0.prepare_env()
    assert(prepare_success)
    try:
        run0.start_env()
    finally:
        run0.close()

def test_start_malmo_dqn():
    ExperimentLoggers.set_all_non_root_loggers(LogManager.DEBUG)
    path = os.path.join(os.getcwd(), 'experiments-gbla/initial-runs')  # type os.path

    base = DirectoryBase()
    base.set_path(path)
    base.completely_load_directory()
    base.delete_all_samples()
    base.completely_load_directory()
    missing_runs = base.calculate_missing_runs()
    assert(len(missing_runs) > 0)
    run0 = missing_runs[0]  # type: Run
    try:
        run0.execute()
    finally:
        run0.close()

def test_run_malmo_dqn_all():
    ExperimentLoggers.set_all_non_root_loggers(LogManager.DEBUG)
    path = os.path.join(os.getcwd(), 'experiments-gbla/initial-runs')  # type os.path

    base = DirectoryBase()
    base.set_path(path)
    base.completely_load_directory()
    base.delete_all_samples()
    base.completely_load_directory()
    missing_runs = base.calculate_missing_runs()
    assert(len(missing_runs) > 0)
    for run in missing_runs:
        try:
            run.execute()
        finally:
            run.close()
