#!/usr/bin/env python
import sys
if sys.version_info < (3, 6):
    raise Exception("Must be using Python version >= 3.6")
assert sys.version_info >= (3, 6)

import os
import sys

from actorsim_logging.LogManager import LogManager
from actorsim_experiments.directory_base import DirectoryBase
from actorsim_experiments.experiment_loggers import ExperimentLoggers

directory=sys.argv[1]
logger = ExperimentLoggers.experiment_root_logger

ExperimentLoggers.set_all_non_root_loggers(LogManager.DEBUG)
path = os.path.join(os.getcwd(), directory)  # type os.path

base = DirectoryBase()
base.set_path(path)
base.completely_load_directory()
missing_runs = base.calculate_missing_runs()  # type: List[Run]

if len(missing_runs) > 1:
    missing_run = missing_runs[0]  # type: Run
    logger.info("Executing run {}".format(missing_run))
    try:
        missing_run.execute()
    finally:
        missing_run.close()
else:
    logger.info("All are finished")
