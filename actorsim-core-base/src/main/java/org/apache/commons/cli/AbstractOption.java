package org.apache.commons.cli;

public abstract class AbstractOption extends org.apache.commons.cli.Option {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    public AbstractOption(String shortOption, String longOption, boolean hasArg, String description) {
        super(shortOption, longOption, hasArg, description);
        setType(String.class);
    }

    public AbstractOption(String shortOption, boolean hasArg, String description) {
        super(shortOption, hasArg, description);
        setType(String.class);
    }

    /**
     * Sets the default value of this option.
     *
     * @param value the value
     */
    public abstract AbstractOption setDefault(Object value);

    /**
     * Sets the current or first value of this option.
     *
     * @param value the value
     */
    @SuppressWarnings("UnusedReturnValue")
    public abstract AbstractOption setValue(Object value);

    /**
     * Resets this option to its default value.
     */
    public abstract void resetToDefault();

    /**
     * Remove any current values that are set without setting values to the default.
     * To set values to the default, call <code> resetToDefault()</code>.
     */
    public abstract void clear();

    /**
     * Disables this option, which is interpreted differently based on the subclass.
     */
    public abstract void disable();

    /**
     * Enables this option, which is interpreted differently based on the subclass.
     */
    public abstract void enable();

    /**
     * Returns the first value or the default if the value is unset as an integer.
     * Throws a NumberFormatException if the value or default cannot be parsed into an integer or are both unset.
     *
     * @return the value
     */
    public abstract int getValueOrDefaultAsInt();

    /**
     * Returns the first value or the default as an integer.
     * Returns 0 if the value or default is cannot be parsed into an integer or the value is unset.
     *
     * @return the value
     */
    public abstract int getValueOrDefaultAsInt_NoThrow();


    /**
     * Returns the first value as an integer.
     * Throws a NumberFormatException if the value is cannot be parsed into an integer or the value is unset.
     *
     * @return the value
     */
    public abstract int getValueAsInt() throws NumberFormatException;

    /**
     * Returns the first value as an integer.
     * Returns 0 if the value is cannot be parsed into an integer or the value is unset.
     *
     * @return the value
     */
    public abstract int getValueAsInt_NoThrow();

    /**
     * Returns the first value or the default if the value is unset as a long.
     * Throws a NumberFormatException if the value or default cannot be parsed into an long or are both unset.
     *
     * @return the value
     */
    public abstract long getValueOrDefaultAsLong();

    /**
     * Returns the first value or the default as an long.
     * Returns 0 if the value or default is cannot be parsed into an long or the value is unset.
     *
     * @return the value
     */
    public abstract long getValueOrDefaultAsLong_NoThrow();


    /**
     * Returns the first value as an long.
     * Throws a NumberFormatException if the value is cannot be parsed into an long or the value is unset.
     *
     * @return the value
     */
    public abstract long getValueAsLong() throws NumberFormatException;

    /**
     * Returns the first value as an long.
     * Returns 0 if the value is cannot be parsed into an long or the value is unset.
     *
     * @return the value
     */
    public abstract long getValueAsLong_NoThrow();

    /**
     * Returns the first value or the default if the value is unset as a double.
     * Throws a NumberFormatException if the value or default cannot be parsed into an double or are both unset.
     *
     * @return the value
     */
    public abstract double getValueOrDefaultAsDouble();

    /**
     * Returns the first value or the default as an double.
     * Returns 0 if the value or default is cannot be parsed into an double or the value is unset.
     *
     * @return the value
     */
    public abstract double getValueOrDefaultAsDouble_NoThrow();


    /**
     * Returns the first value as an double.
     * Throws a NumberFormatException if the value is cannot be parsed into an double or the value is unset.
     *
     * @return the value
     */
    public abstract double getValueAsDouble() throws NumberFormatException;

    /**
     * Returns the first value as an double.
     * Returns 0 if the value is cannot be parsed into an double or the value is unset.
     *
     * @return the value
     */
    public abstract double getValueAsDouble_NoThrow();

    /**
     * Returns the first value or the default if the value is unset as a boolean.
     * Throws a NumberFormatException if the value or default cannot be parsed into boolean or are both unset.
     *
     * @return the value
     */
    public abstract boolean getValueOrDefaultAsBoolean() throws NumberFormatException;

    /**
     * Returns the first value or the default as a boolean.
     * Returns false if the value or default is cannot be parsed into a boolean or the value is unset.
     *
     * @return the value
     */
    public abstract boolean getValueOrDefaultAsBoolean_NoThrow();

    /**
     * Returns the first value as a boolean.
     * Throws a NumberFormatException if the value is cannot be parsed into a boolean or the value is unset.
     *
     * @return the value
     */
    public abstract boolean getValueAsBoolean() throws NumberFormatException;

    /**
     * Returns the first value as a boolean..
     * Returns 0 if the value is cannot be parsed into a boolean or the value is unset.
     *
     * @return the value
     */
    public abstract boolean getValueAsBoolean_NoThrow();


    /////////////////////////////////////////////
    //
    //  Object Overrides
    //
    /////////////////////////////////////////////
    @Override
    public String toString() {
        String string = getOpt();
        if (this.hasArg()) {
            string += this.getValue();
        }
        return string;
    }

}
