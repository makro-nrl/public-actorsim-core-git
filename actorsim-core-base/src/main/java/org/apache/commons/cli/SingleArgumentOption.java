package org.apache.commons.cli;

import org.slf4j.LoggerFactory;

import java.util.List;

public class SingleArgumentOption extends AbstractOption {
    private final static org.slf4j.Logger logger = LoggerFactory.getLogger(SingleArgumentOption.class);

    private static final long serialVersionUID = 1L;

    private static final boolean argumentIsNeeded = true;
    private static final String NOT_INITIALIZED = "NOT_INITIALIZED";
    private static final String ENABLED_BY_DEFAULT = "ENABLED_BY_DEFAULT";

    String defaultValue = NOT_INITIALIZED;

    public SingleArgumentOption(List<? super AbstractOption> owner, String shortOption, String description) {
        super(shortOption, argumentIsNeeded, description);
        owner.add(this);
    }

    public SingleArgumentOption(List<? super AbstractOption> owner, String shortOption, String value, String description, String defaultValue) {
        super(shortOption, argumentIsNeeded, description);
        owner.add(this);
        setValue(value);
        setDefault(defaultValue);
    }

    @Override
    public AbstractOption setDefault(Object value) {
        if (value instanceof String) {
            this.defaultValue = (String) value;
        } else {
            defaultValue = NOT_INITIALIZED;
        }
        return this;
    }

    @Override
    public AbstractOption setValue(Object value) {
        if (value instanceof String) {
            clearValues();
            addValueForProcessing((String) value);
        } else {
            String valueString = value.toString();
            clearValues();
            addValueForProcessing(valueString);

        }
        return this;
    }

    public String getValueOrDefault() {
        String value;
        if (this.isEnabledIgnoreDefault()) {
            value = getValue();
        } else {
            value = defaultValue;
        }
        return value;
    }

    @Override
    public int getValueOrDefaultAsInt() throws NumberFormatException {
        int value = 0;
        if (isEnabledIgnoreDefault()) {
            value = Integer.parseInt(getValueOrDefault());
        }
        return value;
    }

    @Override
    public int getValueOrDefaultAsInt_NoThrow() {
        int value = 0;
        try {
            value = Integer.parseInt(getValueOrDefault());
        } catch (NumberFormatException nfe) {
            logger.warn("Cannot parse into an integer the value {}", getValue());
        }
        return value;
    }

    @Override
    public int getValueAsInt_NoThrow() {
        int value = 0;
        try {
            if (isEnabledIgnoreDefault()) {
                value = Integer.parseInt(getValue());
            }
        } catch (NumberFormatException nfe) {
            logger.warn("Cannot parse into an integer the value {}", getValue());
        }
        return value;
    }

    @Override
    public int getValueAsInt() throws NumberFormatException {
        return Integer.parseInt(getValueOrDefault());
    }

    @Override
    public long getValueOrDefaultAsLong() throws NumberFormatException {
        long value = 0;
        if (isEnabledIgnoreDefault()) {
            value = Long.parseLong(getValue());
        }
        return value;
    }

    @Override
    public long getValueOrDefaultAsLong_NoThrow() {
        long value = 0;
        try {
            value = Long.parseLong(getValueOrDefault());
        } catch (NumberFormatException nfe) {
            logger.warn("Cannot parse into an long the value {}", getValue());
        }
        return value;
    }


    @Override
    public long getValueAsLong() throws NumberFormatException {
        long value = 0;
        if (isEnabledIgnoreDefault()) {
            value = Long.parseLong(getValue());
        }
        return value;
    }

    @Override
    public long getValueAsLong_NoThrow() {
        long value = 0;
        try {
            if (isEnabledIgnoreDefault()) {
                value = Long.parseLong(getValue());
            }
        } catch (NumberFormatException nfe) {
            logger.warn("Cannot parse into an long the value {}", getValue());
        }
        return value;
    }

    @Override
    public double getValueOrDefaultAsDouble() throws NumberFormatException {
        double value = 0;
        if (isEnabledIgnoreDefault()) {
            value = Double.parseDouble(getValue());
        }
        return value;
    }

    @Override
    public double getValueOrDefaultAsDouble_NoThrow() {
        double value = 0;
        try {
            value = Double.parseDouble(getValueOrDefault());
        } catch (NumberFormatException nfe) {
            logger.warn("Cannot parse into an double the value {}", getValue());
        }
        return value;
    }


    @Override
    public double getValueAsDouble() throws NumberFormatException {
        double value = 0;
        if (isEnabledIgnoreDefault()) {
            value = Double.parseDouble(getValue());
        }
        return value;
    }

    @Override
    public double getValueAsDouble_NoThrow() {
        double value = 0;
        try {
            if (isEnabledIgnoreDefault()) {
                value = Double.parseDouble(getValue());
            }
        } catch (NumberFormatException nfe) {
            logger.warn("Cannot parse into an double the value {}", getValue());
        }
        return value;
    }


    @Override
    public boolean getValueOrDefaultAsBoolean() throws NumberFormatException {
        boolean value = false;
        if (isEnabledIgnoreDefault()) {
            value = Boolean.parseBoolean(getValue());
        }
        return value;
    }

    @Override
    public boolean getValueOrDefaultAsBoolean_NoThrow() {
        boolean value = false;
        try {
            value = Boolean.parseBoolean(getValueOrDefault());
        } catch (NumberFormatException nfe) {
            logger.warn("Cannot parse into an double the value {}", getValue());
        }
        return value;
    }


    @Override
    public boolean getValueAsBoolean() throws NumberFormatException {
        boolean value = false;
        if (isEnabledIgnoreDefault()) {
            value = Boolean.parseBoolean(getValue());
        }
        return value;
    }

    @Override
    public boolean getValueAsBoolean_NoThrow() {
        boolean value = false;
        try {
            if (isEnabledIgnoreDefault()) {
                value = Boolean.parseBoolean(getValue());
            }
        } catch (NumberFormatException nfe) {
            logger.warn("Cannot parse into an double the value {}", getValue());
        }
        return value;
    }


    @Override
    public void resetToDefault() {
        this.clearValues();
        if (!defaultValue.equals(NOT_INITIALIZED)) {
            addValueForProcessing(defaultValue);
        }
    }

    @Override
    public void clear() {
        this.clearValues();
    }

    public void disableByDefault() {
        defaultValue = NOT_INITIALIZED;
    }

    public void enabledByDefault() {
        defaultValue = ENABLED_BY_DEFAULT;
    }

    public void enable() {
        throw new UnsupportedOperationException("This method doesn't make sense for a single argument option");
    }

    public void disable() {
        this.clearValues();
    }

    public boolean isEnabledIgnoreDefault() {
        boolean returnValue = false;
        if (this.getValue() != null) {
            returnValue = true;
        }
        return returnValue;
    }

    /////////////////////////////////////////////
    //
    //  Object Overrides
    //
    /////////////////////////////////////////////
    @Override
    public String toString() {
        String string = getOpt() + "=";
        if (!this.isEnabledIgnoreDefault()) {
            if (!defaultValue.equals(NOT_INITIALIZED)) {
                string += "[" + defaultValue + "]";
            } else {
                string += "[NI]";
            }
        } else {
            string += getValue();
        }
        return string;
    }

}
