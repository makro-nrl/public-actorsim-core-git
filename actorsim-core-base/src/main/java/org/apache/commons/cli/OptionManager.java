package org.apache.commons.cli;

import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;


/**
 * An option manager for options in \asim.
 * <p>
 * This class and its related classes are declared part of the org.apache.commons.cli
 * so that they have package private access to specific members in that original package.
 *
 * @author Mak Roberts
 */
public class OptionManager {
    private final static org.slf4j.Logger logger = LoggerFactory.getLogger(OptionManager.class);

    /**
     * Lists of options that the subclass wants to process.
     */
    protected final List<List<AbstractOption>> optionLists = new ArrayList<>();

    protected OptionManager() {
    }

    public void resetAllOptionsToDefault() {
        for (List<AbstractOption> optionList : optionLists) {
            for (AbstractOption option : optionList) {
                option.resetToDefault();
            }
        }
    }

    public void addOptions(Options commandLineOptions) {
        for (List<AbstractOption> optionList : optionLists) {
            for (Option option : optionList) {
                commandLineOptions.addOption(option);
            }
        }
    }

    public void parseOptions(CommandLine line) {
        for (List<AbstractOption> optionList : optionLists) {
            for (AbstractOption option : optionList) {
                if (line.hasOption(option.getOpt())) {
                    logger.debug("processing option:{}", option);
                    if (option instanceof BooleanOption) {
                        BooleanOption booleanOption = (BooleanOption) option;
                        booleanOption.enable();
                    } else if (option instanceof SingleArgumentOption) {
                        try {
                            Object value = line.getParsedOptionValue(option.getOpt());
                            option.setValue(value);
                        } catch (ParseException e) {
                            logger.error("There was a problem setting option {} from the commandline", option);
                            e.printStackTrace();
                        }
                    }
                }
            }
        }
    }


    /**
     * Reads options from the provided path if it exists.
     * <p>
     * The file is assumed to be in INI format where each line
     * consists of a key=value pair.
     * <p>
     * If a key is not found the current list of options
     * an error message is printed and it is ignored.
     *
     * @param optionsFilePath the path to use
     */
    public void parseOptions(Path optionsFilePath) {
        if (Files.exists(optionsFilePath)) {
            try {
                List<String> lines = Files.readAllLines(optionsFilePath);
                for (String line : lines) {
                    String[] tokens = line.split("=");
                    if (tokens.length == 2) {
                        String key = tokens[0];
                        String value = tokens[1];
                        setOptionValue(key, value);
                    }
                }
            } catch (IOException e) {
                logger.error("While reading the options file {} exception occurred:{}", optionsFilePath, e.getMessage());
            }
        } else {
            logger.info("No file found at {} .. are you sure this was what you intended?", optionsFilePath);
        }
    }

    private void setOptionValue(String key, String value) {
        AbstractOption optionToChange = null;
        for (List<AbstractOption> optionList : optionLists) {
            for (AbstractOption option : optionList) {
                String optionName = option.getOpt();
                if (optionName.equals(key)) {
                    optionToChange = option;
                    break;
                }
            }
            if (optionToChange != null) {
                break;
            }
        }
        if (optionToChange != null) {
            optionToChange.setValue(value);
            if (optionToChange instanceof BooleanOption) {
                BooleanOption booleanOption = (BooleanOption) optionToChange;
                if (value.compareToIgnoreCase("false") == 0) {
                    booleanOption.disable();
                }
            }
        } else {
            logger.warn("Could not find option {} in list of options.  Ignoring.", key);
        }
    }

    /**
     * Set the status of all options to NOT_INITIALIZED so no goals are produced in the goalNode manager.
     */
    public void clearAllOptions() {
        for (List<AbstractOption> optionList : optionLists) {
            for (AbstractOption option : optionList) {
                option.clear();
            }
        }
    }


}
