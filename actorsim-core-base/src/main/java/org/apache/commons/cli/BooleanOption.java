package org.apache.commons.cli;

import java.util.List;


public class BooleanOption extends AbstractOption {
    /**
     *
     */
    private static final long serialVersionUID = 1L;

    private static final boolean noArgumentIsNeeded = false;
    Status defaultValue = Status.NOT_INITIALIZED;
    Status currentValue = Status.NOT_INITIALIZED;
    public BooleanOption(List<? super AbstractOption> owner, String shortOption, String longOption, String description) {
        super(shortOption, longOption, noArgumentIsNeeded, description);
        AbstractOption option = this;
        owner.add(option);
    }

    public BooleanOption(List<? super AbstractOption> owner, Status value, String shortOption, String longOption, String description) {
        this(owner, shortOption, longOption, description);
        this.currentValue = value;
    }

    public BooleanOption(List<? super AbstractOption> owner, String shortOption, String longOption, String description, Status defaultValue) {
        this(owner, shortOption, longOption, description);
        setDefault(defaultValue);
    }

    public BooleanOption(List<? super AbstractOption> owner, Status value, String shortOption, String longOption, String description, Status defaultValue) {
        this(owner, shortOption, longOption, description);
        setDefault(defaultValue);
        this.currentValue = value;
    }

    public BooleanOption(List<? super AbstractOption> owner, String shortOption, String description) {
        super(shortOption, noArgumentIsNeeded, description);
        AbstractOption option = this;
        owner.add(option);
    }

    public BooleanOption(List<? super AbstractOption> owner, String shortOption, Status value, String description) {
        this(owner, shortOption, description);
        this.currentValue = value;
    }

    public BooleanOption(List<? super AbstractOption> owner, String shortOption, Status value, String description, Status defaultValue) {
        this(owner, shortOption, description);
        this.currentValue = value;
        setDefault(defaultValue);
    }

    @Override
    public AbstractOption setDefault(Object defaultIn) {
        if (defaultIn instanceof Status) {
            this.defaultValue = (Status) defaultIn;
        } else {
            defaultValue = Status.NOT_INITIALIZED;
        }
        return this;
    }

    @Override
    public AbstractOption setValue(Object value) {
        if (value instanceof Status) {
            this.currentValue = (Status) value;
        } else if (value instanceof String) {
            boolean booleanValue = Boolean.parseBoolean((String) value);
            if (booleanValue) {
                currentValue = Status.ENABLED;
            } else {
                currentValue = Status.DISABLED;
            }
        } else {
            currentValue = Status.NOT_INITIALIZED;
        }
        return this;
    }

    @Override
    public void resetToDefault() {
        currentValue = defaultValue;
    }

    @Override
    public void clear() {
        currentValue = Status.NOT_INITIALIZED;
    }

    public void disableByDefault() {
        defaultValue = Status.DISABLED;
    }

    public void enabledByDefault() {
        defaultValue = Status.ENABLED;
    }

    @Override
    public void enable() {
        currentValue = Status.ENABLED;
    }

    @Override
    public void disable() {
        currentValue = Status.DISABLED;
    }

    @Override
    public int getValueOrDefaultAsInt() {
        if (isEnabledIgnoreDefault()) {
            return getValueAsInt();
        } else {
            if (defaultValue != Status.NOT_INITIALIZED) {
                if (defaultValue == Status.ENABLED) {
                    return 1;
                } else {
                    return 0;
                }
            } else {
                throw new NumberFormatException("This option and its default are not set");
            }
        }
    }

    @Override
    public int getValueOrDefaultAsInt_NoThrow() {
        if (isEnabledIgnoreDefault()) {
            return getValueAsInt();
        } else {
            if (defaultValue == Status.ENABLED) {
                return 1;
            } else {
                return 0;
            }
        }
    }

    @Override
    public int getValueAsInt() throws NumberFormatException {
        if (currentValue != Status.NOT_INITIALIZED) {
            if (isEnabledIgnoreDefault()) {
                return 1;
            } else {
                return 0;
            }
        } else {
            throw new NumberFormatException("This option is not set.");
        }
    }

    @Override
    public int getValueAsInt_NoThrow() {
        if (isEnabledIgnoreDefault()) {
            return 1;
        } else {
            return 0;
        }
    }

    @Override
    public long getValueOrDefaultAsLong() {
        if (isEnabledIgnoreDefault()) {
            return getValueAsLong();
        } else {
            if (defaultValue != Status.NOT_INITIALIZED) {
                if (defaultValue == Status.ENABLED) {
                    return 1L;
                } else {
                    return 0L;
                }
            } else {
                throw new NumberFormatException("This option and its default are not set");
            }
        }
    }

    @Override
    public long getValueOrDefaultAsLong_NoThrow() {
        if (isEnabledIgnoreDefault()) {
            return getValueAsLong();
        } else {
            if (defaultValue == Status.ENABLED) {
                return 1L;
            } else {
                return 0L;
            }
        }
    }

    @Override
    public long getValueAsLong() throws NumberFormatException {
        if (currentValue != Status.NOT_INITIALIZED) {
            if (isEnabledIgnoreDefault()) {
                return 1;
            } else {
                return 0;
            }
        } else {
            throw new NumberFormatException("This option is not set.");
        }
    }

    @Override
    public long getValueAsLong_NoThrow() {
        if (isEnabledIgnoreDefault()) {
            return 1;
        } else {
            return 0;
        }
    }

    @Override
    public double getValueOrDefaultAsDouble() {
        if (isEnabledIgnoreDefault()) {
            return getValueAsDouble();
        } else {
            if (defaultValue != Status.NOT_INITIALIZED) {
                if (defaultValue == Status.ENABLED) {
                    return 1L;
                } else {
                    return 0L;
                }
            } else {
                throw new NumberFormatException("This option and its default are not set");
            }
        }
    }

    @Override
    public double getValueOrDefaultAsDouble_NoThrow() {
        if (isEnabledIgnoreDefault()) {
            return getValueAsDouble();
        } else {
            if (defaultValue == Status.ENABLED) {
                return 1L;
            } else {
                return 0L;
            }
        }
    }

    @Override
    public double getValueAsDouble() throws NumberFormatException {
        if (currentValue != Status.NOT_INITIALIZED) {
            if (isEnabledIgnoreDefault()) {
                return 1;
            } else {
                return 0;
            }
        } else {
            throw new NumberFormatException("This option is not set.");
        }
    }

    @Override
    public double getValueAsDouble_NoThrow() {
        if (isEnabledIgnoreDefault()) {
            return 1;
        } else {
            return 0;
        }
    }

    @Override
    public boolean getValueOrDefaultAsBoolean() throws NumberFormatException {
        if (isEnabledIgnoreDefault()) {
            return getValueAsBoolean();
        } else {
            if (defaultValue != Status.NOT_INITIALIZED) {
                return defaultValue == Status.ENABLED;
            } else {
                throw new NumberFormatException("This option and its default are not set");
            }
        }
    }

    @Override
    public boolean getValueOrDefaultAsBoolean_NoThrow() {
        if (isEnabledIgnoreDefault()) {
            return getValueAsBoolean();
        } else {
            return defaultValue == Status.ENABLED;
        }
    }

    @Override
    public boolean getValueAsBoolean() throws NumberFormatException {
        if (currentValue != Status.NOT_INITIALIZED) {
            return isEnabledIgnoreDefault();
        } else {
            throw new NumberFormatException("This option is not set.");
        }
    }

    @Override
    public boolean getValueAsBoolean_NoThrow() {
        return isEnabledIgnoreDefault();
    }

    public boolean isEnabledIgnoreDefault() {
        boolean returnValue = false;
        if (currentValue == Status.ENABLED) {
            returnValue = true;
        }
        return returnValue;
    }

    public boolean isDisabledIgnoreDefault() {
        return !isEnabledIgnoreDefault();
    }

    /////////////////////////////////////////////
    //
    //  Object Overrides
    //
    /////////////////////////////////////////////
    @Override
    public String toString() {
        String string = getOpt() + "=";
        if (this.currentValue == Status.ENABLED) {
            string += "E";
        } else if (this.currentValue == Status.DISABLED) {
            string += "D";
        } else {
            string += "NI";
        }
        return string;
    }

    public enum Status {
        NOT_INITIALIZED,
        ENABLED,
        DISABLED
    }
}
