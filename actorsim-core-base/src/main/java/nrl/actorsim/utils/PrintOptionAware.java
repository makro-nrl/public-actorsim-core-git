package nrl.actorsim.utils;

public interface PrintOptionAware {
    String toString(PrintOptions printOptions);
}
