package nrl.actorsim.utils;

import org.apache.commons.cli.OptionManager;

import java.time.Duration;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;


/**
 * An abstract, threaded class that runs a worker task indefinitely.
 * <p>
 *
 * The worker runs a loop that performs the following in order:
 * <code>
 * <pre>
 * while(notShuttingDown()) {
 *   synchronized(workerLock) {  //an optional lock that clients may use
 *       workerLock.wait();  //use notifyWorkerThereIsWork() to release
 *   }
 *
 *   try {
 *       boolean shutdownRequested = performWork();
 *       if (shutdownRequested) {
 *           startShutdown();
 *       }
 *   } catch ({@link InterruptedException} {
 *       //log exception
 *   } catch ({@link Exception}) {
 *       //log exception
 *   } finally {
 *       notifyCycleLatches();
 *   }
 * }
 * </pre>
 * </code>
 * <p>
 * Subclasses may override the behavior to specialize any of these methods,
 * especially the performWork() method.
 * <p>
 * Subclasses can synchronize with the worker thread by providing a <code>workerLock</code>,
 * to the Options.Builder.  If missing, an internal <code>workerLock</code> object
 * is created by the worker's constructor.
 */
public class WorkerThread extends Thread implements Named {
    final static org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(WorkerThread.class);

    protected final Options options;
    private final Object workerLock;

    private boolean isShuttingDown;
    private final List<CountDownLatch> cycleLatches;
    private int completedCycles;

    protected WorkerThread(Options options) {
        this.options = options;
        this.setName("Thread-" + getShortName());

        isShuttingDown = false;
        workerLock = Objects.requireNonNullElseGet(options.tmpWorkerLock, Object::new);

        cycleLatches = new ArrayList<>();
    }

    @Override
    public String getShortName() {
        return options.shortName;
    }

    public boolean isShuttingDown() {
        return isShuttingDown;
    }

    public boolean notShuttingDown() {
        return ! isShuttingDown;
    }

    public void startShutdown() {
        isShuttingDown = true;
        notifyWorkerThereIsWork();
    }

    public void notifyWorkerThereIsWork() {
        synchronized (workerLock) {
            workerLock.notify();
        }
    }

    public void notifyWorkerAndWaitOnCycle() {
        int waitForever = 0;
        boolean notify = true;
        //noinspection ConstantConditions
        waitOnCycle(notify, waitForever);
    }

    @SuppressWarnings("unused")
    public void notifyWorkerAndWaitOnCycle(int timeoutInSeconds) {
        boolean notify = true;
        //noinspection ConstantConditions
        waitOnCycle(notify, timeoutInSeconds);
    }

    public void waitWithoutNotify() {
        int waitForever = 0;
        boolean notify = false;
        //noinspection ConstantConditions
        waitOnCycle(notify, waitForever);
    }

    private void waitOnCycle(boolean notifyWorker, long timeoutInSeconds) {
        CountDownLatch latch = new CountDownLatch(1);
        try {
            addCycleLatch(latch);
            if (notifyWorker) {
                notifyWorkerThereIsWork();
            }
            if (latch.getCount() > 0) {
                if (timeoutInSeconds == 0) {
                    latch.await();
                } else {
                    //noinspection ResultOfMethodCallIgnored
                    latch.await(timeoutInSeconds, TimeUnit.SECONDS);
                }
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public void addCycleLatch(CountDownLatch latch) {
        cycleLatches.add(latch);
    }

    protected void notifyCycleLatches() {
        for (Iterator<CountDownLatch> it = cycleLatches.iterator(); it.hasNext(); ) {
            CountDownLatch latch = it.next();
            latch.countDown();
            if (latch.getCount() == 0) {
                it.remove();
            }
        }
    }

    @SuppressWarnings("unused")
    public int getCompletedCycles() {
        return completedCycles;
    }

    @Override
    public void run() {
        while (notShuttingDown()) {
            try {
                synchronized (workerLock) {
                    //noinspection DuplicatedCode
                    logger.debug("=========================================================================");
                    logger.debug("");
                    logger.debug("=========================================================================");
                    logger.debug("");
                    logger.debug("=========================================================================");
                    logger.debug("");
                    logger.debug("==================");
                    logger.debug("waiting on changes");
                    logger.debug("==================");
                    workerLock.wait();
                }

                if (isShuttingDown()) {
                    continue;
                }

                WorkResult result = WorkResult.MORE_WORK;
                while (result == WorkResult.MORE_WORK) {
                    result = performWork();
                }

                if (result == WorkResult.SHUTDOWN_REQUESTED) {
                    startShutdown();
                }
            } catch (InterruptedException ex) {
                if (isShuttingDown()) {
                    logger.info("{}: shutting down worker thread", options.shortName);
                } else {
                    logger.warn("{}: event processor caught exception {}", options.shortName, ex.getCause());
                }
            } catch (Exception e) {
                logger.error("{}: exception thrown during the process method.  Continuing...", options.shortName, e);
           } finally {
                completedCycles++;
                notifyCycleLatches();
            }
        }
    }

    public enum WorkResult {
        CONTINUE,
        MORE_WORK,
        SHUTDOWN_REQUESTED
    }

    /**
     * Allows subclasses to customize the work.
     *
     * A subclass requests additional calls to performWork()
     * by returning MORE_WORK.
     *
     * A subclass requests a shutdown of the worker thread
     * by returning SHUTDOWN_REQUESTED.
     *
     * A subclass requests a wait for more work
     * by returning CONTINUE.
     *
     * @return WorkResult value as described above
     */
    public WorkResult performWork() {
        logger.info("{}: Empty performWork() was called, requesting a shutdown.", getShortName());
        return WorkResult.SHUTDOWN_REQUESTED;
    }


    public void setWakeUpCall(Duration duration) {
        logger.debug("Setting a wake up call for {} second away", duration);
        new Thread(() -> {
            try {
                Thread.sleep(duration.toMillis());
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            logger.debug("Wake up call ... ");
            WorkerThread.this.notifyWorkerThereIsWork();
        }).start();
    }

    Options getOptions() {
        return options;
    }

    public static class Options extends OptionManager {
        protected String shortName;
        protected Object tmpWorkerLock;

        protected Options() {

        }

        public static Builder<? extends Options, ? extends Builder<?,?>> builder() {
            return new Builder<>(new Options());
        }


        public static class Builder<T extends Options, B extends Builder<T, B>> {
            protected T optionsInstance;

            protected Builder(T instance) {
                this.optionsInstance = instance;
            }

            protected T getOptionsInstance() {
                return (T) optionsInstance;
            }

            protected B getThis() {
                //noinspection unchecked
                return (B) this;
            }

            public B shortName(String shortName) {
                optionsInstance.shortName = shortName;
                return getThis();
            }

            public B workerLock(Object workerLock) {
                optionsInstance.tmpWorkerLock = workerLock;
                return getThis();
            }

            protected void validate() {
                if (optionsInstance.shortName == null) {
                    String message = "The WorkerThread requires name to instantiate.";
                    logger.error(message);
                    throw new IllegalArgumentException(message);
                }
            }

            public T build() {
                return optionsInstance;
            }
        }
    }


}
