package nrl.actorsim.utils;

import java.util.*;

public interface ChangeNotifier {

    List<Runnable> getChangeListeners();

    default void notifyOfInternalChange() {
        for (Runnable listener : getChangeListeners()) {
            listener.run();
        }
    }

    default void addChangeListener(Runnable callback) {
        getChangeListeners().add(callback);
    }
}
