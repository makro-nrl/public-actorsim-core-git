package nrl.actorsim.utils;

import java.util.*;

public class ChangeNotifierImpl implements ChangeNotifier {

    private List<Runnable> changeListeners = null;

    @Override
    public List<Runnable> getChangeListeners() {
        if (changeListeners == null) {
            changeListeners = new ArrayList<>();
        }
        return changeListeners;
    }
}
