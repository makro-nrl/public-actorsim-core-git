package nrl.actorsim.utils;


/**
 * An interface to allow objects to return a short name for display/debugging output
 * while still allowing the <code> toString() </code> method to return more detailed
 * information.
 *
 * @author Mak Roberts
 */
public interface Named {
    String getShortName();
}
