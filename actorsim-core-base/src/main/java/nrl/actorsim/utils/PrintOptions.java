package nrl.actorsim.utils;


import java.time.ZoneId;
import java.time.format.DateTimeFormatter;

import static nrl.actorsim.utils.PrintOptions.PathDetail.*;
import static nrl.actorsim.utils.PrintOptions.TypeDetail.*;

public class PrintOptions {
    public static PrintOptions DEFAULT_OPTIONS = new PrintOptions();

    PathDetail pathDetail = NO_PATH;
    TypeDetail typeDetail = NO_TYPE;

    @SuppressWarnings("FieldMayBeFinal")
    private DateTimeFormatter timeFormatter = DateTimeFormatter
            .ofPattern("ddLLLkkmm")
            .withZone(ZoneId.of("UTC"));

    public PrintOptions() {

    }

    public PrintOptions(PathDetail pathDetail) {
        this.pathDetail = pathDetail;
    }

    public PrintOptions(PrintOptions other) {
        this.pathDetail = other.pathDetail;
        this.typeDetail = other.typeDetail;
        this.timeFormatter = other.timeFormatter;
    }

    @SuppressWarnings("unused")
    public PrintOptions setPathDetail(PathDetail value) {
        pathDetail = value;
        return this;
    }

    @SuppressWarnings("unused")
    public PrintOptions setTypeDetail(TypeDetail value) {
        typeDetail = value;
        return this;
    }

    public boolean hidePath() {
        return pathDetail == NO_PATH;
    }

    public boolean pathNameOnly() {
        return pathDetail == PATH_NAME_ONLY;
    }

    @SuppressWarnings("BooleanMethodIsAlwaysInverted")
    public boolean hideTyping() {
        return typeDetail == NO_TYPE;
    }

    public DateTimeFormatter getTimeFormatter() {
        return timeFormatter;
    }


    public enum PathDetail {
        NO_PATH,
        PATH_NAME_ONLY,
        FULL_PATH
    }

    public enum TypeDetail {
        NO_TYPE,
        FULL_TYPE
    }

}
