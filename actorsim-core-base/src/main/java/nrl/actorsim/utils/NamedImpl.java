package nrl.actorsim.utils;

public class NamedImpl implements Named {
    protected String shortName;


    @Override
    public String getShortName() {
        return shortName;
    }
}
