package nrl.actorsim.utils;

import ch.qos.logback.classic.Level;
import ch.qos.logback.classic.LoggerContext;
import nrl.actorsim.domain.Bindings;
import nrl.actorsim.domain.WorldArgument;
import org.slf4j.LoggerFactory;

import java.util.*;
import java.util.stream.*;

public class Utils {
    protected final static org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(Utils.class);

    public static final String ARGS_START = "(";
    public static final String ARGS_END = ")";
    public static final String BINDINGS_START = "<";
    public static final String BINDINGS_END = ">";
    public static final String IMPLIED_BINDINGS_START = "<<";
    public static final String IMPLIED_BINDINGS_END = ">>";
    public static final String VALUE_TYPE_START = "{";
    public static final String VALUE_TYPE_END = "}";
    public static final String PATH_START = "|";
    public static final String PATH_END = "|";

    public static final String MISSING_ID = "?";

    public static String collectArgs(List<WorldArgument> args, PrintOptions options) {
        if (args == null) {
            return ARGS_START + ARGS_END;
        }
        return Utils.collect(ARGS_START, args, ARGS_END, options);
    }

    public static String collectValueTypes(List<WorldArgument> valueTypes,
                                           PrintOptions options) {
        if (valueTypes == null) {
            return VALUE_TYPE_START + VALUE_TYPE_END;
        }
        return Utils.collect(VALUE_TYPE_START, valueTypes, VALUE_TYPE_END, options);
    }

    public static String collectBindings(Bindings bindings, PrintOptions options) {
        if (bindings == null
                || bindings.size() == 0) {
            return BINDINGS_START + BINDINGS_END;
        }
        return Utils.collect(BINDINGS_START, bindings, BINDINGS_END, options);
    }

    public static String collectFoundBindings(Bindings bindings, PrintOptions options) {
        if (bindings == null
                || bindings.size() == 0) {
            return IMPLIED_BINDINGS_START + IMPLIED_BINDINGS_END;
        }
        return Utils.collect(IMPLIED_BINDINGS_START, bindings, IMPLIED_BINDINGS_END, options);
    }

    public static String collect(String prefix, List<?> values, String suffix, PrintOptions options) {
        String result = prefix;
        result += collect(values, options);
        result += suffix;
        return result;
    }

    public static String collect(List<?> values, PrintOptions options) {
        StringBuilder result = new StringBuilder();
        String sep = "";
        for (Object value : values) {
            String valueString;
            if (value instanceof PrintOptionAware) {
                valueString = ((PrintOptionAware)value).toString(options);
            } else {
                valueString = value.toString();
            }

            result.append(String.format("%s%s", sep, valueString));
            sep = ", ";
        }
        return result.toString();
    }

    @SuppressWarnings({"rawtypes", "unchecked"})
    public static int compareToHelper(List<? extends Comparable> lhs, List<? extends Comparable> rhs) {
        int compare = lhs.size() - rhs.size();
        if (compare == 0) {
            for (int i = 0; i < lhs.size(); ++i) {
                Comparable lhsInner = lhs.get(i);
                Comparable rhsInner = rhs.get(i);
                compare = lhsInner.compareTo(rhsInner);
                if (compare != 0) {
                    return compare;
                }
            }
        }
        return compare;
    }

    public static boolean confirmEqual(List<?> lhs, List<?> rhs) {
        if (lhs.size() == rhs.size()) {
            for (int i = 0; i < lhs.size(); ++i) {
                Object lhsInner = lhs.get(i);
                Object rhsInner = rhs.get(i);
                if (!lhsInner.equals(rhsInner)) {
                    return false;
                }
            }
            return true;
        }
        return false;
    }




    public static class Tuple<LEFT_T, RIGHT_T> {
        private final LEFT_T left;
        private final RIGHT_T right;

        public Tuple(LEFT_T left, RIGHT_T right) {
            this.left = left;
            this.right = right;
        }

        public LEFT_T left() {
            return left;
        }

        public RIGHT_T right() {
            return right;
        }
    }

    public static <T> Stream<Tuple<Integer, T>> zipWithIndex(Stream<T> stream) {
        Stream<Integer> integerStream = IntStream.range(0, Integer.MAX_VALUE).boxed();
        Iterator<Integer> integerIterator = integerStream.iterator();
        return stream.map(x -> new Tuple<>(integerIterator.next(), x));
    }

    public static
    <K, V>
    Map<K, V> zipMap(List<K> left, List<V> right) {
        if (left.size() != right.size()) {
            logger.error("the left and right sides must be equal");
            return Collections.emptyMap();
        }
        //NB: disabling the inspection for Comparable<K>
        //    which means the client must ensure this variant
        //noinspection SortedCollectionWithNonComparableKeys
        Map<K, V> map = new TreeMap<>();
        zip(left, right).forEach(
                (Tuple<K, V> tuple) -> {
                    map.put(tuple.left(), tuple.right());
                });
        return map;
    }

    public static <L, R> Stream<Tuple<L, R>> zip(List<L> left, List<R> right) {
        Stream<L> lStream = left.stream();
        Stream<R> rStream = right.stream();
        return zip(lStream, rStream);
    }

    public static <L, R> Stream<Tuple<L, R>> zip(Stream<L> lStream, Stream<R> rStream) {
        Iterator<R> rIter = rStream.iterator();
        return lStream.map(x -> new Tuple<>(x, rIter.next()));
    }

    public static Class<?> getOriginalClass(Object obj) {
        //see: https://stackoverflow.com/questions/15202997/what-is-the-difference-between-canonical-name-simple-name-and-class-name-in-jav
        Class<?> cls = obj.getClass();
        if (cls.isAnonymousClass()) {
            if (cls.getInterfaces().length == 0) {
                return cls.getSuperclass();
            } else {
                return cls.getInterfaces()[0];
            }
        } else {
            return cls;
        }
    }

    public static void enableDebugLogging() {
        LoggerContext loggerContext = (LoggerContext) LoggerFactory.getILoggerFactory();
        ch.qos.logback.classic.Logger rootLogger = loggerContext.getLogger("nrl.actorsim");
        rootLogger.setLevel(Level.DEBUG);
    }

}



