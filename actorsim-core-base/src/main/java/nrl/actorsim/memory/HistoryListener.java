package nrl.actorsim.memory;

import java.util.function.Consumer;
import java.util.function.Predicate;

public class HistoryListener<T> {
    protected Predicate<T> filter = null;
    protected Consumer<HistoryEvent<T>> consumer = null;
    String name;


    public HistoryListener() {
        this.filter = null;
        this.consumer = null;
    }

    HistoryListener(Consumer<HistoryEvent<T>> consumer) {
        this.consumer = consumer;
    }

    HistoryListener(Predicate<T> filter, Consumer<HistoryEvent<T>> consumer) {
        this.filter = filter;
        this.consumer = consumer;
    }

    HistoryListener(Predicate<T> filter) {
        this.filter = filter;
    }

    public <L extends HistoryListener<T>>
    L setName(String name) {
        this.name = name;
        //noinspection unchecked
        return (L) this;
    }

    @Override
    public String toString() {
        if (name != null) {
            return name;
        }
        return this.getClass().getSimpleName();
    }

    public boolean matches(HistoryEvent<T> event) {
        if (filter != null) {
            return this.filter.test(event.modified);
        } else {
            return true;
        }
    }

    public void accept(HistoryEvent<T> event) {
        if (matches(event)) {
            if (consumer != null) {
                consumer.accept(event);
            }
        }
    }

}
