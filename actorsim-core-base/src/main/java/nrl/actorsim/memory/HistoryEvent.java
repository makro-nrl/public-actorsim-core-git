package nrl.actorsim.memory;

import org.jetbrains.annotations.NotNull;

import java.time.Instant;
import java.util.concurrent.atomic.AtomicInteger;


/**
 * An immutable class that stores an event.
 *
 * @param <T> The type of the modified instance that is tracked.
 */
public class HistoryEvent<T> implements Comparable<HistoryEvent<T>> {
    public static final AtomicInteger NEXT_ID = new AtomicInteger(0);

    final Instant instant;
    final int id;

    final Double simulationTime;
    final Type type;
    final T modified;

    HistoryEvent(Instant instant, Double simulationTime, Type type, T o) {
        this.id = NEXT_ID.getAndIncrement();
        this.instant = instant;
        this.simulationTime = simulationTime;
        this.type = type;
        modified = o;
    }

    HistoryEvent(Double simulationTime, Type type, T o) {
        this.id = NEXT_ID.getAndIncrement();
        this.instant = Instant.now();
        this.simulationTime = simulationTime;
        this.type = type;
        modified = o;
    }

    @Override
    public int compareTo(@NotNull HistoryEvent<T> other) {
        int instantCmp = this.instant.compareTo(other.instant);
        if (instantCmp == 0) {
            int idCmp = this.id - other.id;
            if (idCmp == 0) {
                if (this.modified instanceof Comparable) {
                    //noinspection rawtypes,unchecked
                    return ((Comparable) this.modified).compareTo(other.modified);
                } else {
                    return this.modified.hashCode() - other.modified.hashCode();
                }
            }
            return idCmp;
        }
        return instantCmp;
    }

    @Override
    public String toString() {
        return simulationTime + " " + type + " " + modified.toString();
    }

    public T getModified() {
        return modified;
    }

    public enum Type {
        INSERTED,
        DELETED,
        UPDATED
    }
}
