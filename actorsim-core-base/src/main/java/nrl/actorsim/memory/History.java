package nrl.actorsim.memory;


import java.time.Instant;
import java.time.temporal.TemporalUnit;
import java.util.*;
import java.util.stream.Collectors;

import static nrl.actorsim.memory.HistoryEvent.Type.*;

/**
 * A class that tracks events and notifies listeners of any changes.
 */
public class History<T extends Comparable<?>> {
    final Map<HistoryEvent<T>, HistoryEvent<T>> events = new TreeMap<>();
    final List<HistoryListener<T>> listeners = new ArrayList<>();

    public void inserted(T item, double simulationTime) {
        HistoryEvent<T> event = new HistoryEvent<>(simulationTime, INSERTED, item);
        insert(event);
    }

    public void updated(T item, double simulationTime) {
        HistoryEvent<T> event = new HistoryEvent<>(simulationTime, UPDATED, item);
        insert(event);
    }

    public void deleted(T item, double simulationTime) {
        HistoryEvent<T> event = new HistoryEvent<>(simulationTime, DELETED, item);
        insert(event);
    }

    public void addListener(HistoryListener<T> listener) {
        listeners.add(listener);
    }

    public void removeListener(HistoryListener<T> listener) {
        listeners.remove(listener);
    }

    public void clearAllListeners() {
        listeners.clear();
    }

    protected void insert(HistoryEvent<T> event) {
        events.put(event, event);
        notifyListeners(event);
    }

    protected void notifyListeners(HistoryEvent<T> event) {
        for (HistoryListener<T> listener : listeners) {
            listener.accept(event);
        }
    }

    Set<HistoryEvent<T>> eventsAtOrSince(Instant instant) {
        return events.values().stream()
                .filter(event -> event.instant.equals(instant) || event.instant.isAfter(instant))
                .collect(Collectors.toSet());
    }

    Set<HistoryEvent<T>> eventsBefore(Instant instant) {
        return events.values().stream()
                .filter(event -> event.instant.isBefore(instant))
                .collect(Collectors.toSet());
    }

    Set<HistoryEvent<T>> eventsAt(Instant instant) {
        return events.values().stream()
                .filter(event -> event.instant.equals(instant))
                .collect(Collectors.toSet());
    }

    Set<HistoryEvent<T>> eventsAt(Instant instant, TemporalUnit truncatedTo) {
        Instant truncated = instant.truncatedTo(truncatedTo);
        return events.values().stream()
                .filter(event -> event.instant.equals(truncated))
                .collect(Collectors.toSet());
    }


    Set<HistoryEvent<T>> eventsAtOrSince(double simulationTime) {
        return events.values().stream()
                .filter(event -> simulationTime <= event.simulationTime)
                .collect(Collectors.toSet());
    }

    Set<HistoryEvent<T>> eventsBefore(double simulationTime) {
        return events.values().stream()
                .filter(event -> event.simulationTime < simulationTime)
                .collect(Collectors.toSet());
    }

    Set<HistoryEvent<T>> eventsAt(double simulationTime) {
        return events.values().stream()
                .filter(event -> event.simulationTime.equals(simulationTime))
                .collect(Collectors.toSet());
    }

    public List<? extends T> insertedAt(double simulationTime) {
        return eventsAt(simulationTime)
                .stream()
                .filter(event -> event.type == INSERTED)
                .map(HistoryEvent::getModified)
                .collect(Collectors.toList());
    }

    public List<? extends T> insertedAtOrSince(double simulationTime) {
        return eventsAtOrSince(simulationTime)
                .stream()
                .filter(event -> event.type == INSERTED)
                .map(HistoryEvent::getModified)
                .collect(Collectors.toList());
    }

    public List<? extends T> updatedAtOrSince(double simulationTime) {
        return eventsAtOrSince(simulationTime)
                .stream()
                .filter(event -> event.type == UPDATED)
                .map(HistoryEvent::getModified)
                .collect(Collectors.toList());
    }

    public List<? extends T> deletedAtOrSince(double simulationTime) {
        return eventsAtOrSince(simulationTime)
                .stream()
                .filter(event -> event.type == DELETED)
                .map(HistoryEvent::getModified)
                .collect(Collectors.toList());
    }
}
