package nrl.actorsim.chronicle;


/**
 * A class that imposes an ordering over two objects.
 *
 * @param <T> the type to store
 */
public class TemporalOrdering<T> {
    protected final T lhs;
    protected final T rhs;
    protected AllenIntervalOperator operator;

    public TemporalOrdering(T lhs, AllenIntervalOperator operator, T rhs) {
        this.lhs = lhs;
        this.setOperator(operator);
        this.rhs = rhs;
    }

    @SuppressWarnings({"DuplicatedCode", "rawtypes"})
    @Override
    public String toString() {
        String lhsString = lhs.toString();
        if (lhs instanceof Class) {
            lhsString = ((Class) lhs).getSimpleName();
        }
        String rhsString = rhs.toString();
        if (rhs instanceof Class) {
            rhsString = ((Class) rhs).getSimpleName();
        }
        return String.format("%s %s %s", lhsString, getOperator().toString(), rhsString);
    }

    public T getLhs() {
        return lhs;
    }

    public T getRhs() {
        return rhs;
    }

    @SuppressWarnings({"rawtypes", "unchecked"})
    public Class getRhsClass() {
        return (rhs instanceof Class) ? (Class) rhs : (Class<T>) rhs.getClass();
    }

    public String getLhsKey() {
        return getKey(lhs);
    }

    public String getRhsKey() {
        return getKey(rhs);
    }

    @SuppressWarnings("rawtypes")
    protected String getKey(T item) {
        return (item instanceof Class) ? ((Class) item).getSimpleName() : item.getClass().getSimpleName();
    }

    public AllenIntervalOperator getOperator() {
        return operator;
    }

    public void setOperator(AllenIntervalOperator operator) {
        this.operator = operator;
    }

    public boolean usesPrecedesOperator() {
        return AllenIntervalOperator.PRECEDES_OPERATORS.contains(getOperator());
    }

    public TemporalOrdering<T> invert() {
        AllenIntervalOperator invertedOperator = AllenIntervalOperator.invert(this.getOperator());
        TemporalOrdering<T> inverted;
        inverted = new TemporalOrdering<>(rhs, invertedOperator, lhs);
        return inverted;
    }

    public boolean matches(T lhs, T rhs) {
        return matchesLHS(lhs)
                && matchesRHS(rhs);
    }

    public boolean matchesLHS(Object item) {
        return lhs == item;
    }

    public boolean matchesRHS(Object item) {
        return rhs == item;
    }

}
