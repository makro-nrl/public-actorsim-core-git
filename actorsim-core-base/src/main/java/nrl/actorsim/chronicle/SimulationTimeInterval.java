package nrl.actorsim.chronicle;

import nrl.actorsim.utils.Named;

public class SimulationTimeInterval implements Named {
    public static final SimulationTimeInterval ALL_TIME = new SimulationTimeInterval();

    protected final String name;
    private double start;
    private double end;

    private SimulationTimeInterval() {
        this.name = "EMPTY";
        this.start = Double.MIN_VALUE;
        this.end = Double.MAX_VALUE;
    }

    public SimulationTimeInterval(String name, double start, double end) {
        this.name = name;
        this.start = start;
        this.end = end;
    }

    public SimulationTimeInterval(SimulationTimeInterval interval) {
        this(interval.name, interval.start, interval.end);
    }

    public SimulationTimeInterval copy() {
        return new SimulationTimeInterval(this.name, this.start, this.end);
    }

    @Override
    public String getShortName() {
        return name;
    }

    @Override
    public String toString() {
        String startString = "-INF";
        if (start != Double.MIN_VALUE) {
            startString = Double.toString(start);
        }
        String endString = "+INF";
        if (end != Double.MAX_VALUE) {
            endString = Double.toString(end);
        }
        return String.format("[%s,%s]", startString, endString);
    }

    @Override
    public boolean equals(Object other) {
        if (other instanceof SimulationTimeInterval) {
            SimulationTimeInterval otherInterval = (SimulationTimeInterval) other;
            return this.getStart() == otherInterval.getStart()
                    && this.getEnd() == otherInterval.getEnd();
        }
        return false;
    }

    public double getStart() {
        return start;
    }

    public void setStart(double start) {
        this.start = start;
    }

    public double getEnd() {
        return end;
    }

    public void setEnd(double end) {
        this.end = end;
    }

    public int compareTo(SimulationTimeInterval other) {
        double startCompare = other.start - this.start;
        if (startCompare == 0) {
            return (int) (other.end - this.end);  //yep, we are throwing away some non-critical detail.
        }
        return (int) startCompare;
    }

}
