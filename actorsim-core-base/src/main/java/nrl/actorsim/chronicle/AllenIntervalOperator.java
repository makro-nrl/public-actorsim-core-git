package nrl.actorsim.chronicle;

import nrl.actorsim.utils.Named;

import java.util.Arrays;
import java.util.List;

/**
 * An overview of the 13 intervals is provided below,
 * where  X op Y indicates the reading, e.g., X BEFORE Y.
 * <p>
 * Note the symmetry about the EQUALS operator
 * <p>
 * <pre>
 * {@code
 * X op Y
 * / (use top Y interval)            |-------Y-------|
 * /------BEFORE         |----X1----|.               .
 * //-----MEETS           |----X2----|               .
 * //                                .               .
 * // /----OVERLAPS           |----X3----|           .
 * // //---FINISHED_BY        |----------X4----------|
 * // ///--CONTAINS           |----------------X5------------|
 * // ///                            .               .
 * // ///   /-STARTS                 |----X6----|    .
 * == ===   = EQUALS                 |------X7-------|
 * \\\\ \\\\\\   \\-STARTED_BY       |----------X8-----------|
 * \\\\ \\\\\\                       .               .
 * \\\\ \\\\\\--DURING               .  |----X9----| .
 * \\\\ \\\\---FINISHES              .  |----X10-----|
 * \\\\ \\----OVERLAPPED_BY          .  |--------X11--------|
 * \\\\                              .               .
 * \\\\-----MET_BY                   .               |----X12----|
 * \\----AFTER                       .               .|----X13----|
 * \\ (use bottom Y interval)        |-------Y-------|
 * }
 * </pre>
 *
 * A good overview of the relationships between sets of
 * Allen's intervals are found in:
 *  https://thomasalspaugh.org/pub/fnd/allen.html
 * which provides a set of references, in particular:
 *
 * Reasoning about temporal relations: The tractable subalgebras of Allen's interval algebra
 * https://dl.acm.org/doi/10.1145/876638.876639
 *
 *
 */
public enum AllenIntervalOperator implements Named {
    NOOP("~~"),

    // X1 BEFORE Y
    // |----X1----|
    //              |------Y------|
    // Y BEFORE X13
    // |----Y----|
    //              |------X13------|
    BEFORE("Be"),
    AFTER("Af"),

    // X MEETS Y
    // |----X2---|
    //           |------Y------|
    MEETS("Mt"),
    MET_BY("Mb"),

    // X3 OVERLAPS Y
    // |----X3----|
    //        |------Y------|
    // X11 OVERLAPPED BY Y
    // |----Y----|
    //        |------X11------|
    OVERLAPS("Ov"),
    OVERLAPPED_BY("Ob"),

    // X10 FINISHES Y
    //      |---X10---|
    //  |------Y------|
    // X10 FINISHED_BY Y
    //         |---Y---|
    //  |------X4------|
    FINISHES("Fn"),
    FINISHED_BY("Fb"),

    // X DURING Y
    //   |----X9----|
    // |------Y------|
    DURING("Du"),
    CONTAINS("Co"),

    // X STARTS Y
    // |----X6----|
    // |------Y------|
    STARTS("St"),
    STARTED_BY("Sb"),

    // X == Y
    // X EQUALS Y
    // |----X----|
    // |----Y----|
    EQUALS("Eq");

    public static final List<AllenIntervalOperator> PRECEDES_OPERATORS = Arrays.asList(BEFORE, MEETS, OVERLAPS);
    final String shortName;

    AllenIntervalOperator(String shortName) {
        this.shortName = shortName;
    }

    public static AllenIntervalOperator invert(AllenIntervalOperator operator) {
        switch (operator) {
            case BEFORE:
                return AFTER;
            case AFTER:
                return BEFORE;
            case MEETS:
                return MET_BY;
            case MET_BY:
                return MEETS;
            case OVERLAPS:
                return OVERLAPPED_BY;
            case OVERLAPPED_BY:
                return OVERLAPS;
            case STARTS:
                return STARTED_BY;
            case STARTED_BY:
                return STARTS;
            case FINISHES:
                return FINISHED_BY;
            case FINISHED_BY:
                return FINISHES;
            case DURING:
                return CONTAINS;
            case CONTAINS:
                return DURING;
            case EQUALS:
                return EQUALS;
        }
        return NOOP;
    }

    @Override
    public String getShortName() {
        return shortName;
    }

}
