package nrl.actorsim.chronicle;

import nrl.actorsim.domain.NamedPathImpl;
import nrl.actorsim.utils.PrintOptions;

import java.time.Instant;
/**
 * A Timepoint is a named Instant.  The naming is important so that a future language
 * can refer to timepoints correctly.
 */
public class Timepoint extends NamedPathImpl {
    public static final Timepoint NULL_TIME_POINT = new Timepoint("null", Instant.EPOCH);
    public static final String START_NAME = "start";
    public static final Timepoint START_OF_TIME = createStart(Instant.MIN);
    public static final String END_NAME = "end";
    public static final Timepoint END_OF_TIME = createEnd(Instant.MAX);

    final String name;
    private final Instant instant;

    public Timepoint(String name, Instant instant) {
        super(name);
        this.name = name;
        this.instant = instant;
    }

    @SuppressWarnings("unused")
    public Timepoint(Timepoint other) {
        super(other.name);
        this.name = other.name;
        this.instant = other.getInstant();
    }

    public static Timepoint createStart(Instant instant) {
        return new Timepoint(START_NAME, instant);
    }

    public static Timepoint createEnd(Instant instant) {
        return new Timepoint(END_NAME, instant);
    }

    public String toString() {
        String instantString;
        if (this == START_OF_TIME) {
            instantString = "-INF";
        } else if (this == END_OF_TIME) {
            instantString = "+INF";
        } else {
            instantString = instant.toString();
        }
        return String.format("%s:%s", name, instantString);
    }

    @Override
    public String toString(PrintOptions options) {
        String instantString;
        if (this == START_OF_TIME) {
            instantString = "-INF";
        } else if (this == END_OF_TIME) {
            instantString = "+INF";
        } else {
            instantString = options.getTimeFormatter().format(instant);
        }
        return String.format("%s:%s", name, instantString);
    }


    public Instant getInstant() {
        return instant;
    }
}
