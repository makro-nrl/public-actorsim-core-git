package nrl.actorsim.chronicle;

import nrl.actorsim.domain.NamedPathImpl;
import nrl.actorsim.utils.Named;
import nrl.actorsim.utils.PrintOptions;
import org.jetbrains.annotations.NotNull;

import java.time.*;


/**
 * A TimeInterval is a named interval between a start and end.
 *
 */
@SuppressWarnings("unused")
public class TimeInterval extends NamedPathImpl implements Named {
    public static final TimeInterval ALL_TIME = new TimeInterval("EMPTY", Timepoint.START_OF_TIME, Timepoint.END_OF_TIME);

    protected final String name;
    private Timepoint start;
    private Timepoint end;

    public TimeInterval(String name, Instant start, Instant end) {
        this.name = name;
        this.start = Timepoint.createStart(start);
        this.end = Timepoint.createEnd(end);
    }

    public TimeInterval(String name, Timepoint start, Timepoint end) {
        this.name = name;
        this.start = start;
        this.end = end;
    }

    public TimeInterval(TimeInterval interval) {
        this(interval.name, interval.start, interval.end);
    }

    public TimeInterval copy() {
        return new TimeInterval(this.name, this.start, this.end);
    }

    @Override
    public String getShortName() {
        return name;
    }

    @Override
    public String toString() {
        return toString(getPrintOptions());
    }

    private PrintOptions getPrintOptions() {
        return PrintOptions.DEFAULT_OPTIONS;
    }

    @Override
    public String toString(PrintOptions printOptions) {
        String startString = start.toString(printOptions);
        String endString = end.toString(printOptions);
        return String.format("[%s,%s] %s", startString, endString, toStringPath(printOptions));
    }

    @Override
    public boolean equals(Object other) {
        if (other instanceof TimeInterval) {
            TimeInterval otherInterval = (TimeInterval) other;
            return this.getStart() == otherInterval.getStart()
                    && this.getEnd() == otherInterval.getEnd();
        }
        return false;
    }

    public Timepoint getStart() {
        return start;
    }

    public void setStart(Timepoint start) {
        this.start = start;
    }

    public void setStart(Instant start) {
        this.start.getInstant().adjustInto(start);
    }

    public Timepoint getEnd() {
        return end;
    }

    public void setEnd(Timepoint end) {
        this.end = end;
    }

    public void setEnd(Instant end) {
        this.end.getInstant().adjustInto(end);
    }

    public int compareTo(TimeInterval other) {
        int startCompare = other.start.getInstant().compareTo(this.start.getInstant());
        if (startCompare == 0) {
            return other.end.getInstant().compareTo(this.end.getInstant());
        }
        return startCompare;

    }

    public boolean contains(@NotNull Instant instant) {
        return startsBefore(instant)
                && endsAfter(instant);
    }

    public boolean containsInclusiveAll(Instant instant) {
        return startsAtOrBefore(instant)
                && endsAtOrAfter(instant);
    }

    public boolean containsInclusiveStart(Instant instant) {
        return startsAtOrBefore(instant)
                && endsAfter(instant);
    }

    public boolean startsAtOrBefore(Instant instant) {
        return startsAtOrBefore(this, instant);
    }

    public boolean startsBefore(Instant instant) {
        return startsBefore(this, instant);
    }

    public boolean endsAtOrAfter(Instant instant) {
        return endsAtOrAfter(this, instant);
    }

    public boolean endsAfter(Instant instant) {
        return endsAfter(this, instant);
    }

    public boolean endsBefore(Instant instant) {
        return endsBefore(this, instant);
    }

    public static boolean startsAtOrBefore(TimeInterval interval, Instant instant) {
        return interval.start.getInstant().equals(instant)
                || startsBefore(interval, instant);
    }

    public static boolean startsBefore(TimeInterval interval, Instant instant) {
        return interval.start.getInstant().isBefore(instant);
    }

    public static boolean endsAtOrAfter(TimeInterval interval, Instant instant) {
        return interval.end.getInstant().equals(instant)
                || endsAfter(interval, instant);
    }

    public static boolean endsAfter(TimeInterval interval, Instant instant) {
        return interval.end.getInstant().isAfter(instant);
    }

    public static boolean endsBefore(TimeInterval interval, Instant instant) {
        return interval.end.getInstant().isBefore(instant);
    }

    public Duration getDuration() {
        return Duration.between(start.getInstant(), end.getInstant());
    }

    public boolean overlapsInclusiveStart(TimeInterval other) {
        return this.containsInclusiveStart(other.getStart().getInstant())
                || this.contains(other.getEnd().getInstant());
    }
}
