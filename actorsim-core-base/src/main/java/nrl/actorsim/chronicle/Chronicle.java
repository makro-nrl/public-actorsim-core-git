package nrl.actorsim.chronicle;


import nrl.actorsim.domain.BindingConstraint;
import nrl.actorsim.domain.NamedPathImpl;
import nrl.actorsim.domain.PersistenceStatement;
import nrl.actorsim.domain.TransitionStatement;

import java.util.List;

/**
 * A Chronicle as described in APA, Chapter 4, pp. 125ff.
 */
public class Chronicle extends NamedPathImpl {
    List<PersistenceStatement<?>> persistentAssertions;
    List<TransitionStatement<?>> transitionAssertions;

    List<TimepointConstraint> temporalConstraints;
    List<BindingConstraint> bindingConstraints;

    Chronicle() {

    }
}
