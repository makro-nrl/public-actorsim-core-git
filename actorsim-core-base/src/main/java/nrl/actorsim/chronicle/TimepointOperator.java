package nrl.actorsim.chronicle;

public enum TimepointOperator {
    NOOP(),
    EQ("=="),
    NEQ("!="),
    LT("<"),
    LTE("<="),
    GT(">"),
    GTE(">=");

    private final String symbol;

    TimepointOperator() {
        this.symbol = "NOOP";
    }

    TimepointOperator(String symbol) {
        this.symbol = symbol;
    }
}
