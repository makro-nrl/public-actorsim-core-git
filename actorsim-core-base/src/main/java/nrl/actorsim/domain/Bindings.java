package nrl.actorsim.domain;

import nrl.actorsim.utils.PrintOptionAware;
import nrl.actorsim.utils.PrintOptions;

import java.util.*;

import static nrl.actorsim.utils.Utils.*;


/**
 * An unordered list of bindings.
 */
public class Bindings extends ArrayList<Binding> implements PrintOptionAware {

    public Bindings() {

    }

    public Bindings(Map<? extends Bindable, ? extends Bindable> map) {
        for(Map.Entry<? extends Bindable, ? extends Bindable> entry : map.entrySet()) {
            add(Binding.equal(entry.getKey(), entry.getValue()));
        }
    }

    @Override
    public String toString() {
        StringBuilder result = new StringBuilder();
        String sep = "";
        for (Binding binding : this) {
            result.append(sep);
            result.append(binding.toString());
            sep = ", ";
        }
        return result.toString();
    }

    @Override
    public String toString(PrintOptions options) {
        StringBuilder result = new StringBuilder();
        String sep = "";
        for (Binding binding : this) {
            result.append(sep);
            result.append(binding.toString(options));
            sep = ", ";
        }
        return result.toString();
    }

    /**
     * Return a new Bindings object where each
     * individual binding is a *reference copy*
     * of the original bindings.
     *
     * @return a new instance of Bindings
     */
    public Bindings copy() {
        return copy(this);
    }


    /**
     * Return a new Bindings object where each
     * individual binding is a *reference copy*
     * of the original bindings.
     *
     * @param bindings the bindings to copy
     * @return a new instance of Bindings
     */
    public static Bindings copy(Bindings bindings) {
        Bindings tmpBindings = new Bindings();
        tmpBindings.addAll(bindings);
        return tmpBindings;
    }
}
