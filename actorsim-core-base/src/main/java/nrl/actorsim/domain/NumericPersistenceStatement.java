package nrl.actorsim.domain;

import java.util.List;

/**
 * A convenience {@link PersistenceStatement<Number>} that hides
 * the generic template.
 *
 * All new functionality should be added to {@link PersistenceStatement}.
 */
public class NumericPersistenceStatement extends PersistenceStatement<Number> {
    public static final NumericPersistenceStatement NULL_STATEMENT =  StateVariableTemplate.NULL_SVT.numericPersistenceStatement();

    public NumericPersistenceStatement(StateVariableTemplate svt, WorldObject binding, Number value) {
        super(svt, binding, value);
    }

    public NumericPersistenceStatement(StateVariableTemplate svt, List<WorldObject> bindings, Number value) {
        super(svt, bindings, value);
    }

    public NumericPersistenceStatement(StateVariable sv, List<WorldObject> bindings, Number value) {
        super(sv, bindings, value);
    }

    public NumericPersistenceStatement(StateVariable sv, Number value) {
        super(sv, value);
    }

    public NumericPersistenceStatement(StateVariable sv) {
        super(sv);
    }

    public NumericPersistenceStatement assign(Number value) {
        return (NumericPersistenceStatement) super.assign(value);
    }

}
