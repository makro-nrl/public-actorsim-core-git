package nrl.actorsim.domain;

import java.util.List;


/**
 * A convenience {@link PersistenceStatement<Boolean>} that hides
 * the generic template.
 */
public class BooleanPersistenceStatement extends PersistenceStatement<Boolean> {
    public BooleanPersistenceStatement(StateVariableTemplate svt, WorldObject binding, Boolean value) {
        super(svt, binding, value);
    }

    public BooleanPersistenceStatement(StateVariableTemplate svt, List<WorldObject> bindings, Boolean value) {
        super(svt, bindings, value);
    }

    public BooleanPersistenceStatement(StateVariable sv, List<WorldObject> bindings, Boolean value) {
        super(sv, bindings, value);
    }

    public BooleanPersistenceStatement(StateVariable sv, Boolean value) {
        super(sv, value);
    }

    public BooleanPersistenceStatement(StateVariable sv) {
        super(sv);
    }
}
