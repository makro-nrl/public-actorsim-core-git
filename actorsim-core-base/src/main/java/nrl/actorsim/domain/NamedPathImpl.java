package nrl.actorsim.domain;


import nrl.actorsim.utils.PrintOptions;
import org.jetbrains.annotations.NotNull;

import java.util.*;

/**
 * A default base class for NamedPaths.
 * Subclasses that cannot extend this class can incorporate
 * this class as a class member or implement their own
 * {@link NamedPath} and rely on static details in this class.
 */
public class NamedPathImpl implements NamedPath {
    final static org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(NamedPathImpl.class);

    public static String ROOT = "/";
    public static String UNBOUND_ROOT = "~";
    public static String PARENT_SEP = "^";
    public static String CHILD_SEP = ".";
    public static NamedPath EMPTY_PATH;
    static {
        EMPTY_PATH = new NamedPathImpl();
        EMPTY_PATH.addPathParent(EMPTY_PATH);
    }

    protected Options pathOptions;
    protected String pathName = "";
    protected NamedPath pathParent = EMPTY_PATH;
    protected List<NamedPath> pathChildren = Collections.emptyList();

    protected NamedPathImpl() {
        this.pathOptions = createDefaultOptions();
    }


    protected NamedPathImpl(String pathName) {
        this();
        setPathName(pathName);
    }

    @Override
    public String toString() {
        return toStringPath();
    }

    @Override
    public String toString(PrintOptions options) {
        return toStringPath(options);
    }

    @Override
    public String toStringPath() {
        return NamedPath.super.toStringPath();
    }

    @Override
    public String toStringPath(PrintOptions options) {
        return NamedPath.super.toStringPath(options);
    }

    // ====================================================
    // region<Static Helpers>

    public static Options createDefaultOptions() {
        return new Options();
    }

    public static
    <T> T castContent(Object content) {
        T value = null;
        try {
            //noinspection unchecked
            value = (T) content;
        } catch (ClassCastException e) {
            if (content instanceof Bindable) {
                Bindable bindable = (Bindable) content;
                logger.error("Problem casting NamedPath {}", bindable.getPathName(), e);
            } else {
                logger.error("Problem casting content", e);
            }
        }
        return value;

    }

    // endregion
    // ====================================================


    // ====================================================
    // region<NamedPath Interface>

    @Override
    public Options getPathOptions() { return pathOptions; }

    @Override
    public String getPathName() {
        return pathName;
    }

    @Override
    public void setPathName(String pathName) {
        this.pathName = pathName;
    }

    @Override
    public NamedPath getPathParent() {
        return pathParent;
    }

    @Override
    public List<NamedPath> getPathChildren() {
        return Collections.unmodifiableList(pathChildren);
    }

    @Override
    public void addPathParent(@NotNull NamedPath pathParent) {
        this.pathParent = pathParent;
    }

    @Override
    public void removePathParent() {
        this.pathParent = EMPTY_PATH;
    }

    private void initPathChildrenIfNeeded() {
        if (pathChildren == Collections.EMPTY_LIST) {
            pathChildren = new ArrayList<>();
        }
    }

    @Override
    public void addPathChild(NamedPath pathChild) {
        if (canAddChild()) {
            initPathChildrenIfNeeded();
            pathChildren.add(pathChild);
        } else {
            String msg = String.format("Max (%d) reached; cannot add child '%s'", pathOptions.maxAllowedChildren, pathChild);
            throw new UnsupportedOperationException(msg);
        }
    }

    @Override
    public void removePathChild(NamedPath child) {
        pathChildren.remove(child);
    }

    // endregion
    // ====================================================
}
