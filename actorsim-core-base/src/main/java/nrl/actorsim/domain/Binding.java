package nrl.actorsim.domain;

import nrl.actorsim.utils.PrintOptionAware;
import nrl.actorsim.utils.PrintOptions;
import org.jetbrains.annotations.NotNull;

/**
 * Specifies a (possibly empty) link between two {@link NamedPath}s.
 * Usually, the link exists between a WorldArgument of a StateProperty
 * and a WorldObject from a domain.
 *
 */
public class Binding implements Comparable<Binding>, PrintOptionAware {
    Bindable source;
    Bindable target;
    boolean equals = true;

    private Binding(Bindable source, Bindable target) {
        this.source = source;
        this.target = target;
    }

    public Binding(Binding other) {
        this.source = other.source;
        this.target = other.target;
        this.equals = other.equals;
    }

    public static Binding equal(Bindable source, Bindable target) {
        if (source.canBindTo(target)) {
            return new Binding(source, target);
        }
        throw new IllegalArgumentException();
    }

    public static Binding notEqual(Bindable source, Bindable target) {
        if (source.canBindTo(target)) {
            Binding instance = new Binding(source, target);
            instance.equals = false;
            return instance;
        }
        throw new IllegalArgumentException();
    }

    @Override
    public String toString() {
        String eqString = "==";
        if (!equals) {
            eqString = "!=";
        }
        return source + eqString + target;
    }

    @Override
    public String toString(PrintOptions printOptions) {
        String sourceString = "";
        String targetString = "";
        if (printOptions.pathNameOnly()) {
            sourceString = source.getPathName();
            targetString = target.getPathName();
        } else {
            if (source != null) {
                sourceString = source.toString(printOptions);
            }
            if (target != null) {
                targetString = target.toString(printOptions);
            }
        }
        String eqString = "==";
        if (!equals) {
            eqString = "!=";
        }
        return sourceString + eqString + targetString;
    }


    @Override
    public int compareTo(@NotNull Binding other) {
        int sourceCompare = compareToSources(this, other);
        if (sourceCompare == 0) {
            return compareToTargets(this, other);
        }
        return sourceCompare;
    }

    private int compareToSources(Binding lBinding, Binding rBinding) {
        Bindable lBindable = lBinding.source;
        Bindable rBindable = rBinding.source;

        return lBindable.getPathName().compareTo(rBindable.getPathName());
    }

    private int compareToTargets(Binding lBinding, Binding rBinding) {
        Bindable lBindable = lBinding.target;
        Bindable rBindable = rBinding.target;

        return lBindable.getPathName().compareTo(rBindable.getPathName());
    }

    public Bindable getSource() {
        return source;
    }

    public Bindable getTarget() {
        return target;
    }
}
