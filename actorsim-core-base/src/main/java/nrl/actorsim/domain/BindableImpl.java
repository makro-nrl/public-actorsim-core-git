package nrl.actorsim.domain;


import nrl.actorsim.utils.PrintOptions;
import nrl.actorsim.utils.Utils;
import org.apache.commons.lang3.NotImplementedException;
import org.slf4j.*;
import java.util.*;

/**
 * A base class that implements NamedPath and Bindable interface
 * to objects in the system.
 *
 * Assumptions:
 *   - Each argument gets a unique name, so its path in the tree is unique
 *   - A Bindable stores the arguments and their bindings
 *   - Bindable.Options control the way bindings are resolved
 */
public class BindableImpl
        extends NamedPathImpl
        implements Bindable
{
    protected final org.slf4j.Logger logger  = LoggerFactory.getLogger(BindableImpl.class);

    protected final Bindable.Options bindableOptions;

    protected List<WorldArgument> args;

    /**
     * The bindings stored at this node.
     *
     * Clients should not directly access this variable because
     * subclasses may use bindings from a parent or child
     * based on Bindable.Options.
     */
    private Bindings bindings;

    private List<Runnable> changeListeners = null;

    // ====================================================
    // region<Static Helpers>

    /**
     * Creates a Bindable parent that wraps the provided child
     * that defers all binding operations to the child.
     * @param child the child to wrap
     * @return a BindableImpl that wraps the child
     */
    public static BindableImpl createWrapper(BindableImpl child) {
        BindableImpl parent = new BindableImpl();
        parent.getPathOptions().setWrapperOptions();
        NamedPath.link(parent, child);
        return parent;
    }

    /**
     * Creates a Bindable child of the provided parent
     * that defers all bindings operations to the parent.
     * @param parent the parent to which to add a child
     * @return the dependent BindableImpl
     */
    @SuppressWarnings("unused")
    public static BindableImpl createDependent(BindableImpl parent) {
        BindableImpl child = new BindableImpl();
        child.getPathOptions().setDependentOptions();
        NamedPath.link(parent, child);
        return child;
    }

    @SuppressWarnings("unused")
    public static BindableImpl copyArgsInstance(BindableImpl other) {
        BindableImpl instance = new BindableImpl();
        instance.args = new ArrayList<>(other.args);
        return instance;
    }

    // endregion
    // ====================================================

    // ====================================================
    // region<Constructor, Object, Accessors>

    public BindableImpl() {
        bindableOptions = createDefaultOptions();
        pathOptions = bindableOptions;
    }

    public BindableImpl(String pathName) {
        bindableOptions = createDefaultOptions();
        pathOptions = bindableOptions;
        setPathName(pathName);
    }

    /**
     * Create a deep copy of bindable details.
     * @param other the Bindable to copy
     */
    public BindableImpl(Bindable other) {
        this.bindableOptions = new Bindable.Options(other.getBindableOptions());
        cloneBindable(other);
    }

    /**
     * Create a deep copy of bindable details.
     * @param other the Bindable to copy
     */
    public void cloneBindable(Bindable other) {
        validateWithinClassHierarchyThrow(other);
        setPathName(other.getPathName());
        cloneArgs(other);
        cloneBindings(other);
        clonePrintOptions(other);
    }

    public void cloneBindings(Bindable other) {
        for (Binding binding : other.getBindingsAtThisNodeOnly()) {
            addBinding(new Binding(binding));
        }
    }

    public void cloneArgs(Bindable other) {
        for (WorldArgument arg : other.getArgs()) {
            addArgument(new WorldArgument(arg));
        }
    }

    public void clonePrintOptions(Bindable other) {
        setPrintOptions(new PrintOptions(other.getPrintOptions()));
    }

    /**
     * Ensure that we are within the same class hierarchy
     * so that cloning makes sense.
     *
     * @param other the other Bindable to check
     * @throws IllegalArgumentException if the check fails
     */
    private void validateWithinClassHierarchyThrow(Bindable other) {
        if (other.getClass().isAssignableFrom(this.getClass())) {
            return;
        }

        Class<?> thisSuper = this.getClass().getSuperclass();
        Class<?> otherSuper = other.getClass().getSuperclass();
        Object o = new Object();
        if ((thisSuper != o.getClass())
                && (thisSuper == otherSuper)) {
            return;
        }
        if (otherSuper.isAssignableFrom(thisSuper)) {
            return;
        }

        String message = String.format("Cannot clone bindable '%s' outside its class hierarchy", other);
        throw new IllegalArgumentException(message);
    }

    public static Bindable.Options createDefaultOptions() {
        return new Bindable.Options();
    }

    private void initArgsIfNeeded() {
        if (args == null) {
            args = new ArrayList<>();
        }
    }

    private void initBindingsIfNeeded() {
        if (bindings == null) {
            bindings = new Bindings();
        }
    }

    @Override
    public String toString() {
        return "BindableImpl{}";
    }

    public String toStringArgs() {
        return Utils.collectArgs(args, getPrintOptions());
    }

    public String toStringArgs(PrintOptions options) {
        return Utils.collectArgs(args, options);
    }

    public String toStringBindings() {
        if (bindings == null && !bindableOptions.includeParentBindings && !bindableOptions.useParentBindingsOnly) {
            return Utils.collectBindings(null, getPrintOptions());
        }
        return Utils.collectFoundBindings(findBindings(), getPrintOptions());
    }

    public String toStringBindings(PrintOptions options) {
        if (bindings != null) {
            return Utils.collectBindings(getBindingsAtThisNodeOnly(), options);
        }
        return Utils.collectFoundBindings(findBindings(), options);
    }

    // endregion
    // ====================================================

    // ====================================================
    // region<Arguments>


    @Override
    public WorldArgument addArgument(WorldArgument arg) {
        confirmArgumentIsUnique_Throw(arg);
        WorldArgument newArg = new WorldArgument(arg);
        initArgsIfNeeded();
        args.add(newArg);
        NamedPath.link(this, newArg);
        return newArg;
    }


    @Override
    public WorldArgument specializeArgument(WorldArgument arg) {
        WorldArgument argToReplace = WorldArgument.NULL_WORLD_ARGUMENT;
        for (WorldArgument tmpArg : args) {
            if (tmpArg.getPathName().equals(arg.getPathName())) {
                argToReplace = tmpArg;
                break;
            }
        }
        if (argToReplace != WorldArgument.NULL_WORLD_ARGUMENT) {
            if (arg.equalsOrInheritsFromType(argToReplace)) {
                args.remove(argToReplace);
                NamedPath.unlink(this, argToReplace);
            }
        }
        addArgument(arg);
        return argToReplace;
    }

    private void confirmArgumentIsUnique_Throw(WorldArgument arg) {
        if (getArgs().contains(arg)) {
            String message = String.format("%s: Cannot add an argument with the same name '%s'", this, arg);
            logger.error(message);
            throw new IllegalArgumentException(message);
        }
    }

    public List<WorldArgument> getArgs() {
        if (bindableOptions.deferArgumentsToChild) {
            Bindable firstChild =  getFirstChild().content();
            return firstChild.getArgs();
        }
        if (args == null) {
            return Collections.emptyList();
        }
        return Collections.unmodifiableList(args);
    }

    // endregion
    // ====================================================

    //===========================================
    // region<Bindings>

    @Override
    public BindResult setBindings(Bindings bindings) {
        if(getBindableOptions().useParentBindingsOnly) {
            Bindable parentBindable = getPathParent().content();
            return parentBindable.setBindings(bindings);
        }
        this.bindings = bindings;
        return BindResult.SUCCESS;
    }

    @Override
    public void removeBindings() {
        bindings = null;
    }

    @Override
    public BindResult bind(Bindable source, Bindable target) {
        if (! canStore(source)) {
            String message = String.format("Bindable '%s' cannot store source '%s'", this, source);
            logger.error(message);
            return BindResult.cannotStoreSource(message);
        }

        if (! source.canBindTo(target)) {
            String message = String.format("Source '%s' disallows bind to target '%s'", source, target);
            logger.error(message);
            return BindResult.disallowedBinding(message);
        }

        if (! consistentWithConstraints(source, target)) {
            String message = String.format("Binding ('%s', '%s') violates constraints", source, target);
            logger.error(message);
            return BindResult.violatesBindingConstraint(message);
        }
        addBindingImpl(Binding.equal(source, target));
        return BindResult.SUCCESS;
    }

    public Binding addBinding(Binding binding) {
        Binding newBinding = new Binding(binding);
        return addBindingImpl(newBinding);
    }
    private Binding addBindingImpl(Binding binding) {
        initBindingsIfNeeded();
        bindings.add(binding);
        return binding;
    }

    public boolean hasBindings() {
        return bindings != null
                && bindings.size() > 0;
    }

    @Override
    public boolean hasImpliedBindings() {
        if (getBindableOptions().useParentBindingsOnly) {
            return ((Bindable)getPathParent()).hasImpliedBindings();
        }
        return hasBindings();
    }

    @Override
    public Bindings findBindings() {
        NamedPath parent = getPathParent();
        if (getBindableOptions().useParentBindingsOnly) {
            if (parent != NamedPathImpl.EMPTY_PATH) {
                Bindable parentBindable = parent.content();
                return parentBindable.getBindingsAtThisNodeOnly();
            } else {
                return new Bindings();
            }
        }
        if (bindings != null) {
            Bindings returnedBindings = new Bindings();
            returnedBindings.addAll(bindings);
            if (getBindableOptions().includeParentBindings
                    && parent.content() instanceof Bindable) {
                Bindable parentBindable = parent.content();
                returnedBindings.addAll(parentBindable.getBindingsAtThisNodeOnly());
            }
            return returnedBindings;
        }

        if (bindings == null) {
            return new Bindings();
        }

        return bindings;
    }

    public Bindings getBindingsAtThisNodeOnly() {
        if (bindings == null) {
            return new Bindings();
        }
        return bindings;
    }

    @Override
    public WorldObject getBinding(String name) {
        if(bindableOptions.useParentBindingsOnly) {
            return getBindingFromParent(name);
        }

        for (Binding binding : getBindingsAtThisNodeOnly()) {
            if (!binding.equals) {
                continue;
            }
            if (binding.source.getPathName().equals(name)) {
                return binding.target.content();
            }
        }

        if (bindableOptions.includeParentBindings) {
            return getBindingFromParent(name);
        }
        return WorldObject.NULL_WORLD_OBJECT;
    }

    private WorldObject getBindingFromParent(String name) {
        if (pathParent instanceof Bindable) {
            return ((Bindable) pathParent).getBinding(name);
        }
        return WorldObject.NULL_WORLD_OBJECT;
    }

    /**
     * Filters the bindings to those that match the bindable's arguments.
     *
     * @return the bindings that match this Bindable's arguments
     */
    public Bindings filteredBindings() {
        Bindings filteredBindings = new Bindings();
        List<WorldArgument> args = getArgs();

        findBindings().stream()
                .filter(bind -> args.contains(bind.source))
                .forEach(filteredBindings::add);

        return filteredBindings;
    }

    // endregion
    // ==========================================

    // ====================================================
    // region<Bindable Interface>

    @Override
    public Bindable.Options getPathOptions() {
        return bindableOptions;
    }

    @Override
    public Bindable.Options getBindableOptions() {
        return bindableOptions;
    }

    // endregion
    // ====================================================

    // ====================================================
    // region<Bindable Helpers>

    /**
     * Move the binding in ancestor to parent,
     * checking that no conflicting bindings occur between
     * ancestor and parent, including the parent.
     * @param parent the parent to store the binding
     * @param parentObj the binding to store
     * @param ancestor the ancestor to move from
     * @param ancestorObj the ancestor binding to remove
     * @return the result of the bind
     */
    @SuppressWarnings("unused")
    public static BindResult generalize(Bindable parent, WorldObject parentObj,
                                        Bindable ancestor, WorldObject ancestorObj) {
        throw new NotImplementedException("Not Yet Implemented");
        //return BindResult.FAILURE_UNSPECIFIED;
    }


    // endregion
    // ====================================================

    // ====================================================
    // region<ChangeNotifier>
    @Override
    public List<Runnable> getChangeListeners() {
        if (changeListeners == null) {
            changeListeners = new ArrayList<>();
        }
        return changeListeners;
    }

    // endregion
    // ====================================================

}
