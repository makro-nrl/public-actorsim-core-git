package nrl.actorsim.domain;

import nrl.actorsim.utils.ChangeNotifierImpl;
import nrl.actorsim.utils.PrintOptions;

import java.util.List;


/**
 * A temporary class to help organize and structure
 * passthrough code for using the Bindable interface.
 *
 */
class BindablePassthrough implements Bindable {
    BindableImpl bindable;

    // ====================================================
    // region<PrintOptionAware interface>

    @Override
    public String toString(PrintOptions options) {
        return toString();
    }

    // endregion
    // ====================================================

    //===============================================
    // region<NamedPath Interface>

    @Override
    public void setPathName(String pathName) {
        bindable.setPathName(pathName);
    }

    @Override
    public String getPathName() {
        return bindable.getPathName();
    }

    @Override
    public Bindable.Options getPathOptions() {
        return bindable.getPathOptions();
    }

    @Override
    public NamedPath getPathParent() {
        return bindable.getPathParent();
    }

    @Override
    public void addPathChild(NamedPath child) {
        bindable.addPathChild(child);
    }

    @Override
    public void removePathChild(NamedPath child) {
        bindable.removePathChild(child);
    }

    @Override
    public String getAbsolutePath() {
        return bindable.getAbsolutePath();
    }

    @Override
    public List<NamedPath> getPathChildren() {
        return bindable.getPathChildren();
    }

    @Override
    public boolean canAddChild() {
        return bindable.canAddChild();
    }

    @Override
    public void addPathParent(NamedPath pathParent) {
        bindable.addPathParent(pathParent);
    }

    @Override
    public void removePathParent() {
        bindable.removePathParent();
    }

    // endregion<NamedPath>
    //===============================================

    // ====================================================
    // region<Bindable>


    @Override
    public Options getBindableOptions() {
        return bindable.getBindableOptions();
    }

    @Override
    public <T> T content() {
        return NamedPathImpl.castContent(bindable);
    }

    @Override
    public WorldArgument addArgument(WorldArgument arg) {
        return bindable.addArgument(arg);
    }

    @Override
    public WorldArgument specializeArgument(WorldArgument newArg) {
        return bindable.specializeArgument(newArg);
    }

    @Override
    public BindResult setBindings(Bindings bindings) {
        return bindable.setBindings(bindings);
    }

    @Override
    public void removeBindings() {
        bindable.removeBindings();
    }

    @Override
    public WorldObject getBinding(String name) {
        return bindable.getBinding(name);
    }

    @Override
    public Binding addBinding(Binding binding) {
        return bindable.addBinding(binding);
    }

    @Override
    public BindResult bind(String arg, WorldObject obj) {
        return bindable.bind(arg, obj);
    }

    @Override
    public BindResult bind(Bindable arg, Bindable obj) {
        return bindable.bind(arg, obj);
    }

    // endregion
    // ====================================================


    @Override
    public void closeBindings() {
        bindable.closeBindings();
    }

    @Override
    public void openBindings() {
        bindable.openBindings();
    }


    // ====================================================
    // region<ChangeNotifier>

    ChangeNotifierImpl changeNotifier = new ChangeNotifierImpl();

    @Override
    public List<Runnable> getChangeListeners() {
        return changeNotifier.getChangeListeners();
    }

    // endregion
    // ====================================================
}