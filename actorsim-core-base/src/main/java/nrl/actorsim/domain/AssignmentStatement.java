package nrl.actorsim.domain;

import nrl.actorsim.utils.*;
import org.jetbrains.annotations.NotNull;

import java.util.List;
import java.util.Map;

/**
 * Defines a statement of the form
 * <p>
 * [start, end] sv(..) := value
 * <p>
 * where value is a generic.
 * <p>
 * An {@link AssignmentStatement} is equivalent to a literal or an atom.
 */
public class AssignmentStatement<V> extends Statement {
    final static org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(AssignmentStatement.class);

    V value;

    /**
     * Targets a new {@link StateVariable} instance from the template with new bindings and
     * assigns it to value.
     *
     * @param svt      the template used to build the new instance
     * @param bindings the bindings for the new instance
     * @param value    the value assigned to the instance
     */
    public AssignmentStatement(StateVariableTemplate svt, List<WorldObject> bindings, V value) {
        super(svt.copier().build(bindings));
        assign(value);
    }

    /**
     * Targets a new {@link StateVariable} instance from the template with new bindings and
     * assigns it to value.
     *
     * @param sv       the template used to build the new instance
     * @param bindings the bindings for the new instance
     * @param value    the value assigned to the instance
     */
    public AssignmentStatement(StateVariable sv, List<WorldObject> bindings, V value) {
        super(sv.copier().build());
        bindArgs(bindings);
        assign(value);
    }

    /**
     * Targets an existing {@link StateVariable} with a value.
     *
     * @param sv    the {@link StateVariable} to link
     * @param value the value to store
     */
    public AssignmentStatement(StateVariable sv, V value) {
        this(sv);
        assign(value);
    }

    /**
     * Targets an existing {@link StateVariable} without and assignment.
     *
     * @param sv the {@link StateVariable} to link
     */
    public AssignmentStatement(StateVariable sv) {
        super(sv);
    }

    @Override
    public String toStringValue() {
        return toStringValue(getPrintOptions());
    }

    @Override
    public String toStringValue(PrintOptions printOptions) {
        if (value instanceof PrintOptionAware) {
            return String.format(" := %s", ((PrintOptionAware) value).toString(printOptions));
        } else {
            return String.format(" := %s", value);
        }
    }

    public V getValue() {
        return value;
    }

    public void assign(V value) {
        this.value = value;
    }

    @SuppressWarnings("rawtypes")
    @Override
    public int compareTo(@NotNull Object other) {
        int statementCompare = super.compareTo(other);
        if (statementCompare == 0
                && (this.id != ((Statement)other).id)
                && other instanceof AssignmentStatement) {
            AssignmentStatement otherStatement = (AssignmentStatement) other;
            return compareValue(this.value, otherStatement.value);
        }
        return statementCompare;
    }

    @SuppressWarnings("unchecked")
    public Cloner<? extends AssignmentStatement<V>, ?> cloner() {
        return new Cloner<>(this);
    }

    public class Cloner<S extends AssignmentStatement<V>, B extends Cloner<S, B>>
            extends Statement.Cloner<S, B> {
        final V value;

        protected Cloner(AssignmentStatement<V> statement) {
            super(statement);
            this.value = statement.value;
        }


        @Override
        public S build() {
            //noinspection unchecked
            return (S) new AssignmentStatement<>(target, value);
        }
    }

}
