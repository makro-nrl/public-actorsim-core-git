package nrl.actorsim.domain;

import nrl.actorsim.domain.WorldObject;

import static nrl.actorsim.domain.WorldObject.NULL_WORLD_OBJECT;
import static nrl.actorsim.domain.WorldObject.PLACEHOLDER;

public class BindingConstraint {

    Operator op;
    WorldObject lhs;
    WorldObject rhs;
    enum Operator {
        EQ("=="),
        NEQ("!=");
        private String str;

        Operator(String str) {
            this.str = str;
        }

        @Override
        public String toString() {
            return str;
        }
    }

    public static BindingConstraint notEqual(String lName, String rName) {
        WorldObject lhs = PLACEHOLDER.instance(lName);
        WorldObject rhs = PLACEHOLDER.instance(rName);
        return new BindingConstraint(lhs, rhs, Operator.NEQ);
    }

    public static BindingConstraint notEqual(WorldObject lhs, WorldObject rhs) {
        return new BindingConstraint(lhs, rhs, Operator.NEQ);
    }

    private BindingConstraint(WorldObject lhs, WorldObject rhs, Operator op) {
        this.lhs = lhs;
        this.rhs = rhs;
        this.op = op;
    }

    @Override
    public String toString() {
        return lhs.toString() + " " + op + " " + rhs;
    }

    public boolean isViolated(Bindable bindable, Bindable source, Bindable target) {
        return ! isConsistent(bindable, source, target);
    }

    public boolean isConsistent(Bindable bindable, Bindable source, Bindable target) {
        if (lhs.compareToPathName(source) == 0) {
            WorldObject rhsBinding = bindable.getBinding(rhs.getPathName());
            if (rhsBinding != NULL_WORLD_OBJECT) {
                if (op == Operator.EQ) {
                    if (rhsBinding.compareToPathName(target) != 0) {
                        return false;
                    }
                } else {
                    if (rhsBinding.compareToPathName(target) == 0) {
                        return false;
                    }
                }
            }
        }
        if (rhs.compareToPathName(source) == 0) {
            WorldObject lhsBinding = bindable.getBinding(lhs.getPathName());
            if (lhsBinding != NULL_WORLD_OBJECT) {
                if (op == Operator.EQ) {
                    if (lhsBinding.compareToPathName(target) != 0) {
                        return false;
                    }
                } else {
                    if (lhsBinding.compareToPathName(target) == 0) {
                        return false;
                    }
                }
            }
        }
        return true;
    }
}
