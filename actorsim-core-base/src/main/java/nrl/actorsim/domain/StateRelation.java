package nrl.actorsim.domain;

/**
 * A StateProperty with bindable arguments that cannot be assigned a value.
 * See {@link StateProperty} for details.
 * An instance remains lifted until all variables are assigned.
 */
@SuppressWarnings("unused")
public class StateRelation extends StateProperty {
    final static org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(StateRelation.class);

    // ====================================================
    // region<Constructors>

    private StateRelation() {
        super();
    }

    /**
     * A shallow copy constructor. A deep copy can be built with its {@link Builder}.
     * <p>
     * This class uses a Builder to construct instances, so its constructors
     * should only be called by Builders.  For details, see {@link Builder}.
     *
     * @param other the StateRelation to copy
     */
    private StateRelation(StateRelation other) {
        super(other);
    }

    /**
     * A shallow copy constructor. A deep copy can be built with its {@link Builder}.
     * <p>
     * This class uses a Builder to construct instances, so its constructors
     * should only be called by Builders.  For details, see {@link Builder}.
     *
     * @param other the StateRelationTemplate to copy
     */
    private StateRelation(StateRelationTemplate other) {
        super(other);
    }

    // endregion
    // ====================================================

    // ====================================================
    // region<Static Builder Helpers>

    /**
     * A convenience method to return the correctly typed builder for this class.
     * <p>
     * Note: calling "Class.builder()" is the same as calling "new Class.Builder<>()"
     *
     * @return the builder for this class
     */
    public static
    Builder<? extends StateRelation, ? extends Builder<?,?>>
    builder() {
        return new Builder<>(new StateRelation());
    }

    public static
    Builder<? extends StateRelation, ? extends Builder<?,?>>
    copier(StateRelationTemplate template) {
        return new Builder<>(new StateRelation(template));
    }

    @Override
    public Builder<? extends StateRelation, ? extends Builder<?,?>> copier() {
        return new StateRelation.Builder<>(new StateRelation(this));
    }

    // endregion
    // ====================================================

    // ====================================================
    // region<Builder>

    public static class Builder<T extends StateRelation, B extends Builder<T, B>>
            extends StateProperty.Builder<T, B> {

        public Builder(StateRelationTemplate srt) {
            //noinspection unchecked
            super((T) new StateRelation(srt));
            init();
        }

        Builder(T instance) {
            super(instance);
            init();
        }

        private void init() {
            isBindable();
            isNotAssignable();
            if (instance.valueTypes == null) {
                resetValueTypes();
            }
        }



        protected StateRelation buildWithoutBindings() {
            return removeBindings()
                    .build();
        }

        public StateRelation build(WorldObject... bindings) throws IllegalArgumentException {
            return bindArgs(bindings)
                    .build();
        }

        protected StateRelation buildWithClonedBindings() {
            return cloneBindings()
                    .build();
        }

        public StateRelation buildWithClonedBindings(WorldObject... bindings) {
            return cloneBindings(bindings)
                    .build();
        }
    }

    // endregion
    // ====================================================
}
