package nrl.actorsim.domain;

import java.util.List;

/**
 * A convenience {@link BooleanPersistenceStatement} that stores a boolean value
 * which is always true by default. This is helpful in closed worlds
 * where a predicate in memory is always true.  For example, instead of
 * writing the persistence statement <code>loc(robot1) == dock1</code> one
 * can write <code>loc(robot1, dock1) [== true]</code> where the value
 * assignment is implied.
 *
 *
 * <p>
 * Use BooleanPersistenceStatement if the initial value should be false.
 */
public class PredicateStatement extends BooleanPersistenceStatement {
    /** A convenience statement that can be used for default statements or mocking.  */
    public static final PredicateStatement ALWAYS_TRUE_STATEMENT
            = new PredicateStatement(StateVariable.ALWAYS_TRUE_SV);

    public PredicateStatement(StateVariableTemplate svt, WorldObject binding) {
        super(svt, binding, true);
    }

    public PredicateStatement(StateVariableTemplate svt, List<WorldObject> bindings) {
        super(svt, bindings, true);
    }

    public PredicateStatement(StateVariable sv, List<WorldObject> bindings) {
        super(sv, bindings, true);
    }

    public PredicateStatement(StateVariable sv) {
        super(sv, true);
    }
}
