package nrl.actorsim.domain;

import java.io.Serializable;

public class WorldObjectWrapper implements Serializable {

    final Integer id;
    final String typeName;

    public WorldObjectWrapper(WorldObject worldObject) {
        id = Integer.parseInt(worldObject.getId());
        typeName = worldObject.typeName;
    }

    public Integer getId() {
        return id;
    }

    public String getTypeName() {
        return typeName;
    }
}
