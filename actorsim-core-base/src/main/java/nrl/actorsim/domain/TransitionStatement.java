package nrl.actorsim.domain;

import nrl.actorsim.utils.PrintOptionAware;
import nrl.actorsim.utils.PrintOptions;
import org.jetbrains.annotations.NotNull;

import java.util.List;
import java.util.Map;

/**
 * Defines a statement of the form
 * <p>
 * [start, end] sv(..) == from -> to,
 * <p>
 * where from and to are the same generic type.
 * <p>
 * A {@link TransitionStatement} is equivalent to a literal or an atom.
 */
public class TransitionStatement<V> extends Statement {
    final static org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(TransitionStatement.class);

    private V from;
    private V to;

    protected boolean showEstimatedTime = true;

    /**
     * Targets a new {@link StateVariable} instance from the template with new bindings and
     * assigns it transition values for from and to.
     *
     * @param svt      the template used to build the new instance
     * @param bindings the bindings for the new instance
     * @param from     the value at the start of the transition
     * @param to       the value at the end of the transition
     */
    public TransitionStatement(StateVariableTemplate svt, List<WorldObject> bindings, V from, V to) {
        super(svt.copier().build(bindings));
        assign(from, to);
    }

    /**
     * Targets a new {@link StateVariable} instance from the existing instance with new bindings and
     * assigns it transition values for from and to.
     *
     * @param sv       the StateVariable used to build the new instance
     * @param bindings the bindings for the new instance
     * @param from     the value at the start of the transition
     * @param to       the value at the end of the transition
     */
    public TransitionStatement(StateVariable sv, List<WorldObject> bindings, V from, V to) {
        super(sv.copier().build());
        bindArgs(bindings);
        assign(from, to);
    }

    /**
     * Targets an existing {@link StateVariable} with transition values for from and to.
     *
     * @param sv   the {@link StateVariable} to link
     * @param from the value at the start of the transition
     * @param to   the value at the end of the transition
     */
    public TransitionStatement(StateVariable sv, V from, V to) {
        super(sv);
        assign(from, to);
    }

    /**
     * Targets an existing {@link StateVariable} without an assignment.
     *
     * @param sv the {@link StateVariable} to link
     */
    public TransitionStatement(StateVariable sv) {
        super(sv);
    }

    @Override
    public String toStringValue() {
        return toStringValue(getPrintOptions());
    }

    @Override
    public String toStringValue(PrintOptions printOptions) {
        if (from instanceof PrintOptionAware) {
            return String.format(" == %s -> %s",
                    ((PrintOptionAware)from).toString(printOptions),
                    ((PrintOptionAware)to).toString(printOptions));
        } else {
            return String.format(" == %s -> %s", from, to);
        }
    }




    public void assign(V from, V to) {
        this.from = from;
        this.to = to;
    }

    @SuppressWarnings("rawtypes")
    @Override
    public int compareTo(@NotNull Object other) {
        int statementCompare = super.compareTo(other);
        if (statementCompare == 0
                && (this.id != ((Statement)other).id)
                && other instanceof TransitionStatement) {
            TransitionStatement otherStatement = (TransitionStatement) other;
            int fromCompare = compareValue(from, otherStatement.from);
            if (fromCompare == 0) {
                return compareValue(to, otherStatement.to);
            }
            return fromCompare;
        }
        return statementCompare;
    }

    @SuppressWarnings("unchecked")
    public Cloner<? extends TransitionStatement<V>, ?> cloner() {
        return new Cloner<>(this);
    }

    public class Cloner<S extends TransitionStatement<V>, B extends Cloner<S, B>>
            extends Statement.Cloner<S, B> {
        final V from;
        final V to;

        protected Cloner(TransitionStatement<V> statement) {
            super(statement);
            this.from = statement.from;
            this.to = statement.to;
        }


        @Override
        public S build() {
            //noinspection unchecked
            return (S) new TransitionStatement<>(target, from, to);
        }
    }


}
