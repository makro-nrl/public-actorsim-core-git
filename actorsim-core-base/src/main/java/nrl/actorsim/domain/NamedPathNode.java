package nrl.actorsim.domain;

public interface NamedPathNode {
    default <T extends NamedPath> T accept(NamedPathVisitor visitor) {
        return visitor.visit(this);
    }
}

