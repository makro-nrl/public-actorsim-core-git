package nrl.actorsim.domain;

import nrl.actorsim.utils.PrintOptionAware;
import nrl.actorsim.utils.PrintOptions;

public class WorldObjectAdapter<T> extends WorldObject {
    final protected T item;

    protected WorldObjectAdapter(WorldType type, T item) {
        super(type);
        this.item = item;
    }

    protected WorldObjectAdapter(WorldType type, T item, String id) {
        super(type, id);
        this.item = item;
    }

    public T getItem() {
        return item;
    }

    @Override
    public WorldObject instance() {
        WorldType type = TypeManager.get(type());
        return new WorldObjectAdapter<T>(type, item, id);
    }

    @Override
    public WorldObject instance(String idIn) {
        throw new IllegalArgumentException("Cannot change ID of existing WorldObjectAdapter, use a constructor instead of instance()");
    }

    public T asItem() {
        return item;
    }

    @Override
    protected String toStringImpl(PrintOptions options) {
        if (item instanceof PrintOptionAware) {
            return ((PrintOptionAware) item).toString(options);
        }
        return item.toString();
    }
}
