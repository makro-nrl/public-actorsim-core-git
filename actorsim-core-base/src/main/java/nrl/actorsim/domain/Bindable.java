package nrl.actorsim.domain;

import nrl.actorsim.utils.*;

import java.util.*;

import static nrl.actorsim.domain.NamedPathImpl.EMPTY_PATH;

public interface Bindable extends NamedPath, ChangeNotifier {

    enum BindResult {
        SUCCESS,

        FAILURE_UNSPECIFIED,
        FAILURE_SOURCE_DISALLOWS_BIND_TO_TARGET,
        FAILURE_BINDABLE_CANNOT_STORE_SOURCE,
        FAILURE_CANNOT_FIND_CHILD_NAME,
        FAILURE_VIOLATES_BINDING_CONSTRAINT,
        FAILURE_MISMATCHED_ARGUMENTS;

        String failureReason = "";

        public static BindResult cannotStoreSource(String message) {
            return FAILURE_BINDABLE_CANNOT_STORE_SOURCE.setReason(message);
        }

        public static BindResult disallowedBinding(String message) {
            return FAILURE_SOURCE_DISALLOWS_BIND_TO_TARGET.setReason(message);
        }

        public static BindResult violatesBindingConstraint(String message) {
            return FAILURE_VIOLATES_BINDING_CONSTRAINT.setReason(message);
        }

        public static BindResult hasMismatchedArguments(String message) {
            return FAILURE_MISMATCHED_ARGUMENTS.setReason(message);
        }

        public BindResult setReason(String reason) {
            this.failureReason = reason;
            return this;
        }

        public boolean isFailure() {
            return this != SUCCESS;
        }

        public boolean isSuccess() {
            return this == SUCCESS;
        }
    }

    /**
     * Allows a Bindable to restrict with which Bindable objects it can bind.
     * For example, {@link WorldArgument}s should only bind to {@link WorldObject}s.
     *
     * @param bindable the Bindable to confirm
     * @return
     */
    default boolean canBindTo(Bindable bindable) {
        return true;
    }


    /**
     * Allows a bindable to determine if the proposed binding would
     * violate a constraint.  If source is listed in constraints,
     * check whether target violates the constraint.
     * For example, a {@link StateRelation} may require arguments to differ.
     *
     * @param source the source of the binding
     * @param target the target of the binding
     * @return whether the proposed binding is consistent
     */
    default boolean consistentWithConstraints(Bindable source, Bindable target) {
        return true;
    }

    /**
     * Allows a Bindable to restrict the kinds of bindings it will store.
     * For example, {@link nrl.actorsim.chronicle.Chronicle}s should only
     * bind to {@link nrl.actorsim.chronicle.Timepoint}s
     *
     * @param bindable the Bindable to confirm
     * @return
     */
    default boolean canStore(Bindable bindable) {
        return true;
    }

    /**
     * Add a _copy_ of the provided argument this bindable.
     *
     * @param arg the WorldArgument to add
     * @return the added argument
     */
    WorldArgument addArgument(WorldArgument arg);


    /**
     * Replace an existing argument with the provided one
     * of the same path name so long as it is a subtype
     * of the original argument.
     * @param arg the argument to insert
     * @return the replaced argument or NULL_WORLD_ARGUMENT if replacement fails
     */
    WorldArgument specializeArgument(WorldArgument arg);

    /**
     * Gets the arguments for this bindable.
     * @return the arguments
     */
    default List<WorldArgument> getArgs() {
        return Collections.emptyList();
    }


    BindResult setBindings(Bindings bindings);

    void removeBindings();

    /**
     * Add a _copy_ of the provided binding this bindable.
     *
     * @return the added binding
     */
    Binding addBinding(Binding binding);

    default boolean hasImpliedBindings() {
        return false;
    }

    default boolean hasBindings() {
        return false;
    }

    /**
     * Returns exactly the bindings of this object.
     * If you want to get the best bindings for this object
     * then use findBindings();
     *
     * @return the bindings or an empty Bindings if no bindings exist
     */
    default Bindings getBindingsAtThisNodeOnly() {
        return new Bindings();
    }

    /**
     * Based on the options of this Bindable, traverse the Bindings Tree to
     * locate and return the appropriate bindings.
     *
     * @return the found bindings or an empty Bindings if no bindings exist
     */
    default Bindings findBindings() {
        return new Bindings();
    }

    WorldObject getBinding(String name);

    default BindResult bind(String pathName, WorldObject obj) {
        Bindable path = findChild(pathName).content();
        if (path == EMPTY_PATH) {
            String message = String.format("Unknown child:'%s' for '%s'", pathName, this);
            return BindResult.FAILURE_CANNOT_FIND_CHILD_NAME.setReason(message);
        }
        return bind(path, obj);
    }

    BindResult bind(Bindable source, Bindable target);

    /**
     * Unifies a Set of WorldObjects to this Bindable.
     *
     */
    default BindResult unify(Set<Bindable> objs) {
        return BindResult.FAILURE_UNSPECIFIED;
    }

    /**
     * Allows a parent to pull a binding up from a child.
     *
     */
    default BindResult bindFromChild(Bindable child, Bindable source, Bindable target) {
        return BindResult.FAILURE_UNSPECIFIED;
    }

    /**
     * Allows a child to establish its binding in a parent.
     *
     */
    default BindResult bindInParent(Bindable source, Bindable target) {
        return BindResult.FAILURE_UNSPECIFIED;
    }

    /**
     * Iterates from this bindable up through parents for a possible object
     * matching arg.  Returns the first match.
     */
    default WorldObject getFirstSpecificBindingFor(Bindable target) {
        return WorldObject.NULL_WORLD_OBJECT;
    }

    /**
     * Iterates from the root parent to this bindable
     * for a possible object matching arg.  Returns the first match.
     */
    default WorldObject getFirstGeneralBindingFor(Bindable target) {
        return WorldObject.NULL_WORLD_OBJECT;
    }


    // ====================================================
    // region<Options Helpers>
    @Override
    Bindable.Options getPathOptions();

    Bindable.Options getBindableOptions();

    default PrintOptions getPrintOptions() {
        Bindable.Options options = getBindableOptions();
        if (options.printOptions != null) {
            return options.printOptions;
        }
        if (getPathParent() != EMPTY_PATH) {
            Bindable parent = getPathParent().content();
            PrintOptions parentOptions = parent.getPrintOptions();
            if (parentOptions != null) {
                return parentOptions;
            } else {
                return parent.getPrintOptions();
            }
        }
        return PrintOptions.DEFAULT_OPTIONS;
    };

    default void setPrintOptions(PrintOptions printOptions) {
        Bindable.Options bindableOptions = getBindableOptions();
        bindableOptions.printOptions = printOptions;
    }

    /**
     * Close this Bindable to new bindings.
     */
    default void openBindings() {
        getBindableOptions().open = true;
    }


    /**
     * Close this Bindable to new bindings.
     */
    default void closeBindings() {
        getBindableOptions().open = false;
    }


    default void includeParentBindings() {
        getBindableOptions().includeParentBindings = true;
    }

    default void holdArgumentsOnly() {
        closeBindings();
        getBindableOptions().includeParentBindings = false;
    }

    default void disallowChildren() {
        getBindableOptions().disallowChildren();
    }

    default void excludeParentBindings() {
        getBindableOptions().includeParentBindings = false;
    }

    default void onlyUseParentBindings() {
        getBindableOptions().useParentBindingsOnly = true;
    }

    default void deferArgumentsToChild() {
        getBindableOptions().enableDeferArgumentsToChild();
    }


    // endregion
    // ====================================================

    // ====================================================
    // region<Options>

    public static class Options extends NamedPath.Options {
        /**
         * Whether this Bindable is open to (i.e., allows)
         * new bindings.  Closed by default so subclasses may
         * control when new bindings are allowed.
         * An example of needing to init closed is in
         * {@link StateProperty}, where bindings are disallowed
         * for some of its subclasses but allowed for others.
         */
        boolean open = true;


        boolean includeParentBindings = true;

         /**
          * Forces a bindable to use its parent bindings
          * Implies allowParentBindings.
          */
        boolean useParentBindingsOnly = false;

        /**
         * Allows shells to defer calls regarding arguments
         * to their child.  For example, a {@link Statement}'s
         * arguments come from its target {@link StateVariable}.
         *
         * Implies singleChildOnly
         */
        boolean deferArgumentsToChild = false;

        PrintOptions printOptions;

        public Options() {

        }

        public Options(Options other) {
            this.includeParentBindings = other.includeParentBindings;
            this.useParentBindingsOnly = other.useParentBindingsOnly;
            this.deferArgumentsToChild = other.deferArgumentsToChild;
            this.printOptions = other.printOptions;
        }

        public void enableDeferArgumentsToChild() {
            maxAllowedChildren = 1;
            deferArgumentsToChild = true;
        }

        public void setWrapperOptions() {
            maxAllowedChildren = 1;
            deferArgumentsToChild = true;
        }

        public void setDependentOptions() {
            useParentBindingsOnly = true;
        }
    }

    // endregion
    // ====================================================

}
