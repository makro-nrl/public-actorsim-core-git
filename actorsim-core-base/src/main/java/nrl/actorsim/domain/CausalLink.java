package nrl.actorsim.domain;

public class CausalLink {
    Statement source;
    Statement destination;

    CausalLink(Statement source, Statement destination) {
        this.source = source;
        this.destination = destination;
    }

    public boolean isLinkedTo(Statement other) {
        return source.equals(other)
                || destination.equals(other);
    }

    public boolean isSource(Statement other) {
        return source.equals(other);
    }

    public boolean isDestination(Statement other) {
        return destination.equals(other);
    }


    public boolean matches(Statement predecessor, Statement successor) {
        return source == predecessor
                    && destination == successor;
    }


}
