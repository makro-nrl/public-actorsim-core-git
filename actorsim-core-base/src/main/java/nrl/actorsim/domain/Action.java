package nrl.actorsim.domain;


import org.apache.commons.lang3.NotImplementedException;

/**
 * An Action is an instance of an Operator with bound variables.
 */
public class Action extends Operator {
    public static final Action NULL_ACTION = NULL_OPERATOR.instance();

    final boolean isValid;
    final StateAssignment state;

    Action(Operator operator) {
        super(operator);

        state = null;
        this.isValid = false;
    }

    /**
     * Destructively applies this action to the current state.
     */
    public void apply() {
        String msg = "State application is not yet implemented!";
        logger.error(msg);
        throw new NotImplementedException(msg);
        //if(isApplicable()) {
        // network.apply(state);
        //}
    }

    // ====================================================
    // region<WorldObjectAdapter>

    public AsWorldObject asWorldObject() {
        return new AsWorldObject(this);
    }

    public static class AsWorldObject extends WorldObjectAdapter<Action> {
        protected AsWorldObject(Action action) {
            super(WorldObject.ACTION, action, action.pathName);
        }
    }

    // endregion
    // ====================================================
}
