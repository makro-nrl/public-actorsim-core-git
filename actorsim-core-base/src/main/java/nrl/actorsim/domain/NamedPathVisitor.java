package nrl.actorsim.domain;

public interface NamedPathVisitor {
    public static final NamedPathVisitor TRANSFORMER = new NamedPathVisitorImpl();

//    <ReturnType> ReturnType visit(ReturnType value);


    <T extends NamedPath> T visit(NamedPath namedPath);
    <T extends NamedPathNode> T visit(NamedPathNode namedPathNode);

//    <ReturnType> ReturnType visit(WorldType type);
//    <ReturnType> ReturnType visit(WorldObject obj);
//    <ReturnType> ReturnType visit(WorldArgument arg);
//
//
//    <ReturnType> ReturnType visit(StateVariableTemplate svt);
    <ReturnType> ReturnType visit(StateVariable sv);
//    <ReturnType> ReturnType visit(StateRelationTemplate srt);
//    <ReturnType> ReturnType visit(StateRelation sr);
//
    <T extends NamedPathNode> T visit(Statement statement);
//    <ReturnType> ReturnType visit(Operator operator);
//    <ReturnType> ReturnType visit(Operator.Action operator);

    /**
     * Visits a root container of bindable objects.
     * This includes:
     * <ul>
     *     <li> {@link TypeManager}
     *     <li> {@link PlanningProblem}
     *     <li> {@link PlanningDomain}
     * </ul>
     * as well as root containers in other modules.
     * @param root
     * @param <ReturnType>
     * @return
     */
//    <ReturnType> ReturnType visit(NamedPathContainer root);

    // endregion
    // ====================================================
}

