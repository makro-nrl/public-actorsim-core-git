package nrl.actorsim.domain.test;

import nrl.actorsim.domain.*;

import java.util.HashMap;
import java.util.Map;

import static nrl.actorsim.domain.WorldType.BOOLEAN;

@SuppressWarnings("unused")
public class TestItemExistsDomain extends PlanningDomain {
    public static final WorldType ROBOT = new WorldType("robot");
    public static final WorldType ITEM = new WorldType("item");
    public static final WorldType PACKAGE = new WorldType("PACKAGE", ITEM);
    public static final WorldType FUEL = new WorldType("FUEL");

    public static final WorldType LOCATION = new WorldType("LOCATION");

    public static final StateVariableTemplate EXISTS = StateVariableTemplate.builder()
            .name("exists")
            .argTypes(ITEM)
            .valueTypes(BOOLEAN)
            .build();

    public static final StateVariableTemplate AT = StateVariableTemplate.builder()
            .name("at")
            .addArgument(ITEM.asArgument("item"))
            .valueTypes(LOCATION.asArgument("loc"))
            .build();

    public static final StateVariableTemplate DELIVERY_NEEDED = StateVariableTemplate.builder()
            .name("image_needed")
                .argTypes(ITEM, LOCATION)
                .valueTypes(WorldType.BOOLEAN)
                .build();

    public static final StateVariableTemplate AVAILABLE_FUEL = StateVariableTemplate.builder()
            .name("available_fuel")
                .argTypes(ROBOT)
                .valueTypes(WorldType.NUMBER)
                .build();

    public static final String[] itemNames
            = {"A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M",
            "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z"};
    public static WorldObject A;
    public static WorldObject B;
    public static WorldObject C;
    public static WorldObject D;
    public static WorldObject E;
    public static WorldObject F;
    public static WorldObject G;
    public static WorldObject H;
    public static WorldObject I;
    public static WorldObject J;
    public static WorldObject K;
    public static WorldObject L;
    public static WorldObject M;
    public static WorldObject N;
    public static WorldObject O;
    public static WorldObject P;
    public static WorldObject Q;
    public static WorldObject R;
    public static WorldObject S;
    public static WorldObject T;
    public static WorldObject U;
    public static WorldObject V;
    public static WorldObject W;
    public static WorldObject X;
    public static WorldObject Y;
    public static WorldObject Z;

    public static WorldObject loc1 = new WorldObject(LOCATION, "loc1");
    public static WorldObject loc2 = new WorldObject(LOCATION, "loc2");
    public static WorldObject loc3 = new WorldObject(LOCATION, "loc3");
    public static WorldObject loc4 = new WorldObject(LOCATION, "loc4");

    public static WorldObject r1 = new WorldObject(ROBOT, "rob1");

    public static Operator move;

    static {
        initObjects();
        initMoveOperator();
    }

    /**
     * A map of common items for use in testing networks and problems.
     * <p>
     * Normally, WorldObject instances would be created in the problem
     * instance, but these are placed here since some problem instances
     * would only use a subset of these objects.
     */
    public static Map<String, WorldObject> itemMap;

    private static void initObjects() {
        itemMap = new HashMap<>();
        for (String name : itemNames) {
            WorldObject item = ITEM.instance(name).hideTypeInformation();
            itemMap.put(name, item);
        }
        A = itemMap.get("A");
        B = itemMap.get("B");
        C = itemMap.get("C");
        D = itemMap.get("D");
        E = itemMap.get("E");
        F = itemMap.get("F");
        G = itemMap.get("G");
        H = itemMap.get("H");
        I = itemMap.get("I");
        J = itemMap.get("J");
        K = itemMap.get("K");
        L = itemMap.get("L");
        M = itemMap.get("M");
        N = itemMap.get("N");
        O = itemMap.get("O");
        P = itemMap.get("P");
        Q = itemMap.get("Q");
        R = itemMap.get("R");
        S = itemMap.get("S");
        T = itemMap.get("T");
        U = itemMap.get("U");
        V = itemMap.get("V");
        W = itemMap.get("W");
        X = itemMap.get("X");
        Y = itemMap.get("Y");
        Z = itemMap.get("Z");
    }

   private static void initMoveOperator() {
        move = new Operator("move");

        WorldArgument argFrom = move.addArgument(new WorldArgument(LOCATION, "from"));
        WorldArgument argTo = move.addArgument(new WorldArgument(LOCATION, "to"));
        WorldArgument argItem = move.addArgument(new WorldArgument(ITEM, "item"));
        Binding bindingFromNeqTo = move.addBinding(Binding.notEqual(argFrom, argTo));

        StateVariable itemAtFrom = AT.stateVariableBuilder()
                .specializeValueTypes(argFrom)
                .buildWithoutBindings();
        Statement preItemAtFrom = move.addPrecondition(itemAtFrom.persistenceStatement(LOCATION.getClass()));

        StateVariable itemAtTo = AT.stateVariableBuilder()
                .specializeValueTypes(argTo)
                .buildWithoutBindings();
        Statement effItemAtTo =  move.addEffect(itemAtTo.persistenceStatement(LOCATION.getClass()));
    }

    public static PredicateStatement existsStatement(WorldObject obj) {
        return new PredicateStatement(EXISTS, obj);
    }



    public TestItemExistsDomain(Options options) {
        super(options);
    }

    public TestItemExistsDomain() {
        super(Options.builder()
                .name("testItems")
                .closedWorldAssumption()
                .build());
    }



}
