package nrl.actorsim.domain;

import nrl.actorsim.utils.PrintOptions;
import org.jetbrains.annotations.NotNull;
import org.slf4j.LoggerFactory;

import java.time.Duration;
import java.util.*;

public class Operator extends BindableImpl implements Comparable<Object>  {
    public final static Operator NULL_OPERATOR = new Operator("No-op");
    protected final static org.slf4j.Logger logger = LoggerFactory.getLogger(Operator.class);

    protected final int id;
    protected final String label;
    protected List<Statement> preconditions;  //must exist in state
    protected List<Statement> effects;  // changes state
    private int numberOfInstances = 0;

    // ====================================================
    // region<Constructors and Instance Methods>

    public Operator(String name) {
        id = 0; //the operator is the zeroth instance
        label = name;
        init(name);
        holdArgumentsOnly();
    }

    protected Operator(Operator other) {
        super(other);
        label = other.label;
        other.numberOfInstances++;
        id = other.numberOfInstances;
        init(other.getPathName() + "_" + id);

        for(Statement precondition : other.preconditions) {
            addPrecondition(precondition);
        }
        for(Statement effect : other.effects) {
            addEffect(effect.cloner().build());
        }

    }

    private void init(String name) {
        setPathName(name);
        preconditions = new ArrayList<>();
        effects = new ArrayList<>();
    }

    public Action instance() {
        return new Action(this);
    }

    // endregion
    // ====================================================

    // ====================================================
    // region<Object Overrides>

    @Override
    public String toString() {
        return toString(getPrintOptions());
    }

    @Override
    public String toString(PrintOptions printOptions) {
        String name = getPathName();
        String argsString = toStringArgs(printOptions);
        String bindingsString = "";
        if (this instanceof Action){
            bindingsString = toStringBindings(printOptions);
        }
        return name + argsString + bindingsString;
    }

    // endregion
    // ====================================================

    // ====================================================
    // region<Accessors>

    public String getLabel() {
        return label;
    }

    // endregion
    // ====================================================

    /**
     * Adds a _copy_ of the provided precondition to this operator.
     *
     * @param precondition the effect to add
     * @return the added statement to allow chaining
     */
    public Statement addPrecondition(Statement precondition) {
        Statement newPrecondition = precondition.cloner().build();
        preconditions.add(newPrecondition);
        NamedPath.link(this, newPrecondition);
        newPrecondition.onlyUseParentBindings();
        return newPrecondition;
    }

    /**
     * Returns the preconditions
     *
     * @return an unmodifiable Collection of the effects
     */
    public Collection<Statement> getPreconditions() {
        return Collections.unmodifiableCollection(preconditions);
    }

    /**
     * Adds a _copy_ of the provided effect to this operator.
     *
     * @param effect the effect to add
     * @return the added Statement to allow chaining
     */
    public Statement addEffect(Statement effect) {
        Statement newEffect = effect.cloner().build();
        effects.add(newEffect);
        NamedPath.link(this, newEffect);
        return newEffect;
    }

    /**
     * Returns the effects
     *
     * @return an unmodifiable Collection of the effects
     */
    public Collection<Statement> getEffects() {
        return Collections.unmodifiableCollection(effects);
    }


    /**
     * A statement is complete if it is entailed by the StateAssignment.
     *
     * @param state the state to check
     * @return whether the state is entailed
     */
    public boolean isEntailedBy(StateAssignment state) {
        logger.debug("Checking whether state entails operator.");
        return false;
    }

    /***
     * Determines if this operator is applicable for the state.
     *
     * @return whether this method applies
     */
    @SuppressWarnings("SameReturnValue")
    public boolean isApplicable(StateAssignment state) {
        return false;
    }

    @Override
    public int compareTo(@NotNull Object otherObj) {
        return this.toString().compareTo(otherObj.toString());
    }

    public void add(List<WorldArgument> args) {
        for (WorldArgument arg : args) {
            addArgument(arg);
        }
    }


    public Duration getDuration() {
        return Duration.ofMinutes(1);
    }
}
