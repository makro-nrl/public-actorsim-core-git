package nrl.actorsim.domain;

import java.util.*;

public class PlanningProblem extends BindableImpl implements NamedPathContainer {
    final static org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(PlanningProblem.class);

    protected final Options options;
    protected Set<WorldType> objectTypes = new HashSet<>();
    protected Map<String, WorldObject> objectsMap = new HashMap<>();
    protected StateAssignment init;

    public PlanningProblem(Options options) {
        this.options = options;
        init = new StateAssignment(this);
    }

    public boolean add(WorldObject... objects) {
        boolean result = true;
        for (WorldObject obj : objects) {
            if (!add(obj)) {
                result = false;
                break;
            }
        }
        return result;
    }

    public boolean add(WorldObject object) {
        boolean result = true;
        if (options.allowNewObjects) {
            this.objectTypes.add(object);
            String name = object.getId();
            this.objectsMap.put(name, object);
            if (options.warnOnNew) {
                logger.warn("New object added {}", object);
            }
        } else {
            logger.error("Problem is closed and cannot accept new objects, ignoring {}", object);
            result = false;
        }
        return result;
    }

    /**
     * Attempts to find an object in the planning problem by name,
     * returning the defaultObject if not found.
     *
     * @param name the name of the object to find
     * @param defaultObject the default object to return if not found
     * @return the WorldObject if it is found
     */
    public WorldObject getObject(String name, WorldObject defaultObject) {
        return objectsMap.getOrDefault(name, defaultObject);
    }

    /**
     * Attempts to find an object in the planning problem by name,
     * throwing an {@link IllegalArgumentException} if the object's
     * name is not in the problem.
     *
     * This method is generally best used for testing.
     * For production code, use getObject(name, defaultObject).
     *
     * @param name the name of the object to find
     * @return the WorldObject if it is found
     * @throws IllegalArgumentException if the named object is not found
     */
    public WorldObject getObject(String name) throws IllegalArgumentException {
        if (objectsMap.containsKey(name)) {
            return objectsMap.get(name);
        } else {
            String msg = String.format("The planning problem does not have an object with the name '%s'", name);
            logger.error(msg);
            throw new IllegalArgumentException(msg);
        }
    }

    public Collection<WorldObject> getObjects() {
        return Collections.unmodifiableCollection(objectsMap.values());
    }

    public StateAssignment getNewState() {
        return new StateAssignment(this);
    }

    public void setInit(StateAssignment init) {
        this.init = init;
    }

    public static class Options {
        final PlanningDomain domain;
        String name;
        public final boolean warnOnNew = false;
        boolean allowNewObjects = true;

        private Options(PlanningDomain domain, String name) {
            this.domain = domain;
            this.name = name;
        }

        public void close() {
            allowNewObjects = false;
        }

        public boolean allowsNewObjects() {
            return allowNewObjects;
        }

        public static Builder builder() {
            return new Builder();
        }

        public void updateName(String name) {
            this.name = name;
        }

        public static class Builder {
            boolean allowNewObjects = true;
            private PlanningDomain domain;
            private String name;

            public Builder domain(PlanningDomain domain) {
                this.domain = domain;
                return this;
            }

            public Builder name(String name) {
                this.name = name;
                return this;
            }

            public Builder allowNewObjects() {
                allowNewObjects = true;
                return this;
            }

            public Builder disallowNewObjects() {
                return close();
            }

            public Builder close() {
                allowNewObjects = false;
                return this;
            }

            public Options build() {
                if (domain == null
                        || name == null) {
                    String msg = "You must specify a domain and name to instantiate a problem.";
                    logger.error(msg);
                    throw new IllegalArgumentException(msg);
                }

                Options options = new Options(domain, name);
                options.allowNewObjects = this.allowNewObjects;
                return options;
            }

        }

    }
}
