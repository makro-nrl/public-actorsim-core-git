package nrl.actorsim.domain;

public interface BindableContainer extends NamedPathContainer, Bindable {
}
