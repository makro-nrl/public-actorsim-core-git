package nrl.actorsim.domain;


import nrl.actorsim.utils.*;

import java.util.Map;

/**
 * A temporary holding Statement that is used during
 * development to hold values that will eventually
 * be valid instances of Statements.
 *
 */
public class HoldingStatement extends Statement {
    public String statementToHold;

    protected HoldingStatement(String statementToHold) {
        super(StateVariable.ALWAYS_TRUE_SV);
        this.statementToHold = statementToHold;
    }

    @Override
    public String toString() {
        return "HOLDING_STATEMENT:" + statementToHold;
    }

    @Override
    public String toStringValue() {
        return toString();
    }

    @Override
    public String toStringValue(PrintOptions printOptions) {
        return toString();
    }

    @Override
    public <S extends Statement, B extends Cloner<S, B>> Cloner<S, B> cloner() {
        return null;
    }
}
