package nrl.actorsim.domain;

import nrl.actorsim.utils.PrintOptionAware;
import nrl.actorsim.utils.PrintOptions;
import org.jetbrains.annotations.NotNull;

import java.util.Collections;
import java.util.List;

/**
 * Defines a statement of the form
 * <p>
 * sv(..) == value
 * <p>
 * where value is a generic type V.
 */
public class PersistenceStatement<V> extends Statement {
    @SuppressWarnings("unused")
    final static org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(PersistenceStatement.class);
    private V value;

    public PersistenceStatement(StateVariableTemplate svt, WorldObject binding, V value) {
        this(svt, Collections.singletonList(binding), value);
    }

    /**
     * Targets a new {@link StateVariable} instance from the template with new bindings and
     * assigns it to value.
     *
     * @param svt      the template used to build the new instance
     * @param bindings the bindings for the new instance
     * @param value    the value assigned to the instance
     */
    public PersistenceStatement(StateVariableTemplate svt, List<WorldObject> bindings, V value) {
        super(svt.asStateVariable());
        BindResult result = bindArgs(bindings);
        if (result.isFailure()) {
            throw new IllegalArgumentException(result.toString());
        }
        assign(value);
    }

    /**
     * Targets a new {@link StateVariable} instance from the existing instance with new bindings and
     * assigns it to value.
     *
     * @param sv       the StateVariable used to build the new instance
     * @param bindings the bindings for the new instance
     * @param value    the value assigned to the instance
     */
    public PersistenceStatement(StateVariable sv, List<WorldObject> bindings, V value) {
        super(sv.copier().build());
        bindArgs(bindings);
        assign(value);
    }

    /**
     * Targets an existing {@link StateVariable} with a value.
     *
     * @param sv    the {@link StateVariable} to link
     * @param value the value to store
     */
    public PersistenceStatement(StateVariable sv, V value) {
        this(sv);
        assign(value);
    }

    /**
     * Targets an existing {@link StateVariable} without an assignment.
     *
     * @param sv the {@link StateVariable} to link
     */
    public PersistenceStatement(StateVariable sv) {
        super(sv.copier().build());
    }

    @Override
    public String toStringValue() {
        return toStringValue(getPrintOptions());
    }

    @Override
    public String toStringValue(PrintOptions printOptions) {
        if (value instanceof PrintOptionAware) {
            return String.format(" == %s", ((PrintOptionAware) value).toString(printOptions));
        } else {
            return String.format(" == %s", value);
        }
    }

    @Override
    public String getShortName() {
        return toString();
    }

    public V getValue() {
        return value;
    }

    public PersistenceStatement<V> assign(V value) {
        validateValue(value);
        this.value = value;
        return this;
    }

    private void validateValue(V value) {
        if (value instanceof WorldType) {
            WorldType vType = (WorldType) value;
            boolean matchesAtLeastOneType = false;
            for (WorldType valueType : target.valueTypes) {
                    if (vType.equalsOrInheritsFromTypeThrow(valueType)) {
                        matchesAtLeastOneType = true;
                    }
            }
            if (! matchesAtLeastOneType) {
                String msg = String.format("The value '%s' does not match at least one value type.", value);
                logger.error(msg);
                throw new IllegalArgumentException(msg);
            }
        }
    }

    @SuppressWarnings("rawtypes")
    @Override
    public int compareTo(@NotNull Object otherObj) {
        int statementCompare = super.compareTo(otherObj);
        if (statementCompare == 0
                && (! this.id.equals(((Statement) otherObj).id))
                && otherObj instanceof PersistenceStatement) {
            return compareValues((PersistenceStatement) otherObj);
        }
        return statementCompare;
    }

    private int compareValues(PersistenceStatement otherObj) {
        PersistenceStatement otherStatement = (PersistenceStatement) otherObj;
        boolean thisValueNull = this.value == null;
        boolean otherValueNull = otherStatement.value == null;
        if (thisValueNull && otherValueNull) {
            return 0; //both statements have the same empty "value"
        } else if(thisValueNull) {
            return -1; //swap because other has more information
        } else if(otherValueNull) {
            return 1; //this has more information, keep ordering
        }
        return compareValue(this.value, otherStatement.value);
    }


    public int compareToResolved(@NotNull Object otherObj) {
        int statementCompare = super.compareTo(otherObj);
        if (statementCompare == 0
                && otherObj instanceof PersistenceStatement) {
            return compareValues((PersistenceStatement) otherObj);
        }
        return statementCompare;
    }

    @SuppressWarnings("unchecked")
    public Cloner<? extends PersistenceStatement<V>, ?> cloner() {
        return new Cloner<>(this);
    }

    public class Cloner<S extends PersistenceStatement<V>, B extends Cloner<S, B>>
            extends Statement.Cloner<S, B> {
        V value;

        protected Cloner(PersistenceStatement<V> statement) {
            super(statement);
            this.value = statement.value;
        }

        public void value(V value) {
            this.value = value;
        }

        public V getValue() { return value;}

        @SuppressWarnings("unchecked")
        @Override
        public S build() {
            PersistenceStatement<V> s = new PersistenceStatement<>(target);
            s.cloneBindings(originalStatement);
            s.clonePrintOptions(originalStatement);
            s.assign(value);
            s.setEstimatedInterval(estimatedInterval);
            return (S) s;
        }
    }

    // ====================================================
    // region<State and Value checking>

    @Override
    public boolean isEntailedBy(StateAssignment state) {
        return state.contains(this);
    }

    public double diffValues(@NotNull PersistenceStatement<?> other) {
        double diff = Double.MAX_VALUE;
        if (this.hasNumericValue()
                && other.hasNumericValue()) {
            double thisValue = ((Number) this.getValue()).doubleValue();
            double otherValue = ((Number) other.getValue()).doubleValue();
            return thisValue - otherValue;
        } else {
            logger.error("One of the values is a Number, so the following are incomparable this:{} other:{}", this, other);
        }

        return diff;
    }

    // endregion
    // ====================================================


}
