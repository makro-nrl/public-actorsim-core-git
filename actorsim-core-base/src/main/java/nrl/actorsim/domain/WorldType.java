package nrl.actorsim.domain;

import nrl.actorsim.utils.PrintOptionAware;
import nrl.actorsim.utils.PrintOptions;
import org.jetbrains.annotations.NotNull;

import java.util.*;

public class WorldType
        extends BindableImpl
        implements Comparable<WorldType>, PrintOptionAware
{
    public static WorldType OBJECT;
    public static WorldType ANY;
    public static WorldType NULL_TYPE;
    public static WorldType NIL;
    public static WorldType BOOLEAN;
    public static WorldType NUMBER;
    @SuppressWarnings("unused")
    final static org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(WorldType.class);

    static {
        initializeStaticTypes();
    }

    protected static void initializeStaticTypes() {
        //initialize in a specific order to avoid race conditions in Type.equalsType() and Type.compareToType()
        OBJECT = BuildRootType("object");
        ANY = BuildRootType("any");
        ANY.addSuperType(OBJECT);
        NULL_TYPE = BuildRootType("null");
        NIL = NULL_TYPE;
        BOOLEAN = new WorldType("bool");
        NUMBER = new WorldType("number");
    }


    protected final String typeName;

    // ====================================================
    // region<Constructors>

    /**
     * Creates a root type.
     */
    @SuppressWarnings("unused") //for unused parameter isRoot
    private WorldType(String typeName, boolean isRoot) {
        this.typeName = TypeManager.validate(typeName);
        TypeManager.add(this);
    }

    private static WorldType BuildRootType(String typeName) {
        return new WorldType(typeName, true);
    }

    /**
     * For subclasses only: Instantiates this instance of type.
     *
     * @param type       the new type to add
     */
    @SuppressWarnings("CopyConstructorMissesField") //typeName set by other constructor
    protected WorldType(WorldType type) {
        this(type.type());
        cloneBindable(type);  // in lieu of calling super(other)
    }

    /**
     * Convenience method that calls {@link WorldType}(typeName, WorldType.OBJECT).
     *
     * @param typeName the new type to add
     */
    public WorldType(String typeName) {
        this(typeName, WorldType.OBJECT);
    }

    /**
     * Convenience method that calls {@link WorldType}(typeName, superTypes).
     *
     * @param typeName   the new type to add
     * @param superTypes the superTypes of this type
     */
    public WorldType(String typeName, WorldType... superTypes) {
        this(typeName, Arrays.asList(superTypes));
    }

    /**
     * Creates a new type from typeName and adds it to the {@link TypeManager}, if needed.
     */
    public WorldType(String typeName, List<WorldType> superTypes) {
        this.typeName = TypeManager.validate(typeName);
        setPathName(typeName);
        Set<WorldType> supersWithObject = new TreeSet<>(superTypes);
        if (supersWithObject.size() == 0) {
            supersWithObject = new TreeSet<>(superTypes);
            supersWithObject.add(OBJECT);
        }
        if (TypeManager.missingType(typeName)) {
            WorldType newType = TypeManager.add(this);
            newType.addSuperType(supersWithObject);
        }
    }

    // endregion
    // ====================================================

    // ====================================================
    // region<Object, PrintOptionAware, and Comparable>

    @Override
    public String toString() {
        return toStringPath() + typeName;
    }

    @Override
    public String toString(PrintOptions options) {
        String typeString = "";
        if (! options.hideTyping()) {
            typeString += typeName;
        }
        return toStringPath(options) + typeString;
    }

    @Override
    public boolean equals(Object other) {
        if (other instanceof WorldType) {
            return equalsOrInheritsFromType((WorldType) other);
        }
        return false;
    }

    // ====================================================
    // Comparable Interface
    // ====================================================

    /**
     * Subclasses can override this compareTo() method  because we want
     * to ensure that comparisons are only done with respect to the actual objects.
     * <p>
     * If you want to store Types in a container, use a {@link TypeComparator}.
     *
     * @param other the type to compare to
     */
    @Override
    public int compareTo(@NotNull WorldType other) {
        return compareToType(other);
    }

    int compareToType(WorldType other) {
        return this.type().compareTo(other.type());
    }


    // endregion
    // ====================================================

    // ====================================================
    // region<Type Interface>

    @SuppressWarnings("unused")
    boolean isInstance() {
        return false;
    }

    public String type() {
        return typeName;
    }

    public WorldObject instance(String id) {
        return new WorldObject(this, id);
    }

    public WorldArgument asArgument(String argumentName) {
        return new WorldArgument(this, argumentName);
    }

    public WorldArgument asArgument() {
        return new WorldArgument(this, this.type().toLowerCase());
    }

    @SuppressWarnings("UnusedReturnValue")
    public WorldType addSuperType(WorldType... superTypes) {
        return TypeManager.add(this, superTypes);
    }

    @SuppressWarnings("UnusedReturnValue")
    public WorldType addSuperType(Set<WorldType> superTypes) {
        TypeManager.getInstance().addSuperTypes(this, superTypes);
        return this;
    }

    public WorldType addSuperType(String... superTypeNames)  {
        List<WorldType> superTypes = new ArrayList<>();
        for (String superTypeName : superTypeNames) {
            if (!superTypeName.equalsIgnoreCase(OBJECT.typeName)) {
                WorldType superType = TypeManager.add(superTypeName);
                superTypes.add(superType);
            }
        }
        TypeManager.add(this, superTypes);
        for (WorldType superType : superTypes) {
            TypeManager.addTransitiveSupers(this, superType);
            TypeManager.addSubtype(superType, this);
        }

        Set<WorldType> subTypes = TypeManager.getSubTypes(this);
        for (WorldType subType : subTypes) {
            TypeManager.addTransitiveSupers(subType, this);
        }

        return this;
    }


    Set<WorldType> superTypes() {
        return TypeManager.getSuperTypes(this);
    }

    Set<WorldType> subTypes() {
        return TypeManager.getSubTypes(this);
    }

    @SuppressWarnings("unused")
    void close() {
        TypeManager.close(this);
    }

    @SuppressWarnings("unused")
    boolean isClosed() {
        return TypeManager.isClosed(this);
    }

    // endregion
    // ====================================================

    // ====================================================
    // region<Type checking methods>


    /**
     * Determines if the specified Class variable can be
     * assigned to the this WorldType.
     *
     * For example, WorldType.BOOLEAN should accept values
     * from both WorldType.BOOLEAN as well as java.lang.Boolean.
     * Thus, BOOLEAN.canBeAssigned(java.lang.Boolean.class)
     * and BOOLEAN.canBeAssigned(BOOLEAN) are true.
     *
     * Similarly, WorldType.NUMBER should accept values
     * from any class inheriting from java.lang.Number.
     * Thus NUMBER.canBeAssigned(java.lang.Integer.class) or
     * NUMBER.canBeAssigned(java.lang.Float.class) are true.
     * @param cls the class to check against
     * @return whether cls can be assigned to this type
     */
    public boolean canBeAssigned(Class<?> cls) {
        boolean isDirectlyAssignable = WorldType.class.isAssignableFrom(cls);
        if (this.equalsOrInheritsFromType(BOOLEAN)) {
            boolean isJavaBoolean = Boolean.class.isAssignableFrom(cls);
            return  isJavaBoolean || isDirectlyAssignable;

        } else if (this.equalsOrInheritsFromType(NUMBER)) {
            boolean isNumber = Number.class.isAssignableFrom(cls);
            return isNumber || isDirectlyAssignable;
        }
        return isDirectlyAssignable;
    }

    public boolean canBeAssignedThrow(Class<?> cls) throws IllegalArgumentException {
        if (canBeAssigned(cls)) {
            return true;
        }
        String message = String.format("Class %s cannot be assigned to %s", cls, this);
        logger.error(message);
        throw new IllegalArgumentException(message);
    }


      // ====================================================
      // region<Static, throwing type checking methods>

    static boolean equalsOrInheritsFromTypeThrow(Bindings bindings) {
        for (Binding binding : bindings) {
            WorldArgument arg = binding.source.content();
            WorldObject obj = binding.target.content();
            obj.equalsOrInheritsFromTypeThrow(arg);
        }
        return true;
    }

    static boolean equalsOrInheritsFromTypeThrow(List<? extends WorldType> lhs,
                                                 List<? extends WorldType> rhs)
            throws IllegalArgumentException, IndexOutOfBoundsException {

        logger.trace("lhs:{} rhs:{}", lhs, rhs);
        if (lhs.size() == rhs.size()) {
            for (int i = 0; i < lhs.size(); ++i) {
                WorldType lhsType = lhs.get(i);
                WorldType rhsType = rhs.get(i);
                lhsType.equalsOrInheritsFromTypeThrow(rhsType);
            }
        } else {
            String message = String.format("lhs.size %d does not equal rhs.size %d",  lhs.size(), rhs.size());
            logger.error(message);
            throw new IndexOutOfBoundsException(message);
        }
        return true;
    }

    @SuppressWarnings("unused")
    static boolean equalsOrInheritsFromTypeThrow(WorldType lhs, WorldType rhs)
            throws IllegalArgumentException {
        return lhs.equalsOrInheritsFromTypeThrow(rhs);
    }

      // endregion
      // ====================================================

      // ====================================================
      // region<Instance, throwing type checking methods>

    boolean equalsOrInheritsFromTypeThrow(WorldType other)
            throws IllegalArgumentException {
        if (this.inheritsFrom(other)) {
            return true;
        } else if (this.type().equals(other.type())) {
            return true;
        } else {
            String message = String.format("Type %s mismatches type %s", this, other);
            logger.error(message);
            throw new IllegalArgumentException(message);
        }
    }

      // endregion
      // ====================================================

      // ====================================================
      // region<Static, NON-throwing type checking methods>
    // ====================================================
    static boolean equalsOrInheritsFromType(Bindings bindingList) {
        boolean equalsOrInherits = true;
        for (Binding binding : bindingList) {
            WorldArgument arg = binding.source.content();
            WorldObject obj = binding.target.content();
            if (!obj.equalsOrInheritsFromTypeThrow(arg)) {
                equalsOrInherits = false;
                break;
            }
        }
        return equalsOrInherits;
    }

    @SuppressWarnings("unused")
    static boolean equalsOrInheritsFromType(List<? extends WorldType> lhs,
                                            List<? extends WorldType> rhs) {
        boolean equalsOrInherits = true;
        if (lhs.size() == rhs.size()) {
            for (int i = 0; i < lhs.size(); ++i) {
                WorldType lhsType = lhs.get(i);
                WorldType rhsType = rhs.get(i);
                if (!lhsType.equalsOrInheritsFromType(rhsType)) {
                    equalsOrInherits = false;
                    break;
                }
            }
        }
        return equalsOrInherits;
    }

      // endregion
      // ====================================================

      // ====================================================
      // region<Instance, NON-throwing type checking methods>

    public boolean typeEquals(WorldType type) {
        return equalsOrInheritsFromType(type);
    }

    /**
     * Determine if this type inherits from or is equal to other.
     * <p>
     * Note: call with this as the most specific type so that inheritance will work properly.
     *
     * @param other the type to check
     * @return whether the type is equal
     */
    public boolean equalsOrInheritsFromType(WorldType other) {
        if(other.type().equals(ANY.type())) {  // NB: using other.equals(ANY) will cause infinite recursion
            return true;
        } else if (this.inheritsFrom(other)) {
            return true;
        } else {
            return this.type().equals(other.type());  // NB: using this.equals(other) will cause infinite recursion
        }
    }

    public boolean inheritsFrom(WorldType type) {
        return TypeManager.inheritsFrom(this, type);
    }

      // endregion
      // ====================================================

    // endregion
    // ====================================================
}
