package nrl.actorsim.domain;

import java.util.Comparator;

public class TypeComparator implements Comparator<WorldType> {
    @Override
    public int compare(WorldType lhs, WorldType rhs) {
        return lhs.compareToType(rhs);
    }
}
