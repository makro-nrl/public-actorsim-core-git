package nrl.actorsim.domain;

import java.util.List;

/**
 * Defines a statement of the form
 * <p>
 * sv(..) != value
 * <p>
 * where value is a generic type.
 */
@SuppressWarnings("unused")
public class NegatedPersistenceStatement<V> extends PersistenceStatement<V> {
    public NegatedPersistenceStatement(StateVariableTemplate svt, WorldObject binding, V value) {
        super(svt, binding, value);
    }

    public NegatedPersistenceStatement(StateVariableTemplate svt, List<WorldObject> bindings, V value) {
        super(svt, bindings, value);
    }

    public NegatedPersistenceStatement(StateVariable sv, List<WorldObject> bindings, V value) {
        super(sv, bindings, value);
    }

    public NegatedPersistenceStatement(StateVariable sv, V value) {
        super(sv, value);
    }

    public NegatedPersistenceStatement(StateVariable sv) {
        super(sv);
    }

    @Override
    public boolean isNegated() {
        return true;
    }

    @Override
    @SuppressWarnings("unchecked")
    public Cloner<? extends NegatedPersistenceStatement<V>, ?> cloner() {
        return new Cloner<>(this);
    }

    public class Cloner<S extends NegatedPersistenceStatement<V>, B extends Cloner<S, B>>
            extends PersistenceStatement.Cloner {

        V value;
       protected Cloner(NegatedPersistenceStatement<V> statement) {
            super(statement);
            this.value = statement.getValue();
        }

        @SuppressWarnings("unchecked")
        @Override
        public S build() {
            NegatedPersistenceStatement<V> s = new NegatedPersistenceStatement<>(target);
            s.cloneBindings(originalStatement);
            s.assign(value);
            s.setEstimatedInterval(estimatedInterval);
            return (S) s;
        }
    }
}
