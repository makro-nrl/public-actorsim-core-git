package nrl.actorsim.domain;

import nrl.actorsim.utils.Named;
import nrl.actorsim.utils.PrintOptionAware;
import nrl.actorsim.utils.PrintOptions;
import nrl.actorsim.utils.Utils;
import org.jetbrains.annotations.NotNull;
import org.slf4j.LoggerFactory;

import java.util.*;

import static nrl.actorsim.utils.Utils.*;

/**
 * A StateProperty captures relationships or attributes of objects in the world.
 * This abstract base class consolidates comparisons, printing, type checking, etc.
 * <p>
 * This class uses a Builder to construct instances, so its constructors
 * should only be called by Builders.  For details, see {@link Builder}.
 * <p>
 * A StateProperty is distinguished by whether it is bindable or assignable,
 * properties we will clarify below.  An overview of how these properties relate
 * is shown in the following table, where an uppercase item (e.g., {@code DOCK})
 * indicates an object type while a lowercase object (e.g., {@code dock1})
 * indicates an object instance.
 * See <a href="../../../../../notation.html">Notation</a> for details.
 * <p>
 * <pre>
 *                  |                Arguments Bindable?                |
 *                  |           NO           |           YES            |
 * Assignable Value?|       (Template)       |       (Instance)         |
 * -----------------|---------------------------------------------------|
 *        NO        | StateRelationTemplate  | StateRelation            |
 *    (Relation)    | connected(DOCK, DOCK)  | connected(dock1, dock2)  |
 * -----------------|---------------------------------------------------|
 *       YES        | StateVariableTemplate  | StateVariable            |
 *    (Variable)    | location(ROBOT) = DOCK | location(robot1) = DOCK  |
 * ---------------------------------------------------------------------
 * </pre>
 * <p>
 * A StateProperty has several components:
 *    <ul>
 *    <li> *name*: is a {@link String} specifying its name.
 *    For example, "connected" or "location". </li>
 *    <li> *args*: is a List of {@link WorldArgument}s specifying the
 *    argument types (and possibly variable names)</li>
 *    <li> *bindings*: (**bindable only**) is a list of {@link WorldObject}s
 *    specifying bindings for args.
 *    Bindings must match the types or supertypes of args.</li>
 *    <li> *valueTypes*: (**assignable only**) is a list of {@link WorldArgument}s
 *    specifying the types of allowed assigned values.
 *    </li>
 *    </ul>
 *
 * Roughly, the two columns indicate a template vis-a-vis an instance,
 * where the abstract template specifies allowed types for an instance.
 * If a StateProperty is bindable (right column), then its arguments can
 * be bound with actual {@link WorldObject}s.
 * The arguments of a StateProperty may be bound completely, partially, or
 * not at all. If all arguments are bound then the StateProperty is said to
 * be *grounded*, otherwise it is said to be *lifted*.
 *
 * Considering unassignable StateProperties (top row):
 *    <ul>
 *    <li> A{@link StateRelationTemplate} specifies the name and args
 *    for a relationship between two objects.  For example,
 *    {@code connected(DOCK, DOCK)} states that two docks are connected.
 *    </li>
 *    <li> a {@link StateRelation} is a *bindable* instance of a StateRelationTemplate
 *    that can accept bindings for its arguments.
 *    </li>
 *    </ul>
 * <p>
 *
 * Value binding for a StateProperty works differently than argument binding.
 * While argument bindings are stored in the StateProperty class,
 * value bindings are instead stored in the {@link Statement} class because
 * values change over time.
 * {@link Statement}s link {@link StateVariable}s to values and timepoints.
 * This design allows flexibility during search for, or execution of, assignments.
 * Thus, only the value *type* is specified in a StateProperty;
 * the bound value of an *assignable* {@link StateProperty} is in a Statement.
 * Considering assignable StateProperties (bottom row), there are three types:
 *    <ul>
 *    <li>A {@link StateVariableTemplate} specifies the name, argument types
 *    and valueType(s) to make an attribute of an object.
 *    For example, {@code location(ROBOT) = DOCK} states that a robot can
 *    be assigned a location of type DOCK.
 *    </li>
 *    </ul>
 *    <li> A {@link StateVariable} is an *bindable instance of a StateVariableTemplate
 *    that can accept bindings for its arguments.
 *    </li>
 * Examples of fully ground StateRelation and StateVariable are shown in the above table.
 * <p>
 */
public abstract class StateProperty
        extends BindableImpl
        implements Comparable<StateProperty>, Named, PrintOptionAware {
    protected final static org.slf4j.Logger logger = LoggerFactory.getLogger(StateProperty.class);

    protected String name;
    protected List<BindingConstraint> bindingConstraints;

    boolean isBindable;

    boolean isAssignable;
    protected List<WorldArgument> valueTypes;

    protected boolean showAssignment;

    // ====================================================
    // region<Constructors>

    protected StateProperty() {
    }

    /**
     * A shallow copy constructor. A deep copy can be built with its {@link Builder}.
     * <p>
     * This class uses a Builder to construct instances, so its constructors
     * should only be called by Builders.  For details, see {@link Builder}.
     *
     * @param other the StateProperty to copy
     */
    protected StateProperty(StateProperty other) {
        super(other);
        this.name = other.name;
        this.args = other.args;
        this.bindingConstraints = other.bindingConstraints;
        this.pathParent = other.pathParent;
        this.isBindable = other.isBindable;
        this.isAssignable = other.isAssignable;
        this.valueTypes = other.valueTypes;
        this.showAssignment = other.showAssignment;
    }

    // endregion
    // ====================================================

    // ====================================================
    // region<Object Overrides>

    @Override
    public int hashCode() {
        return this.toString().hashCode();
    }

    @Override
    public boolean equals(Object otherObject) {
        boolean result = false;
        if (otherObject instanceof StateProperty) {
            StateProperty other = (StateProperty) otherObject;
            boolean nameEqual = nameCompareTo(other) == 0;
            boolean argsEqual = argsCompareTo(other) == 0;
            boolean bindingsEqual = bindingsCompareTo(other) == 0;
            boolean assignmentEqual = assignmentCompareTo(other) == 0;
            result = nameEqual
                    && argsEqual
                    && bindingsEqual
                    && assignmentEqual;
        }
        return result;
    }

    @Override
    public String toString() {
        String result = String.format("%s", getName());
        result += toStringArgs();
        if (isBindable) {
            result += toStringBindings(getPrintOptions());
        }
        if (isAssignable) {
            result += toStringValueTypes(getPrintOptions());
        }
        return result;
    }

    @Override
    public String toString(PrintOptions options) {
        String result = String.format("%s", getName());
        result += toStringArgs(options);
        if (isBindable) {
            result += toStringBindings(options);
        }
        if (isAssignable) {
            result += toStringValueTypes(options);
        }
        return result;
    }

    public String toStringValueTypes(PrintOptions options) {
        return Utils.collectValueTypes(valueTypes, options);
    }

    // endregion
    // ====================================================

    // ====================================================
    // region<Comparable Interface and helpers>

    @Override
    public int compareTo(@NotNull StateProperty other) {
        int nameCompare = nameCompareTo(other);
        if (nameCompare == 0) {
            int argsCompare = argsCompareTo(other);
            if (argsCompare == 0) {
                int bindingsCompare = bindingsCompareTo(other);
                if (bindingsCompare == 0) {
                    return assignmentCompareTo(other);
                }
                return bindingsCompare;
            }
            return argsCompare;
        }
        return nameCompare;
    }

    //return only those bindings that correspond to args and values
    public Bindings filteredBindings() {
        Bindings filteredBindings = new Bindings();
        List<WorldArgument> args = getArgs();

        for (Binding bind : findBindings()) {
            //noinspection SuspiciousMethodCalls
            if (args.contains(bind.source) || valueTypes.contains(bind.source)) {
                filteredBindings.add(bind);
            }
        }

        return filteredBindings;
    }

    public boolean nameEquals(StateProperty other) {
        return this.nameCompareTo(other) == 0;
    }

    public int nameCompareTo(StateProperty other) {
        return this.getName().compareTo(other.getName());
    }

    public int argsCompareTo(StateProperty other) {
        return Utils.compareToHelper(getArgs(), other.getArgs());
    }

    /**
     * Orders NamedProperties by their bindings if both are bindable.
     * Otherwise, orders templates before bindable objects.
     *
     * @param other the object to compare
     * @return the diff
     */
    public int bindingsCompareTo(StateProperty other) {
        if (this.isBindable) {
            if (other.isBindable) {
                return Utils.compareToHelper(this.filteredBindings(), other.filteredBindings());
            } else {
                return -1; //this is bindable but other is a template; reverse ordering
            }
        } else {
            if (other.isBindable) {
                return 1; //this is be a template and other is bindable, keep ordering
            } else {
                return 0; //neither is bindable, so there's no preferred ordering
            }
        }
    }

    /**
     * Orders {@link StateProperty} by whether they are assingnable.
     * If only one is assignable, orders relations before variables.
     * <p>
     * NOTE:  This does not check any possible assignments of this property.
     * For that, you need to compare two {@link Statement}s.
     *
     * @param other the object to compare
     * @return the diff
     */
    public int assignmentCompareTo(StateProperty other) {
        if (this.isAssignable) {
            if (other.isAssignable) {
                return 0;  //both are assignable, so there's no preferred ordering
            } else {
                return -1; //this is a variable but other a relation; reverse ordering
            }
        } else {
            if (other.isAssignable) {
                return 1; //this is a relation and other is a variable; keep ordering
            } else {
                return 0;  //neither are assignable, so there's no preferred ordering
            }
        }


    }

    // endregion
    // ====================================================

    // ====================================================
    // region<Output helpers>

    public String getShortName() {
        return getShortName(showAssignment);
    }

    @SuppressWarnings("unused")
    public String getTerseName() {
        return getShortName(false);
    }

    public String getShortName(boolean showAssignment) {
        String result = String.format("%s", getName());
        if (isBindable) {
            if (hasBindings() || hasImpliedBindings()) {
                result += toStringBindings();
            } else {
                result += toStringArgs();
            }
        }
        if (isAssignable
                && showAssignment) {
            result += toStringValueTypes(getPrintOptions());
        }
        return result;
    }

    // endregion
    // ====================================================

    // ====================================================
    // region<Accessors>

    public String getName() {
        return name;
    }

    @SuppressWarnings("unused")
    public List<BindingConstraint> getBindingConstraints() {
        if (bindingConstraints == null) {
            return Collections.emptyList();
        } else {
            return Collections.unmodifiableList(bindingConstraints);
        }
    }
    // endregion
    // ====================================================

    // ====================================================
    // region<Binding related>

    @Override
    public boolean consistentWithConstraints(Bindable source, Bindable target) {
        if (bindingConstraints != null) {
            for (BindingConstraint constraint : bindingConstraints) {
                if (constraint.isViolated(this, source, target)) {
                    return false;
                }
            }
        }
        return true;
    }



    // endregion
    // ====================================================

    // ====================================================
    // region<Builder related>

    /**
     * A convenience method that returns the correct builder for a subclass
     * that also makes a shallow copy of this instance.
     * <p>
     * This allows creating copies of instances:
     * <p>
     * x.copier().instance(worldObject);
     * <p>
     * for some x that inherits from StateProperty.
     *
     * @return a new builder of the correct type
     */
    abstract public Builder<? extends StateProperty, ? extends Builder<?, ?>> copier();


    /**
     * A solution using generics for correctly building subclasses adapted from
     * http://egalluzzo.blogspot.com/2010/06/using-inheritance-with-fluent.html
     * except that the enclosing class hierarchy inherits newBuilder(),
     * which allows a subclass instance to return the correct builder.
     *
     * Unfortunately, this feature requires a parameterized builder
     * using a rather unwieldy curiously recursive generic pattern
     * (i.e., Builder<T.., B extends Builder<T,B>> instead of the simpler Builder<T,B>).
     * Further details of this pattern, in the context of a builder, are at:
     * - https://stackoverflow.com/questions/21086417/builder-pattern-and-inheritance
     * with a great answer: https://stackoverflow.com/a/46036268
     * - https://stackoverflow.com/questions/17164375/subclassing-a-java-builder-class
     */
    @SuppressWarnings("UnusedReturnValue")
    protected abstract static class Builder<T extends StateProperty, B extends Builder<T, B>> {
        protected T instance;

        // ====================================================
        // region<Builder>

        Builder(T instance) {
            this.instance = instance;
            //NB: do not modify instance! It may cause side effects for subclasses
        }

        protected B getThis() {
            //noinspection unchecked
            return (B) this;
        }

        public T build() throws IllegalArgumentException {
            validate();
            return instance;
        }

        protected void validate() throws IllegalArgumentException {
            String errorMsg = "";
            String sep = " ";
            if (instance.name == null) {
                errorMsg = "Name must be specified";
                sep = "; ";
            }
            if (instance.getArgs().size() == 0) {
                errorMsg += sep + "Args must not be null";
                sep = "; ";
            }
            if (instance.isAssignable
                    && instance.valueTypes == null) {
                errorMsg += sep + "Values must not be null for an assignable";
            }

            if (! errorMsg.equals("")) {
                throw new IllegalArgumentException(errorMsg);
            }
        }

        // endregion
        // ====================================================

        // ====================================================
        // region<Name>

        public B name(String name) {
            instance.name = name;
            instance.setPathName(name);
            return getThis();
        }

        // endregion
        // ====================================================

        // ====================================================
        // region<Args>

        public B addArgument(WorldArgument arg) {
            instance.addArgument(arg);
            return getThis();
        }


        public B argTypes(WorldType... args) {
            List<WorldArgument> argList = new ArrayList<>();
            for (WorldType arg : args) {
                argList.add(arg.asArgument());
            }
            return argTypes(argList);
        }

        public B argTypes(WorldArgument... args) {
            return argTypes(Arrays.asList(args));
        }

        public B argTypes(List<WorldArgument> args) {
            instance.args = args;
            return getThis();
        }

        @SuppressWarnings("unused")
        public B copyNameAndArgs(StateProperty obj) {
            instance.name = obj.getName();
            instance.args = obj.args;
            return getThis();
        }

        public B copyNameAndArgs(Builder<? extends StateProperty, ? extends Builder<?, ?>> builder) {
            instance.name = builder.instance.name;
            instance.args = builder.instance.args;
            return getThis();
        }

        public B bindingConstraint(BindingConstraint constraint) {
            if (instance.bindingConstraints == null) {
                instance.bindingConstraints = new ArrayList<>();
            }
            instance.bindingConstraints.add(constraint);
            return getThis();
        }

        /**
         * Clones the arguments for instance
         *
         */
        public B cloneArgs() {
            List<WorldArgument> cloned = new ArrayList<>();
            for (WorldArgument arg : instance.args) {
                cloned.add(new WorldArgument(arg));
            }
            instance.args = cloned;
            return getThis();
        }

        /**
         * Convenience function for calling the implementation of specializedArgs()
         */
        public B specializeArgs(WorldArgument... newArgs)
                throws IllegalArgumentException, IndexOutOfBoundsException {
            List<WorldArgument> newArgsList = Arrays.asList(newArgs);
            return specializeArgs(newArgsList);
        }


        /**
         * Clones and then specializes the arguments of instance.
         * A clone is required because arguments are shallow copied by default
         * to save memory when lots of the same StateProperty are instantiated.
         *
         * @param newArgsList the new arguments, which must be subTypes of the
         *                existing args
         */
        public B specializeArgs(List<WorldArgument> newArgsList)
                throws IllegalArgumentException, IndexOutOfBoundsException {
            cloneArgs();
            if (WorldType.equalsOrInheritsFromTypeThrow(newArgsList, instance.args)) {
                for (WorldArgument newArg : newArgsList) {
                    instance.specializeArgument(newArg);
                }
            }
            return getThis();
        }

        // endregion
        // ====================================================

        // ====================================================
        // region<Bindings>

        protected B isBindable() {
            instance.isBindable = true;
            //NB: do not modify instance! It may cause side effects for subclasses
            return getThis();
        }

        protected B isNotBindable() {
            instance.isBindable = false;
            //NB: do not modify instance! It may cause side effects for subclasses
            return getThis();
        }

        /**
         * Convenience method for calling {@code bindArgs(List<WorldObject>)}

         * @param objects the objects to bind
         * @return this builder to allow chaining
         */
        public B bindArgs(@NotNull WorldObject... objects) {
            return bindArgs(Arrays.asList(objects));
        }

        /**
         * Binds objects to the arguments by mapping them
         * exactly in order of the provided objects.
         *
         * If you already have a map, call instead bindArgs(Map).
         *
         * @param objectList the objects to bind
         * @return this builder to allow chaining
         */
        public B bindArgs(@NotNull List<WorldObject> objectList) {
            List<WorldArgument> args = instance.getArgs();
            if (objectList.size() == 0) {
                return bindArgs(new Bindings());
            }
            if (args.size() == objectList.size()) {
                Map<WorldArgument, WorldObject> map = zipMap(args, objectList);
                return bindArgs(map);
            }
            String message = "The args and objects are different sizes!";
            logger.error(message);
            throw new IllegalArgumentException(message);
        }


        /**
         * Reorders objects from the provided Map to the match
         * order of arguments of this StateProperty.
         *
         * @param map of WorldArguments to WorldObjects
         * @return this builder to allow chaining
         */
        public B bindArgs(@NotNull Map<WorldArgument, WorldObject> map) {
            Bindings proposedBindings = new Bindings(map);
            bindArgs(proposedBindings);
            return getThis();
        }


        /**
         * Directly sets the bindings to the provided Bindings
         * after checking that the WorldObjects (Binding targets)
         * equalsOrInheritsFrom their paired WorldArguments (Binding sources)
         *
         * @param bindings the bindings to check and add
         * @return this builder to allow chaining
         * @throws IllegalArgumentException if any object mismatches its argument type
         */
        public B bindArgs(@NotNull Bindings bindings)
                throws IndexOutOfBoundsException, IllegalArgumentException{
            if (bindings.size() == 0) {
                //NB: allow empty bindings objects that may be filled later
                instance.setBindings(bindings);
            } else if (WorldType.equalsOrInheritsFromTypeThrow(bindings)) {
                instance.setBindings(bindings);
            }
            return getThis();
        }

        public B removeBindings() {
            instance.removeBindings();
            return getThis();
        }

        @SuppressWarnings("unused")
        public B copyBindings(@NotNull StateProperty obj) {
            bindArgs(obj.getBindingsAtThisNodeOnly());
            return getThis();
        }

        /**
         * Binds a "deep" copy of the current bindings
         * where a new Bindings object has cloned each {@link Binding}.
         * The source and target objects for each binding are also cloned.
         * @return this builder
         */
        public B cloneBindings() {
            Bindings bindings = instance.getBindingsAtThisNodeOnly();
            Bindings cloned = new Bindings();
            for (Binding binding : bindings) {
                WorldObject source = binding.source.content();
                WorldObject target = binding.source.content();
                if (binding.equals) {
                    cloned.add(Binding.equal(source.instance(), target.instance()));
                } else {
                    cloned.add(Binding.notEqual(source.instance(), target.instance()));
                }
            }
            bindArgs(cloned);
            return getThis();
        }

        /**
         * Binds a clone of the provided bindings.
         * @return this builder
         */
        public B cloneBindings(@NotNull WorldObject... bindings) {
            List<WorldObject> cloned = new ArrayList<>();
            for (WorldObject obj : bindings) {
                cloned.add(obj.instance());
            }
            bindArgs(cloned);
            return getThis();
        }

        // endregion
        // ====================================================

        // ====================================================
        // region<Assignable ValueTypes>

        protected B isAssignable() {
            instance.isAssignable = true;
            //NB: do not modify instance! It may cause side effects for subclasses
            return getThis();
        }

        protected B isNotAssignable() {
            instance.isAssignable = false;
            return getThis();
        }

        public B valueTypes(@NotNull WorldType... valueTypes) {
            List<WorldArgument> args = new ArrayList<>();
            for (WorldType type : valueTypes) {
                args.add(type.asArgument());
            }
            return valueTypes(args);
        }

        public B valueTypes(@NotNull WorldArgument... valueTypes) {
            clearOrInstantiateValueTypes();
            instance.valueTypes.addAll(Arrays.asList(valueTypes));
            return getThis();
        }

        public B valueTypes(@NotNull List<WorldArgument> valueTypes) {
            clearOrInstantiateValueTypes();
            instance.valueTypes.addAll(valueTypes);
            return getThis();
        }

        /**
         * Allows an instance to specialize the type of the argument.
         *
         * @param newValueTypes the new arguments, which must be subTypes of the
         *                existing args
         */
        public B specializeValueTypes(WorldArgument... newValueTypes)
                throws IllegalArgumentException, IndexOutOfBoundsException {
            List<WorldArgument> newValuesList = Arrays.asList(newValueTypes);
            return specializeValueTypes(newValuesList);
        }

        /**
         * Allows an instance to specialize the type of the argument.
         *
         * @param newValuesList the new arguments, which must be subTypes of the
         *                existing args
         */
        public B specializeValueTypes(List<WorldArgument> newValuesList)
                throws IllegalArgumentException, IndexOutOfBoundsException {
            if (WorldType.equalsOrInheritsFromTypeThrow(newValuesList, instance.valueTypes)) {
                instance.valueTypes = newValuesList;
            }
            return getThis();
        }


        public B clearOrInstantiateValueTypes() {
            if (instance.valueTypes == null) {
                resetValueTypes();
            } else {
                instance.valueTypes.clear();
            }
            return getThis();
        }

        public B resetValueTypes() {
            instance.valueTypes = new ArrayList<>();
            return getThis();
        }


        @SuppressWarnings("unused")
        public B showAssignment() {
            instance.showAssignment = true;
            return getThis();
        }

        public B hideAssignment() {
            instance.showAssignment = false;
            return getThis();
        }

        // endregion
        // ====================================================
    }

    // endregion Builder related
    // ====================================================


}
