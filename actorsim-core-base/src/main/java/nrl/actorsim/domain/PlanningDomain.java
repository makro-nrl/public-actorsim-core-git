package nrl.actorsim.domain;

import java.util.*;

@SuppressWarnings("unused")
public class PlanningDomain extends BindableImpl implements NamedPathContainer {
    protected final static org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(PlanningDomain.class);
    private final static Options NULL_OPTIONS = Options.builder()
            .name("NULL_PLANNING_OPTIONS")
            .closedWorldAssumption()
            .build();
    public final static PlanningDomain NULL_PLANNING_DOMAIN = new PlanningDomain(NULL_OPTIONS);

    protected final Options options;

    protected final Set<WorldType> types = new TreeSet<>(new TypeComparator());

    protected final Set<WorldType> constantObjectTypes = new TreeSet<>(new TypeComparator());
    protected Map<String, WorldObject> constantObjectMap = new TreeMap<>();

    protected final Set<StateRelationTemplate> srts = new TreeSet<>();
    protected Map<String, StateRelationTemplate> srtsMap = new TreeMap<>();
    protected final Set<StateVariableTemplate> svts = new TreeSet<>();
    protected Map<String, StateVariableTemplate> svtMap = new TreeMap<>();

    protected Map<String, Operator> operatorMap = new TreeMap<>();

    protected PlanningProblem problem;


    // ============================================================
    // region<Constructors and Object Overrides>

    public PlanningDomain(Options options) {
        this.options = options;

        PlanningProblem.Options problemOptions = PlanningProblem.Options.builder()
                .domain(this)
                .name("not-yet-loaded")
                .allowNewObjects()
                .build();
        problem = new PlanningProblem(problemOptions);
    }

    @Override
    public String toString() {
        return "PlanningDomain:" + options.name;
    }

    // endregion

    // ============================================================
    // region<Accessors>

    public Collection<StateVariableTemplate> getPredicates() {
        return Collections.unmodifiableCollection(svtMap.values());
    }

    protected StateVariableTemplate getPredicate(String name) {
        if (svtMap.containsKey(name)) {
            return svtMap.get(name);
        }
        return StateVariableTemplate.NULL_SVT;
    }

    public WorldObject getConstantObject(String name) {
        if (constantObjectMap.containsKey(name)) {
            return constantObjectMap.get(name);
        }
        return WorldObject.NULL_WORLD_OBJECT;
    }

    public Collection<WorldObject> getConstantObjects() {
        return Collections.unmodifiableCollection(constantObjectMap.values());
    }

    protected boolean containsConstant(String name) {
        return constantObjectMap.containsKey(name);
    }

    public Collection<Operator> getOperators() {
        return Collections.unmodifiableCollection(operatorMap.values());
    }

    public Operator getOperator(String name) {
        if (operatorMap.containsKey(name)) {
            return operatorMap.get(name);
        }
        return Operator.NULL_OPERATOR;
    }

    public PlanningProblem getProblem() {
        return problem;
    }

    public Collection<WorldType> getTypes() {
        return Collections.unmodifiableCollection(types);
    }

    // endregion
    // ============================================================


    // ====================================================
    // region<Action>
    public Action getAction(String step) {
        return Action.NULL_ACTION;
    }

    // endregion
    // ====================================================


    public void add(WorldType... types) {
        for (WorldType type : types) {
            add(type);
        }
    }

    public WorldType add(WorldType type) {
        types.add(type);
        return type;
    }

    public void add(WorldObject... constants) {
        for (WorldObject constant: constants) {
            add(constant);
        }
    }

    public WorldObject add(WorldObject constant) {
        this.constantObjectTypes.add(constant);
        this.constantObjectMap.put(constant.id, constant);
        return constant;
    }

    public void add(StateRelationTemplate... srts) {
        for (StateRelationTemplate srt : srts) {
            add(srt);
        }
    }

    public void add(StateRelationTemplate srt) {
        this.srts.add(srt);
    }

    public void add(StateVariableTemplate... svts) {
        for (StateVariableTemplate svt : svts) {
            add(svt);
        }
    }

    public void add(StateVariableTemplate svt) {
        this.svts.add(svt);
        this.svtMap.put(svt.name, svt);
    }

    public void add(Operator operator) {
        operatorMap.put(operator.getPathName(), operator);
    }

    public PlanningProblem buildEmptyProblem() {
        PlanningProblem.Options options = new PlanningProblem.Options.Builder()
                .domain(this)
                .name("EmptyProblem")
                .build();
        return new PlanningProblem(options);
    }

    public String getName() {
        return options.name;
    }

    public Operator findOperator(String name) {
        return operatorMap.get(name);
    }

    // ====================================================
    // region<Options>

    public static class Options {
        String name;
        Boolean hasClosedWorldAssumption = false;
        boolean hideTypesWhenPossible = false;

        public Options() {
        }

        public static
        Builder<? extends Options, ? extends Builder<?, ?>> builder() {
            return new Builder<>(new Options());
        }

        public void updateName(String name) {
            this.name = name;
        }

        public boolean hideTypesWhenPossible() {
            return hideTypesWhenPossible;
        }

        public Boolean hasClosedWorldAssumption() {
            return hasClosedWorldAssumption;
        }

        public static class Builder<T extends Options, B extends Builder<T, B>> {
            final T instance;

            protected Builder(T instance) {
                this.instance = instance;
            }

            B getThis() {
                //noinspection unchecked
                return (B) this;
            }

            public B name(String name) {
                instance.name = name;
                return getThis();
            }

            public B closedWorldAssumption() {
                instance.hasClosedWorldAssumption = true;
                return getThis();
            }

            public B hideTypesWhenPossible() {
                instance.hideTypesWhenPossible = true;
                return getThis();
            }

            public B openWorldAssumption() {
                instance.hasClosedWorldAssumption = false;
                return getThis();
            }

            public T build() {
                String errorMsg = "";
                if (instance.name == null) {
                    errorMsg += " name?";
                }
                if (instance.hasClosedWorldAssumption == null) {
                    errorMsg += " closed/open world?";
                }
                if (!errorMsg.equals("")) {
                    String msg = "Missing required details to instantiate domain:" + errorMsg;
                    logger.error(msg);
                    throw new IllegalArgumentException(msg);
                }
                return instance;
            }
        }
    }

    // endregion
    // ====================================================


}
