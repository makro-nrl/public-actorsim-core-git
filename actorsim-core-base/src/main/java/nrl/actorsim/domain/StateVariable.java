package nrl.actorsim.domain;

import java.util.*;

import static nrl.actorsim.domain.WorldObject.*;

/**
 * A StateProperty with an assignable value whose arguments and value are bindable.
 * See {@link StateProperty} for details.
 * An instance remains lifted until all variables are assigned.
 */
@SuppressWarnings("unused")
public class StateVariable extends StateProperty {
    final static org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(StateVariable.class);
    public static final StateVariable ALWAYS_TRUE_SV =
            StateVariable.builder().name("vacuouslyTrue")
                    .argTypes(TRUE).valueTypes(TRUE).build();

    // ====================================================
    // region<Constructors>

    protected StateVariable() {
        super();
    }

    /**
     * A shallow copy constructor. A deep copy can be built with its {@link Builder}.
     * <p>
     * This class uses a Builder to construct instances, so its constructors
     * should only be called by Builders.  For details, see {@link Builder}.
     *
     * @param other the StateVariable to copy
     */
    protected StateVariable(StateVariable other) {
        super(other);
    }

    /**
     * A shallow copy constructor. A deep copy can be built with its {@link Builder}.
     * <p>
     * This class uses a Builder to construct instances, so its constructors
     * should only be called by Builders.  For details, see {@link Builder}.
     *
     * @param other the StateVariableTemplate to copy
     */
    protected StateVariable(StateVariableTemplate other) {
        super(other);
    }

    // endregion
    // ====================================================

    // ====================================================
    // region<Static Statement Builder Helpers>

    /**
     * @deprecated
     */
    public PredicateStatement predicateStatement() {
        return new PredicateStatement(this);
    }

    /**
     * @deprecated
     */
    public PredicateStatement predicateStatement(WorldObject... bindings) {
        return new PredicateStatement(this, Arrays.asList(bindings));
    }


    /**
     * @deprecated
     */
    public PredicateStatement predicateStatement(List<WorldObject> bindings) {
        return new PredicateStatement(this, bindings);
    }

    /**
     * A convenience method to convert this into a PersistenceStatement.
     *
     * @param valueClass the class of values that is needed, which
     *                   must match the value type of any assigned value
     * @param bindings the objects to use in constructing the statement
     *                where the final object is assumed to be the value
     * @return the appropriate persistence statement
     */
    public
    <V>
    PersistenceStatement<V> persistenceStatement(Class<V> valueClass, WorldObject ... bindings) {
        if (valueTypes.size() != 1) {
            String msg = "Can only wrap single value state variables";
            logger.error(msg);
            throw new IllegalArgumentException(msg);
        }

        WorldType valueType = valueTypes.get(0);
        valueType.canBeAssignedThrow(valueClass);

        PersistenceStatement<V> statement = new PersistenceStatement<>(this.copier().build());
        BindResult result = statement.bindArgs(bindings);
        if(result.isFailure()) {
            throw new IllegalArgumentException(result.failureReason);
        }
        return statement;
    }

    /**
     * A helper method to convert this template into a PersistenceStatement.
     */
    public
    <V>
    PersistenceStatement<V> negatedPersistenceStatement(Class<V> valueClass, WorldObject ... bindings) {
        if (valueTypes.size() > 1) {
            throw new RuntimeException("StateVariable has more than one value type; but PersistenceStatement assumes there is only 1.");
        }
        NegatedPersistenceStatement<V> statement = new NegatedPersistenceStatement<>(this.copier().build());
        statement.bindArgs(bindings);
        return statement;
    }

    // endregion
    // ====================================================

    // ====================================================
    // region<Static Builder Helpers>

    /**
     * A convenience method to return the correctly typed builder for this class.
     * <p>
     * Note: calling "Class.builder()" is the same as calling "new Class.Builder<>()"
     *
     * @return the builder for this class
     */
    public static
    Builder<? extends StateVariable, ? extends Builder<?, ?>>
    builder() {
        //NB: the return type of this class must use generic wildcards because StateVariable has a subclass
        return new Builder<>(new StateVariable());
    }

    public static
    Builder<? extends StateVariable, ? extends Builder<?, ?>>
    copier(StateVariableTemplate template) {
        //NB: the return type of this class must use generic wildcards because StateVariable has a subclass
        return new Builder<>(new StateVariable(template));
    }

    @Override
    public
    Builder<? extends StateVariable, ? extends Builder<?, ?>>
    copier() {
        //NB: the return type of this class must use generic wildcards because StateVariable has a subclass
        return new Builder<>(new StateVariable(this));
    }

    /**
     * Convert this StateVariable into a template.
     * @return a builder for constructing a template
     */
    public
    <X extends StateVariableTemplate, Y extends StateVariableTemplate.Builder<X, Y>>
    StateVariableTemplate.Builder<X, Y>
    templater() {
        return new StateVariableTemplate.Builder<>(this);
    }

    public
    StateVariableTemplate asTemplate() {
        return templater().build();
    }

    // endregion
    // ====================================================

    // ====================================================
    // region<Builder>

    public static class Builder<T extends StateVariable, B extends Builder<T, B>>
            extends StateProperty.Builder<T, Builder<T, B>> {

        protected Builder(T instance) {
            super(instance);
            init();
        }
        public Builder(StateVariableTemplate svt) {
            //noinspection unchecked
            super((T) new StateVariable(svt));
            init();
        }

        private void init() {
            isBindable();
            isAssignable();
            if (instance.valueTypes == null) {
                resetValueTypes();
            }
        }


        /**
         * Build a StateVariable instance with empty bindings.
         *
         * @return StateVariable
         */
        public StateVariable buildWithoutBindings() {
            return removeBindings()
                    .build();
        }

        /**
         * Build a StateVariable with the provided bindings.
         *
         * @param bindings to bind
         * @return StateVariable
         * @throws IllegalArgumentException when the bindings do not match the allowed arguments
         */
        public StateVariable build(WorldObject... bindings) throws IllegalArgumentException {
            return this.bindArgs(bindings)
                    .build();
        }

        /**
         * Build a StateVariable with the provided bindings.
         *
         * @param objects to bind
         * @return StateVariable
         */
        public StateVariable build(List<WorldObject> objects) throws IllegalArgumentException {
            return bindArgs(objects)
                    .build();
        }

        /**
         * Build a StateVariable with a _clone_ of the bindings in this builder.
         *
         * @return StateVariable
         */
        public StateVariable buildWithClonedBindings() {
            return cloneBindings()
                    .build();
        }

        /**
         * Build a StateVariable with a _clone_ of the provided bindings.
         *
         * @param objects to bind
         * @return StateVariable
         */
        public StateVariable buildWithClonedBindings(WorldObject... objects) {
            return cloneBindings(objects)
                    .build();
        }
    }

    // endregion
    // ====================================================
}
