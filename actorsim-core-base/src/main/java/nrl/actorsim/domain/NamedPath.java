package nrl.actorsim.domain;

import nrl.actorsim.chronicle.Chronicle;
import nrl.actorsim.utils.*;
import org.jetbrains.annotations.NotNull;

import java.util.*;
import static nrl.actorsim.domain.NamedPathImpl.*;

/**
 * An interface for assisting objects in a hierarchy to
 * characterize a unique path (i.e., URL) to themselves.
 *
 * NamedPaths are used by the Bindable and Binding classes to:
 *   - bind arguments for a bindable {@link StateProperty}
 *     (i.e. {@link StateRelation} or {@link StateVariable}),
 *   - bind arguments or values for {@link Statement}s
 *   - bind arguments for {@link Action}s
 *
 * NamedPaths are also used to bind temporal variables in {@link Chronicle}s
 * to uniquely identify timepoints within a hierarchy.
 *
 * NamedPaths are {@link PrintOptionAware}, which means
 * that one can control what gets printed by using the
 * toStringXXX() method variants that accept {@link PrintOptions}
 */
public interface NamedPath extends NamedPathNode, PrintOptionAware {
    // ====================================================
    // region<Static Helpers>

    /**
     * Links a parent to a child.
     *
     * @param parent
     * @param child
     */
    public static void link(NamedPath parent, NamedPath child) {
        child.addPathParent(parent);
        parent.addPathChild(child);
    }

    public static void unlink(NamedPath parent, NamedPath child) {
        child.removePathParent();
        parent.removePathChild(child);
    }


    // endregion
    // ====================================================


    default int compareToPathName(NamedPath other) {
        return this.getPathName().compareTo(other.getPathName());
    }

    /**
     * A convenience method to return the content of "this"
     * path via the TRANSFORMER.
     *
     * Returns null if the stored item is not a type T
     * or if the stored item is null.
     *
     * @param <ReturnType>> The type to return
     * @return the stored item as the correct type
     */
    default <ReturnType> ReturnType content() {
        return this.accept(NamedPathVisitorImpl.TRANSFORMER);
    };

    void setPathName(String name);

    String getPathName();

    Options getPathOptions();

    void addPathParent(@NotNull NamedPath parentPath);

    void removePathParent();

    default NamedPath getPathParent() {
        return EMPTY_PATH;
    }

    void addPathChild(NamedPath child);

    void removePathChild(NamedPath child);

    default String getAbsolutePath() {
        NamedPath parent = getPathParent();
        String parentPathString = "";
        if (parent instanceof NamedPathContainer) {
            parentPathString = ROOT;
        } else if (parent == EMPTY_PATH) {
            parentPathString = UNBOUND_ROOT;
        } else {
            parentPathString = parent.getAbsolutePath() + CHILD_SEP;
        }
        return parentPathString + getPathName();
    }

    default List<NamedPath> getPathChildren() {
        return Collections.emptyList();
    }

    default boolean canAddChild() {
        Options options = getPathOptions();
        return (options.maxAllowedChildren < 0)
                || (getPathChildren().size() < options.maxAllowedChildren);
    }

    default String toStringPath() {
        return Utils.PATH_START + getAbsolutePath() + Utils.PATH_END;
    }

    default String toStringPath(PrintOptions printOptions) {
        if (printOptions.hidePath()) {
            return "";
        } else if (printOptions.pathNameOnly()) {
            return Utils.PATH_START + getPathName() + Utils.PATH_END;
        } else {
            return toStringPath();
        }
    }

    default String toStringPathDetails() {
        boolean includeContent = false;
        return toStringPathDetails(includeContent);
    }

    default String toStringPathDetails(boolean includeContent) {
        StringBuilder pathString = new StringBuilder();
        pathString.append(getAbsolutePath());
        for (NamedPath child : getPathChildren()) {
            pathString.append("\n  ")
                    .append(CHILD_SEP)
                    .append(child.getPathName());
            if (includeContent) {
                pathString.append(" = ")
                        .append(child.toString());
            }
        }
        NamedPath parent = getPathParent();
        if (parent != EMPTY_PATH) {
            pathString.append("\n  ");
            while(parent != EMPTY_PATH) {
                pathString.append(PARENT_SEP)
                        .append(parent.getPathName());
                if (includeContent) {
                    pathString.append(" = ")
                            .append(parent.toString());
                }
                parent = parent.getPathParent();
            }
        }
        return pathString.toString();
    }

    default NamedPath findChild(String pathName) {
        for (NamedPath child : getPathChildren()) {
            if (child.getPathName().equals(pathName)) {
                return child;
            }
        }
        return EMPTY_PATH;
    }

    default NamedPath getFirstChild() {
        List<NamedPath> children = getPathChildren();
        if (children.size() > 0) {
            return children.get(0);
        }
        else return EMPTY_PATH;
    }

    public static class Options {
        /**
         * Whether this path is a relative path
         */
        boolean isRelativePath = false;

        public static int UNLIMITED = -1;
        /**
         * The number of children this path is allowed to store.
         * Default is to allow any number of children.
         */
        int maxAllowedChildren = UNLIMITED;

        /**
         * Whether shared parents are allowed.
         * Defaults to false.
         */
        boolean sharedParentsAllowed = false;

        public void setMaxAllowedChildren(int value) {
            maxAllowedChildren = value;
        }

        public void disallowChildren() {
            maxAllowedChildren = 0;
        }
    }
}
