package nrl.actorsim.domain;

import org.slf4j.LoggerFactory;

import java.util.Arrays;
import java.util.List;

import static nrl.actorsim.domain.WorldType.BOOLEAN;
import static nrl.actorsim.domain.WorldType.NUMBER;

/**
 * A StateProperty Template with an assignable value but its arguments and value are not bindable.
 * See {@link StateProperty} for details.
 * An instance remains always lifted because variables are not assignable.
 */
@SuppressWarnings("rawtypes")
public class StateVariableTemplate extends StateProperty {
    protected final static org.slf4j.Logger logger = LoggerFactory.getLogger(StateVariableTemplate.class);
    public static final StateVariableTemplate NULL_SVT = StateVariableTemplate.builder()
            .name("vacuouslyFalse")
            .argTypes(WorldObject.ANY)
            .valueTypes(WorldObject.ANY)
            .build();

    // ====================================================
    // region<Constructors>

    private StateVariableTemplate() {
        super();
    }

    /**
     * A shallow copy constructor. A deep copy can be built with its {@link Builder}.
     * <p>
     * This class uses a Builder to construct instances, so its constructors
     * should only be called by Builders.  For details, see {@link Builder}.
     *
     * @param other the StateVariableTemplate to copy
     */
    private StateVariableTemplate(StateVariableTemplate other) {
        super(other);
    }

    /**
     * A shallow copy constructor. A deep copy can be built with its {@link Builder}.
     * <p>
     * This class uses a Builder to construct instances, so its constructors
     * should only be called by Builders.  For details, see {@link Builder}.
     *
     * @param other the StateVariable to copy
     */
    private StateVariableTemplate(StateVariable other) {
        super(other);
    }

    // endregion
    // ====================================================

    // ====================================================
    // region<Static Statement Builder Helpers>

    /**
     * A convenience method to convert this template into a PersistenceStatement.
     *
     * @param bindings the objects to use in constructing the statement
     *                where the final object is assumed to be the value
     * @return the appropriate persistence statement
     */
    public PersistenceStatement persistenceStatement(WorldObject ... bindings) {
        if ((valueTypes.size() == 1)
                && (valueTypes.get(0).equalsOrInheritsFromType(BOOLEAN))) {
            return new BooleanPersistenceStatement(this, Arrays.asList(bindings), true);
        }
        PersistenceStatement statement = new PersistenceStatement(this.asStateVariable());
        BindResult result = statement.bindArgs(bindings);
        if (result.isFailure()) {
            throw new IllegalArgumentException(result.toString());
        }
        return statement;
    }

    public NumericPersistenceStatement numericPersistenceStatement(WorldObject ... bindings) {
        if ((this.valueTypes.size() == 1)
                && (valueTypes.get(0).equalsOrInheritsFromType(NUMBER))) {
            NumericPersistenceStatement statement = new NumericPersistenceStatement(this.asStateVariable());
            BindResult result = statement.bindArgs(bindings);
            if (result.isFailure()) {
                throw new IllegalArgumentException(result.toString());
            }
            return statement;
        }
        return NumericPersistenceStatement.NULL_STATEMENT;
    }

    public PersistenceStatement negatedPersistenceStatement(WorldObject ... bindings) {
        PersistenceStatement newStatement;
        if ((valueTypes.size() == 1)
                && (valueTypes.get(0).equalsOrInheritsFromType(BOOLEAN))) {
            newStatement = new BooleanPersistenceStatement(this, Arrays.asList(bindings), false);
        } else {
            newStatement = new NegatedPersistenceStatement(this.asStateVariable());
            newStatement.bindArgs(bindings);
        }
        return newStatement;
    }

    // endregion
    // ====================================================

    // ====================================================
    // region<Static Builder Helpers>

    /**
     * A convenience method to return the correctly typed builder for this class.
     * <p>
     * Calling "Class.builder()" is equivalent to "new Class.Builder<>()".
     *
     * @return a new SVT builder
     */
    public static Builder<? extends StateVariableTemplate, ? extends Builder> builder() {
        return new Builder<>(new StateVariableTemplate());
    }

    @Override
    public Builder<? extends StateVariableTemplate, ? extends Builder> copier() {
        return new StateVariableTemplate.Builder<>(new StateVariableTemplate(this));
    }

    public StateVariable asStateVariable() {
        return stateVariableBuilder().build();
    }

    /**
     * Convert this StateVariableTemplate into a StateVariable.
     * @return a builder for constructing
     */
    public
    <X extends StateVariable, Y extends StateVariable.Builder<X, Y>>
    StateVariable.Builder<X, Y>
    stateVariableBuilder() {
        return new StateVariable.Builder<>(this);
    }

    // endregion
    // ====================================================

    // ====================================================
    // region<Builder>

    @SuppressWarnings("unused")
    public static class Builder<T extends StateVariableTemplate, B extends Builder<T, B>>
            extends StateProperty.Builder<T, B> {
        protected Builder(T instance) {
            super(instance);
            init();
        }

        private void init() {
            isNotBindable();
            isAssignable();
        }

        @SuppressWarnings("unchecked")
        protected Builder(StateVariable instance) {
            this((T) new StateVariableTemplate(instance));
        }

        /**
         * Build a StateVariable instance with empty bindings.
         *
         * @return StateVariable
         */
        public StateVariable buildWithoutBindings() {
            return StateVariable.copier(this.instance)
                    .buildWithoutBindings();
        }

        /**
         * Build a StateVariable with the provided bindings.
         *
         * @param bindings to bind
         * @return StateVariable
         * @throws IllegalArgumentException when the bindings do not match the allowed arguments
         */
        public StateVariable build(WorldObject... bindings) throws IllegalArgumentException {
            return StateVariable.copier(this.instance)
                    .build(bindings);
        }

        /**
         * Build a StateVariable with the provided bindings.
         *
         * @param bindings to bind
         * @return StateVariable
         * @throws IllegalArgumentException when the bindings do not match the allowed arguments
         */
        public StateVariable build(List<WorldObject> bindings) throws IllegalArgumentException {
            logger.debug("temporary -- builder " + this.hashCode() + " and svt instance " + this.instance.hashCode());
            return StateVariable.copier(this.instance)
                    .build(bindings);
        }


        /**
         * Build a StateVariable with a _clone_ of the provided bindings.
         *
         * @param bindings to bind
         * @return StateVariable
         */
        public StateVariable buildWithClonedBindings(WorldObject... bindings) {
            return StateVariable.copier(this.instance)
                    .buildWithClonedBindings(bindings);
        }
    }

    // endregion
    // ====================================================
}
