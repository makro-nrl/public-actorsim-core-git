package nrl.actorsim.domain;

import org.slf4j.LoggerFactory;

/**
 * A StateProperty Template with no assignable value and arguments that cannot be bound.
 * See {@link StateProperty} for details.
 * An instance remains always lifted because variables are not assignable.
 */
@SuppressWarnings("unused")
public class StateRelationTemplate extends StateProperty {
    protected final static org.slf4j.Logger logger = LoggerFactory.getLogger(StateRelationTemplate.class);

    // ====================================================
    // region<Constructors>

    private StateRelationTemplate() {
        super();
    }

    /**
     * A shallow copy constructor. A deep copy can be built with its {@link Builder}.
     * <p>
     * This class uses a Builder to construct instances, so its constructors
     * should only be called by Builders.  For details, see {@link Builder}.
     *
     * @param other the StateRelationTemplate to copy
     */
    private StateRelationTemplate(StateRelationTemplate other) {
        super(other);
    }

    // endregion
    // ====================================================

    // ====================================================
    // region<Static Builder Helpers>

    /**
     * A convenience method to return the correctly typed builder for this class.
     * <p>
     * Note: calling "Class.builder()" is the same as calling "new Class.Builder<>()"
     *
     * @return the builder for this class
     */
    public static
    Builder<? extends StateRelationTemplate, ? extends Builder<?, ?>>
    builder() {
        return new Builder<>(new StateRelationTemplate());
    }

    @Override
    public
    Builder<? extends StateRelationTemplate, ? extends Builder<?, ?>>
    copier() {
        return new Builder<>(new StateRelationTemplate(this));
    }

    public StateRelation asStateRelation() {
        return stateRelationBuilder().build();
    }

    /**
     * Convert this StateRelationTemplate into a StateRelation.
     * @return a builder for constructing
     */
    public
    <X extends StateRelation, Y extends StateRelation.Builder<X, Y>>
    StateRelation.Builder<X, Y>
    stateRelationBuilder() {
        return new StateRelation.Builder<>(this);
    }

    // endregion
    // ====================================================

    // ====================================================
    // region<Builder>

    public static class Builder<T extends StateRelationTemplate, B extends Builder<T, B>>
            extends StateProperty.Builder<T, B> {

        protected Builder(T instance) {
            super(instance);
            init();
        }

        private void init() {
            isNotBindable();
            isNotAssignable();
        }

        /**
         * Construct a StateRelation using bindings out of this template builder.
         *
         * @param bindings the bindings for the StateRelation
         * @return a StateRelation
         * @throws IllegalArgumentException if the binding types do not match argument types
         */
        public StateRelation build(WorldObject... bindings) throws IllegalArgumentException {
            return StateRelation.copier(this.instance)
                    .build(bindings);
        }
    }

    // endregion
    // ====================================================
}
