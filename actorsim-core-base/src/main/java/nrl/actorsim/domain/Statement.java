package nrl.actorsim.domain;


import nrl.actorsim.chronicle.TimeInterval;
import nrl.actorsim.chronicle.Timepoint;
import nrl.actorsim.utils.Named;
import nrl.actorsim.utils.*;
import org.jetbrains.annotations.NotNull;

import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.Predicate;

import static nrl.actorsim.utils.PrintOptions.PathDetail.PATH_NAME_ONLY;
import static nrl.actorsim.utils.Utils.zipMap;

/**
 * An abstraction for linking a {@link StateVariable} with a temporal
 * assertion, as described in <a href=http://projects.laas.fr/planning/>Automated Planning and Acting</a>, Chapter 4, pp. 116ff.
 * <p>
 * All statements have a target {@link StateVariable} to which they apply.
 * Also, all statements have temporal extent [start, end].
 * Non-temporal planning applications can simply ignore the default interval,
 * [start=-INF, end=+INF], which obviates  a separate class for non-temporal statements.
 * <p>
 * This class should never be instantiated, even for the NullObject
 * pattern, so it is abstract.
 * <p>
 * Example {@link Statement}s include:
 * <ul>
 *   <li> {@link PersistenceStatement}: <code>[start, end] sv(..) == value</code>
 *   <li> {@link NegatedPersistenceStatement}:  <code>[start, end] sv(..) != value</code>
 *   <li> {@link TransitionStatement}:   <code>[start, end] sv(..) = from -> to</code>
 *   <li> {@link AssignmentStatement}:   <code>[start, end] sv(..) := value</code>
 * </ul>
 *
 * JGraphT relies on a consistent implementation of both equals()
 * *and* hashCode(), and our implementation of equals() relies on compareTo().
 * Failing to synchronize these methods in subclasses results in
 * incorrect behavior in the graph library or algorithms that use it!
 * For details, see:
 * <ul>
 *     <li>https://github.com/jgrapht/jgrapht/wiki/Users%3A-EqualsAndHashCode</li>
 *     <li>https://www.ibm.com/developerworks/java/library/j-jtp05273/index.html</li>
 * </ul>
 *
 */
@SuppressWarnings("unused")
public abstract
class Statement
        extends BindableImpl
        implements Comparable<Object>, Named, PrintOptionAware
{
    final static org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(Statement.class);
    static PrintOptions DEFAULT_PRINT_OPTIONS= new PrintOptions(PATH_NAME_ONLY);

    public static AtomicInteger NEXT_ID_VALUE = new AtomicInteger(0);

    protected final Integer id = NEXT_ID_VALUE.getAndIncrement();
    final StateVariable target;
    TimeInterval estimatedInterval;
    //notes for later intervals
    //TimeInterval executedInterval = TimeInterval.ALL_TIME.copy();
    //SimulationTimeInterval estimatedSimulation = SimulationTimeInterval.ALL_TIME.copy();
    //SimulationTimeInterval executedSimulation = SimulationTimeInterval.ALL_TIME.copy();

    protected boolean showEstimatedTime = true;

    private List<Runnable> internalChangeListeners = Collections.emptyList();

    PrintOptions childPrintOptions;

    List<CausalLink> causalLinks;

    //===============================================
    // region<Constructors>
    
    /**
     * Constructs a Statement that stores the provided target.
     * Subclasses must make a copy of target before calling
     * this constructor.
     *
     * @param target the target to store
     */
    protected Statement(StateVariable target) {
        //logger.debug("creating new statement with target " + target.hashCode());
        this.target = target;
        init(target.getName() + "_" + id);
    }

    /**
     * Constructs a Statement that stores the provided target
     * while also naming this statement with the provided name.
     * Subclasses must make a copy of target before calling
     * this constructor.
     *
     * @param target the target to store
     * @param name the name of this statement
     */
    protected Statement(StateVariable target, String name) {
        this.target = target;
        init(name);
    }

    private void init(String name) {
        setPathName(name);
        NamedPath.link(this, target);
        //Statement gets its arguments from the target
        deferArgumentsToChild();

        //Statement holds bindings and values for its target
        target.closeBindings();
        target.onlyUseParentBindings();
        setEstimatedInterval(TimeInterval.ALL_TIME.copy());
        for (Binding binding : target.getBindingsAtThisNodeOnly()) {
            this.addBinding(binding);
        }
        target.removeBindings();

        bindableOptions.printOptions = DEFAULT_PRINT_OPTIONS;
        childPrintOptions = new PrintOptions(bindableOptions.printOptions);
        childPrintOptions.hidePath();
    }

    // endregion<Constructors>
    //===============================================

    //===============================================
    // region<Object and Comparable>

    /**
     * Returns the comparison between this object and other.
     *
     * JGraphT relies on a consistent implementation of both equals()
     * *and* hashCode(), and our implementation of equals() relies on compareTo().
     * Failing to synchronize these methods in subclasses results in
     * incorrect behavior in the graph library or algorithms that use it!
     * For details, see:
     * <ul>
     *     <li>https://github.com/jgrapht/jgrapht/wiki/Users%3A-EqualsAndHashCode</li>
     *     <li>https://www.ibm.com/developerworks/java/library/j-jtp05273/index.html</li>
     * </ul>
     *
     * @param otherObj the other class to compare against
     * @return negative, zero, or positive integers for less-than, equals, or
     *         greater-than comparison results
     */
    @Override
    public int compareTo(@NotNull Object otherObj) {
        if (this == otherObj) {
            return 0;
        } if (otherObj instanceof Statement) {
            if (this.id == ((Statement)otherObj).id) {
                return 0; // ids are equal so this is the same object!
            }
            Statement other = (Statement) otherObj;
            int targetCompare = target.compareTo(other.target);
            if (targetCompare == 0) {
                int bindingsCompare = Utils.compareToHelper(this.filteredBindings(), other.filteredBindings());
                if (bindingsCompare == 0) {
                    if (estimatedInterval != TimeInterval.ALL_TIME
                            && other.estimatedInterval != TimeInterval.ALL_TIME) {
                        return this.estimatedInterval.compareTo(other.estimatedInterval);
                    }
                }
                return bindingsCompare;
            }
            return targetCompare;
        }
        logger.debug("not a statement; returning string compare");
        return this.toString().compareTo(otherObj.toString());
    }

    /**
     * This is used to check for "shallow" equality of statements.
     * i.e., when fully resolved (wherever bindings, etc. came from),
     * do these statements match?
     *
     * Akin to:
     * open(table0) = true
     * vs
     * open(|~open.end|POSITION)<<|~move.a| ARM==|~right| ARM:right, |~move.end| POSITION==|~table0| TABLE:table0, |~move.start| POSITION==|~right-home| POSITION:right-home>>{|~bool|BOOL}
     * and then also comparing values.
     *
     * I haven't done this quite like that, yet, but I've at least simplified a few things that were
     * stopping this from evaluating correctly.
     * */
    public int compareToResolved(Statement otherStatement) {
		int compare = target.nameCompareTo(otherStatement.target);
		List<WorldArgument> lhs = target.getArgs();
		List<WorldArgument> rhs = otherStatement.target.getArgs();

		if (compare == 0) {
			compare = compareToResolvedArguments(lhs, rhs);
		}
		if (compare == 0) {
			compare = compareToResolvedBindings(otherStatement, compare, lhs, rhs);
		}
		if (compare == 0 && estimatedInterval != TimeInterval.ALL_TIME
					&& otherStatement.estimatedInterval != TimeInterval.ALL_TIME) {
			compare = this.estimatedInterval.compareTo(otherStatement.estimatedInterval);
		}

		return compare;
	}
	private static int compareToResolvedArguments(List<WorldArgument> lhs, List<WorldArgument> rhs) {
		int compare = lhs.size() - rhs.size();
		if (compare == 0) {
			for (int i = 0; i < lhs.size() && compare == 0; i++) {
				compare = lhs.get(i).compareToShallow(rhs.get(i));
			}
		}
		return compare;
	}

	private int compareToResolvedBindings(Statement otherStatement, int compare,
												 List<WorldArgument> lhs, List<WorldArgument> rhs) {
		Bindings lhsB = target.filteredBindings();
		Bindings rhsB = otherStatement.target.filteredBindings();
		for (int i = 0; i < lhs.size() && compare == 0; ++i) {
			String lhsID = findBinding(lhs.get(i).getPathName(), lhsB);
			String rhsID = findBinding(rhs.get(i).getPathName(), rhsB);
			if (lhsID != null) {
				compare = lhsID.compareTo(rhsID);
			} else {
				compare = -1;
			}
		}
		return compare;
	}


    private String findBinding(String pathName, Bindings bindings) {
        for (Binding bind : bindings) {
            if (bind.source.getPathName().compareTo(pathName) == 0) {
                return bind.target.getPathName();
            }
        }
        return null;
    }

    @SuppressWarnings({"rawtypes", "unchecked"})
    public static int compareValue(@NotNull Object lhs, @NotNull Object rhs) {
        Class thisClass = lhs.getClass();
        Class otherClass = rhs.getClass();
        if (thisClass.equals(otherClass)) {
            if (lhs instanceof Comparable) {
                return ((Comparable) lhs).compareTo(rhs);
            } else {
                return lhs.toString().compareTo(rhs.toString());
            }
        } else {
            return thisClass.toString().compareTo(otherClass.toString());
        }
    }

    /**
     * Returns the hashCode for this object.
     *
     * JGraphT relies on a consistent implementation of both equals()
     * *and* hashCode(), and our implementation of equals() relies on compareTo().
     * Failing to synchronize these methods in subclasses results in
     * incorrect behavior in the graph library or algorithms that use it!
     * For details, see:
     * <ul>
     *     <li>https://github.com/jgrapht/jgrapht/wiki/Users%3A-EqualsAndHashCode</li>
     *     <li>https://www.ibm.com/developerworks/java/library/j-jtp05273/index.html</li>
     * </ul>
     *
     * @return the hash code for this object.
     */
    @Override
    public int hashCode() {
        return id.hashCode();
    }

    /**
     * Returns whether this object equals other.
     *
     * JGraphT relies on a consistent implementation of both equals()
     * *and* hashCode(), and our implementation of equals() relies on compareTo().
     * Failing to synchronize these methods in subclasses results in
     * incorrect behavior in the graph library or algorithms that use it!
     * For details, see:
     * <ul>
     *     <li>https://github.com/jgrapht/jgrapht/wiki/Users%3A-EqualsAndHashCode</li>
     *     <li>https://www.ibm.com/developerworks/java/library/j-jtp05273/index.html</li>
     * </ul>
     *
     * @param other the object to test
     * @return whether this equals other
     */
    @Override
    public boolean equals(Object other) {
        if (other instanceof Statement) {
            return compareTo(other) == 0;
        }
        return false;
    }

    /**
     * Returns whether this is a negated statement or not.
     * Corresponds to whether it is an add or delete effect.
     * The default is false and should be overridden by Statement classes
     * that are negated.
     */
    public boolean isNegated() {
        return false;
    }

    @Override
    public String toString() {
        String pathString = toStringPath(getPrintOptions());
        String timeString = toStringEstimatedTime();
        String targetString = getTarget().getName();
        String argsString = "";
        if (hasBindings()) {
            argsString = toStringBindings(getPrintOptions());
        } else {
            argsString = toStringArgs(childPrintOptions);
        }
        String valueString = toStringValue();
        return String.format("%s%s%s%s%s", pathString, timeString, targetString, argsString, valueString);
    }

    public String toStringEstimatedTime() {
        String timeString = "";
        if (showEstimatedTime) {
            timeString = estimatedInterval.toString() + " ";
        }
        return timeString;
    }

    abstract public String toStringValue();

    abstract public String toStringValue(PrintOptions printOptions);


    public String getShortName() {
        return toString();
    }

    public int getId() {
        return id;
    }

    // endregion<Object and Comparable Overrides>
    //===============================================

    //===============================================
    // region<Accessors>


    public StateVariable getTarget() {
        return target;
    }

    /**
     * A statement is complete if it is entailed by the StateAssignment.
     *
     * @param state the state to check
     * @return whether the state is entailed
     */
    public boolean isEntailedBy(StateAssignment state) {
        return false;
    }


    @SuppressWarnings("SameReturnValue")
    public boolean modifiesStateWhenApplied() {
        return false;
    }

    @SuppressWarnings("EmptyMethod")
    public void apply() {

    }

    // endregion<Accessors>
    //===============================================

    //===============================================
    // region<Temporal Accessors>

    public TimeInterval getTimeInterval() {
        return estimatedInterval;
    }

    public Timepoint getStart() {
        return estimatedInterval.getStart();
    }

    public Timepoint getEnd() {
        return estimatedInterval.getEnd();
    }

    public void setEstimatedInterval(TimeInterval interval) {
        this.estimatedInterval = interval;
    }



    /**
     * Used to introduce an initial condition where
     * the statement is true at the beginning of time.
     */
    public void estimatedEndEqualsStart() {
        estimatedInterval.setEnd(estimatedInterval.getStart());
    }

    public void estimatedStartEqualsEnd() {
        estimatedInterval.setStart(estimatedInterval.getEnd());
    }

    // endregion<Cloner>
    //===============================================

    //===============================================
    // region<Cloner>

    public abstract <S extends Statement, B extends Cloner<S, B>>
    Cloner<S, B> cloner();

    public abstract static class Cloner<S extends Statement, B extends Cloner<S, B>> {
        final Statement originalStatement;
        StateVariable target;
        TimeInterval estimatedInterval;

        protected Cloner(Statement statement) {
            originalStatement = statement;
            setTargetByReference();
            estimatedInterval = statement.estimatedInterval.copy();
        }

        protected B getThis() {
            //noinspection unchecked
            return (B) this;
        }

        @SuppressWarnings("UnusedReturnValue")
        public Cloner<S, B> setTargetByReference() {
            this.target = originalStatement.target;
            return getThis();
        }

        public abstract S build();
    }

    // endregion<Cloner>
    //===============================================

    // ====================================================
    // region<Bindings>

    public BindResult bindArgs(WorldObject... objects) {
        return bindArgs(Arrays.asList(objects));
    }

    public BindResult bindArgs(List<WorldObject> objects) {
        BindResult result = BindResult.FAILURE_UNSPECIFIED;
        if (objects.size() == 0) {
            setBindings(new Bindings());
            result = BindResult.SUCCESS;
        } else {
            List<WorldArgument> args = getArgs();
            if (args.size() == objects.size()) {
                Map<WorldArgument, WorldObject> map = zipMap(args, objects);
                for (Map.Entry<WorldArgument, WorldObject> entry : map.entrySet()) {
                    result = bind(entry.getKey(), entry.getValue());
                    if (result.isFailure()) {
                        break;
                    }
                }
                result = BindResult.SUCCESS;
            } else {
                String msg = String.format("Binding %s, objects:%s do not match arguments:%s", toString(), objects, args);
                logger.error(msg);
                result = BindResult.hasMismatchedArguments(msg);
            }
        }
        return result;
    }

    public BindResult bindArgs(Map<String, WorldObject> bindingMap) {
        for (Map.Entry<String, WorldObject> entry : bindingMap.entrySet()) {
            BindResult result = bind(entry.getKey(), entry.getValue());
            if (result.isFailure()) {
                return result;
            }
        }
        return BindResult.SUCCESS;
    }

    public BindResult bindArgs(Bindings bindings) {
        return setBindings(bindings);
    }

    // endregion
    // ====================================================

    // ====================================================
    // region<Subclass distinguish helpers>

    public static boolean hasNumericValue(Statement stored) {
        return stored.hasNumericValue();
    }
    public boolean hasNumericValue() {
        if (this instanceof PersistenceStatement) {
            return ((PersistenceStatement<?>) this).getValue() != null
                    && ((PersistenceStatement<?>) this).getValue() instanceof Number;
        }
        return false;
    }


    // endregion
    // ====================================================


    // ====================================================
    // region<Causal Links>

    public void add(CausalLink causalLink) {
        initCausalLinksIfNeeded();
        causalLinks.add(causalLink);
    }

    private void initCausalLinksIfNeeded() {
        if (causalLinks == null) {
            causalLinks = new ArrayList<>();
        }
    }

    public void linkAsSuccessor(Statement successor) {
        CausalLink causalLink = new CausalLink(this, successor);
        this.add(causalLink);
        successor.add(causalLink);
    }

    public void clearLink(Statement successor) {
        causalLinks.removeIf(causalLink -> {
            return causalLink.matches(Statement.this, successor);
        });

        successor.causalLinks.removeIf(causalLink -> {
            return causalLink.matches(Statement.this, successor);
        });
    }

    public boolean isLinkedTo(Statement other) {
        return getCausalLinks().stream().anyMatch(causalLink -> causalLink.isLinkedTo(other));
    }

    public boolean hasPredecessor(Statement predecessor) {
        return getCausalLinks().stream().anyMatch(causalLink -> causalLink.isSource(predecessor));
    }

    public boolean hasSuccessor(Statement successor) {
        return getCausalLinks().stream().anyMatch(causalLink -> causalLink.isDestination(successor));
    }

    public Collection<CausalLink> getCausalLinks() {
        if (causalLinks != null) {
            return causalLinks;
        }
        return Collections.emptyList();
    }


    // endregion
    // ====================================================


}
