package nrl.actorsim.domain;


import nrl.actorsim.utils.PrintOptions;
import org.jetbrains.annotations.NotNull;

/**
 * A class that stores details about argument variables while
 * leveraging the code in WorldObject, WorldType, and Type.
 *
 * The path stores the name of the argument.
 * The id of this class is not valid.
 * This class cannot be a valid instance of WorldObject
 * To create an instance, use WorldObject.instance() methods.
 *
 * Methods from WorldObject not directly related to type information
 * should be ignored, as they may be overloaded to throw exceptions
 * in the future.
 */
public class WorldArgument extends WorldObject {
    @SuppressWarnings("unused")
    public static WorldArgument NULL_WORLD_ARGUMENT = new WorldArgument(NULL_TYPE, "NULL_WORLD_ARG");

    public WorldArgument(WorldType type, String argumentName) {
        super(type);
        setPathName(argumentName);
        holdArgumentsOnly();
    }

    public WorldArgument(WorldArgument other) {
        super(other);
        setPathName(other.getPathName());
        holdArgumentsOnly();
    }

    @Override
    public int compareTo(@NotNull WorldType other) {
        int result = compareToType(other);
        if (result == 0) {
            if (other instanceof WorldArgument) {
                result = compareToPathName(other);
            }
        }
        return result;
    }

    @Override
    protected String toStringImpl(PrintOptions options) {
        String pathString = getPathName();
        String typeString = "";
        if (! options.hideTyping()) {
            typeString += typeName + ":";
        }

        return typeString + pathString;
    }

    public int compareToShallow(@NotNull WorldType other) {
        int result = compareToType(other);
        if(result != 0 && superTypes().contains(other)) {
        	result = 0;
        }
        if(result != 0 && subTypes().contains(other)) {
        	result = 0;
        }
        return result;
    }



    // ====================================================
    // region<Bindable Overrides>

    @Override
    public boolean canBindTo(Bindable bindable) {
        return bindable instanceof WorldObject
                && ((WorldObject)bindable).equalsOrInheritsFromType(this);
    }

    // endregion
    // ====================================================

    // ====================================================
    // region<WorldObject Overrides>

    @Override
    public String getId() {
        throw new UnsupportedOperationException();
    }

    @Override
    public void setId(@NotNull String id) {
        //no op
        //NB: throwing an exception here causes the
        //    WorldObject copy constructor to fail!
    }

    @Override
    public WorldArgument instance() {
        return new WorldArgument(this, id);
    }

    // endregion
    // ====================================================
}


