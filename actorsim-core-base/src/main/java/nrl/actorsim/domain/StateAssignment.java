package nrl.actorsim.domain;

import org.jetbrains.annotations.NotNull;

import java.util.Set;
import java.util.TreeSet;

public class StateAssignment {
    private final PlanningProblem problem;
    final Set<Statement> statements = new TreeSet<>();
    final Set<StateRelation> relations = new TreeSet<>();

    /**
     * Construct a StateAssignment for the given problem.
     * Not to be called directly; instead, call PlanningProblem.getNewState()
     *
     * @param problem the owner of the assignment
     */
    StateAssignment(@NotNull PlanningProblem problem) {
        this.problem = problem;
    }

    /**
     * Add a statement to this StateAssignment.
     *
     * @param statement the statement to add
     * @return this, which allows chaining
     */
    @SuppressWarnings("UnusedReturnValue")
    public StateAssignment add(Statement statement) {
        statements.add(statement);
        return this;
    }

    /**
     * Add a StateRelation to this StateAssignment.
     *
     * @param relation the relation to add
     * @return this, which allows chaining
     */
    @SuppressWarnings("UnusedReturnValue")
    public StateAssignment add(StateRelation relation) {
        for (Binding binding : relation.findBindings()) {
            WorldObject obj = binding.getTarget().content();
            problem.add(obj);
        }
        relations.add(relation);
        return this;
    }

    public boolean contains(Statement statement) {
        return statements.contains(statement);
    }
}
