package nrl.actorsim.domain;

import nrl.actorsim.utils.PrintOptions;
import nrl.actorsim.utils.Utils;
import org.jetbrains.annotations.NotNull;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;


public class WorldObject extends WorldType implements Bindable {
    public static String MISSING_INSTANCE_ID;
    public static WorldObject NULL_WORLD_OBJECT;
    public static Object NULL_ATTRIBUTE;

    public static WorldObject PLACEHOLDER;

    public static WorldObject TRUE;
    public static WorldObject FALSE;

    public static WorldObject OPERATOR;
    public static WorldObject ACTION;

    public static WorldObject GOAL;
    public static WorldObject GOAL_MODE;
    public static WorldObject GOAL_STRATEGY;

    static {
        initializeStaticTypes();
    }

    protected static void initializeStaticTypes() {
        WorldType.initializeStaticTypes();
        MISSING_INSTANCE_ID = "missingId";

        NULL_WORLD_OBJECT = new WorldObject(WorldType.NULL_TYPE);
        NULL_ATTRIBUTE = new Object();

        PLACEHOLDER = new WorldObject(WorldType.NULL_TYPE, "placeholder");

        TRUE = new WorldObject(WorldType.BOOLEAN, "true");
        FALSE = new WorldObject(WorldType.BOOLEAN, "false");

        OPERATOR = new WorldObject("OPERATOR");
        ACTION = new WorldObject("ACTION");

        GOAL = new WorldObject("GOAL");
        GOAL_MODE = new WorldObject("GOAL_MODE");
        GOAL_STRATEGY = new WorldObject("GOAL_STRATEGY");
    }

    protected String id = MISSING_INSTANCE_ID;
    protected Map<String, Object> attributes;  // Hold details that might assist during translation

    /**
     * A flag to show type information in toString() output.
     */
    protected boolean showType = true;

    // ====================================================
    // region<Constructors and Intializers>

    protected WorldObject(WorldType type) {
        super(type);
        setPathName("NULL");
    }

    public WorldObject(WorldType type, String id) {
        super(type);
        this.setId(id);
    }

    protected WorldObject(WorldObject other) {
        super(other);
        this.setId(other.id);
        this.showType = other.showType;
        copyAttributes(other);
    }

    /**
     * @param typeName the name of the type
     * @param id       the id of the type
     */
    public WorldObject(String typeName, String id) {
        super(typeName);
        this.setId(id);
    }

    /**
     * @param typeName the name of the type
     */
    public WorldObject(String typeName) {
        super(typeName);
    }

    public WorldObject hideTypeInformation() {
        showType = false;
        return this;
    }

    // endregion
    // ====================================================

    // ====================================================
    // region<Argument and Instance>

    @Override
    public WorldArgument asArgument(String argumentName) {
        return new WorldArgument(this, argumentName);
    }

    /**
     * A convenience method to clone a new instance of this WorldObject
     * using the appropriate copy constructors.
     * <p>
     * We are not using the clonable interface because it is cumbersome:
     * it does not play well with final fields and copy constructors.
     * Also, covariant return types in subclasses are allowed, mitigating
     * the need to create a complex Builder infrastructure to support
     * creating new instances in subclasses of WorldObject.
     *
     * @return a new instance of WorldObject
     */
    public WorldObject instance() {
        return new WorldObject(this, id);
    }

    public WorldObjectWrapper wrapper() {
        return new WorldObjectWrapper(this);
    }

    /**
     * A convenience method to clone a new instance of this WorldObject
     * using the appropriate copy constructors.
     * <p>
     * We are not using the clonable interface because it is cumbersome:
     * it does not play well with final fields and copy constructors.
     * Also, covariant return types in subclasses are allowed, mitigating
     * the need to create a complex Builder infrastructure to support
     * creating new instances in subclasses of WorldObject.
     *
     * @return a new instance of WorldObject
     * @param idIn the id of the new instance
     */
    public WorldObject instance(String idIn) {
        return new WorldObject(this, idIn);
    }


    // endregion
    // ====================================================

    // ====================================================
    // region<Substitution>

    public void update(WorldObject worldObject)
            throws IllegalArgumentException {
        this.substitute(worldObject);
    }

    public void substitute(WorldObject other)
            throws IllegalArgumentException {
        if (equalsOrInheritsFromTypeThrow(other)) {
            setId(other.id);
            copyAttributes(other);
        }
    }

    @SuppressWarnings("unused")
    public void substitute(String instanceId) {
        this.setId(instanceId);
    }

    // endregion
    // ====================================================

    // ====================================================
    // region<toString>

    @Override
    public String toString() {
        return toStringImpl(PrintOptions.DEFAULT_OPTIONS);
    }

    @Override
    public String toString(PrintOptions options) {
        return toStringImpl(options);
    }

    protected String toStringImpl(PrintOptions options) {
        String pathString = toStringPath(options);
        String typeString = "";
        String idString = "";
        if (! options.hideTyping()) {
            typeString += " " + typeName;
            idString = ":";
        }

        if (id.equals(MISSING_INSTANCE_ID)) {
            idString = Utils.MISSING_ID;
        } else {
            idString += id;
        }
        return pathString + typeString + idString;
    }

    // endregion
    // ====================================================

    // ====================================================
    // region<Comparison>

    @Override
    public int hashCode() {
        return id.hashCode();
    }

    @Override
    public boolean equals(Object other) {
        if (other instanceof WorldObject) {
            return this.compareTo((WorldObject) other) == 0;
        }
        return false;
    }

    @Override
    public int compareTo(@NotNull WorldType other) {
        int result = compareToType(other);
        if (result == 0) {
            if (other instanceof WorldObject) {
                result = compareToInstanceId((WorldObject) other);
            }
        }
        return result;
    }

    public int compareToInstanceId(WorldObject other) {
        return this.id.compareTo(other.id);
    }

    @SuppressWarnings("unused")
    public int compareToType(WorldObject other) {
        if (this.equals(WorldType.ANY) || other.equals(WorldType.ANY)) {
            return 0;
        } else {
            return this.compareTo(other);
        }
    }

    // endregion
    // ====================================================

    // ====================================================
    // region<Accessors>

    public String getId() {
        return id;
    }

    protected void setId(@NotNull String id) {
        this.id = id;
        setPathName(id);
    }

    // endregion
    // ====================================================

    // ====================================================
    // region<Type Interface>

    @Override
    public boolean isInstance() {
        return !(this instanceof WorldArgument);
    }

    @Override
    public WorldObject addSuperType(WorldType... superTypes) {
        super.addSuperType(superTypes);
        return this;
    }

    @Override
    public WorldObject addSuperType(String... superTypeNames) {
        return (WorldObject) super.addSuperType(superTypeNames);
    }

    // endregion
    // ====================================================

    // ====================================================
    // region<Attributes>

    /***
     * Allows the application to add specific objects to assist with translating between
     * application logic and planning logic.
     *
     * @param key the key of the attribute
     * @param value the value of the attribute
     */
    public void addAttribute(String key, Object value) {
        if (Objects.isNull(attributes)) {
            attributes = new HashMap<>();
        }
        attributes.put(key, value);
    }

    public boolean contains(String key) {
        boolean hasAttribute = false;
        if (!Objects.isNull(attributes)) {
            if (attributes.containsKey(key)) {
                hasAttribute = true;
            }
        }
        return hasAttribute;
    }

    @SuppressWarnings("unused")
    public Object getAttribute(String key) {
        Object value = NULL_ATTRIBUTE;
        if (contains(key)) {
            value = attributes.get(key);
        }
        return value;
    }

    private void copyAttributes(WorldObject other) {
        if (other.attributes != null) {
            for (Map.Entry<String, Object> entry : attributes.entrySet()) {
                addAttribute(entry.getKey(), entry.getValue());
            }
        }
    }

    // endregion
    // ====================================================
}

