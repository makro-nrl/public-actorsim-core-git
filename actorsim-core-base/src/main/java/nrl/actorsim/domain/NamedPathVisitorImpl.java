package nrl.actorsim.domain;

public class NamedPathVisitorImpl implements NamedPathVisitor {
    @Override
    public <T extends NamedPath> T visit(NamedPath namedPath) {
        return (T) namedPath;
    }

    @Override
    public <T extends NamedPathNode> T visit(NamedPathNode namedPathNode) {
        return (T) namedPathNode;
    }


    @Override
    public <T extends NamedPathNode> T visit(Statement statement) {
        return (T) statement;
    }

    @Override
    public <ReturnType> ReturnType visit(StateVariable sv) {
        return null;
    }

}
