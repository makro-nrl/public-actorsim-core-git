package nrl.actorsim.domain;

import java.util.List;

@SuppressWarnings("unused")
/**
 * A convenience {@link TransitionStatement<Number>} that hides
 * the generic template.
 *
 * All new functionality should be added to {@link TransitionStatement}.
 */
public class NumberTransitionStatement extends TransitionStatement<Number> {
    public NumberTransitionStatement(StateVariableTemplate svt, List<WorldObject> bindings, Number from, Number to) {
        super(svt, bindings, from, to);
    }

    public NumberTransitionStatement(StateVariable sv, List<WorldObject> bindings, Number from, Number to) {
        super(sv, bindings, from, to);
    }

    public NumberTransitionStatement(StateVariable sv, Number from, Number to) {
        super(sv, from, to);
    }

    public NumberTransitionStatement(StateVariable sv) {
        super(sv);
    }
}
