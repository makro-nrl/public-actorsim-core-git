package nrl.actorsim.domain;

import java.util.*;

public class TypeManager extends BindableImpl implements NamedPathContainer {
    final static org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(TypeManager.class);

    private static TypeManager instance;
    private final Set<WorldType> closedTypes = new TreeSet<>(new TypeComparator());
    final Map<String, WorldType> types = new TreeMap<>();
    final Map<WorldType, Set<WorldType>> superTypes = new TreeMap<>(new TypeComparator());
    final Map<WorldType, Set<WorldType>> subTypes = new TreeMap<>(new TypeComparator());



    private TypeManager() {
    }

    public static TypeManager getInstance() {
        if (instance == null) {
            instance = new TypeManager();
        }
        return instance;
    }

    static String validate(String typeName) {
        return typeName.toUpperCase();
    }


    public static void resetInstanceForTesting() {
        instance = new TypeManager();
    }

    public static WorldType get(String typeName) {
        String validated = TypeManager.validate(typeName);
        TypeManager instance = TypeManager.getInstance();
        if (instance.types.containsKey(validated)) {
            return instance.types.get(validated);
        } else {
            String message = String.format("There is no type '%s'", validated);
            logger.error(message);
            throw new RuntimeException(message);
        }
    }

    public static WorldType add(String typeName, WorldType... superTypes) {
        return add_new_type(typeName, Arrays.asList(superTypes), false);
    }

    public static WorldType add(String typeName, List<WorldType> superTypes) {
        return add_new_type(typeName, superTypes, false);
    }

    public static WorldType add(String typeName, Set<WorldType> superTypes) {
        return add_new_type(typeName, new ArrayList<>(superTypes), false);
    }

    @SuppressWarnings("unused")
    public static WorldType addAndClose(String typeName, WorldType... superTypes) {
        return add_new_type(typeName, Arrays.asList(superTypes), true);
    }

    @SuppressWarnings("unused")
    public static WorldType addAndClose(String typeName, List<WorldType> superTypes) {
        return add_new_type(typeName, superTypes, false);
    }

    protected static WorldType add_new_type(String typeName, List<WorldType> superTypes, boolean closeAfterAdding) {
        String validated = validate(typeName);
        WorldType type = TypeManager.getInstance().types.get(validate(validated));
        if (type == null) {
            logger.info("Creating simple WorldType for name '{}' with superTypes: {}", validated, superTypes);
            type = new WorldType(validated, superTypes);
        }
        if (closeAfterAdding) {
            close(type);
        }
        return type;
    }

    @SuppressWarnings("UnusedReturnValue")
    public static WorldType add(WorldType type, WorldType... superTypes) {
        return add_impl(type, Arrays.asList(superTypes), false);
    }

    @SuppressWarnings("UnusedReturnValue")
    public static WorldType add(WorldType type, List<WorldType> superTypes) {
        return add_impl(type, superTypes, false);
    }

    @SuppressWarnings("unused")
    public static WorldType addAndClose(WorldType type, WorldType... superTypes) {
        return add_impl(type, Arrays.asList(superTypes), true);
    }

    @SuppressWarnings("unused")
    public static WorldType addAndClose(WorldType type, List<WorldType> superTypes) {
        return add_impl(type, superTypes, true);
    }

    protected static WorldType add_impl(WorldType type, List<WorldType> superTypes, boolean closeAfterAdding) {
        TypeManager instance = TypeManager.getInstance();
        String validated = type.type();
        if (!instance.types.containsKey(validated)) {
            logger.debug("Adding type '{}' with superTypes {}", validated, superTypes);
            instance.types.put(validated, type);
        }
        instance.addSuperTypes(type, superTypes);

        if (closeAfterAdding) {
            close(type);
        }
        return type;
    }

    public static boolean inheritsFrom(WorldType type, WorldType ofType) {
        Set<WorldType> superTypes = getSuperTypes(type);
        return superTypes.contains(ofType);
    }

    public static boolean hasType(String typeName) {
        String validated = validate(typeName);
        return TypeManager.getInstance().types.containsKey(validated);
    }

    public static boolean missingType(String typeName) {
        String validated = validate(typeName);
        return !hasType(validated);
    }

    public static Set<WorldType> getSuperTypes(WorldType type) {
        Set<WorldType> superTypes = Collections.emptySet();
        TypeManager instance = TypeManager.getInstance();
        if (instance.superTypes.containsKey(type)) {
            superTypes = instance.superTypes.get(type);
        }
        return superTypes;
    }

    public static void addTransitiveSupers(WorldType type, WorldType superType) {
        Set<WorldType> superSuperTypes = getSuperTypes(superType);
        for(WorldType superSuperType : superSuperTypes) {
            if(!type.inheritsFrom(superSuperType)) {
                type.addSuperType(superSuperType);
                addTransitiveSupers(type, superSuperType);
            }
        }

    }

    public static void addSubtype(WorldType type, WorldType subType) {
        TypeManager instance = TypeManager.getInstance();
        Set<WorldType> existingSubs = instance.subTypes.computeIfAbsent(type, k -> new TreeSet<>());
        existingSubs.add(subType);
    }


    public static Set<WorldType> getSubTypes(WorldType type) {
        Set<WorldType> subTypes = Collections.emptySet();
        TypeManager instance = TypeManager.getInstance();
        if (instance.subTypes.containsKey(type)) {
            subTypes = instance.subTypes.get(type);
        }
        return subTypes;
    }

    public static void close(WorldType type) {
        getInstance().closedTypes.add(type);
    }

    public static boolean isClosed(WorldType type) {
        return getInstance().closedTypes.contains(type);
    }

    public void addSuperTypes(WorldType type, WorldType... superTypes) {
        addSuperTypes(type, Arrays.asList(superTypes));
    }

    public void addSuperTypes(WorldType type, Set<WorldType> superTypes) {
        addSuperTypes(type, new ArrayList<>(superTypes));
    }

    public void addSuperTypes(WorldType type, List<WorldType> superTypes) {
        if (superTypes.size() > 0) {
            if (isClosed(type)) {
                logger.warn("Type '{}' closed, refusing to add superType '{}'", type, superTypes);
            } else {
                Set<WorldType> existingSupers = this.superTypes.computeIfAbsent(type, k -> new TreeSet<>());
                existingSupers.addAll(superTypes);
                for(WorldType superType : superTypes) {
                    Set<WorldType> transitiveSupers = getSuperTypes(superType);
                    for (WorldType transitiveSuper : transitiveSupers) {
                        if (!existingSupers.contains(transitiveSuper)) {
                            addSuperTypes(type, transitiveSuper);
                        }
                    }
                    addSubtype(superType, type);
                }
            }
        }

    }
}
