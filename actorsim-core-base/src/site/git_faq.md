You can think of git as a four-layer system for revision control (ordered by
 closest to your IDE):
	
- your working directory (aka working tree), where you hack away at the matrix;
- the index (aka cache) of your local repository, where `add`, `rm`, or `reset` 
put or remove changes in the cache;
- branches on your local repository, where `commit` or `reset` put or remove 
changes to the branch, and checkout switches branches;
- remote repositories, where `fetch`, `pull`, or `push` put or get changes,  
which usually reside within branches, to a repository on a remote server.
	
A great book to understand what's going on under the hood of git is Git Pro by Chacon and Strab (2014): https://www.apress.com/gp/book/9781484200773  It's great for late night reading