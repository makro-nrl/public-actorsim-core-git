# Effective Use of Git

Large projects require good revision control discipline from each developer so 
that code changes go through a known, stable workflow.

One of the first things you will do is `clone` a repository to access the 
code.  Then you will create a working `branch` from a stable point to
make changes.  While implementing changes, you `commit` changesets.  
For long-running tasks or tasks that you share with another developer,
you may `push` your working branch back to the remote repository so that
other developers can access your code.  Otherwise, your working branch is local.
Often, a code review will be needed when you have finished your working branch.
Once you have approval, you `merge` your working branch back to the stable point 
and, finally, `push` your changes to a remote repository.  

## Branch Naming with GitFlow 
# Branch Naming 

We follow a variant of the [Gitflow](https://www.atlassian.com/git/tutorials/comparing-workflows/gitflow-workflow) 
workflow for branch naming in ActorSim.
The branch types include:
- a single **master** branch holds releases and tags.
- multiple **release** branches hold the state of the code
  for a particular release.  See [release_notes.md](release_notes.md)
  for details about releases.  Release tags in git are 
  just a commit hash and are not really a full branch.  
- a single **develop** branch holds the latest merged code 
  that has been approved through code review.  The latest _approved_
  work for team is usually found here.
- multiple **feature** branches hold the latest work for a specific feature.
  Feature branches originate from develop 
  and are merged back onto develop once approved. 
- multiple **bugfix** branches hold the latest work for a specific 
  patch to correct a known bug.  Bugfix branches usually originate
  from develop or from a release branch and should be merged back 
  to the originating branch and back to develop if originating elsewhere.


## Creating New Branches
_**If you don't know what to name your new branch, please ask your 
development lead.**_

## Branch names
Please create a new branch (we use gitflow branch naming) 
off of the appropriate originating branch. For example, a bugfix branches
something is something like `bugfix/short-description`.  
An example feature branch might be something like `feature/short-description`.

If you are working with tickets from DI2E, `short-description` should
include a ticket number follwed by a pithy, descriptive name.  For example,
`feature/ACTORSIM-530-maven-build` indicates work done for the feature 
to move to a Maven build structure.


## Pushing back to develop
Generally, you will need to do a code review before you merge your code back
to develop.  The only exception to this is when you have documentation.

## Documentation can always be added on develop
Any documentation changes can just be put right on develop.
There's no need for a code review or approval mechanism!


## Committing code

Strive to have each commit represent the smallest possible related set of
changes.  This makes it easy to move commits between branches in a large 
team.  

### Example
While working on a large feature for ActorSim under ticket ACTORSIM-318, 
Mak writes the file you are reading `BranchNaming.md`.  Normally, documentation
changes can be pushed directly to the `develop` branch, but Mak proceeds
to commit this new file to the feature branch.  

While on branch `feature/ACTORSIM-318...`, Mak uses his alias `glods`, to  
run the command:
```
git log --graph --pretty='\''%Cred%h%Creset -%C(auto)%d%Creset %s %Cgreen(%ad) %C(bold blue)<%an>%Creset'\'' --date=short
```
The output shows the commit hash of the desired commits, which are marked with `V`.
```git
08:57:18 in actorsim-core-git on  feature/ACTORSIM-316-hubo-scenario-1-strategies [⇡!?]
➜ glods
  VVVVVVV
* f80047c - (HEAD -> feature/ACTORSIM-316-hubo-scenario-1-strategies) docs: updated BranchNaming.md (2020-07-15) <Mak Roberts>
* 308a294 - remove unused method (2020-07-15) <Mak Roberts>
*   a21b24f - Merge branch 'develop' into feature/ACTORSIM-316-hubo-scenario-1-strategies (2020-07-15) <Mak Roberts>
|\
| * 6056c96 - (di2e/develop, di2e/HEAD, develop) added switch to disable memory view on start (2020-07-09) <Mak Roberts>
[..]VVVVVVV
* | 2a5341b - docs: BranchNaming.md and CodeStyle.md (2020-06-27) <Mak Roberts>
* | 784cb14 - docs: clarify javadoc and minor formatting (2020-06-27) <Mak Roberts>
* | 8877782 - rename PlannerConnector.java (2020-06-26) <Mak Roberts>
* | 02d2d68 - initial draft of planner worker (2020-06-26) <Mak Roberts>
* | 85fb9b3 - initial draft of PDDLGenerator (2020-06-26) <Mak Roberts>
* | 30fed5a - minor formatting (2020-06-26) <Mak Roberts>
* | fdd5ad8 - correct cycle latching (2020-06-26) <Mak Roberts>
* | 62e5bff - use paths instead of files for domain and problem (2020-06-22) <Mak Roberts>
|/
* 63ac1d9 - docs: added plantuml discussion (2020-06-15) <Mak Roberts>
* 32e72ce - docs: cleaned up release_notes.md (2020-06-15) <Mak Roberts>
* 402ad6b - docs: tweaking the README.md and INSTALL.md (2020-06-15) <Mak Roberts>
* 90b8774 - docs: moved GettingStarted.md to README.md (2020-06-15) <Mak Roberts>
* 953d212 - docs: added GettingStarted.md (2020-06-15) <Mak Roberts>
* e2dbb22 - docs: added release_notes.md (2020-06-14) <Mak Roberts>
* 960fc3e - (di2e/feature/ACTORSIM-530-maven-build) docs: updated to reflect recent changes (2020-06-14) <Mak Roberts>
```
Good!  Changes to this file exist in a two commits, `2a5341b` and `f80047c`. 
So it will be easy to copy these changes to the develop branch using a command
called `git-cherrypick` (alias `gcp`).

So now Mak switches to the develop branch (`git checkout develop` alias: `gcd`)
and cherrypicks the two commits...
```shell script
09:08:45 in actorsim-core-git on  feature/ACTORSIM-316-hubo-scenario-1-strategies [⇡?] took 2s
➜ gcd
Switched to branch 'develop'
Your branch is up-to-date with 'di2e/develop'.

09:09:12 in actorsim-core-git on  develop [?]
➜ gcp 2a5341b
[develop ad57617] docs: BranchNaming.md and CodeStyle.md
 Date: Sat Jun 27 20:46:29 2020 -0400
 3 files changed, 64 insertions(+), 6 deletions(-)
 create mode 100644 actorsim-core-base/src/site/BranchNaming.md
 rewrite actorsim-core-base/src/site/CodeStyle.md (63%)

09:09:55 in actorsim-core-git on  develop [⇡?]
➜ gcp f80047c
[develop 1b31cb6] docs: updated BranchNaming.md
 Date: Wed Jul 15 09:02:20 2020 -0400
 1 file changed, 46 insertions(+), 1 deletion(-)

```
... and pushes the change up to the remote:
```git
09:10:48 in actorsim-core-git on  develop [⇡?]
➜ git push
Counting objects: 14, done.
Delta compression using up to 8 threads.
Compressing objects: 100% (14/14), done.
Writing objects: 100% (14/14), 3.29 KiB | 0 bytes/s, done.
Total 14 (delta 6), reused 0 (delta 0)
To ssh://git@bitbucket.di2e.net:7999/actorsim/actorsim-core.git
   6056c96..1b31cb6  develop -> develop
```
You can see the cherrypick of `2a5341b` also included some changes to CodeStyle.md,
which were still documentation, so it worked out fine.  
But had these documentation changes been mixed in as part of a larger commit, 
it would be impossible to quickly move them to another branch. 
  

