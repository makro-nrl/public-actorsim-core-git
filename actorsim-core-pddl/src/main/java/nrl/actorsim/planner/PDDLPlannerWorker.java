package nrl.actorsim.planner;

import nrl.actorsim.memory.WorkingMemory;
import org.apache.commons.cli.*;

import java.io.*;
import java.nio.file.*;
import java.util.*;
import java.util.concurrent.TimeUnit;

import static org.apache.commons.cli.BooleanOption.Status.DISABLED;

/**
 * Generates plans by saving the contents of the {@link PlanRequest}
 * to local files and running the planner executive on those files.
 *
 *
 */
public class PDDLPlannerWorker extends PlannerWorkerBase<PlanRequest> {
    final static org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(PDDLPlannerWorker.class);

    protected final PDDLPlannerOptions pddlPlannerOptions;

    protected final PlannerDetails paths;
    String domainFilename = "domain.pddl";
    String problemFilename = "problem.pddl";
    String outputFilename = "output.txt";
    String solutionFilePrefix = "sol.txt";  //not required by every planner

    public PDDLPlannerWorker(PDDLPlannerOptions options) {
        super(options);
        this.pddlPlannerOptions = options;
        this.paths = new PlannerDetails();
    }

    // ====================================================
    // region<Request>

    @Override
    public PDDLPlanRequestWithMemory getRequestWithMemory(WorkingMemory memory) {
        return new PDDLPlanRequestWithMemory(memory, getPlannerOptions().getDomain());
    }


    // endregion
    // ====================================================


    // ====================================================
    // region<worker related>

    @Override
    protected void runPlanner() {
		logger.debug("Running planner (timeout={}secs)...", pddlPlannerOptions.plannerTimeoutInSeconds.getValueAsInt());

        paths.recalculate();
        paths.deleteExistingPaths();
        paths.writePDDLFiles();

		List<String> command = paths.command();
		File outputFile = paths.outputFile();

		try {
            logger.debug("command {} \n  redirect out+err to {}", String.join("\n  ", command), outputFile);
            ProcessBuilder builder = new ProcessBuilder(command);
            builder.directory(paths.execPath.toFile());
            builder.redirectOutput(ProcessBuilder.Redirect.to(outputFile));
            builder.redirectErrorStream(true);

            Process process = builder.start();
			boolean cleanExit = process.waitFor(pddlPlannerOptions.plannerTimeoutInSeconds.getValueAsInt(), TimeUnit.SECONDS);

			BufferedReader outputReader = new BufferedReader(new InputStreamReader(process.getInputStream()));
			String line;
			while ((line = outputReader.readLine()) != null) {
				logger.debug("Uncaught planner output: {}", line);
			}

			if (! cleanExit) {
				logger.error("The planner did not exit cleanly within the timeout {}s; forcibly destroying the process.", pddlPlannerOptions.plannerTimeoutInSeconds);
				process.destroyForcibly();
			}
		} catch (IOException | InterruptedException e) {
		    logger.error("{}: {}", getShortName(), e);
		}
		logger.debug("planner finished.");

        parseOutputFile();
        if (pddlPlannerOptions.parseSolutionFile.isEnabledIgnoreDefault()) {
            parseSolutionFile();
        }
        postRunHook();
	}

    protected void postRunHook() {

    }

    protected void parseOutputFile() {
        String msg = String.format("Parsing output from %s...", paths.outputPath);
        logger.debug(msg);
        try {
            if (Files.exists(paths.outputPath)) {
                List<String> outputLines = Files.readAllLines(paths.outputPath);
                if (currentRequest.capturePlannerOutput()
                        || pddlPlannerOptions.capturePlanOutput.isEnabledIgnoreDefault()) {
                    currentRequest.addOutput(outputLines);
                }
                if (pddlPlannerOptions.parseSolutionFile.isDisabledIgnoreDefault()) {
                    parseForPlan(outputLines);
                }
            } else {
                logger.debug(".. file does not exist!");
                if (currentRequest.capturePlannerOutput()) {
                    msg +=  ".. file does not exist!";
                    currentRequest.addOutput(Collections.singletonList(msg));
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        logger.debug("Finished parsing ... {}", currentRequest.report());
    }

    protected void parseSolutionFile() {
        String msg = String.format("Parsing output from variants of %s...", paths.solutionPath);
        logger.debug(msg);
        try {
            Path curSolutionPath = null;
            DirectoryStream.Filter<Path> directoryFilter = path -> path.toString().contains(solutionFilePrefix);
            for (Path path : Files.newDirectoryStream(paths.outputRoot, directoryFilter)) {
                //noinspection ConstantConditions
                if (curSolutionPath == null
                        || curSolutionPath.toString().compareTo(path.toString()) > 0) {
                    curSolutionPath = path;
                }
                break;
            }
            if (curSolutionPath != null
                    && Files.exists(curSolutionPath)) {
                List<String> outputLines = Files.readAllLines(curSolutionPath);
                if (currentRequest.capturePlannerOutput()
                        || pddlPlannerOptions.capturePlanOutput.isEnabledIgnoreDefault()) {
                    currentRequest.addOutput(outputLines);
                }
                parseForPlan(outputLines);
            } else {
                logger.debug(".. file does not exist!");
                if (currentRequest.capturePlannerOutput()) {
                    msg +=  ".. file does not exist!";
                    currentRequest.addOutput(Collections.singletonList(msg));
                }
            }
        } catch (IOException e) {
            logger.error("got an exception " + e);
        }
        logger.debug("Finished parsing ... {}", currentRequest.report());
    }

    protected void parseForPlan(List<String> outputLines) {
        boolean solutionLines = false;
        String success = pddlPlannerOptions.pddlPlannerSuccess.getValue();
        currentRequest.resetPlan();
        for (String line : outputLines){
            logger.debug("plan-line("+ outputLines.indexOf(line) + "): {}", line);
            if (solutionLines) {
                currentRequest.addPlanLine(line);
            }
            if (line.contains(success)){
                solutionLines = true;
            }
        }
    }

    // endregion
    // ====================================================

    // ====================================================
    // region<Options>

    @Override
    public PDDLPlannerOptions getPlannerOptions() {
        return pddlPlannerOptions;
    }

    public static class PDDLPlannerOptions extends PlannerOptions {

        public static Builder<? extends PDDLPlannerOptions, ? extends Builder<?,?>> builder() {
            return new Builder<>(new PDDLPlannerOptions());
        }

        public PDDLPlannerOptions() {
            super();
            optionLists.add(experimentOptions);
            optionLists.add(pddlOptions);
        }

        @Override
        public String toString() {
            return "PlannerOptions{" +
                    experimentOptions +
                    ", \n  " + experimentDirectory +
                    ", \n  " + pddlOutputDir +
                    ", \n  " + pddlPlannerDir +
                    ", \n  " + pddlPlannerExec +
                    ", \n  " + pddlPlannerOptions +
                    ", \n  " + pddlPlannerSuccess +
                    ", \n  " + plannerTimeoutInSeconds +
                    '}';
        }

        public static class Builder<T extends PDDLPlannerOptions, B extends Builder<T,B>>
                extends PlannerWorkerBase.PlannerOptions.Builder<T, Builder<T, B>> {
            Builder(T instance) {
                super(instance);
            }
        }


        List<AbstractOption> experimentOptions = new ArrayList<>();
        protected SingleArgumentOption experimentDirectory
                = new SingleArgumentOption(
                experimentOptions,"expDir", "",
                "The data directory for experiment files.", "");

        List<AbstractOption> pddlOptions = new ArrayList<>();
        protected SingleArgumentOption pddlOutputDir
                = new SingleArgumentOption(
                pddlOptions,"pddlOutputDir", "",
                "The directory to run planners. Relative paths are" +
                        " allowed. 'expDir' is replaced by the value of that" +
                        " option. [default=cwd]", "");
        public SingleArgumentOption pddlPlannerDir
                = new SingleArgumentOption(
                pddlOptions,"pddlPlannerDir", "",
                "The directory containing the planner exec file(s). " +
                        "'expDir' is replaced by the value of that option. " +
                        "[default=cwd]", "");
        protected SingleArgumentOption pddlPlannerOptions
                = new SingleArgumentOption(
                pddlOptions,"pddlPlannerOptions", "DOMAIN PROBLEM SOL",
                "The planner options. If present, DOMAIN/PROBLEM/SOL" +
                        " will be replaced by the domain, problem, or solution" +
                        " filenames. [default='DOMAIN PROBLEM SOL']",
                "DOMAIN PROBLEM SOL");
        protected SingleArgumentOption pddlPlannerSuccess
                = new SingleArgumentOption(
                        pddlOptions,"pddlPlannerSuccess",
                ";;;; Solution Found",
                "The start of the line in the output if a" +
                        " solution was found [default=';;;; Solution Found']",
                ";;;; Solution Found");

        //required!
        protected SingleArgumentOption pddlPlannerExec
                = new SingleArgumentOption(
                pddlOptions,"pddlPlannerExec",
                "The planner command to run.");

        public BooleanOption parseSolutionFile
                = new BooleanOption(
                pddlOptions, "parseSolutionFile", DISABLED,
                "Whether to look for and parse the solution file when reading the planner result", DISABLED);

        public String getExperimentDirectory() {
            return experimentDirectory.getValue();
        }

    }

    // endregion
    // ====================================================

    // ====================================================
    // region<PlannerDetails>

    protected class PlannerDetails {
        Path outputRoot;
        Path domainPath;
        Path problemPath;
        Path solutionPath;
        Path outputPath;

        Path execPath;
        String execString;
        String[] execOptions;

        private void deleteExistingPaths() {
            try {
                if (Files.exists(domainPath)) {
                    Files.delete(domainPath);
                }
                if (Files.exists(problemPath)) {
                    Files.delete(problemPath);
                }
                if (Files.exists(solutionPath)) {
                    Files.delete(solutionPath);
                }
                if (Files.exists(outputPath)) {
                    Files.delete(outputPath);
                }
                deleteExistingSolutionFiles();
            } catch (IOException e) {
                logger.error("{}: {}", getShortName(), e);
            }
        }

        private void deleteExistingSolutionFiles() throws IOException {
            DirectoryStream.Filter<Path> directoryFilter = path -> path.toString().contains(solutionFilePrefix);
            for (Path path : Files.newDirectoryStream(paths.outputRoot, directoryFilter)) {
                Files.delete(path);
            }
        }

        private void recalculate() {
            String experimentDirectory = pddlPlannerOptions.getExperimentDirectory();

            String outputRootString = pddlPlannerOptions.pddlOutputDir.getValue();
            outputRootString = outputRootString.replace("expDir", experimentDirectory);
            if (outputRootString.equals("")) {
                outputRoot = Paths.get(System.getProperty("user.dir"));
            } else {
                outputRoot = Paths.get(outputRootString);
            }
            logger.debug("outputRoot: {}", outputRoot);

            String plannerPathValue = pddlPlannerOptions.pddlPlannerDir.getValue();
            plannerPathValue = plannerPathValue.replace("expDir", experimentDirectory);
            execPath = Paths.get(plannerPathValue);
            execString = pddlPlannerOptions.pddlPlannerExec.getValue();
            execOptions = pddlPlannerOptions.pddlPlannerOptions.getValue().split(" ");

            logger.debug(pddlPlannerOptions.toString());

            paths.domainPath = outputRoot.resolve(domainFilename);
            paths.problemPath = outputRoot.resolve(problemFilename);
            paths.solutionPath = outputRoot.resolve(solutionFilePrefix);
            paths.outputPath = outputRoot.resolve(outputFilename);
        }

        private void writePDDLFiles() {
            logger.debug("Writing PDDL files to directory {} ...", outputRoot);

            try {
                logger.debug("Writing domain file {}", domainPath);
                BufferedWriter domainWriter = domainWriter();
                domainWriter.write(currentRequest.domainBuffer.toString());
                domainWriter.close();

                logger.debug("Writing problem file {}", problemPath);
                BufferedWriter problemWriter = problemWriter();
                problemWriter.write(currentRequest.domainDebugBuffer.toString());
                problemWriter.write("");
                problemWriter.write(currentRequest.problemBuffer.toString());
                problemWriter.write("");
                problemWriter.write(currentRequest.problemDebugBuffer.toString());
                problemWriter.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
            logger.debug("finished writing PDDL files.");
        }


        public BufferedWriter domainWriter() throws IOException {
            return Files.newBufferedWriter(paths.domainPath, StandardOpenOption.CREATE);
        }

        public BufferedWriter problemWriter() throws IOException {
            return Files.newBufferedWriter(paths.problemPath, StandardOpenOption.CREATE);
        }

        public String planerExec() {
            return execPath.toString() + "/"+ execString;
        }

        public String solutionFilename() {
            return outputRoot.toString() + "/" + solutionFilePrefix;
        }

        public CharSequence domainFilename() {
            return domainPath.toString();
        }

        public CharSequence problemFilename() {
            return problemPath.toString();
        }

        public String outputFilename() {
            return outputPath.toString();
        }

        public List<String> command() {
            List<String> command = new ArrayList<>();
            command.add(planerExec());
            for (String option : execOptions) {
                String newOption
                        = option.replace("DOMAIN", domainFilename())
                                .replace("PROBLEM", problemFilename())
                                .replace("SOL", solutionFilename());
                command.add(newOption);
            }
            return command;
        }

        public File outputFile() {
            return new File(outputFilename());
        }
    }

    // endregion
    // ====================================================
}


