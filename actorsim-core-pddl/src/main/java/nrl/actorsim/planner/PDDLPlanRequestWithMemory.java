package nrl.actorsim.planner;

import nrl.actorsim.chronicle.Timepoint;
import nrl.actorsim.domain.*;
import nrl.actorsim.goalnetwork.GoalLifecycleNode;
import nrl.actorsim.memory.WorkingMemory;

import java.io.IOException;
import java.time.Duration;
import java.util.*;

import static nrl.actorsim.domain.WorldObject.NULL_WORLD_OBJECT;
import static nrl.actorsim.goalrefinement.lifecycle.GoalMode.*;
import static nrl.actorsim.utils.Utils.zipMap;

public class PDDLPlanRequestWithMemory extends PlanRequestWithMemory {

    public PDDLPlanRequestWithMemory(WorkingMemory memory, PDDLPlanningDomain pddlDomain) {
        super(memory, pddlDomain);
    }


    // ====================================================
    // region<Statements>

    @Override
    protected void convertPlanToPlanSteps() {
        if (planSteps.size() == 0) {
            planSteps = new ArrayList<>();
            for (String step : plan) {
                if (step.startsWith(";")) {
                    continue;
                }
                planSteps.add(createPlanStep(step));
            }
        }
    }

    /**
     * Convert string from planner into a plan step
     * that aligns with the WorkingMemory.
     *
     * @param stepString the string to convert
     * @return A PlanStep with objects and names bound to WorkingMemory
     */
    private PlanStep createPlanStep(String stepString) {
        // Example plan from POPF
        // ; Time 0.04
        // 0.000: (move_to_table right right-home table0)  [60.000]
        // 60.001: (pickup_cup cup0 right table0)  [10.000]
        // 70.002: (move_from_table right table0 scan)  [60.000]
        // 130.003: (scan_cup cup0 right scanner0 scan)  [100.000]
        // 230.004: (move_to_table right scan table0)  [60.000]
        // 290.005: (putdown_cup cup0 right table0)  [10.000]
        // 300.006: (move_from_table right table0 right-home)  [60.000]

        //noinspection RegExpRedundantEscape
        String cleaned = stepString.replaceAll("[\\(\\):\\[\\]]", " ");
        cleaned = cleaned.replaceAll("\\s+", " ").trim();
        String[] tokens =cleaned.split(" ");
        // 0.000 move_to_table right right-home table0  60.000
        double simulationStartTime = Double.parseDouble(tokens[0]);

        String operatorName = tokens[1];
        Operator operator = domain.findOperator(operatorName);

        List<WorldArgument> opArgs = operator.getArgs();
        List<WorldObject> stepObjects = new ArrayList<>();
        int lastTokenIndex = tokens.length - 1;
        for (int i = 2; i < lastTokenIndex; ++i) {
            String param = tokens[i];
            WorldObject worldObject = domain.getProblem().getObject(param, NULL_WORLD_OBJECT);
            if (worldObject == NULL_WORLD_OBJECT) {
                logger.error("Object named is not in problem for parameter {} from step {}", param, stepString);
            }
            stepObjects.add(worldObject);
        }

        Action action = operator.instance();
        for (Map.Entry<WorldArgument, WorldObject> entry : zipMap(opArgs, stepObjects).entrySet()) {
            action.bind(entry.getKey(), entry.getValue());
        }

        String durString = "PT"+ tokens[lastTokenIndex] + "S";
        Duration duration = Duration.parse(durString);

        //noinspection UnnecessaryLocalVariable
        PlanStep step = PlanStep.builder()
                .step(stepString)
                .action(action)
                .duration(duration)
                .start(simulationStartTime)
                .build();
        return step;
    }

    // endregion
    // ====================================================


    // ====================================================
    // region<Generate Problem PDDL>


    @Override
    public void generateDomain() {
        try {
            PDDLPlanningDomain domain = getDomain();
            readDomain(domain.getPddlOptions().domainPath);
        } catch (IOException e) {
            logger.error("Error occurred reading domain: ", e);
        }
    }

    @Override
    public void generateProblem() {
        openDefine();
        insertProblemName();
        insertDomainDetails();
        insertObjectsFromMemory();
        insertInitialState();
        insertGoals();
        closeDefine();
        logger.debug("Problem: {}", this.problemBuffer);
    }

    private void openDefine() {
        appendToProblem("(define");
    }

    private void closeDefine() {
        appendToProblem(") ;define");
    }

    private void insertProblemName() {
        appendToProblem("  (problem task)");
    }

    private void insertDomainDetails() {
        appendToProblem(String.format("  (:domain %s)", domain.getName()));
        appendToProblem("");
    }

    private void insertObjectsFromMemory() {
        appendToProblem("  (:objects");
        List<WorldObject> objects = new ArrayList<>(memory.getObjects());
        Collections.sort(objects);
        for (WorldObject object : objects) {
            String type = object.type();
            String name = object.getId();
            appendToProblem(String.format("    %s - %s", name, type));
        }
        appendToProblem("  ) ;objects");
        appendToProblem("");
    }

    private void insertInitialState() {
        appendToProblem("  (:init");
        List<Statement> init = new ArrayList<>(memory.getAddedStatements());
        Collections.sort(init);
        for (Statement statement : init) {
            if (statement.getStart() == Timepoint.START_OF_TIME) {
                appendToProblem(String.format("    %s", toPDDL(statement)));
            }
        }
        appendToProblem("  ) ;init");
        appendToProblem("");
    }

    private void insertGoals() {
        appendToProblem("  (:goal ");
        appendToProblem("    (and");
        Collection<Statement> statements;
        if (requestGoals != null) {
            statements = requestGoals;
        } else {
            statements = new ArrayList<>();
            for (GoalLifecycleNode node : memory.getGoalMemory().find(SELECTED, EXPANDING)) {
                Statement statement = node.getStored();
                if (considered.contains(statement)) {
                    logger.debug("Goal is already considered in another request");
                } else {
                    logger.debug("Inserting '{}'", statement);
                    statements.add(statement);
                }
            }
        }

        for (Statement statement : statements) {
            if (statement.getStart() == Timepoint.END_OF_TIME) {
                appendToProblem(String.format("      %s", toPDDL(statement)));
                considered.add(statement);
            }
        }
        appendToProblem("    ) ;and");
        appendToProblem("  ) ;goal");
        appendToProblem("");
    }

    private String toPDDL(Statement statement) {
        StateVariable sv = statement.getTarget();
        StringBuilder sb = new StringBuilder();
        sb.append("(");
        sb.append(sv.getName());
        String sep = " ";
        for (Binding binding : sv.findBindings()) {
            WorldObject obj = binding.getTarget().content();
            sb.append(sep);
            sb.append(obj.getId());
        }

        if (statement instanceof PersistenceStatement) {
            //noinspection rawtypes
            Object value = ((PersistenceStatement) statement).getValue();
            if (value instanceof WorldObject) {
                sb.append(sep);
                sb.append(((WorldObject)value).getId());
            }
        }
        sb.append(")");
        return sb.toString();
    }

    // endregion
    // ====================================================

}
