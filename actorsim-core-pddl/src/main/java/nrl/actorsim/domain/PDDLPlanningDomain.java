package nrl.actorsim.domain;

import fr.uga.pddl4j.parser.*;
import fr.uga.pddl4j.planners.ProblemFactory;
import org.apache.commons.lang3.NotImplementedException;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Field;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;

import static nrl.actorsim.domain.WorldObject.NULL_WORLD_OBJECT;
import static nrl.actorsim.domain.WorldType.BOOLEAN;


/**
 * Converts a PDDL (domain, problem) pair into a TGN PlanningDomain
 * using the PDDL4j library.
 *
 * PDDL Types are converted into {@link WorldType}s that match
 * the type hierarchy of the PDDL domain.
 *
 * Predicates are converted into {@link StateVariableTemplate}s
 * of two types.  The first converts a PDDL predicate directly to
 * a StateVariable, which works well for single-argument predicates
 * or for predicates that express a one-to-one relationship
 * between objects.  For example, the predicate
 * <code> available(?a - arm)</code> would convert
 * to the SVT <code>available(?a - arm) == BOOLEAN</code>
 *
 * The second type of construction allows for some
 * 'preprocessing' of state variables from predicates
 * to multi-valued state variables. A single parameter may begin
 * the string "value-" to indicate this special type of template.
 * The second converts an argument whose name starts with "value-" to
 * a {@link StateVariableTemplate} where the value object is the
 * type of that argument. For example, the predicate
 * <code>location(?r - robot ?value-d -dock)</code>
 * would convert to the SVT <code>location(ROBOT:r, DOCK:d) == BOOLEAN</code>.
 * The name of the variable will be passed through
 * with the "value-" prefix removed.
 *
 */
public class PDDLPlanningDomain extends PlanningDomain {
    final PDDLPlanningDomain.Options pddlOptions;
    Parser parser;

    // ============================================================
    // region<Constructor and Init>

    public PDDLPlanningDomain(Options options) {
        super(options);
        pddlOptions = options;
        if (options.loadPddlOnInit) {
            loadPDDL();
        }
    }

    public void loadPDDL() {
        Path domainPath = pddlOptions.domainPath;
        Path problemPath = pddlOptions.problemPath;
        logger.info("Parsing domain {}", domainPath);
        logger.info("Parsing problem {}", problemPath);
        final ProblemFactory factory = new ProblemFactory();

        try {
            File domainFile = domainPath.toFile();
            File problemFile = problemPath.toFile();
            factory.parse(domainFile, problemFile);
        } catch (IOException e) {
            logger.error("Error parsing domain file {} or problem file {}: {}",
                    domainPath, problemPath, e);
        }

        try {
            //a hack to get the parser!
            Field f = factory.getClass().getDeclaredField("parser");
            f.setAccessible(true);
            parser = (Parser) f.get(factory);
        } catch (NoSuchFieldException | IllegalAccessException e) {
            e.printStackTrace();
        }

        updateDomainName();
        addDomainTypes();
        addDomainConstants();
        addDomainPredicates();
        addOperators();

        updateProblemName();
        addInitialObjectsIfRequested();
        addProblemGoalsIfRequested();

    }

    // endregion  // pddl processing
    // ============================================================

    // ====================================================
    // region<PDDL Domain Processing>

    void updateDomainName() {
        options.updateName(parser.getDomain().getName().getImage());
    }


    void addDomainTypes() {
        List<TypedSymbol> pddlTypes = parser.getDomain().getTypes();
        for(TypedSymbol pddlType : pddlTypes) {
            String pddlImage = pddlType.getImage().toUpperCase();
            List<WorldType> worldTypeSupers = new ArrayList<>();
            for (Symbol pddlSuper : pddlType.getTypes()) {
                String superType = pddlSuper.getImage().toUpperCase();
                WorldType worldTypeSuper = new WorldType(superType);
                worldTypeSupers.add(worldTypeSuper);
            }
            WorldType newType = new WorldType(pddlImage, worldTypeSupers);
            logger.info("Added type {} with supertypes {}", newType, worldTypeSupers);
        }
    }

    void addDomainConstants() {
        List<TypedSymbol> pddlConstants = parser.getDomain().getConstants();
        getOrInsertConstantObjects(pddlConstants);
    }

    void addDomainPredicates() {
        List<NamedTypedList> pddlPredicates = parser.getDomain().getPredicates();
        for(NamedTypedList pddlPredicate : pddlPredicates) {
            Symbol pddlName = pddlPredicate.getName();
            List<TypedSymbol> pddlArgs = pddlPredicate.getArguments();

            Pair<WorldArgument, List<WorldArgument>> pair = convertPredicateArgs(pddlArgs);
            WorldArgument valueType = pair.getLeft();
            List<WorldArgument> args = pair.getRight();

            StateVariableTemplate svt = StateVariableTemplate.builder()
                    .name(pddlName.getImage())
                    .argTypes(args)
                    .valueTypes(valueType)
                    .build();
            add(svt);
        }
    }

    private void addOperators() {
        List<Op> ops = parser.getDomain().getOperators();
        for (Op op : ops) {
            String pddlName = op.getName().getImage();
            logger.info("Working on pddlOp:{}", pddlName);
            logger.trace(" pddlOp expression:{}", op);
            Operator operator = new Operator(pddlName);

            addArguments(operator, op);
            addPreconditions(operator, op);
            addEffects(operator, op);
            add(operator);
        }
    }

    private void addArguments(Operator operator, Op op) {
        List<WorldArgument> args = convertArgs(op.getParameters());
        operator.add(args);
    }

    private void addPreconditions(Operator operator, Op op) {
        Exp opPrecondition = op.getPreconditions();
        String connective = opPrecondition.getConnective().getImage();
        if (connective.equalsIgnoreCase("AND")) {
            for (Exp expression : opPrecondition.getChildren()) {
                operator.addPrecondition(convertToStatement(expression, operator));
            }
        } else {
            String msg = "Only conjunctive preconditions are supported.";
            logger.error(msg);
            throw new IllegalArgumentException(msg);
        }
    }

    private void addEffects(Operator operator, Op op) {
        Exp opEffects = op.getEffects();
        String connective = opEffects.getConnective().getImage();
        if (connective.equalsIgnoreCase("AND")) {
            for (Exp expression : opEffects.getChildren()) {
                operator.addEffect(convertToStatement(expression, operator));
            }
        } else {
            String msg = "Only conjunctive effects are supported.";
            logger.error(msg);
            throw new IllegalArgumentException(msg);
        }
    }

    private Statement convertToStatement(Exp expression, Operator operator) {
        assert(expression.getChildren().size() == 1);

        Exp filtered = expression.getChildren().get(0);
        boolean isNegatedExpression = false;
        if (filtered.getConnective().toString().equals("NOT")) {
            isNegatedExpression = true;
            assert(filtered.getChildren().size() == 1);
            filtered = filtered.getChildren().get(0);
        }

        List<Symbol> pddlExpressionList = new ArrayList<>(filtered.getAtom());
        String svtName = pddlExpressionList.remove(0).toString();
        StateVariableTemplate svt = getPredicate(svtName);

        List<WorldArgument> argList = new ArrayList<>();
        List<WorldArgument> valueList = new ArrayList<>();
        alignArgsFromOperator(svt, operator, pddlExpressionList, argList, valueList);

        //noinspection rawtypes
        StateVariable.Builder builder = svt.stateVariableBuilder()
                .cloneArgs()
                .specializeArgs(argList);
        if (valueList.size() > 0) {
            //noinspection unchecked
            builder.specializeValueTypes(valueList);
        }
        StateVariable sv = builder.buildWithoutBindings();

        WorldArgument valueArg = sv.valueTypes.get(0);
        Statement statement;
        if (isNegatedExpression) {
            statement = sv.negatedPersistenceStatement(valueArg.getClass());
        } else {
            statement = sv.persistenceStatement(valueArg.getClass());
        }

        logger.trace("created new persistence statement {} of class {}", statement.toString(), statement.getClass());
        logger.trace("statement's sv is {}", statement.getTarget().toString());
        return statement;
    }

    private void alignArgsFromOperator(StateVariableTemplate svt,
                                       Operator operator,
                                       List<Symbol> pddlExpressionList,
                                       List<WorldArgument> argList,
                                       List<WorldArgument> valueList) {
        for(Symbol pddlSymbol : pddlExpressionList) {
            String argName = sanitizeArgument(pddlSymbol.toString());
            WorldType argType = findArgumentType(argName, operator);
            WorldArgument newArg = new WorldArgument(argType, argName);
            if (argList.size() < svt.args.size()) {
                argList.add(newArg);
            } else {
                int numValues = valueList.size();
                WorldType svtValueType = svt.valueTypes.get(numValues);
                if(BOOLEAN.typeEquals(svtValueType)) {
                    valueList.add(svtValueType.asArgument());
                } else {
                    valueList.add(newArg);
                }
            }
        }
    }

    private String sanitizeArgument(String argName) {
        if (argName.startsWith("?")) {
            return argName.substring(1);
        }
        return argName;
    }

    private WorldType findArgumentType(String argName, Operator operator) {
        List<WorldArgument> argList = operator.args;
        for(WorldArgument arg : argList) {
            if (arg.getPathName().equals(argName)) {
                return arg;
            }
        }
        if (constantObjectMap.containsKey(argName)) {
            return constantObjectMap.get(argName);
        }

        String msg = String.format("argName %s is not in list %s", argName, argList);
        logger.error(msg);
        throw new IllegalArgumentException(msg);
    }

    // endregion
    // ====================================================

    // ====================================================
    // region<PDDL Problem Processing>
    void updateProblemName() {
        problem.options.updateName(parser.getProblem().getName().getImage());
    }

    private void addInitialObjectsIfRequested() {
        List<TypedSymbol> pddlInitialObjects = parser.getProblem().getObjects();
        getOrInsertObjects(pddlInitialObjects);
    }

    private void addProblemGoalsIfRequested() {
        if (pddlOptions.shouldAddGoalsDuringLoad) {
            Exp pddlProblemGoals = parser.getProblem().getGoal();
            logger.debug("{}", pddlProblemGoals);
            throw new NotImplementedException("Adding PDDL goals is not yet implemented");
        }
    }

    // endregion
    // ====================================================

    // ====================================================
    // region<PDDL Convert and Insert helper methods>

    private
    Pair<WorldArgument, List<WorldArgument>>
    convertPredicateArgs(List<TypedSymbol> pddlArguments) {
        List<WorldArgument> args = new ArrayList<>();
        WorldArgument valueType = BOOLEAN.asArgument();
        for (TypedSymbol pddlArg : pddlArguments) {
            if (pddlArg.getTypes().size() == 1) {
                String varTypename = pddlArg.getTypes().get(0).getImage();
                WorldType argType = TypeManager.get(varTypename);
                String varName = pddlArg.getImage().replace("?", "");
                if (varName.startsWith("value-")) {
                    valueType = new WorldArgument(argType, varName.replace("value-", ""));
                } else {
                    WorldArgument arg = new WorldArgument(argType, varName);
                    args.add(arg);
                }
            }
        }
        return ImmutablePair.of(valueType, args);
    }

    private
    List<WorldArgument> convertArgs(List<TypedSymbol> pddlArguments) {
        List<WorldArgument> args = new ArrayList<>();
        for (TypedSymbol pddlArg : pddlArguments) {
            if (pddlArg.getTypes().size() == 1) {
                String varTypename = pddlArg.getTypes().get(0).getImage();
                WorldType argType = TypeManager.get(varTypename);
                String varName = pddlArg.getImage().replace("?", "");
                WorldArgument arg = new WorldArgument(argType, varName);
                args.add(arg);
            }
        }
        return args;
    }


    /**
     * Returns a list of WorldObjects in this domain that match
     * the provided PDDL *constant* TypedSymbols.  A WorldObject corresponding
     * to a TypedSymbol that is missing from the domain is added.
     *
     * @param pddlSymbols the symbols to get or insert
     * @return a list of *constant* WorldObjects from the domain matching the provided pddlSymbols
     */
    @SuppressWarnings("UnusedReturnValue")
    private List<WorldObject> getOrInsertConstantObjects(List<TypedSymbol> pddlSymbols) {
        return getOrInsertObjects_impl(pddlSymbols, true);
    }

    /**
     * Returns a list of WorldObjects in this domain that match
     * the provided PDDL TypedSymbols.  A WorldObject corresponding
     * to a TypedSymbol that is missing from the domain is added.
     *
     * @param pddlSymbols the symbols to get or insert
     * @return a list of WorldObjects from the domain matching the provided pddlSymbols
     */
    @SuppressWarnings("UnusedReturnValue")
    private List<WorldObject> getOrInsertObjects(List<TypedSymbol> pddlSymbols) {
        return getOrInsertObjects_impl(pddlSymbols, false);
    }



    /**
     * Returns a list of WorldObjects in this domain that match
     * the provided PDDL TypedSymbols.  A WorldObject corresponding
     * to a TypedSymbol that is missing from the domain is added.
     *
     * @param pddlSymbols the symbols to get or insert
     * @return a list of WorldObjects from the domain matching the provided pddlSymbols
     */
    @SuppressWarnings("UnusedReturnValue")
        private List<WorldObject> getOrInsertObjects_impl(List<TypedSymbol> pddlSymbols, boolean objectsAreConstant) {
        List<WorldObject> objects = new ArrayList<>();
        for (TypedSymbol pddlSymbol : pddlSymbols) {
            WorldObject obj = getOrInsertObject(pddlSymbol, objectsAreConstant);
            objects.add(obj);
        }
        return objects;
    }

    private WorldObject getOrInsertObject(TypedSymbol symbol, boolean objectIsConstant) {
        String name = symbol.getImage();
        WorldObject obj;
        if (objectIsConstant) {
            obj = constantObjectMap.getOrDefault(name, NULL_WORLD_OBJECT);
        } else {
            obj = problem.getObject(name, NULL_WORLD_OBJECT);
        }
        if (obj == NULL_WORLD_OBJECT) {
            logger.debug("Attempting to create and insert new object '{}'", name);
            if (symbol.getTypes().size() != 1) {
                String message = "This method can only convert symbols with one type.";
                logger.error(message);
                throw new IllegalArgumentException(message);
            }
            String typename = symbol.getTypes().get(0).getImage();
            WorldType type = TypeManager.get(typename);
            obj = new WorldObject(type, name);
            if (objectIsConstant) {
                add(obj);
                logger.debug("Added constant WorldObject '{}'", obj);
            }
            if (problem.add(obj)) {
                logger.debug("Inserted WorldObject '{}'", obj);
            } else {
                logger.error("Failed to insert WorldObject '{}'", obj);
                obj = NULL_WORLD_OBJECT;
            }
        }
        return obj;
    }

    // endregion //convert and insert
    // ====================================================

    // ============================================================
    // region<Options>

    public Options getPddlOptions() {
        return pddlOptions;
    }

    public static class Options extends PlanningDomain.Options {
        public Path domainPath;
        public Path problemPath;
        boolean loadPddlOnInit = false;
        boolean shouldAddGoalsDuringLoad = false;

        public static
        Options.Builder<? extends Options, ? extends Options.Builder<?, ?> >
        builder() {
            return new Options.Builder<>(new Options());
        }


        public static class Builder<T extends Options, B extends Builder<T, B>>
                extends PlanningDomain.Options.Builder<T, B> {

            protected Builder(T options) {
                super(options);
            }

            public B domainFilename(Path domainPath) {
                instance.domainPath = domainPath;
                instance.name = "FROM_DOMAIN_FILE";
                return getThis();
            }

            public B problemFilename(Path problemPath) {
                instance.problemPath = problemPath;
                return getThis();
            }

            public B addProblemGoalsOnLoad() {
                instance.addGoalsDuringLoad();
                return getThis();
            }
        }

        public void addGoalsDuringLoad() {
            shouldAddGoalsDuringLoad = true;
        }
    }

    // endregion
    // ============================================================
}
