#!/bin/bash

#make sure we have at least one command argument
if [ $# -eq 0 ]; then
    echo "You need to specify a release name!"
    exit
fi

release_name=$1


start_dir=`pwd`
prefix="actorsim-"
suffix="-git"

cd ../../../
base_dir=`pwd`

release_dir=$base_dir/$release_name
echo "Bundling into release directory '$release_dir'"
if [ -d $release_dir ]
then
  echo "Release directory '$release_dir' already exists"
else
  mkdir "$base_dir/$release_name"
fi

git_dirs=`ls -d $prefix*$suffix`
echo "Releasing directories staring with '$prefix' and ending with '$suffix'"
echo "Git directories include '$git_dirs'"

echo "Working in directory " `pwd`

readme=$release_dir/README.md
{
  echo "This directory contains the tagged release of ActorSim $release_name"
  echo "Each module of ActorSim is stored in two formats"
  echo "  - A zip file with the working tree of the develop branch (without any git content)"
  echo "  - A git bundle of the develop branch"
  echo ""
  echo "There is also a set of compiled documents in docs-html.zip"
  echo "To get started:"
  echo "  - unzip docs-html.zip"
  echo "  - open html/index.html"
} > $readme || exit

for git_dir in $git_dirs
do
    echo "Switching to '$base_dir/$git_dir'"
    cd $base_dir/$git_dir || exit
    connector_name=`echo $git_dir | sed s/$suffix//`
    develop_bundle_dir=$release_dir/$connector_name-develop_git_bundle
    echo "Bundling directory `pwd`"
    echo "  .. bundling into '$develop_bundle_dir'"
    git checkout develop || exit
    git tag -f $release_name -m"ActorSim $release_name Release" || exit
    git push di2e $release_name --force
    develop_zip_name=$release_dir/$connector_name.zip
    git bundle create $develop_bundle_dir develop || exit
    cd ..
    echo "Zipping '$git_dir' from `pwd` into $develop_zip_name"
    zip -q --exclude="*.git*" -r $develop_zip_name $git_dir || exit

#    git tag $release_name
#    git bundle create ../$release_name/$git_dir-develop_git-bundle develop
#    git bundle create ../20190926_pub_release/20190926-actorsim-core-all-bundle --all
#    zip --exclude="*.git*" -r ../actorsim/20190926_pub_release/actorsim-roborescue-teamNRL-develop.zip team-nrl-git
done

echo Creating Documentation
cd $base_dir/actorsim-core-git/src/site || exit
source ~/virtualenv/malmo-latest/bin/activate
make html || exit
cd build
docs_zip_name=$release_dir/docs-html.zip
zip -q -r $docs_zip_name html || exit





