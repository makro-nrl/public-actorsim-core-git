# ActorSim's Common Prerequisites

Many ActorSim connectors rely on a common setup.
We describe how to install these here.

Not every connector requires everything listed in this document, 
and each connector will state its requirements.  
**Only install a prerequisite if it is listed in the connector's documentation.**


##### Common Issues
- I don't want to use Ubuntu16 or IntelliJ.  Can I use X instead?  (where X is some other IDE).
  - In short, not really.  We have such limited time as it is that 
    supporting one Platform/IDE is challenging enough. Ubuntu 16.04
    is in the middle of its lifecycle and fairly stable. IntelliJ 
    products support a vibrant ecosystem of plugins that will
    ease your transition.
  - However, we have recently switched to a Maven build structure, which should
    make it much easier for people to install in any IDE.  Just be aware, 
    we'll probably ask you to reproduce any problem in the standard setup first.
- [Add your problem and solution here using Markdown formatting] 



## (Strongly Recommended) Install Markdown Viewer for your browser

Web browsers may not properly display the code segments in this file, and 
you will likely observe formatting issues for many of the markdown primitives 
such as lists and code blocks.  There are several solutions:
1. Open the file as raw text.  If you are in bit bucket, click the `...` 
just above the file view on the right side.  Then click `Open raw`.   
1. install a markdown reader for your browser
1. Open the file inside of an IntelliJ product that has the markdown plugin 
installed.  (This option is only available if you have installed IntelliJ for 
another project.)


[comment]: ==================================================================
[comment]: ==================================================================
[comment]: ==================================================================
*****************************************************************************
## (Optional) System: Install Zsh
Although it is not required, Mak highly recommends the zsh environment with a variety
of excellent plugins, including oh-my-zsh.  If you're interested, 
check out Mak's development setup at
https://bitbucket.org/makro-nrl2/makro-toolbox/src/master/setup/install.md

[comment]: ==================================================================
[comment]: ==================================================================
[comment]: ==================================================================
*****************************************************************************
## System: Install Java OpenJDK 8.0


#### Ubuntu Setup
If you expect to be installing many varieties of Java, you may want to install
via SDKMan: https://sdkman.io/

Otherwise, simply follow the instructions for installing the latest version of
OpenJDK 8.0 on Ubuntu.  


#### Mac Setup
On a Mac, you can install this using homebrew:
```    
brew tap adoptopenjdk/openjdk
brew cask install adoptopenjdk8
```

If you get an error that says that the cask is unavailable an no cask with that name exists try 

```    
brew tap adoptopenjdk/openjdk 
brew cask install adoptopenjdk8
```

Then check to make sure that it is the right version:
```
java -version
```

If not, then go to <https://adoptopenjdk.net/archive.html?variant=openjdk8&jvmVariant=hotspot> and scroll down till you get to the 232-b09 version and install directly.

The version should be 1.8.0_ followed by a 3 digit number that is NOT 242. If not, 
let one of us know and we'll dig up the specific formula for it.


#### Common issues 
[[Place your common issues here]]



[comment]: ==================================================================
[comment]: ==================================================================
[comment]: ==================================================================
*****************************************************************************
## System: Install Python 3.6

#### Ubuntu Setup
For Python 3.6 on Ubuntu 20.04, do the following:

```
sudo add-apt-repository ppa:deadsnakes/ppa
sudo apt-get update
sudo apt-get install python3.6
```



For Python 3.6, on even older machines (Ubuntu 16.04) you may need to install the ppa
You also need to install numpy, image, and future.
 
```
sudo add-apt-repository ppa:jonathonf/python-3.6
sudo apt update
sudo apt install python3.6
sudo apt install python3-pip
pip3 install numpy image future
```

#### Mac Setup
On Macs, if you don't already have python3.6 installed, come talk to
one of us. Homebrew does not make it easy to install earlier versions
of python. 


[comment]: ==================================================================
[comment]: ==================================================================
[comment]: ==================================================================
*****************************************************************************
## Installing A Virtual Environment

Some connectors in ActorSim run from python scripts, 
but their requirements may vary.  For example, some 
connectors require Python 2.7 with specific versions 
of libraries, while others require Python 3+ with 
a different set of libraries.  Virtual environments
are an easy way to separate these multiple environments
while also not polluting your system version of python.

Further, IntelliJ IDEs will set up a default virtual 
environment that is difficult to work with on the command line.
We highly recommended installing and using virtual environments
(virtualenv) for _all_ python development.

If you haven't used virtualenv before, a good tutorial is available at:
https://www.bogotobogo.com/python/python_virtualenv_virtualenvwrapper.php
or https://virtualenvwrapper.readthedocs.io/en/latest/


[comment]: ==================================================================
[comment]: ==================================================================
[comment]: ==================================================================
*****************************************************************************
### Install and configure python's virtual environment

First, you must install virtualenv and virtualenvwrapper
in your base system.
```
sudo apt install virtualenv virtualenvwrapper
```

## Establish a common setup for ActorSim
To complete the common setup, please do the following.

### Setup your base virtualenv directory:
```
cd ~
mkdir virtualenv
```

### Update your shell configuration to load virtualenvwrapper
Add the following config to your shell configuration
(via the .profile, .bashrc, or .zshrc files).  For bash and ZSH, 
the following will add the correct aliases; adapt yours appropriately.

```
export WORKON_HOME=$HOME/virtualenv
source /usr/share/virtualenvwrapper/virtualenvwrapper.sh
```


[comment]: ==================================================================
[comment]: ==================================================================
[comment]: ==================================================================
*****************************************************************************
## Memcached

Some connectors use an in-memory storage utility called memcached: 
http://memcached.org/about
https://github.com/memcached/memcached/wiki


The server relies on memcached, so you will need to install it on your system:
```
sudo apt install memcached
```

Whenever you run examples that require memcached, start it in its own
window
```shell script
memcached -vv
```
where `-vv` causes memcached to report verbose details about connections
and requrests.


### Python Library
https://github.com/linsomniac/python-memcached/blob/master/tests/test_memcache.py

### Java Library

### Alternatives we considered
We also looked at [Redis](elasticache/redis-vs-memca).  A comparison chart is at:
https://aws.amazon.com/elasticache/redis-vs-memcached/

