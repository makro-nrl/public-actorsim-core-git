# Installing ActorSim

ActorSim core has been tested most extensively on Ubuntu 16.04LTS.  For the  
best support, we ask that you follow these installation instructions.  If
you are on a non-Ubuntu host, installing a virtual machine or using Windows
Subsystem for Linux is likely to be your best option.

For the most part, installing ActorSim is done as part of a connector that
links the ActorSim Core Library to a specific application.  
Please install the appropriate connector following that guidance, which 
will occasionally ask you to refer back to the core documentation. 

The [Duckling Tutorial](./tutorial-duckling.md) provides an overview of the Core 
Library for those interested in trying out ActorSim Core on a lightweight connector. 


## Installing IntelliJ IDEA

ActorSim relies on IntelliJ IDEA with the pycharm plugin.  
See [./INSTALL_IDEA.md](./INSTALL_IDEA.md) for detailed instructions.



## Linking to ActorSim
```eval_rst
.. note

   You only need to follow this  section if you are installing ActorSim
   for a new connector or debugging an existing connector.  Otherwise,
   follow the instructions for the appropriate connector.
```
