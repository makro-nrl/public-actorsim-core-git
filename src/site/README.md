# ActorSim Documentation

This directory contains the documentation for the ActorSim Core library.

The documentation is stored as a set of text files with the suffixes `.md` 
or `.rst` and can be viewed with *any* text editor.

These documents use [ReST](https://docutils.sourceforge.io/rst.html) (reStructuredText), 
and the [Sphinx](https://www.sphinx-doc.org/) documentation system. 
Sphinx allows the documentation to be compiled into other forms (PDFs, 
 webpages, XML, etc.) for easier viewing, browsing, or advanced parsing.  
 
However, you do **not** need to install Sphinx to view or edit these files.
Just review these [guidelines](./DocumentingCode.md) and start using them! 

## Release Notes
The latest release notes can be found in: [Release Notes](release-notes.md).

## Maven
ActorSim projects use [Maven](https://maven.apache.org/) for building, 
and Maven follows a [standard directory structure](https://maven.apache.org/guides/introduction/introduction-to-the-standard-directory-layout.html). 
It might take a while to get used to this structure, but once you learn it
you can navigate any ActorSim project (or any Maven project for that matter)!
 
## Using IntelliJ

The easiest way to use the documents is to open them inside of IntelliJ IDEA
directly because IDEA will show a preview pane of the document and displays
links properly in support of limited browsing.  However, you can also 
read them within a standard browser. 


# Getting Started with ActorSim

The public repositories for ActorSim located at: 
 
- ActorSim Core (note the username makro-nrl2)- https://bitbucket.org/makro-nrl2/actorsim-core/src/develop/
- ActorSim Malmo Connector - https://bitbucket.org/makro-nrl/actorsim-sim-malmo
- ActorSim RoboRescue (RR) Server - https://bitbucket.org/makro-nrl/actorsim-sim-roborescue-server
- ActorSim RoboRescue (RR) Client - https://bitbucket.org/makro-nrl/actorsim-sim-roborescue-team-nrl/
 
If you have access to a private NRL repository, please translate the
following links to the correct place in that repository.
 
## Install a connector
The best place to start is installing either the RoboRescue and Malmo code.  
The most up-to-date install instructions will usually be found in the 
specific connector you wish to install.
The links below will take you directly to the INSTALL documentation 
for the public repository of two common connectors:
- Malmo: https://bitbucket.org/makro-nrl/actorsim-sim-malmo/src/develop/actorsim-malmo_controller/src/site/INSTALL.md
- RoboRescue: https://bitbucket.org/makro-nrl/actorsim-sim-roborescue-team-nrl/src/develop/actorsim-sim-roborescue_centralized_dispatcher/src/main/site/INSTALL.md

In case of interest, there is a general set of instructions available in [INSTALL.md](./INSTALL.md).

## Building the documents

To gain full access to the reference library of ActorSim, developers are
*encouraged* to build the full documentation from sources using Sphinx.  
It's easier than you might think!  To create an HTML version of the docs, 
follow the instructions for [Building the Docs](./DocumentingCode.md#Building The Docs).
The documentation in build/html/index.html can then be viewed in a web browser.


## I'm new to Markdown, where do I start?

Check out the [guidelines](./DocumentingCode.md#Writing Documentation).  If you
need a quick primer, just open the source of this file to see commonly used
features.

