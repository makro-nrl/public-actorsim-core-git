#!/bin/bash

source ~/virtualenv/malmo-latest/bin/activate

# use -f to force or -u to update changed files
op=-f   #useful for a complete refresh under active development
#op=-u    #useful for lightweight changes to the javadoc comments

#make sure we have at least one command argument
if [ $# -eq 0 ]; then
    echo "Performing a full refresh"
else
    echo "Performing an update"
    op=-u
fi


start_dir=`pwd`

echo "Starting directory: $start_dir"

cd ../../
base_dir=`pwd`
prefix="$base_dir/actorsim-core"
src_dir="src/main/java"

declare -A modules
modules[base]="Base"
modules[cognitive]="Cognitive"
modules[goal_network]="Goal Network"
modules[goal_refinement]="Goal Refinement"
modules[pddl]="PDDL"

cd $start_dir || exit


for i in "${!modules[@]}"
do
  module=$i
  title="${modules[$i]} Module"
  input=$prefix-$module/$src_dir
  output="generated/$module-module"
  mkdir -p $output
  echo "========================================"
  echo ""
  echo ""
  echo "Creating javasphinx JavaDoc output for"
  echo "  module:'$module'"
  echo "  title:'$title'"
  echo "  input='$input'"
  echo "  output:'$output'"
  echo "========================================"
  javasphinx-apidoc $op --no-member-headers -o $output --title="$title" $input
done


#javasphinx-apidoc -o generated/base-module --title="Base Module" $prefix-base/$src_dir $op --no-member-headers

#javasphinx-apidoc -o generated/cognitive-module --title="Cognitive Module" $prefix-cognitive/$src_dir $op --no-member-headers
#
#javasphinx-apidoc -o generated/goal-network-module --title="Goal Network Module" $prefix-goal_network/$src_dir $op --no-member-headers
#
#javasphinx-apidoc -o generated/goal-refinement-module --title="Goal Refinement Module" $prefix-goal_refinement/$src_dir $op --no-member-headers
