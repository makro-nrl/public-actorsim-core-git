# The Cognitive Module



## The Cognitive Cycle of ActorSim

ActorSim's cogntive cycle is applied after each state update and consists of the following steps:

 - `updateWorkingMemory()` which transforms raw observations from the perception into working memory objects and predicates.
 - `for (iteration i : number of allowed iterations):`
   - `insertApplicableStrategies()` which determines the applicable strategies for this iteration 
   - `rankApplicableStrategies()` which ranks the applicable strategies
   - `applyStrategies()`, which attempts to apply the strategies

  

## Goal Orderings

Goals can be ordered with respect to each other by inserting a GoalOrdering into memory.  For example, the following 
goal orderings state that Ambulances should unbury agents before uburying civilians and that it should unbury civilians
before scouting.  

        goalMemory.add(new GoalOrdering(UnburyAgentGoal.class, BEFORE, UnburyCivilianGoal.class));
        goalMemory.add(new GoalOrdering(UnburyCivilianGoal.class, BEFORE, TransportCiviliansGoal.class));
        goalMemory.add(new GoalOrdering(TransportCiviliansGoal.class, BEFORE, ScoutGoal.class));

Any goal ordering also applies to applicable goals, so strategies are sorted according to these goal orderings 
during the rankStrategies() method of the cognitive cycle.