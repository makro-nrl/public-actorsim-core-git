# Writing PDDL Models

ActorSim uses planning models to represent knowledge about the world.
The most common modelling language is PDDL.


## Some important resources:

http://planning.domains/ 

https://www.icaps-conference.org/competitions/
