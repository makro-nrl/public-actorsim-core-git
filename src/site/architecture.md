# ActorSim Architecture

```eval_rst
.. comments::
   Do not remove! This is an 'empty' code block to initialize the recommonmark
   transformer to support inline, implicit java references within markdown files.
```

The *ActorSim Core Library* contains
a set of  modules that application clients extend. In addition to the Utilities
module, *utils*, which provides basic logging facilities, the other main modules
are listed next.  The Figure at the bottom of the page shows an overview of the
core modeuls and how some of its classes relate.  
 
  - The [*Base*](module-base.md) module (top box with white-background and red dots) provides
    base classes for planning domain objects,
    the ``` :java:ref:`StateProperty` ``` base class hierarchy for tracking
    object attributes and relationships, the ``` :java:ref:`Statement` ``` 
    hierarchy that supports numeric planning,  and a suite of classes that
    support temporal planning.

  - The [*Goal Network*](module-goal-network.md) module  (middle box with green-background and white dots) 
    provides classes for GoalNetworks.  Three base 
    classes, ```:java:ref:`SimpleNetwork` ``` / ```:java:ref:`SimpleNode` ```
    and ```:java:ref:`GoalNetworkBase` ```, provide the main 
    functionality across the module, while convenience classes, namely
    ```:java:ref:`GoalNetwork` ``` / ```:java:ref:`GoalNetworkNode` ``` and
    ```:java:ref:`TemporalGoalNetwork` ``` / ```:java:ref:`TemporalGoalNetworkNode` ```, 
    provide the concrete implementations that clients will use.

  - The [Goal Refinement](module-goal-refinement.md) module (lower middle box with 
    a light blue background) provides classes for the Goal Lifecycle, 
    which include goal modes (e.g., formulated, selected, expanded) and goal 
    strategies (e.g., formulate, select, expand).  This module also provides the
    main base class, ```:java:ref:`BaseGoal` ``` used by clients to create goals.
    
  - The [Planner Module](module-planner.md) module provides classes for connecting
    to planning systems in a general manner.
    
  - The [PDDL Module](module-pddl.md) module provides classes that extend the 
    planner module to connect to PDDL planners.  Submodules within in this module
    connect to specific planners such as Temporal Fast Downward or POPF.

  - The [*Cognitive*](module-cognitive.md) module (lower left box in light gray) provides the classes 
     used during execution to progress goals.  This module includes
     the ```:java:ref:`FactMemory` ```, which holds objects, the
     ```:java:ref:`GoalMemory` ```, which holds the current goals of the system, 
     the ```:java:ref:`WorkingMemory` ```, which holds facts and goals, and 
     the ```:java:ref:`CognitiveCycleWorker` ```, which is the
     main process, i.e., the Goal Reasoner, that progresses goals during execution.
     

Finally, an *Application* module (bottom right in dark gray) provides the
application-specific classes that run ActorSim in an environment.  
This module will be largely rewritten for each new application
(e.g., Malmo, RoboRescue) and will be the code that connects
the core classes to the agent(s) for the environment. 


![text](_images/ActorSim4-classes.png).  

```eval_rst

.. toctree::
   :maxdepth: 1
   :caption: Contents:

   module-base
   module-goal-network
   module-goal-refinement
   module-cognitive
```