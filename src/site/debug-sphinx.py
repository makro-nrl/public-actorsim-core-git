from sphinx.cmd.build import main

"""A method to run/debug sphinx build directly from within IntelliJ.
   To use this, you will need to set up a run configuration for this file
   run from this directory using the parameters "-M html . build"
"""

main()