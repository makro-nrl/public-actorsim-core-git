```eval_rst
.. comments::
   Do not remove! This is an 'empty' code block to initialize the recommonmark
   transformer to support inline, implicit java references within markdown files.
```

# The Base Module


## State Properties


## Statements

A ```:java:ref:`Statement` ``` is a generic mechanism for specifying a goal. 
All goals in ActorSim extend the ```:java:ref:`Statement` ``` class, which
specifies what must be true in order to achieve or maintain the goal.  

As shown in the [class overview](./_images/ActorSim4-classes.png), there are 
several subclasses of  ```:java:ref:`Statement` ``` that can be used to 
express values or change.  The most straightforward is a 
```:java:ref:`PersistenceStatement` ``` such as `moving(robot1) == true`, which 
states that `robot1` has the property `moving`.  Another example is
`location(robot1) = dock1`, which states the `location` of `robot1` is `dock1`.

A ```:java:ref:`Statement` ``` specifies a temporal interval `[start, end]` 
over which it holds, where `start` and `end` are typically integers. 
For example `[start, end] moving(robot1) == true` states `robot1` 
was `moving` from `start` to `end`.
If unstated, then the default interval, `[-INF, +INF]`, is assumed.


### Predicate Statements

A ```:java:ref:`PredicateStatement` ``` is a special kind of ```:java:ref:`BooleanPersistenceStatement` ``` 
convenient for representing boolean statements that commonly appear in languages
like PDDL.
For example, the ```:java:ref:`PersistentStatement` 
```  `exists(item1) == true` or `location(robot1) = dock1`
can be simplified to the ```:java:ref:`PredicateStatement` ``` to `exists(item1)`
or `location(robot1, dock1)`.  


### Transition Statements




## Planning Domain and PlanningProblem

TODO Describe the following elements 
  - [ ] Type, WorldType, WorldObject, WorldArgument
  - [ ] StateProperty and subclasses
  - [ ] StateProperty builders
  - [ ] Bindings
  - [ ] Operator and Action
  - [ ] PlanningDomain and PlanningProblem 
  - [ ] NamedPath
