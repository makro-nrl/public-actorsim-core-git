"""Docutils CommonMark parser"""

__version__ = '0.6.0'



def setup(app):
    """Initialize Sphinx extension."""
    import sphinx
    from custom_recommonmark.custom_parser import ActorSimParser

    if sphinx.version_info >= (1, 8):
        app.add_source_suffix('.md', 'markdown')
        app.add_source_parser(ActorSimParser)
    elif sphinx.version_info >= (1, 4):
        app.add_source_parser('.md', ActorSimParser)

    return {'version': __version__, 'parallel_read_safe': True}
