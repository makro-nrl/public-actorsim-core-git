
Welcome to ActorSim!
=====================================

ActorSim is a library that helps application developers embed goal reasoning
in their system. *ActorSim Core* implements foundational temporal, numeric planning
primitives in support of several automated planning systems.
It also adds facilities to perform goal refinment, goal reasoning, and
a cognitive architecture that supports building autonomous agents.

Goals unify planning and acting.
If a goal is an objective an actor wishes to achieve or maintain,
then planning is deliberating on what action(s) best accomplish the objective
and acting is deliberating on how to perform each action of a plan.
Goal reasoning is deciding what goal(s) to pursue given trade-offs in dynamic,
possibly adversarial, environments, and it enables more responsive autonomy.
Researchers in Code 5514 have examined numerous goal reasoning topics.
ActorSim resulted from a need for a general platform for further study.
Although goal reasoning has strong ties to the planning, acting,
and robotics communities, it is an understudied area of research due to the lack
of publicly available language and implementation.
ActorSim addresses this gap as a software platform for studying goal reasoning
in simulated environments.

Getting Started
---------------
Most folks will start by installing one of the :doc:`./connectors` or reviewing
the :doc:`./architecture`.

To create your own connector, start with :doc:`tutorial-duckling` or install and
copy one of the existing :doc:`./connectors`.

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   INSTALL
   notation
   architecture
   tutorial-duckling

   CodeStyle
   DocumentingCode
   BranchNaming

   connectors

   api-reference

   release-notes

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

