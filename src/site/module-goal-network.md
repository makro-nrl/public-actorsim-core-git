# The Goal Network Module

```eval_rst
.. comments  
   Do not remove! This is an 'empty' code block to initialize the recommonmark
   transformer to support inline, implicit java references within markdown files.
```

A goal network is a graph data structure to hold a (possibly empty) set of goal nodes.
goal networks are used by clients within and outside of ActorSim to store and progress
goals.
Individual goals are stored in a *goal node*, which stores the goal objective
 as well as related information such as the goal's mode or execution status.
A goal network  aggregates a set of related goal nodes along with temporal constraints
and supergoal or subgoal relationships.
There is a family of goal nodes and networks which vary in the specifics of
what they track about each goal. 
Different agents or systems may then store and use these goal network variants. 
Let us start with some basic definitions.


## Statements, Goals, and Temporal Intervals

Goals in ActorSim are of two varieties: an *achievement* goal, also called 
an *attainment* goal, is an objective to attain, for example an agent may wish 
to be at a location.  A *maintenance* goal is an objective an agent wants to sustain.
For example, a robot may need its battery to stay above a certain charge.  All goals
have a notion of temporal extent which states a time interval over which 
the goal must hold.  

 [working link](./module-base)

[broken link](./module-base#statements)

All goals in ActorSim are [statements](./module-base#statements), which  
means that all goals eventually inherit the ```:java:ref:`Statement` ``` class. 

  

## Goal Networks and Goal Nodes

The [class overview](./_images/ActorSim4-classes.png) (left middle) shows the
family of Goal Networks and Goal Nodes available in ActorSim.  All networks rely
on the [JGraphT](https://jgrapht.org/) library.  

**Base classes: SimpleNetwork, SimpleNode, and SimpleEdge**
All networks derive from the base network class ```:java:ref:`SimpleNetwork` ```, 
which uses the directed acyclic graph from the [JGraphT](https://jgrapht.org/) library.  
This base class defines the primary operations for nodes, e.g., add, remove, as 
well as convenience methods for printing and debugging networks, nodes.  
A ```:java:ref:`SimpleNetwork` ``` can store any Java type within its nodes, so it
is not specific to goal networks.

To clarify these relationships, we will use some formal language. 
Let a simple network `$N = (id, G=(V, E))$` be a tuple where `$id$` is a numeric
integer that uniquely identifies `$N$` and where `G = (V, E)` is a directed
acyclic graph with a set of vertices `$V$` and a set of edges `$E$`.  

A vertex `$v \in V$` is a subclass of ```:java:ref:`SimpleNode` ```<T>, where the 
generic parameter `T` is the type of the Java object that can be stored.  

An edge `$e \in E$` is a subclass of ```:java:ref:`SimpleEdge` ```, which extends
the *DefaultEdge* type from JGraphT and includes whether an edge is a 
parent-child edge and the ordering operator that applies, in terms of
 ```:java:ref:`AllenIntervalOperator` ```.

**GoalNetworkBase and GoalNetworkNode**. 
The ```:java:ref:`GoalNetwork` ``` extends a ```:java:ref:`SimpleNetwork` ```
by requiring that the vertices hold subclasses of ```:java:ref:`Statement` ```. 
Vertices in a goal network are subclasses of ```:java:ref:`GoalNetworkNode` ```,
 and the class definition binds the generic parameter `<T>` to extend
```:java:ref:`Statement` ```.

**GoalLifecycleNetwork**.  
The [goal lifecycle](./) 



## The ExecutionStatement

The ```:java:ref:`ExecutionStatement` ``` is a special kind of  ```:java:ref:`TransitionStatement` ```
that tracks the execution of a target ```:java:ref:`Action` ```. 
The ```:java:ref:`ExecutionStatement` ``` is associated within 
a ```:java:ref:`GoalNewtork` ``` because the execution of an
action is associated so clearly with a plan, i.e., a sequence 
of actions to achieve some goal.  




### TemporalGoalNetworks and TemporalGoalLifecycleNetworks

TODO describe temporal goal networks
