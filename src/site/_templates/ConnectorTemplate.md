# The CONNECTOR_NAME Connector

This is a template file that is copied in for connectors that are not 
installed or configured in the docs/conf.py. 

If you desire to include one or more connectors in your documentation build,
please update your local copy (but don't commit it!) of the script
copy-connectors.sh.  It needs to point to the correct location of the 
home directory for the connector you want included.

