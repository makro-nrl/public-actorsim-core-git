# Building ActorSim's Documentation

Developers only need to install or run sphinx to build the latest
version of the documentation.  There are several extensions we use
to support integration across the ActorSim's code modules:
- `sphinx 2.4.4` is needed because some packages below are older
- `javasphinx` supports linking to java code.
- `recommonmark` supports writing documentation in CommonMark, however, we provide
  a custom version of this library in `custom_recommonmark` that supports
  features described below
- `sphinx_markdown_tables` supports simple markdown tables, which are not 
  supported in the CommonMark standard.

To build the latest version of documents:
- start your python virtual environment, ensuring you have python >=3.6  
- install sphinx and javasphinx
  ```
  workon <your-virtualenv>
  pip install sphinx==2.4.4 javasphinx sphinx_markdown_tables 
  ```
- change to this docs folder 
- run `make html` 

****************************
## Building documents inside IntelliJ

For even more accelerated documentation development, you can create a run
configuration to build the scripts from IntelliJ IDEA:
- Install the appropriate IDEA plugins:
  - `python` allows you to run python inside of IDEA; in fact this plugin has
    nearly the same feature set as Pychaorm!
  - `reSTructuredText` adds the DocUtils toolkit to IDEA.

- Create a shell script Tool to run build-java-rst.sh:
  ![ShellScript](_images/IntelliJ-ShellScriptTool-JavasphinxScript.png)

- To copy the connectors, you will also need to create a shell script too
  to run copy-connectors.sh:
  ![DocUtilsConfig](_images/IntelliJ-ShellScriptTool-CopyConnectorst.png)

- Create a Sphinx Task (from Python docs)  to run sphinx:
  ![DocUtilsConfig](_images/IntelliJ-DocUtils-SphinxConfiguration.png)

Furter details can be found at:
- <https://intellij-support.jetbrains.com/hc/en-us/community/posts/115000784044-Sphinx-with-PyCharm> 


****************************
## Learning reSTructuredText

Most of the time, you can just create a Markdown file to document new features. 
But if you want to modify the look or contents of how the documentation is built
then you will need to learn reSTructuredText.  A helpful resource in this 
regard are the [quickref](https://docutils.sourceforge.io/docs/user/rst/quickref.html)
and [directives list](https://docutils.sourceforge.io/docs/ref/rst/directives.html)
from the [docutils documentation](https://docutils.sourceforge.io/docs/).

****************************
## Additional resources

To understand the details about this setup, the following resources may be helpful:
- <http://www.sphinx-doc.org/en/master/index.html>
- <https://github.com/PharkMillups/beautiful-docs>

The whole point of using Markdown is to lower the entry barrier to writing
documentation.  Learning reStructuredText has a somewhat steep learning curve
that is higher than Markdown but less steep than LaTeX.  It's a great scripting 
language for encapsulating everything.  But most developers will simply not 
write documents than learn the strange syntax of reSTructuredText. Still,
some people disagree.  For some counter-arguments to using Markdown, see:
- <https://buttondown.email/hillelwayne/archive/please-dont-write-your-documentation-in-markdown/>
- <https://news.ycombinator.com/item?id=11292280>


### Including javasphinx

Simply:
- install javasphinx in your virtual environment
- ensure the sphinx configuration includes:
```
extensions = ['javasphinx']

javadoc_url_map = {
    'nrl.actorsim' : ('../src/base/src/', 'javadoc8')
}
```
- run javasphinx-apidoc with the appropriate flags. 
  See build-java-rst.sh for examples.

Further details at:
- <https://github.com/bronto/javasphinx>
- <https://bronto-javasphinx.readthedocs.io/en/latest/>
- <http://lukefrisken.com/code/sphinx-for-documenting-java-projects/p>
- <https://stackoverflow.com/questions/14254527/python-parsing-javadoc-with-python-sphinx>

It may be worthwhile at some future point to fork and extend javasphinx or
have sphinx parse the java files directly with something like 
<https://github.com/c2nes/javalang>.


### Including recommonmark

We use a slightly modified version of the recommonmark transformer called
`RevisedAutoStructify` and implemented in `transform.py`.  Our revision
supports referencing java references from javasphinx within markdown using
the implicit, inline code literal described above.  The relevant lines from
 the conf.py include, from the top:
```python
from transform import RevisedAutoStructify
```
and the bottom:
```python
def setup(app):
    app.add_config_value('recommonmark_config', {
            #'url_resolver': lambda url: github_doc_root + url,
            'enable_eval_rst': True,
            'enable_auto_toc_tree': True,
            #'auto_toc_tree_section': 'Contents'
            }, True)

    app.add_transform(RevisedAutoStructify)
```

### Including Markdown files

Follow the instructions from:
- <https://recommonmark.readthedocs.io/en/latest/>
- <https://recommonmark.readthedocs.io/en/latest/auto_structify.html#configuring-autostructify>
