# The Goal Refinement Module
```eval_rst
.. comments 
   Do not remove! This is an 'empty' code block to initialize the recommonmark
   transformer to support inline, implicit java references within markdown files.
```

Many connectors will rely on the GoalRefinement library from ActorSim. 
The relevant components of the goal refinement library include:

  - The *Goal Lifecycle*, described in detail below, defines goal progression.
    
n  - The ``` :java:ref:`GoalNetworkNode` ``` class which stores goals inside of 
    a ``` :java:ref:`GoalNetwork` ``` and define how a network of related goal
    nodes are stored. 

  - ``` :java:ref:`GoalLifecycleNode` ``` extends ``` :java:ref:`GoalNetworkNode` ```
   and links the goal node to the goal lifecycle.    
   
  - ``` :java:ref:`GoalLifecycleNetwork` ``` extends ``` :java:ref:`GoalNetwork` ```
   to store ``` :java:ref:`GoalLifecycleNode` ```s. 

  - The **ApplyStrategyTemplate** base class, which defines an entry point
    for all goal refinement strategies.

  - The **GoalMemory**, which stores and tracks goals.  

  - The **WorkingMemory**, which stores and tracks world state as well as contains the GoalMemory.  For simplicity,
    we will often use the term "memory" for both the GoalMemory and WorkingMemory.  Since the WorkingMemory contains
    the GoalMemory, it should be clear from the context whether we are discussing the WorkingMemory, which contains
    world state and has an instance of the GoalMemory, or we are discussion the GoalMemory, which contains only goals.

  - The **CognitiveCycleWorker**, which updates the WorkingMemory from incoming state updates, determines which 
  strategies apply based on the current memory, ranks strategies, and attempts to apply those strategies.
  
  
## The Goal Lifecycle

The goal lifecycle captures the allowed transitions for goals. 
Goals transition between _modes_ through the application of refinement  _strategies_.

We assume the reader is already familiar with the goal lifecycle and only
provide a brief overview of the goal lifecycle.  For a formal treatment 
of the goal lifecycle see [(Roberts et al., 2016)](http://makro.ink/publications/robertsEtAl16.acs.goalReasoningPlanningAndActing.pdf). 
The figure and table below summarize the full goal lifecycle.  

Recall that goals progress, i.e., transition in seven steps from their creation
to completion: Formulate, Select, Expand, Commit, Dispatch, Evaluate, Finish.  
There are other strategies that are beyond the scope of this explanation, but these
form the key decision points for progressing goals.  Thus, we must design 
and implement a strategy for each of these stages for each goal in the system.  

<img src="./_images/gr-lifecycle-full.png" alt="drawing" width="400"/ class="center">

![Full Goal Lifecycle](_images/gr-lifecycle-full.png)



| Strategy (ShortName) | Resulting Mode (ShortName) | Description |
| :------------------- | :------------------------- | :---------- |
| `FORMULATE` (`FORM`) | `FORMULATED`(`FORMED`) | Places g in the ``` :java:ref:`GoalMemorySimple` ```. |
| `SELECT` (`SEL`) | `SELECTED`(`SELD`) | Selects g.|

TODO finish these strategy descriptions!

## Writing Goal Strategies

In the worst case, we would need to implement the cross-product of these seven strategies for every goal type in the system.
In practice, however, we can implement many fewer strategies by:
1. leveraging a goal taxonomy, 
2. carefully designing goals that perform similar functions to leverage the taxonomy,
3. writing generic strategies for similar goals, and 
4. using default strategies from the goal refinement library whenever possible.

Goals in ActorSim are first class objects.  All goals in the system inherit from
```:java:ref`GoalBase` ```, which defines the core behavior for how goals
are formulated into memory and when a mode progression is allowed. For  
example, a strategy may attempt an invalid transition, 
and it is the ```:java:ref`GoalBase` ``` that would 
disallow such a transition.


### ApplyStrategyTemplate:  The interface for all Goal Strategies

Strategies determine for themselves when they are applicable and what takes place if they are applied to working memory.
Each individual strategy must implement the ApplyStrategyTemplate interface, which defines the methods that help the 
CognitiveCycleWorker determine whether a strategy is applicable and what to do when it is applied.  

If the strategy is applicable, it will always be with espect to one or more goals in memory. Applicable goals
will be stored in a class called ApplyStrategyDetails.  

To determine whether a strategy is applicable, the CognitiveCycleWorker uses the following standard methods, which have
default implementations but can be overriden for specialized behavior:

  - **checkPreconditions(GoalMemory memory, ApplyStrategyDetails applyDetails)**, which determines whether the preconditions
    of this strategy match the contents of working memory.  The default
    implementation of this method calls insertMatchingGoals(GoalMemory memory, ApplyStrategyDetails applyDetails)
    to perform its work
  
  - **insertMatchingGoals(GoalMemory memory, ApplyStrategyDetails applyDetails)**, which performs the actual work 
    of checking memory for applicable goals.  

  - **matchesGoal(Goal goal)**, which determines whether this strategy matches (i.e., is applicable) the provided goal.
  
Often, subclasses will need to provide specialized behavior that is similar but distinct from these standard methods.
For this reason, an additional method allows a subclass to provide a single Predicate<Goal> test to filter applicable 
goals.

  - **insertMatchingGoals(GoalMemory memory, ApplyStrategyDetails applyDetails, Predicate<Goal> test)**, 
    which checks memory for goals that not only succeed at matchesGoal(goal) but also pass the provided test.

In order to call this specialized form, a subclass must also override checkPrecontions() and provide the test.
Examples of this specialized use are found in the RRStrategies class.

Once a strategy has inserted applicable goals into the ApplyStrategyDetails, the CognitiveCycleWorker __may__ attempt to
apply the strategy.  Just because a strategy states it is applicable does not mean it is automatically applied.  For 
example, a strategy may be preempted by a GoalOrdering or there may not be resource available for applying it in the 
current cognitive cycle.
