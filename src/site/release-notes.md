# ActorSim Release Notes

## Actorsim 2021.09 (external point release)

Improvements for ActorSim 2021.09 include

- **ActorSim Core**
  - Finalized draft of new bindings class
  - Improved processing of GoalMode transitions
  - Several small improvements to core classes 

- **ActorSim Memcached** a **NEW** module for external interaction
  - Draft of Module for interacting with [Memcached](https://memcached.org/) 
    for commanding executives, requesting plans from GTPyhop,
    and accessing WorkingMemory (this is how the GUI module obtains its information).
  
- **ActorSim Planning** introduced three **NEW** planner integrations 
  - Integration with [Temporal Fast Downward](http://gki.informatik.uni-freiburg.de/tools/tfd/) 
  - Integration with [OptaPlanner](https://www.optaplanner.org/)
  - Draft of linkage to [GTPyhop](https://github.com/dananau/GTPyhop) through 
    integration with the memcached module.

- **ActorSim Hubo**
  - Completion of demo and publication at FLAIRS-21

- **ActorSim CrazySwarm**
  - Completion of demo and publication at FLAIRS-21

- **ActorSim RoboRescue**
  - Completion of demo and publication at FLAIRS-21

- **ActorSim SimpleSat**
  - Draft of a simple satellite scenario for testing plan repair.
  - Uses new Optaplanner module.

- **ActorSim Minecraft (formerly Malmo)**
  - Rewrote a new controller that uses [FabricMC](https://fabricmc.net/) 
    to interface directly with game instead of going through Malmo, which by now is quite
    stale and based on a very old version of Minecraft.
  - Uses the [Baritone](https://github.com/cabaletta/baritone) Mod to control
    character movement and mining in actual Minecraft worlds.
  - The new controller integrates with the memcached module
    to support controlling the game from a interactive python
    prompt for a variety of commands.
  - The new controller has a draft integration with the GTPyhop planner
    that will be filled in with plan decompositions.
  
*******************************************************
## ActorSim 2020.08 (external point release)

Improvements for ActorSim 2020.08 include: 

- **ActorSim Core**
  - Refactored documents to use maven directory structure
  - Clarified use of StateProperty and Statement classes
  - Drafted new bindings class 
    (in testing, see working branch ACTORSIM-612)

- **ActorSim Planning**
  - link to POPF planner completed; 
    TGNs can now write and read temporal plans

- **ActorSim RoboRescue**
  - Added a Drop strategy

- **ActorSim Hubo**
  - Draft of Hubo planning domain and goal strategies completed

- **ActorSim Malmo**
  - Alex can now dig pits 
    (in testing, see working branch ACTORSIM-372)
  - Added features to the malmo build
    (in testing, see working branches in malmo-build repository)
  
- **ActorSim UI**
  - Designed initial sketch of UI components 

*******************************************************
## Actorsim 2020.06 (internal release - no new features)

CodeName: BuildIt Release

This was a successful internal release push despite the view that many of the "Must" epic tickets remain open at the end of this release and will stay open as "must" for the next release.

Key deliverables include:

- External, public release of 2020.05 that extends 2020.03 with many bug fixes and documents to support the interns
- General: closed some outdated or unnecessary tickets
- Build 
  - Move to a maven build structure for all modules
  - Removed jar files from core library; they are now referenced via maven
  - Improved install documentation for three modules (RoboRescue, Malmo, and Hubo)
  - Moved to using a unified IDE setup
  - Huge push on improving documentation back to using markdown, sphinx, and plantuml
- Malmo code
  - Revived the MongoDB memory setup
  - Resolved issues with building MS Research's malmo code that will enable future changes
- RoboRescue
  - Consolidated non-Maven jar libraries and created a way to install them locally
  - Several small bug fixes
- Hubo
  - Revived PDDL code from old Minecraft Connector; built a new POPF Planner connector
  - Completed TGN strategies for formulating, selecting, and expanding Hubo scan goals
  - Drafted TGN commit strategy; still TBD
- UI
  - Researched possible directions
  - Defined direction and scope of the summer UI effort
  - Drafted initial component design for using Spring+ReactWeb
  - Wrote guidance for summer interns

Because these changes are cosmetic and enable no new features, we released
many of these changes as part of the 2020.05 public release. 

Much of this effort reorganizes ActorSim and updates the documents
to enable easier integration with other projects going forward. 
Specifically, this involved:
  - moving entirely to IntelliJ IDEA with a Python plugin
    (we previously used PyCharm for some projects)

  - renaming repositories (see below),

  - migrating the key repositories to a Maven build directory structure, 

  - refactoring some code so to better support the Maven build, and

  - updating the appropriate README and docs to reflect these changes.

For the most part, these are breaking changes with the existing builds and IDE setup 
you may have.  I apologize for this pain!! Hopefully you'll agree it's 
much easier to install now!  What you need to do:   

  - update your git remotes to the new repository names or clone a new copy 

  - follow the revised instructions for installing your desired repository 

Repository naming has switched to the following.  Note that not every 
repository is on every server; for example external-to-nrl repositories
may have only a subset of these.   An asterisk (*) indicates a repository 
that has moved to the Maven architecture and will require reimporting.
- *actorsim-core

- *actorsim-planner-shop2  (currently used in Malmo project)

- *actorsim-sim-malmo

- *actorsim-sim-roborescue-team-nrl 

- actorsim-sim-roborescue-server

Many outdated or unused repositories are now named with a prefix:
- z-archived-...  (for unused or outdated repositories)

Under development repositories, which are expected for v2020.09, include:
- actorsim-sim-hubo 

- actorsim-planner-popf (used in Hubo project, expect to extend to others)

- actorsim-sim-crazyswarm 

- actorsim-ui-react

- actorsim-sim-duckling (a simple demo that will be revived this summer)


## Actorsim 2020.05 Public Release 

ActorSim 2020.05, based largely on bug fixes from testing ActorSim 2020.03, an
 internal release, is a major release with tons of new features. 

- Release Versions of ActorSim will now be denoted YYYY.MM.

- **ActorSim Core**
  ActorSim 2020.05 is a major refactoring of the core library architecture. 
  This version removes old code to better unify ActorSim’s use across multiple
  projects at NRL.  A new suite of unit and regression tests are now part 
  of the core library to ensure more consistent and reliable code going forward. 
  Version 2020.05 also introduces a substantially unified and simplified 
  documentation system that is rewritten for the new architecture.

- **ActorSim Planning**
  ActorSim 2020.05 introduces a standardized mechanism for linking to 
  planning systems based upon standard automated planning languages 
  (e.g., PDDL, ANML) and the book “Automated Planning and Acting” by
   Ghallab, Nau, & Traverso (2016).
   
- **The Goal Network Library**
  Actorsim 2020.05 introduces a complete refactoring of the GoalNetwork 
  library that builds on a graph theory library and includes regression testing.
  
- **The Goal Refinement Library** and **Cognitive Library**
  ActorSim 2020.05 includes substantial improvements to the base classes for 
  progressing goals in an agent by building on the above planning 
  and core improvements.

- **ActorSim Simulator Connectors** 
  ActorSim 2020.05 extends and improves upon the existing linkage to 
  Microsoft’s Malmo Minecraft Simulator as well as the 
  Robocup Rescue Agent Simulator.
