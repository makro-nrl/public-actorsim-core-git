
# Style Guide

Whenever possible, follow the guidelines provided by the book
*Clean Code: A Handbook of Agile Software Craftsmanship* by Robert Martin.
Feel free to borrow this from Mak if you haven't read it yet!


## Videos on Clean Code

If you cannot access the book, consider watching some videos
by the author of *Clean Code*:

- [Clean Code, Part 1](https://www.youtube.com/watch?v=7EmboKQH8lM)
- [Clean Code, Part 2](https://www.youtube.com/watch?v=2a_ytyt9sf8)
- [Clean Code, Part 3](https://www.youtube.com/watch?v=Qjywrq2gM8o)
- [Clean Code, Part 4](https://www.youtube.com/watch?v=58jGpV2Cg50)
    

# Other resources


[97 Things Every Programmer Should Know](https://www.cs.dartmouth.edu/~cs50/Reading/97_Things_Every_Programmer_Should_Know.pdf)
