# Install IntelliJ IDEA Community Edition

We highly recommend using IntelliJ IDEA for your python and java projects!
IDEA has a python plugin that is nearly identical to PyCharm, but 
IDEA also integrates with Java code.  With the proper plugin, 
both IDEs display Markdown files, like this one, in a nicely formatted
viewer. 

You just need the **community edition** (i.e., the free version) of IDEA.
There are two ways to install it:
1. On ubuntu, consider using `umake ide idea` to install IDEA.  (If you have a
JetBrains license, replace with `umake ide idea-ultimate`.)
   - For details, see the instructions in the section below.
   - Umake can also install eclipse, IDEA, and other IDEs!
2. Download and set up IDEA (https://www.jetbrains.com/idea/download).

You need to ensure that you install IDEA version 2020.1 or later. 


[comment]: ====================================================================
[comment]: ====================================================================
[comment]: ====================================================================
***********************************************************************
### Installing on Ubuntu using Umake 

The following instructions make it possible to install IntelliJ IDEA on Ubuntu.

First, ensure you install the latest version of umake, at least umake 18.05:
- ```
  sudo add-apt-repository ppa:ubuntu-desktop/ubuntu-make
  sudo apt-get update
  sudo apt-get install ubuntu-make 
  ```
Check to make sure ubuntu-make is at least **version 18.05** using 
```umake -version```.

Install IntelliJ with the following command.
```
umake ide idea
```

The default installation path is fine. If you change the installation 
directory it must be a ***different** folder than the git repository for actorsim.

Optionally, set up a soft link to run idea.sh.
If you installed using `umake`, a soft link will make it easier for you
to start idea in the future. 
```shell script
   cd ~
   ln -s .local/share/umake/ide/idea_community/idea.sh
```

So you can then just run `~/idea.sh` to start IDEA.


[comment]: ====================================================================
[comment]: ====================================================================
[comment]: ====================================================================
## Start your journey toward effective IDE use

If you consider how much time you will spend at the keyboard and mouse,
it is easy to justify small changes that can provide big leverage in productivity.
For example, suppose during a typical debugging session you restart your 
test environment 50 times a day.  If starting the debugging session with 
the mouse takes 10 seconds to move your hand to the mouse, find and click the menu, 
and return your hand to the keyboard but can be run instantly with  a single
keystroke, then you stand to gain 500 seconds (= 8 minutes) 
from using that keybinding.  Over a month of development (about 20 days) you 
would be able to reinvest 167 minutes in other activities.  Even if you only 
did this 10 times a day, that is still 30 minutes less a month.  Multiply that 
by your top five activities and this is real time that adds up!
  
There is definitely a trade off (see https://xkcd.com/1205/) where you only 
want to speed up your most common, most frequent, or most costly recurring actions.  
Generally, you can observe productivity and focus increases during development
(and less RSI) when you:

- Minimize your mouse use during development by learning keyboard shortcuts
- Script and streamline every process you do more than a few times a day.  
  For example, if you have to restart multiple servers or clients to run test code, 
  write a shell script that you start with a run configuration from a single keybinding.
- Use the smallest unit test you can to reproduce a problem.  The faster you 
  can iterate with a unit test the quicker you will spot fixes.
- Learn to use breakpoints effectively instead of printing values to the console.  
  Printing out lines to the console requires you to rerun the same test over 
  and over while you hunt through console messages.  Instead, a breakpoint stops
  the code exactly where you are having an issue and allows you to examine 
  the stack trace, objects, and variables at that moment.
- Learn to use the debugger to step into, step over, and continue code.
  An example can be found at https://blog.jetbrains.com/idea/2020/05/debugger-basics-in-intellij-idea/
- Enable and use KeyPromoterX and pay attention to tasks where your key count is high
  enough to justify establishing or using a keybinding.
- Review the productivity guild in `Help > Productivity Guide`
- Read how others are using JetBrains products:
  - https://medium.com/@andrey_cheptsov/top-20-navigation-features-in-intellij-idea-ed8c17075880
  - https://automationhacks.blog/2020/01/26/using-intellij-to-speed-up-your-dev-workflow/
  - https://automationhacks.blog/2020/03/09/how-ive-set-up-my-dev-environment-and-workflow-for-productivity/
- Ask around and talk to other programmers, or ask to shadow them programming 
  for an hour.
- Watch a tutorial or two for IntelliJ:
  - https://www.youtube.com/watch?v=yefmcX57Eyg
  - https://www.youtube.com/watch?v=Mr2mPu1tLhk



[comment]: ====================================================================
[comment]: ====================================================================
[comment]: ====================================================================
***********************************************************************
## Install IDEA plugins
A number of plugins will improve your use of IDEA and provide a streamlined
development environment for our team.

Open `File>Settings` and click `Plugins`  then search the marketplace to 
install the following plugins.

The following are required plugins:
- **GitToolBox** by Jamie Zielinski provides convenient features for git repos.
- **MarkDown** by JetBrains allows you to view and edit our documentation.
- **MultiRun** by Rusian Khmelyuk


If your desired controller requires python, you must also install:
- **Python Plugin** by JetBrains (shown in red in the image below).
You may want to go ahead an install this if you expect to do any python 
programming. 


We also strongly recommend installing the following plugins:
- **Key Promoter X** by hailrutan helps you learn or modify keyboard shortcuts.
- A decent editor keymap. Folks from the team recommend: 
  - **Emacs+ Patched** if you are an emacs user. 
  - **IdeaVIM** if you are a VI user. 
- **PlantUML Plugin** by Eugene Steinberg allows you to view and edit our UML diagrams.
- **GrepConsole** by Vojtech Krasa improves the console output of ActorSim, which
  heavily relies on logging to track state change.

A sample of several plugins is shown below:
![Recommended Plugins](_images/IDEA_RecommendedPlugins.png)


[comment]: ====================================================================
[comment]: ====================================================================
[comment]: ====================================================================
***********************************************************************
## Set up the rest of the stuff
You should be able to complete the setup information in your relevant connector.


