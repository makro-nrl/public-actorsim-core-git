# Effective Use of Git

Large projects require good revision control discipline from each developer so 
that code changes go through a known, stable workflow.

One of the first things you will do is `clone` a repository to access the 
code.  Then you will create a working `branch` from a stable point to
make changes.  While implementing changes, you `commit` changesets.  
For long-running tasks or tasks that you share with another developer,
you may `push` your working branch back to the remote repository so that
other developers can access your code.  Otherwise, your working branch is local.
Often, a code review will be needed when you have finished your working branch.
Once you have approval, you `merge` your working branch back to the stable point 
and, finally, `push` your changes to a remote repository.  

## Branch Naming with GitFlow 

We follow a variant of the [Gitflow](https://www.atlassian.com/git/tutorials/comparing-workflows/gitflow-workflow) 
workflow for branch naming in ActorSim.
The branch types include:
- a single **master** branch holds releases and tags.
- multiple **release** branches hold the state of the code
  for a particular release.  See [release_notes.md](release-notes.md)
  for details about releases.  Release tags in git are 
  just a commit hash and are not really a full branch.  
- a single **develop** branch holds the latest merged code 
  that has been approved through code review.  The latest _approved_
  work for team is usually found here.
- multiple **feature** branches hold the latest work for a specific feature.
  Feature branches originate from develop 
  and are merged back onto develop once approved. 
- multiple **bugfix** branches hold the latest work for a specific 
  patch to correct a known bug.  Bugfix branches usually originate
  from develop or from a release branch and should be merged back 
  to the originating branch and back to develop if originating elsewhere.


## Creating New Branches
_**If you don't know what to name your new branch, please ask your 
development lead.**_

Please create a new branch (we use gitflow branch naming) 
off of the appropriate originating branch. For example, a bugfix branches
something is something like `bugfix/short-description`.  
An example feature branch might be something like `feature/short-description`.

If you are working with tickets from DI2E, `short-description` should
include a ticket number follwed by a pithy, descriptive name.  For example,
`feature/ACTORSIM-530-maven-build` indicates work done for the feature 
to move to a Maven build structure.


## Pushing back to develop
Generally, you will need to do a code review before you merge your code back
to develop.  The only exception to this is when you have documentation.

## Documentation can always be added on develop
Any documentation changes can just be put right on develop.
There's no need for a code review or approval mechanism!


## Committing code
v
Strive to have each commit represent the smallest possible related set of
changes.  This makes it easy to move commits between branches in a large 
team.  

### Example of Cherrypicking a commit from one branch back to develop
While working on a large feature for ActorSim under ticket ACTORSIM-318, 
Mak writes the file you are reading `BranchNaming.md`.  Normally, documentation
changes can be pushed directly to the `develop` branch, but Mak proceeds
to commit this new file to the feature branch `feature/ACTORSIM-318...`.

To start, Mak needs to know which commits to move to develop.
He uses his alias `glods`, which runs the command:
```
git log --graph --pretty='\''%Cred%h%Creset -%C(auto)%d%Creset %s %Cgreen(%ad) %C(bold blue)<%an>%Creset'\'' --date=short
```
The output shows the commit hashes of the two desired commits that are here
marked with `VVVVVV`.
```git
08:36:53 in actorsim-core-git on  feature/ACTORSIM-316-hubo-scenario-1-strategies [⇡!?] took 37s
➜ glods
  VVVVVVV
* f80047c - docs: updated BranchNaming.md (2020-07-15) <Mak Roberts>
*   a21b24f - (HEAD -> feature/ACTORSIM-316-hubo-scenario-1-strategies) Merge branch 'develop' into feature/ACTORSIM-316-hubo-scenario-1-strategies (2020-07-15) <Mak Roberts>
|\
| * 6056c96 - (di2e/develop, di2e/HEAD, develop) added switch to disable memory view on start (2020-07-09) <Mak Roberts>
[...]
* | ef1edec - (di2e/feature/ACTORSIM-316-hubo-scenario-1-strategies) initial draft of converting plans into GoalNodes that contain ExecuteStatements (2020-07-14) <Mak Roberts>
[..]VVVVVVV
* | 2a5341b - docs: BranchNaming.md and CodeStyle.md (2020-06-27) <Mak Roberts>
[..]
* | 62e5bff - use paths instead of files for domain and problem (2020-06-22) <Mak Roberts>
|/
* 63ac1d9 - docs: added plantuml discussion (2020-06-15) <Mak Roberts>
* 32e72ce - docs: cleaned up release_notes.md (2020-06-15) <Mak Roberts>
```
Good!  Changes to this file exist in two commits, `2a5341b` and `f80047c`. 
It will be easy to copy this changeset to the `develop` branch using `git-cherrypick` (alias `gcp`).

Whereas the `merge` command applies all commits from a source branch into the 
destination branch (which defaults to the current branch), a cherrypick merges
specific commit hashes into the current branch.  

So now Mak switches to the destination `develop` branch
(`git checkout develop` alias: `gcd`)
and cherrypicks the two commits in order by time...
```shell script
09:08:45 in actorsim-core-git on  feature/ACTORSIM-316-hubo-scenario-1-strategies [⇡?] took 2s
➜ gcd
Switched to branch 'develop'
Your branch is up-to-date with 'di2e/develop'.

09:09:12 in actorsim-core-git on  develop [?]
➜ gcp 2a5341b
[develop ad57617] docs: BranchNaming.md and CodeStyle.md
 Date: Sat Jun 27 20:46:29 2020 -0400
 3 files changed, 64 insertions(+), 6 deletions(-)
 create mode 100644 actorsim-core-base/src/site/BranchNaming.md
 rewrite actorsim-core-base/src/site/CodeStyle.md (63%)

09:09:55 in actorsim-core-git on  develop [⇡?]
➜ gcp f80047c
[develop 1b31cb6] docs: updated BranchNaming.md
 Date: Wed Jul 15 09:02:20 2020 -0400
 1 file changed, 46 insertions(+), 1 deletion(-)

```
Since no errors occured, he then pushes this merge change up to the remote:
```git
09:10:48 in actorsim-core-git on  develop [⇡?]
➜ git push
Counting objects: 14, done.
Delta compression using up to 8 threads.
Compressing objects: 100% (14/14), done.
Writing objects: 100% (14/14), 3.29 KiB | 0 bytes/s, done.
Total 14 (delta 6), reused 0 (delta 0)
To ssh://git@bitbucket.di2e.net:7999/actorsim/actorsim-core.git
   6056c96..1b31cb6  develop -> develop
```
You can see the cherrypick of `2a5341b` also included some changes to CodeStyle.md,
which were still documentation, so it worked out fine.  
But had these documentation changes been mixed in as part of a larger commit, 
it would be impossible to quickly move them to another branch. 
  

