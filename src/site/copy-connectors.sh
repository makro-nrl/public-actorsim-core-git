#!/bin/bash

copy_files() {
    src=$1
    wildcard=$2
    dest=$3

    echo "  Checking $src$wildcard for files..."
    for f in $src$wildcard; do
      ## Check if the glob gets expanded to existing files.
      ## If not, f here will be exactly the pattern above
      ## and the exists test will evaluate to false.
      if [ -e "$f" ]; then
          echo "    Copying $wildcard from $src to $dest"
          cp -f $src$wildcard $dest
          chmod 500 $dest$wildcard  #prevent accidental editing
      else
          echo "    Skipping $src$wildcard files because none exist"
      fi

      ## This is all we needed to know, so we can break after the first iteration
      break
    done
}


copy_from_home_or_template() {
    name=$1
    home=$2

    echo Working from directory: `pwd`

    mkdir -pv connectors/$name

    if [[ -d $home ]]
    then
      echo "Copying $name from $home"
      copy_files "$home/" "*.md" connectors/$name/
      copy_files "$home/" "*.rst" connectors/$name/

      if [[ -d "$home/_images" ]]
      then
        mkdir -pv connectors/$name/_images
        copy_files "$home/_images/" "*.png" connectors/$name/_images/
      fi

      if [[ -d "$home/uml" ]]
      then
        mkdir -pv connectors/$name/uml
        copy_files "$home/uml/" "*" connectors/$name/uml
      fi
    else
      echo "HOME ($home) does not exist, copying template"
      sed 's/CONNECTOR_NAME/$name/g' _templates/ConnectorTemplate.md \
          > connectors/$name/index.md
    fi

}


start_dir=`pwd`
prefix="actorsim-"
suffix="-git"

cd ../../../
base_dir=`pwd`
git_dirs=`ls -d $prefix*$suffix |grep -v core`

echo "Working on directories (except core) ending with '$suffix'"

echo "Working in directory " `pwd`

for git_dir in $git_dirs:
do
    src_site=$base_dir/$git_dir/src/site
    echo "Checking directory '$src_site'"
    
    if [[ -d $src_site ]]
    then
	connector_name=`echo $git_dir | sed s/$suffix// | sed s/$prefix//`
	echo "  processing '$connector_name' into '$start_dir'"
	cd $start_dir
	copy_from_home_or_template $connector_name $src_site
    fi

done
    

#copy_from_home_or_template roborescue $ROBORESCUE_HOME
#copy_from_home_or_template malmo $MALMO_HOME
