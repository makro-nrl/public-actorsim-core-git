# Notation

```eval_rst
.. comments
   Do not remove! This is an 'empty' code block to initialize the recommonmark
   transformer to support inline, implicit java references within markdown files.
```

ActorSim builds on decades of research in the Automated Planning literature, 
and it thus uses notation from that literature. 
We try to use notation similar to that of  Ghallab, Nau, and Traverso (2016) in 
[*Automated Planning and Acting*](http://projects.laas.fr/planning/).
However, in some cases we may instead rely on ANML-like notation by [Smith, Frank, and Cushing (2008)](https://ti.arc.nasa.gov/m/profile/de2smith/publications/ICAPS08-ANML.pdf). 
Where convenient, we may also revert to notation of PDDL 2.1 by [Fox and Long](http://icaps-conference.org/ipc2008/deterministic/data/fox-long-jair-2003.pdf).
An abbreviated table of notation of this manual is shown below. 

```eval_rst
.. list-table:: Common Notation
   :widths: 60 60
   :header-rows: 1

   * - Notation
     - Description
   * - :math:`a` 
     -  A variable constant, usually intended to be an integer.

   * - :math:`[a, b] stmt`
     -  An temporal assertion that :math:`stmt` is true for every time :math:`t`
        within the interval :math:`a \leq t \leq b` where :math:`stmt` is a subclass
        of :java:ref:`Statement`.

   * - ``TYPE`` 
     - A :java:ref:`WorldType` such as a ``DOCK``, ``ROBOT``, or ``CONTAINER``. 

   * - ``instanceNN`` 
     - A variable instance such as ``dock1``, ``r1``, or ``c1``.  Variable
       instances can be a :java:ref:`WorldType` or a :java:ref:`WorldArgument`.

   * - ``sv(vars..)`` 
     - A parameterized :java:ref:`StateVariable` where ``sv`` is a 
       :java:ref:`StateVariable` and ``vars`` is a list of
       :java:ref:`WorldArgument`. For example, ``loc(r1)`` is a state variable
       indicating the location of robot ``r1``.

   * - ``[start, end] sv(vars..) == value`` 
     - A :java:ref:`PersistenceStatement` that requires ``value`` to  hold
       for :java:ref:`StateVariable` ``sv``
       For example, ``loc(r1) == dock1`` states that
       the location of ``r1`` is ``dock1``.  
       The temporal interval defaults to ``[-INF, +INF]`` if unspecified.

   * - ``[start, end] sv(vars..) := value`` 
     - An :java:ref:`AssignmentStatement` that assigns value to a state variable,
       such as ``loc(r1) := dock1``.  This kind of statement is often called 
       a literal or atom in the planning literature.
       The temporal interval defaults to ``[-INF, +INF]`` if unspecified.

   * - ``sv(vars..) == from -> to`` 
     - A :java:ref:`TransitionStatement` that requires ``value`` to transition 
       between *from* and *to* for a state variable, such as ``loc(r1) == dock1 -> dock2``.  
       The temporal interval defaults to ``[-INF, +INF]`` if unspecified.


   * - ``[start, end] sv(vars.., value)`` 
     - A :java:ref:`PredicateStatement` that subclasses :java:ref:`BooleanPersistenceStatement`
       and conveniently stores a boolean value as a "parameter".  The
       statement is always true for closed worlds.  
       This form is common in some languages such as PDDL.  
       For example, ``loc(r1, dock1)`` states that
       the location of ``r1`` is ``dock1``.  
       The temporal interval defaults to ``[-INF, +INF]`` if unspecified.


```

     
