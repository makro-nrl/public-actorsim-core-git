package nrl.actorsim.goalnetwork;

import nrl.actorsim.domain.*;
import nrl.actorsim.domain.test.TestItemExistsDomain;
import nrl.actorsim.goalnetwork.test.TestGoalNetworkBuilder;
import nrl.actorsim.goalnetwork.test.TestGoalNetworkDomain;
import nrl.actorsim.goalrefinement.strategies.StrategyThread;
import nrl.actorsim.memory.GoalMemoryOptions;
import nrl.actorsim.memory.GoalMemorySimple;
import nrl.actorsim.memory.LifecycleHistory;
import org.slf4j.LoggerFactory;
import org.testng.annotations.Test;

import java.util.*;

import static nrl.actorsim.goalnetwork.test.StrategyThreadHelper.finish;
import static nrl.actorsim.goalnetwork.test.StrategyThreadHelper.release;
import static nrl.actorsim.goalrefinement.lifecycle.GoalMode.FINISHED;


public class TestGoalLifecycleNetwork {
    protected final static org.slf4j.Logger logger = LoggerFactory.getLogger(TestGoalLifecycleNetwork.class);

    @Test
    void testGLN_5_Ord() {
        TestGoalNetworkBuilder<Statement,
                        GoalLifecycleNode,
                        GoalLifecycleNetwork> builder = new TestGoalNetworkBuilder<>();
        GoalLifecycleNetwork network = builder.build_5_Ord(GoalLifecycleNetwork.class);
        network.debug();
        assert (network.size() == 5);
        assert (network.getUnconstrained().size() == 2);

        Set<GoalLifecycleNode> parentRoots = network.getParentRoots();
        assert parentRoots.size() == 1;
        GoalNetworkNode<Statement> existsB = TestGoalNetworkDomain.existsNode(TestItemExistsDomain.B);
        //noinspection SuspiciousMethodCalls
        assert parentRoots.contains(existsB);
    }

    @Test
    void testGLN_5_Ord_ManualReleaseCorrectly() throws InterruptedException {
        TestGoalNetworkBuilder<Statement, GoalLifecycleNode, GoalLifecycleNetwork>
                builder = new TestGoalNetworkBuilder<>();
        GoalLifecycleNetwork network = builder.build_5_Ord(GoalLifecycleNetwork.class);
        GoalMemoryOptions options = GoalMemoryOptions.builder().name("TestMemory").build();
        GoalMemorySimple memory = new GoalMemorySimple(options);
        LifecycleChangeCounter allCounter = new LifecycleChangeCounter();
        memory.add(allCounter);
        LifecycleChangeCounter finishCounter = new LifecycleChangeCounter(
                goalLifecycleNode -> goalLifecycleNode.getMode() == FINISHED);
        memory.add(finishCounter);

        memory.setSimulationTime(0);
        memory.addAndFormulate(network);

        GoalLifecycleNode[] testOrder = {
                builder.d, builder.e, builder.c, builder.b, builder.a
        };
        boolean releaseManually = true;
        assertFinishAndRelease(memory, testOrder, releaseManually);

        LifecycleHistory history = memory.getHistory();
        assert history.insertedAtOrSince(0).size() == 5;
        assert history.updatedAtOrSince(0).size() == 10;
        assert history.deletedAtOrSince(0).size() == 5;
        assert allCounter.changesDetected == 20;
        assert finishCounter.changesDetected == 5;
    }

    private void assertFinishAndRelease(
            GoalMemorySimple memory, GoalLifecycleNode[] testOrder,
            boolean releaseManually) throws InterruptedException {
        for (GoalLifecycleNode testNode : testOrder) {
            GoalLifecycleNode node = memory.findFirst(testNode);
            assert !node.release();
            finish(node);
            assert !releaseManually || release(node);
            assert !memory.contains(nodeToTest -> nodeToTest.equals(node));
        }
    }

    @Test
    void testGLN_5_Ord_AutomaticReleaseCorrectly() throws InterruptedException {
        TestGoalNetworkBuilder<Statement, GoalLifecycleNode, GoalLifecycleNetwork>
                builder = new TestGoalNetworkBuilder<>();
        GoalLifecycleNetwork network = builder.build_5_Ord(GoalLifecycleNetwork.class);
        GoalMemoryOptions options = GoalMemoryOptions.builder()
                .name("TestMemory")
                .dropOnFinish()     //<---- this is the important change!
                .build();
        GoalMemorySimple memory = new GoalMemorySimple(options);
        LifecycleChangeCounter allCounter = new LifecycleChangeCounter();
        memory.add(allCounter);
        LifecycleChangeCounter finishCounter = new LifecycleChangeCounter(
                goalLifecycleNode -> goalLifecycleNode.getMode() == FINISHED);
        memory.add(finishCounter);
        memory.addAndFormulate(network);

        GoalLifecycleNode[] testOrder = {
                builder.d, builder.e, builder.c, builder.b, builder.a
        };
        boolean releaseManually = false;
        assertFinishAndRelease(memory, testOrder, releaseManually);

        assert memory.getNumberOfGoals() == 0;
        LifecycleHistory history = memory.getHistory();
        assert history.insertedAtOrSince(0).size() == 5;
        assert history.updatedAtOrSince(0).size() == 10;
        assert history.deletedAtOrSince(0).size() == 5;

        assert allCounter.changesDetected == 20;
        assert finishCounter.changesDetected == 5;
    }

    @Test
    void testGLN_5_Ord_IncorrectReleaseDetected() throws InterruptedException {
        TestGoalNetworkBuilder<Statement, GoalLifecycleNode, GoalLifecycleNetwork>
                builder = new TestGoalNetworkBuilder<>();
        GoalLifecycleNetwork network = builder.build_5_Ord(GoalLifecycleNetwork.class);
        GoalMemoryOptions options = GoalMemoryOptions.builder()
                .name("TestMemory")
                .dropOnFinish()
                .build();
        GoalMemorySimple memory = new GoalMemorySimple(options);
        LifecycleChangeCounter allCounter = new LifecycleChangeCounter();
        memory.add(allCounter);
        LifecycleChangeCounter finishCounter = new LifecycleChangeCounter(
                goalLifecycleNode -> goalLifecycleNode.getMode() == FINISHED);
        memory.add(finishCounter);
        memory.addAndFormulate(network);

        assert !network.release(memory.findFirst(builder.e));
        assert !network.release(memory.findFirst(builder.b));
        assert !network.release(memory.findFirst(builder.a));

        finish(memory.findFirst(builder.d));
        assert !network.release(memory.findFirst(builder.b));
        assert !network.release(memory.findFirst(builder.a));

        finish(memory.findFirst(builder.e));
        assert !network.release(memory.findFirst(builder.b));
        assert !network.release(memory.findFirst(builder.a));

        finish(memory.findFirst(builder.c));
        assert !network.release(memory.findFirst(builder.a));

        finish(memory.findFirst(builder.b));
        finish(memory.findFirst(builder.a));
        assert !network.contains(memory.findFirst(builder.b));
        assert !network.contains(memory.findFirst(builder.a));


        LifecycleHistory history = memory.getHistory();
        assert history.insertedAtOrSince(0).size() == 5;
        assert history.updatedAtOrSince(0).size() == 10;
        assert history.deletedAtOrSince(0).size() == 5;

        assert allCounter.changesDetected == 20;
        assert finishCounter.changesDetected == 5;
    }

    private static abstract class Worker implements Runnable, StrategyThread {
    }


/*
    private void progressFirstLayer(GoalNetwork network, GoalMemory memory) {
        network.add(atA);
        network.debug();

        StrategyDetails details = new StrategyDetails(atA, memory);
        atA.formulate(details);
        atA.select(details);
        atA.expand(details);
        network.debug();
        network.add(atB, atA);
        network.add(atC, atA);
        network.debug();
        atA.commit(details);
        atB.formulate(new StrategyDetails(atB));
        atC.formulate(new StrategyDetails(atC));
        network.debug();
    }

    private void finishB(GoalNetwork network, GoalMemory memory) {
        StrategyDetails details = new StrategyDetails(atB, memory);
        atB.select(details);
        atB.finish(details);
    }

    @Test
    public void testIsGroundWorks() {
        assert atA.getTarget().isFullyGround();
    }

    @Test
    public void testSimple() {
        GoalNetwork network = new GoalNetwork();
        network.add(atA);
        network.debug();
        network.add(atB, atA);
        network.debug();
        network.add(atC, atA);
        network.debug();
        assert network.getUnconstrained().size() == 2;
        assert network.getRoots().size() == 1;
    }

    @Test
    void testExpand() {
        GoalNetwork network = new GoalNetwork();
        GoalMemory memory = new GoalMemory("TestMemory");
        Thread strategyThread = new StrategyThreadAbstract() {
            @Override
            public void run() {
                progressFirstLayer(network, memory);
            }
        };
        strategyThread.start();
        try {
            strategyThread.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        assert network.getUnconstrained().size() == 2;
        assert network.getRoots().size() == 1;
        assert atA.getMode() == GoalMode.COMMITTED;
        assert atB.getMode() == GoalMode.FORMULATED;
        assert atC.getMode() == GoalMode.FORMULATED;
    }

    @Test
    void testRelease() {
        GoalNetwork network = new GoalNetwork();
        GoalMemory memory = new GoalMemory("TestMemory");
        Thread strategyThread = new StrategyThreadAbstract() {
            @Override
            public void run() {
                progressFirstLayer(network, memory);
                finishB(network, memory);
                network.release(atB);
                network.debug();
                atB.drop(new StrategyDetails(atB));
            }
        };
        strategyThread.start();
        try {
            strategyThread.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        assert network.getUnconstrained().size() == 1;
        assert network.getRoots().size() == 1;
        assert atA.getMode() == GoalMode.COMMITTED;
        assert atB.getMode() == GoalMode.UNFORMED;
        assert atC.getMode() == GoalMode.FORMULATED;
    }
*/
}
