package nrl.actorsim.goalnetwork;

import nrl.actorsim.memory.HistoryEvent;
import nrl.actorsim.memory.LifecycleListener;

import java.util.function.Predicate;

public class LifecycleChangeCounter extends LifecycleListener {
    int changesDetected;

    LifecycleChangeCounter() {
        super();
        this.consumer = this::acceptEvent;
    }

    LifecycleChangeCounter(Predicate<GoalLifecycleNode> filter) {
        super(filter);
        this.consumer = this::acceptEvent;
    }

    public void acceptEvent(HistoryEvent<GoalLifecycleNode> event) {
        changesDetected++;
    }
}
