package nrl.actorsim.chronicle;

import nrl.actorsim.goalnetwork.GoalLifecycleNode;

public class GoalOrdering {
    private final Object lhs;
    private final Object rhs;

    private AllenIntervalOperator operator;

    public GoalOrdering(GoalLifecycleNode lhs,
                        AllenIntervalOperator operator,
                        GoalLifecycleNode rhs) {
        this.lhs = lhs;
        this.setOperator(operator);
        this.rhs = rhs;
    }

    @SuppressWarnings("rawtypes")
    public GoalOrdering(GoalLifecycleNode lhs,
                        AllenIntervalOperator operator,
                        Class rhs) {
        this.lhs = lhs;
        this.setOperator(operator);
        this.rhs = rhs;
    }

    @SuppressWarnings("rawtypes")
    public GoalOrdering(Class lhsClass,
                        AllenIntervalOperator operator,
                        Class rhsClass) {
        this.lhs = lhsClass;
        this.setOperator(operator);
        this.rhs = rhsClass;
    }

    @SuppressWarnings("rawtypes")
    public GoalOrdering(Class lhsClass,
                        AllenIntervalOperator operator,
                        GoalLifecycleNode rhs) {
        this.lhs = lhsClass;
        this.setOperator(operator);
        this.rhs = rhs;
    }

    public static String getKey(Object goalOrClass) {
        //noinspection rawtypes
        return (goalOrClass instanceof GoalLifecycleNode) ?
                goalOrClass.getClass().getSimpleName() :
                ((Class) goalOrClass).getSimpleName();
    }

    @Override
    @SuppressWarnings({"rawtypes", "DuplicatedCode"})
    public String toString() {
        String xName = lhs.toString();
        if (lhs instanceof Class) {
            xName = ((Class) lhs).getSimpleName();
        }
        String yName = rhs.toString();
        if (rhs instanceof Class) {
            yName = ((Class) rhs).getSimpleName();
        }
        return String.format("%s %s %s", xName, getOperator().toString(), yName);
    }

    public Object getLhs() {
        return lhs;
    }

    public Object getRhs() {
        return rhs;
    }

    @SuppressWarnings("unchecked")
    public Class<? extends GoalLifecycleNode> getRhsClass() {
        return (rhs instanceof Class) ? (Class<? extends GoalLifecycleNode>) rhs : (Class<? extends GoalLifecycleNode>) rhs.getClass();
    }

    public String getLhsKey() {
        return getKey(lhs);
    }

    public String getRhsKey() {
        return getKey(rhs);
    }

    public AllenIntervalOperator getOperator() {
        return operator;
    }

    public void setOperator(AllenIntervalOperator operator) {
        this.operator = operator;
    }

    @SuppressWarnings({"rawtypes"})
    public GoalOrdering invert() {
        AllenIntervalOperator invertedOperator = AllenIntervalOperator.invert(this.getOperator());
        GoalOrdering inverted;
        if (lhs instanceof Class) {
            if (rhs instanceof Class) {
                inverted = new GoalOrdering((Class) this.rhs, invertedOperator, (Class) this.lhs);
            } else {
                inverted = new GoalOrdering((GoalLifecycleNode) this.rhs, invertedOperator, (Class) this.lhs);
            }
        } else {
            if (rhs instanceof Class) {
                inverted = new GoalOrdering((Class) this.rhs, invertedOperator, (GoalLifecycleNode) this.lhs);
            } else {
                inverted = new GoalOrdering((GoalLifecycleNode) this.rhs, invertedOperator, (GoalLifecycleNode) this.lhs);
            }
        }
        return inverted;
    }

    public boolean matches(GoalLifecycleNode lhs, GoalLifecycleNode rhs) {
        return matchesLHS(lhs)
                && matchesRHS(rhs);
    }

    @SuppressWarnings("rawtypes")
    public boolean matches(GoalLifecycleNode lhs, Class rhs) {
        return matchesLHS(lhs)
                && matchesRHS(rhs);
    }

    @SuppressWarnings("rawtypes")
    public boolean matches(Class lhs, GoalLifecycleNode rhs) {
        return matchesLHS(lhs)
                && matchesRHS(rhs);
    }

    @SuppressWarnings("rawtypes")
    public boolean matches(Class lhs, Class rhs) {
        return matchesLHS(lhs)
                && matchesRHS(rhs);
    }

    public boolean matchesLHS(GoalLifecycleNode goalNode) {
        return ((lhs instanceof GoalLifecycleNode) && (lhs == goalNode)
                || (goalNode.getClass().equals(lhs)));
    }

    public boolean matchesRHS(GoalLifecycleNode goalNode) {
        return ((rhs instanceof GoalLifecycleNode) && (rhs == goalNode)
                || ((goalNode.getClass().equals(rhs))));
    }

    @SuppressWarnings("rawtypes")
    public boolean matchesLHS(Class lhsClass) {
        return (((lhs instanceof Class) && (lhsClass.equals(lhs)))
                || (lhs.getClass().equals(lhsClass)));
    }

    @SuppressWarnings("rawtypes")
    public boolean matchesRHS(Class rhsClass) {
        return (((rhs instanceof Class) && (rhsClass.equals(rhs)))
                || (rhs.getClass().equals(rhsClass)));
    }

    public boolean usesPrecedesOperator() {
        return AllenIntervalOperator.PRECEDES_OPERATORS.contains(getOperator());
    }
}
