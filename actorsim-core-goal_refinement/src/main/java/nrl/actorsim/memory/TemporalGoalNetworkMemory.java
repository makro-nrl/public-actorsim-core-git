package nrl.actorsim.memory;

import nrl.actorsim.goalnetwork.GoalNetwork;
import nrl.actorsim.goalnetwork.TemporalGoalNetwork;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * This is a placeholder class for when TGNs are implemented.
 * <p>
 * <p>
 * The TemporalGoalNetworkMemory will support multiple TemporalGoalNetworks
 * and sharing between networks.
 */
public class TemporalGoalNetworkMemory extends GoalNetwork {
    List<TemporalGoalNetwork> networks = Collections.synchronizedList(new ArrayList<>());
}
