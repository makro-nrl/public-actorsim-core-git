package nrl.actorsim.memory;

public interface SimulationListener {
    double getSimulationTime();

    void setSimulationTime(double value);
}

