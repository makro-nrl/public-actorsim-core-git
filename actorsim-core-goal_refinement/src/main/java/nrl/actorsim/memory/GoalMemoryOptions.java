package nrl.actorsim.memory;

public class GoalMemoryOptions {
    String name;
    boolean dropOnFinish;

    private GoalMemoryOptions() {

    }

    public static Builder builder() {
        return new Builder();
    }

    public boolean dropOnFinish() {
        return dropOnFinish;
    }

    public static class Builder {
        final GoalMemoryOptions instance;

        private Builder() {
            instance = new GoalMemoryOptions();
        }

        public Builder name(String name) {
            instance.name = name;
            return this;
        }

        public Builder dropOnFinish() {
            instance.dropOnFinish = true;
            return this;
        }

        public GoalMemoryOptions build() {
            return instance;
        }
    }
}
