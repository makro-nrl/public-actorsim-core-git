package nrl.actorsim.memory;

import nrl.actorsim.goalnetwork.GoalLifecycleNode;
import nrl.actorsim.goalrefinement.lifecycle.GoalMode;

import java.time.Instant;

import static nrl.actorsim.memory.HistoryEvent.Type.*;

public class LifecycleEvent extends HistoryEvent<GoalLifecycleNode> {
    final GoalMode oldMode;
    final GoalMode newMode;

    LifecycleEvent(Instant instant, double simulationTime, Type type, GoalLifecycleNode o, GoalMode oldMode) {
        super(instant, simulationTime, type, o);
        this.oldMode = oldMode;
        this.newMode = o.getMode();
    }

    LifecycleEvent(double simulationTime, Type type, GoalLifecycleNode o, GoalMode oldMode) {
        super(simulationTime, type, o);
        this.oldMode = oldMode;
        this.newMode = o.getMode();
    }

    public static LifecycleEvent goalFormulated(double simulationTime, GoalLifecycleNode node) {
        return new LifecycleEvent(simulationTime, INSERTED, node, GoalMode.UNFORMED);
    }

    public static LifecycleEvent goalProgressed(double simulationTime,
                                                GoalLifecycleNode node,
                                                GoalMode oldMode) {
        return new LifecycleEvent(simulationTime, UPDATED, node, oldMode);
    }

    public static LifecycleEvent goalReleased(double simulationTime,
                                              GoalLifecycleNode node,
                                              GoalMode oldMode) {
        return new LifecycleEvent(simulationTime, DELETED, node, oldMode);
    }
}
