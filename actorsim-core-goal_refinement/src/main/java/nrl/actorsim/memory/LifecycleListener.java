package nrl.actorsim.memory;

import nrl.actorsim.goalnetwork.GoalLifecycleNode;

import java.util.function.Consumer;
import java.util.function.Predicate;

public class LifecycleListener extends HistoryListener<GoalLifecycleNode> {
    public LifecycleListener() {
        super();
    }

    public LifecycleListener(Consumer<HistoryEvent<GoalLifecycleNode>> consumer) {
        super(consumer);
    }

    public LifecycleListener(Predicate<GoalLifecycleNode> filter) {
        super(filter);
    }

    public LifecycleListener(Predicate<GoalLifecycleNode> filter,
                             Consumer<HistoryEvent<GoalLifecycleNode>> consumer) {
        super(filter, consumer);
    }

    @SuppressWarnings("unchecked")
    public LifecycleListener setName(String name) {
        this.name = name;
        return this;
    }

}
