package nrl.actorsim.memory;

import nrl.actorsim.domain.*;

import java.time.Instant;
import java.util.*;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import static nrl.actorsim.domain.StateVariableTemplate.NULL_SVT;
import static nrl.actorsim.domain.WorldObject.NULL_WORLD_OBJECT;

/**
 * Stores the objects and statements that describe the world.
 *
 * The {@link FactMemory} does **not** store {@link StateVariable}s,
 * which are a subset of {@link nrl.actorsim.domain.StateProperty}, and
 * which hold information about the attributes of objects and their relations.
 * This is because all {@link StateVariable}s should be wrapped within
 * a {@link Statement} to ensure that the temporal extent of the variable
 * is captured.
 *
 */
@SuppressWarnings({"UnusedReturnValue", "unused"})
public class FactMemory implements SimulationListener {
    final static org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(FactMemory.class);

    protected final String shortName;

    //NB: using a map b/c java.util.Set lacks get()
    protected Map<WorldObject, WorldObject> worldObjects = new TreeMap<>();
    protected final History<WorldObject> worldObjectHistory = new History<>();

    //NB: using a map b/c java.util.Set lacks get()
    protected Map<Statement, Statement> statements = new TreeMap<>();
    protected final History<Statement> statementHistory = new History<>();

    double latestSimulationTime = -1;
    Instant latestInstant = Instant.EPOCH;

    public FactMemory(String name) {
        this.shortName = name;
    }

    // ===========================================================
    // region<Accessors>

    public void clear() {
        worldObjects.clear();
        worldObjects = new TreeMap<>();

        statements.clear();
        statements = new TreeMap<>();
    }

    public double getSimulationTime() {
        return this.latestSimulationTime;
    }

    public void setSimulationTime(double simulationTime) {
        if (simulationTime > latestSimulationTime) {
            latestSimulationTime = simulationTime;
        }
    }

    // endregion
    // ===========================================================

    // ===========================================================
    // region<Logging>

    public void traceVariables(org.slf4j.Logger callLogger, Predicate<Statement> filter) {
        if (callLogger.isTraceEnabled()) {
            for (Statement statement : statements.values()) {
                if (filter.test(statement)) {
                    callLogger.trace("{}: contains {}", shortName, statement);
                }
            }
        }
    }

    // endregion
    // ===========================================================

    // ===============================================
    // region<WorldObjects>
    public void updateObjectsFromBindings(Bindable bindable, double simulationTime) {
        for(Binding binding : bindable.findBindings()) {
            Bindable source = binding.getSource();
            if (source instanceof WorldObject
                    && !(source instanceof WorldArgument)) {
                updateOrCloneThenAdd((WorldObject) source, simulationTime);
            }
            Bindable target = binding.getTarget();
            if (target instanceof WorldObject) {
                updateOrCloneThenAdd((WorldObject) target, simulationTime);
            }
        }
    }

    /**
     * Adds or updates the object.
     * If added, a copy is made beforehand.
     * If updated, the object in memory is updated with state from object.
     *
     * @param worldObject    the object to add
     * @param simulationTime simulation time of the update
     * @return the copy in memory, whether added or updated
     */
    public WorldObject updateOrCloneThenAdd(WorldObject worldObject, double simulationTime) {
        WorldObject copy;
        if (!worldObjects.containsKey(worldObject)) {
            copy = worldObject.instance();
            worldObjects.put(copy, copy);
            worldObjectHistory.inserted(copy, simulationTime);
        } else {
            copy = worldObjects.get(worldObject);
            copy.update(worldObject);
            worldObjectHistory.updated(copy, simulationTime);
        }
        setSimulationTime(simulationTime);
        return copy;
    }

    public boolean missing(WorldObject worldObject) {
        return !contains(worldObject);
    }

    public boolean contains(WorldObject worldObject) {
        return worldObjects.containsKey(worldObject);
    }

    public WorldObject getObject(WorldObject worldObject) {
        return worldObjects.getOrDefault(worldObject, NULL_WORLD_OBJECT);
    }

    public Collection<WorldObject> getObjects() {
        return Collections.unmodifiableCollection(worldObjects.values());
    }

    public WorldObject getObject(String id) {
        //noinspection Convert2Lambda
        Collection<WorldObject> matching = getObjects(new Predicate<>() {
            @Override
            public boolean test(WorldObject worldObject) {
                return worldObject.getId().equalsIgnoreCase(id);
            }
        });

        for (WorldObject object: matching) {
            return object;
        }
        return NULL_WORLD_OBJECT;
    }

    public Collection<WorldObject> getObjects(Predicate<WorldObject> filter) {
        Collection<WorldObject> filtered = new TreeSet<>();
        for (WorldObject obj: worldObjects.values()) {
            if (filter.test(obj)) {
                filtered.add(obj);
            }
        }
        return filtered;
    }

    public Collection<WorldObject> find(Predicate<WorldObject> filter) {
        return getObjects(filter);
    }
    // endregion
    // ===============================================

    // ===============================================
    // region<Statements>

    /**
     * Adds or updates the statement in memory.
     * If added, a copy is made beforehand.
     * If updated, the object in memory is updated with state from the sv.
     *
     * @param statement      the {@link Statement} to update
     * @param simulationTime simulation time of the update
     */
    public Statement updateOrCloneThenAdd(Statement statement, double simulationTime) {
        Statement copy;
        if (!statements.containsKey(statement)) {
            logger.trace("{}: added {}", shortName, statement);
            copy = statement.cloner().build();
            updateObjectsFromBindings(copy, simulationTime);
            statements.put(copy, copy);
            statementHistory.inserted(copy, simulationTime);
        } else {
            copy = statements.get(statement);
            statementHistory.updated(copy, simulationTime);
        }
        logger.trace("{}: update time to {} for sv {}", shortName, simulationTime, copy);
        setSimulationTime(simulationTime);
        return copy;
    }

    public void remove(Statement statement, double simulationTime) {
        if (statements.containsKey(statement)) {
            statements.remove(statement);
            statementHistory.deleted(statement, simulationTime);
        }
        setSimulationTime(simulationTime);
    }

    public List<Statement> findMatches(Statement statement) {
        List<Statement> matches = new ArrayList<>();
        for (Statement memoryStatement : statements.values()) {
            if (statement.equals(memoryStatement)) {
                matches.add(memoryStatement);
            }
        }
        return matches;
    }

    //TODO ACTORSIM-1058 integrate and test this method
    public List<Statement> findMatchesBetter(Statement statement) {
        return statements.values()
                .stream().filter(memoryStatement -> memoryStatement.equals(statement))
                .collect(Collectors.toList());
    }

    public List<Statement> findMatches(Predicate<Statement> predicate) {
        List<Statement> matches = new ArrayList<>();
        for (Statement memoryStatement : statements.values()) {
            if (predicate.test(memoryStatement)) {
                matches.add(memoryStatement);
            }
        }
        return matches;
    }

    //TODO ACTORSIM-1058 integrate and test this method
    public List<Statement> findMatchesBetter(Predicate<Statement> predicate) {
        return statements.values()
                .stream().filter(predicate)
                .collect(Collectors.toList());
    }

    public Statement findFirst(Statement statement) {
        return statements.values().stream()
                .filter(memoryStatement -> memoryStatement.equals(statement))
                .findFirst()
                .orElseGet(NULL_SVT::persistenceStatement);
    }

    public boolean contains(Statement statement) {
        return statements.containsKey(statement);
    }

    public boolean missing(Statement statement) {
        return !contains(statement);
    }

    // endregion
    // ===============================================

    // ===============================================
    // region<HistoryListeners>

    public void addStatementListener(HistoryListener<Statement> listener) {
        statementHistory.addListener(listener);
    }

    public void addWorldObjectListener(HistoryListener<WorldObject> listener) {
        worldObjectHistory.addListener(listener);
    }

    public Collection<? extends Statement> getAddedStatements() {
        return statementHistory.insertedAt(this.latestSimulationTime);
    }

    public Collection<? extends Statement> getAddedStatements(double simulationTime) {
        return statementHistory.insertedAt(simulationTime);
    }

    public Collection<? extends Statement> getAddedStatements(Predicate<Statement> filter) {
        return getAddedStatements(latestSimulationTime, filter);
    }

    public Collection<? extends Statement> getAddedStatements(double simulationTime, Predicate<Statement> filter) {
        ArrayList<Statement> filtered = new ArrayList<>();

        for (Statement added : statementHistory.insertedAt(simulationTime)) {
            if (filter.test(added)) {
                filtered.add(added);
            }
        }
        return filtered;
    }

    public Collection<? extends WorldObject> getAddedObjects() {
        return worldObjectHistory.insertedAt(latestSimulationTime);
    }

    public Collection<? extends WorldObject> getAddedObjects(double simulationTime) {
        return worldObjectHistory.insertedAt(simulationTime);
    }

    public Collection<? extends WorldObject> getAddedObjects(Predicate<WorldObject> filter) {
        return getAddedObjects(latestSimulationTime, filter);
    }

    public Collection<? extends WorldObject> getAddedObjects(double simulationTime, Predicate<WorldObject> filter) {
        ArrayList<WorldObject> filtered = new ArrayList<>();
        for (WorldObject added : worldObjectHistory.insertedAt(simulationTime)) {
            if (filter.test(added)) {
                filtered.add(added);
            }
        }
        return filtered;
    }

    // endregion
    // ===============================================

    // ====================================================
    // region<Test Helpers>
    public void assertMissing(WorldObject obj) {
        assert missing(obj);
    }

    public void assertMissing(Statement statement) {
        assert missing(statement);
    }

    public void assertContains(WorldObject obj) {
        assert contains(obj);
    }

    public void assertContains(Statement statement) {
        assert contains(statement);
    }

    // endregion
    // ====================================================



}
