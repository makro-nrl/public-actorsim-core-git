package nrl.actorsim.memory;

import nrl.actorsim.goalnetwork.GoalLifecycleNode;
import nrl.actorsim.goalrefinement.lifecycle.GoalMode;

public class LifecycleHistory extends History<GoalLifecycleNode> {

    public void goalFormulated(double simulationTime, GoalLifecycleNode node) {
        insert(LifecycleEvent.goalFormulated(simulationTime, node));
    }

    public void goalProgressed(double simulationTime, GoalLifecycleNode node, GoalMode oldMode) {
        insert(LifecycleEvent.goalProgressed(simulationTime, node, oldMode));
    }

    public void goalReleased(double simulationTime, GoalLifecycleNode node, GoalMode oldMode) {
        insert(LifecycleEvent.goalReleased(simulationTime, node, oldMode));
    }
}
