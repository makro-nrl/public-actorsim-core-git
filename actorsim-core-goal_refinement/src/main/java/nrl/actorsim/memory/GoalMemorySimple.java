/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package nrl.actorsim.memory;

import nrl.actorsim.chronicle.AllenIntervalOperator;
import nrl.actorsim.chronicle.GoalOrdering;
import nrl.actorsim.goalnetwork.GoalLifecycleNetwork;
import nrl.actorsim.goalnetwork.GoalLifecycleNode;
import nrl.actorsim.utils.ChangeNotifierImpl;
import nrl.actorsim.goalnetwork.SimpleNetwork;
import nrl.actorsim.goalrefinement.lifecycle.GoalMode;
import nrl.actorsim.utils.Named;
import org.slf4j.LoggerFactory;

import java.util.*;
import java.util.function.Predicate;

import static nrl.actorsim.goalnetwork.SimpleNetwork.AddStatus.ADDED;

/**
 * The {@link GoalMemorySimple} maintains a single GoalNetwork that does
 * not support GoalNode sharing.  Nodes are directly added to the memory, which
 * inserts them into the network.
 * <p>
 * Clients that require tracking multiple, interacting (Temporal)GoalNetworks
 * or that require GoalNode sharing should use the TemporalGoalMemory.
 */
public class GoalMemorySimple
        extends ChangeNotifierImpl
        implements Named, SimulationListener {
    final static org.slf4j.Logger logger = LoggerFactory.getLogger(GoalMemorySimple.class);
    final GoalLifecycleNetwork network;
    final LifecycleHistory lifecycleHistory;
    private final GoalMemoryOptions options;
    final Map<String, List<GoalOrdering>> orderingMap = Collections.synchronizedMap(new HashMap<>());
    final Map<String, Set<String>> goalOrderingPrecedenceMap = new TreeMap<>();
    private double simulationTime = 0;

    // ====================================================
    // region<Constructors and Object Overrides>

    public GoalMemorySimple(GoalMemoryOptions options) {
        this(options, new GoalLifecycleNetwork());
    }

    public GoalMemorySimple(GoalMemoryOptions options, GoalLifecycleNetwork initialNetwork) {
        this.options = options;
        this.network = initialNetwork;
        lifecycleHistory = new LifecycleHistory();
    }

    @Override
    public String toString() {
        return options.name + ":" + network.toDetailedStringNodeIds();
    }

    // endregion
    // ====================================================

    // ====================================================
    // region<Accessors>

    public String getName() {
        return options.name;
    }

    @Override
    public String getShortName() {
        return options.name;
    }


    public GoalMemoryOptions getOptions() {
        return options;
    }

    @SuppressWarnings("unused")
    public int getNumberOfGoals() {
        return network.size();
    }

    public Set<GoalLifecycleNode> getViewRoots() {
        return network.getViewRoots();
    }

    // endregion
    // ====================================================

    // ====================================================
    // region<Node Add, Remove, Progress>

    public SimpleNetwork.AddStatus add(GoalLifecycleNode goalNode) {
        SimpleNetwork.AddStatus status = network.add(goalNode);
        if (status.noError()) {
            logger.debug("{}: added {}", getShortName(), goalNode.toStringDetails());
            goalNode.addChangeListener(this::notifyOfInternalChange);
        } else {
            logger.error("{}: {} was not added", getShortName(), goalNode);
        }
        return status;
    }

    @SuppressWarnings("unused")
    public void addSubgoal(GoalLifecycleNode parent, GoalLifecycleNode child) {
        if (network.add(parent) == ADDED) {
            logger.debug("{}: added {}", getShortName(), parent.toStringDetails());
        }
        if (network.add(child) == ADDED) {
            logger.debug("{}: added {}", getShortName(), child.toStringDetails());
        }
        network.addSubsUnordered(parent, child);
    }

    public void addSubgoalOrderings(List<? extends GoalLifecycleNode> predecessors,
                                    List<? extends GoalLifecycleNode> successors) {
        for (GoalLifecycleNode predecessor : predecessors) {
            for (GoalLifecycleNode successor : successors) {
                addSubgoalOrdering(predecessor, successor);
            }
        }
    }

    public void addSubgoalOrdering(GoalLifecycleNode predecessor, GoalLifecycleNode successor) {
        network.add(predecessor);
        network.add(successor);
        network.addBeforeEdge(predecessor, successor);
    }

    public void addAndFormulate(GoalLifecycleNetwork decomposition) {
        network.addAll(decomposition);

        for (GoalLifecycleNode node : network.getNodes()) {
            node.formulate(this);
        }
    }

    public void removeAll() {
        network.releaseAll();
    }

    // endregion
    // ====================================================

    // ====================================================
    // region<Searching>

    @SuppressWarnings("unused")
    public boolean missing(Predicate<GoalLifecycleNode> predicate) {
        return !contains(predicate);
    }

    public boolean contains(Predicate<GoalLifecycleNode> predicate) {
        return network.contains(predicate);
    }

    public boolean contains(GoalLifecycleNode goal) {
        return contains(node -> node.compareTo(goal) == 0);
    }


    public
    <N extends GoalLifecycleNode>
    Set<N> find(Predicate<N> predicate) {
        //noinspection unchecked
        return (Set<N>) network.find((Predicate<GoalLifecycleNode>) predicate);
    }

    @SuppressWarnings("unused")
    public Set<GoalLifecycleNode> find(GoalLifecycleNode node) {
        return network.find(nodeToCheck -> nodeToCheck.equals(node));
    }

    public
    <N extends GoalLifecycleNode>
    N findFirst(N node) {
        Set<GoalLifecycleNode> goals = network.find(n -> n.equals(node));
        if(goals.size() > 0) {
            //noinspection unchecked
            return (N) goals.iterator().next();
        }
        return null;
    }

    public
    <N extends GoalLifecycleNode>
    N findFirst(Predicate<N> predicate) {
        //noinspection unchecked
        Set<GoalLifecycleNode> goals = network.find((Predicate<GoalLifecycleNode>) predicate);
        if(goals.size() > 0) {
            //noinspection unchecked
            return (N) goals.iterator().next();
        }
        return null;
    }



    /**
     * A convenience function to return all goals in the provided mode.
     * @param mode the mode to check
     * @return all goals in the provided mode
     */
    public Set<GoalLifecycleNode> find(GoalMode mode) {
        return find(node -> node.getMode() == mode);
    }

    public Set<GoalLifecycleNode> find(GoalMode... modes) {
        return find(Arrays.asList(modes));
    }

    public Set<GoalLifecycleNode> find(List<GoalMode> modes) {
        return find(node -> modes.contains(node.getMode()));
    }

    // endregion
    // ====================================================

    // ====================================================
    // region<Goal Ordering>

    @SuppressWarnings("unused")
    public Map<String, List<GoalOrdering>> getOrderingMap() {
        return orderingMap;
    }

    public void add(GoalOrdering ordering) {
        insertOrderingInMapIfNeeded(ordering.getLhsKey(), ordering);
        insertOrderingInMapIfNeeded(ordering.getRhsKey(), ordering.invert());
        updateGoalOrderingPrecedenceMap();
    }

    private void insertOrderingInMapIfNeeded(String key, GoalOrdering ordering) {
        List<GoalOrdering> keyOrderings;
        if (orderingMap.containsKey(key)) {
            keyOrderings = orderingMap.get(key);
        } else {
            keyOrderings = new ArrayList<>();
            orderingMap.put(key, keyOrderings);
        }
        keyOrderings.add(ordering);
    }

    @SuppressWarnings("unused")
    public void remove(GoalOrdering ordering) {
        removeOrderingFromMapIfPresent(ordering.getLhsKey());
        removeOrderingFromMapIfPresent(ordering.getRhsKey());
        updateGoalOrderingPrecedenceMap();
    }

    private void removeOrderingFromMapIfPresent(String key) {
        if (orderingMap.containsKey(key)) {
            List<GoalOrdering> keyOrderings = orderingMap.get(key);
            if (keyOrderings.size() == 1) {
                orderingMap.remove(key);
            }
        }
    }

    public Map<String, Set<String>> getGoalOrderingPrecedenceMap() {
        return goalOrderingPrecedenceMap;
    }

    private void updateGoalOrderingPrecedenceMap() {
        Deque<GoalOrdering> orderingsToCheck = new LinkedList<>();
        orderingMap.forEach((key, value) -> {
            for (GoalOrdering ordering : value) {
                if (ordering.usesPrecedesOperator()) {
                    orderingsToCheck.addLast(ordering);
                }
            }
        });

        updatePrecedenceMap_impl(orderingsToCheck);
    }

    @SuppressWarnings("rawtypes")
    private void updatePrecedenceMap_impl(Deque<GoalOrdering> orderingsToCheck) {
        goalOrderingPrecedenceMap.clear();
        while (orderingsToCheck.size() > 0) {
            GoalOrdering ordering = orderingsToCheck.poll();
            Object lhs = ordering.getLhs();
            Class lhsClass = (lhs instanceof Class) ? (Class) lhs : lhs.getClass();
            Set<String> successorKeys = new TreeSet<>();
            //noinspection unchecked
            List<Class<? extends GoalLifecycleNode>> successorClasses = getSuccessorClasses(lhsClass);
            for (Class<? extends GoalLifecycleNode> goalClass : successorClasses) {
                successorKeys.add(GoalOrdering.getKey(goalClass));
            }
            goalOrderingPrecedenceMap.put(GoalOrdering.getKey(lhs), successorKeys);
        }
    }

    @SuppressWarnings("unused")
    public List<GoalOrdering> getOrderings(GoalLifecycleNode lhs) {
        return getOrderings((Object) lhs);
    }

    @SuppressWarnings("unused")
    public List<GoalOrdering> getOrderings(GoalLifecycleNode lhs, AllenIntervalOperator... operators) {
        return getOrderings(lhs, Arrays.asList(operators));
    }

    @SuppressWarnings({"rawtypes", "unused"})
    public List<GoalOrdering> getOrderings(Class lhs) {
        return getOrderings((Object) lhs);
    }

    @SuppressWarnings({"rawtypes", "unused"})
    public List<GoalOrdering> getOrderings(Class lhs, AllenIntervalOperator... operators) {
        return getOrderings(lhs, Arrays.asList(operators));
    }

    private List<GoalOrdering> getOrderings(Object lhs) {
        List<GoalOrdering> relevantOrderings = new ArrayList<>();
        String key = GoalOrdering.getKey(lhs);
        if (orderingMap.containsKey(key)) {
            List<GoalOrdering> keyOrderings = orderingMap.get(key);
            relevantOrderings.addAll(keyOrderings);
        }
        return relevantOrderings;
    }

    private List<GoalOrdering> getOrderings(Object lhs, List<AllenIntervalOperator> operatorList) {
        List<GoalOrdering> relevantOrderings = getOrderings(lhs);
        relevantOrderings.removeIf(ordering -> !operatorList.contains(ordering.getOperator()));
        return relevantOrderings;
    }

    /**
     * Determines if this goalNode has transitive precedence over other goals.
     *
     * @param lhs the goalNode that precedes others
     * @return the list of successors to lhs
     */
    @SuppressWarnings("unused")
    public List<GoalLifecycleNode> getSuccessors(GoalLifecycleNode lhs) {
        List<GoalLifecycleNode> successors = new ArrayList<>();
        List<GoalLifecycleNode> checked = new ArrayList<>();
        Deque<GoalOrdering> remainingToCheck = new LinkedList<>();
        getSuccessors_impl(lhs, GoalMode.ANY, successors, remainingToCheck);
        checkTransitiveForSuccessors(GoalMode.ANY, successors, remainingToCheck);
        return successors;
    }

    /**
     * Determines if this goalNode has transitive precedence over other goals
     * that also match the successorMode.
     *
     * @param lhs           the goalNode that precedes others
     * @param successorMode the mode for any successor to match
     * @return a list of successors to lhs
     */
    @SuppressWarnings("unused")
    public List<GoalLifecycleNode> getSuccessors(GoalLifecycleNode lhs, GoalMode successorMode) {
        List<GoalLifecycleNode> successors = new ArrayList<>();
        Deque<GoalOrdering> remainingToCheck = new LinkedList<>();
        getSuccessors_impl(lhs, successorMode, successors, remainingToCheck);

        checkTransitiveForSuccessors(successorMode, successors, remainingToCheck);

        return successors;
    }

    /**
     * Confirm that all successors are checked as transitive successors
     *
     * @param successorMode    the mode for any successor to match
     * @param successors       the list of currently known successor goals
     * @param remainingToCheck the list of rhs items to check
     */
    private void checkTransitiveForSuccessors(GoalMode successorMode, List<GoalLifecycleNode> successors, Deque<GoalOrdering> remainingToCheck) {
        while (remainingToCheck.size() > 0) {
            GoalOrdering ordering = remainingToCheck.poll();
            Object lhs = ordering.getLhs();
            getSuccessors_impl(lhs, successorMode, successors, remainingToCheck);
        }
    }

    private void getSuccessors_impl(Object lhs, GoalMode successorMode, List<GoalLifecycleNode> successors, Deque<GoalOrdering> remainingToCheck) {
        List<GoalOrdering> orderings = getOrderings(lhs, AllenIntervalOperator.PRECEDES_OPERATORS);
        for (GoalOrdering ordering : orderings) {
            remainingToCheck.addAll(getOrderings(ordering.getRhs(), AllenIntervalOperator.PRECEDES_OPERATORS));
            successors.addAll(find(goal ->
                    ((goal.modeMatches(successorMode))
                            && ordering.matchesRHS(goal))));
        }
    }

    /**
     * Determines if this goalNode has transitive precedence over other goals.
     *
     * @param lhs the goalNode that precedes others
     * @return the list of successors to lhs
     */
    public List<Class<? extends GoalLifecycleNode>> getSuccessorClasses(Class<? extends GoalLifecycleNode> lhs) {
        List<Class<? extends GoalLifecycleNode>> successors = new ArrayList<>();
        Deque<GoalOrdering> remainingToCheck = new LinkedList<>();
        getSuccessorClasses_impl(lhs, successors, remainingToCheck);
        checkTransitiveForSuccessorClasses(successors, remainingToCheck);
        return successors;
    }

    /**
     * Confirm that all successors are checked as transitive successors
     *
     * @param successors       the list of currently known successor goals
     * @param remainingToCheck the list of rhs items to check
     */
    private void checkTransitiveForSuccessorClasses(List<Class<? extends GoalLifecycleNode>> successors, Deque<GoalOrdering> remainingToCheck) {
        while (remainingToCheck.size() > 0) {
            GoalOrdering ordering = remainingToCheck.poll();
            Object lhs = ordering.getLhs();
            getSuccessorClasses_impl(lhs, successors, remainingToCheck);
        }
    }

    private void getSuccessorClasses_impl(Object lhs, List<Class<? extends GoalLifecycleNode>> successors, Deque<GoalOrdering> remainingToCheck) {
        List<GoalOrdering> orderings = getOrderings(lhs, AllenIntervalOperator.PRECEDES_OPERATORS);
        for (GoalOrdering ordering : orderings) {
            remainingToCheck.addAll(getOrderings(ordering.getRhs(), AllenIntervalOperator.PRECEDES_OPERATORS));
            successors.add(ordering.getRhsClass());
        }
    }

    // endregion
    // ====================================================

    // ====================================================
    // region<History Listener>

    public void add(LifecycleListener listener) {
        lifecycleHistory.addListener(listener);
    }

    public LifecycleHistory getHistory() {
        return lifecycleHistory;
    }

    public double getSimulationTime() {
        return simulationTime;
    }

    public void setSimulationTime(double value) {
        this.simulationTime = value;
    }

    // endregion
    // ====================================================

    // ====================================================
    // region<Test Assertions>

    public void assertContains(GoalLifecycleNode goal) {
        assert contains(node -> node.compareTo(goal) == 0);
    }

    public void assertModeEquals(GoalLifecycleNode goal, GoalMode mode) {
        assert modeEquals(goal, mode);
    }

    public boolean modeEquals(GoalLifecycleNode goal, GoalMode mode) {
        return contains(node ->
                (node.compareTo(goal) == 0)
                        && node.modeMatches(mode));
    }


    // endregion
    // ====================================================

}

