package nrl.actorsim.memory;

import nrl.actorsim.domain.Statement;
import nrl.actorsim.goalrefinement.strategies.ApplyStrategyDetails;
import nrl.actorsim.goalrefinement.strategies.ApplyStrategyDetailsCollection;
import nrl.actorsim.goalrefinement.strategies.StrategyTemplate;
import org.apache.commons.cli.OptionManager;

import java.util.ArrayList;
import java.util.List;

@SuppressWarnings("unused")
public class WorkingMemory extends FactMemory {
    final static org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(WorkingMemory.class);

    private final GoalMemorySimple goalMemory;
    private final Options options;

    private List<StrategyTemplate> strategiesToCheckEveryCycle = new ArrayList<>();
    private List<StrategyTemplate> strategiesToApplyOnceAtEndOfCycle = new ArrayList<>();

    private static WorkingMemory instance;

    public static WorkingMemory startInstance(Options options) {
        if (instance == null) {
            instance = new WorkingMemory(options);
        }
        return instance;
    }

    public static WorkingMemory getInstance() {
        if (instance == null) {
            throw new UnsupportedOperationException("You must initialize using startInstance() before calling this method");
        }
        return instance;
    }

    public WorkingMemory(Options options) {
        super("WorkingMemory");
        this.options = options;
        goalMemory = new GoalMemorySimple(options.goalMemoryOptions);

    }

    public static WorkingMemory getDefaultTestMemory() {
        WorkingMemory.Options options = WorkingMemory.optionsBuilder()
                .useDefaultGoalMemoryOptions()
                .build();
        WorkingMemory memory = new WorkingMemory(options);
        logger.debug("Created default test memory {}", memory);
        return memory;
    }

    public WorkingMemory instance(){
        return getDefaultTestMemory();
    }

    public void clear() {
        super.clear();

        goalMemory.removeAll();

        strategiesToCheckEveryCycle.clear();
        strategiesToCheckEveryCycle = null;
        strategiesToApplyOnceAtEndOfCycle.clear();
        strategiesToApplyOnceAtEndOfCycle = null;
    }

    @Override
    public void setSimulationTime(double value) {
        super.setSimulationTime(value);
        goalMemory.setSimulationTime(value);
    }

    // ====================================================
    // region<Memory View>

    // endregion
    // ====================================================

    // ====================================================
    // region<GoalMemory>

    public GoalMemorySimple getGoalMemory() {
        return goalMemory;
    }

    @Override
    public boolean contains(Statement statement) {
        if (hasGoal(statement)) {
            return true;
        }
        return super.contains(statement);
    }

    public boolean hasGoal(Statement statement) {
        return goalMemory.contains(node -> node.getStored().equals(statement));
    }

    // endregion
    // ====================================================

    // ====================================================
    // region<Strategy Functions>

    public void addStrategiesToCheckEveryCycle(List<StrategyTemplate> strategies) {
        logger.info("Added strategies {}", strategies);
        strategiesToCheckEveryCycle.addAll(strategies);
    }

    public void addStrategyToCheckEveryCycle(StrategyTemplate strategy) {
        logger.info("Added strategy {}", strategy);
        strategiesToCheckEveryCycle.add(strategy);
    }

    public void addApplyOnceAtEndEachCycle(StrategyTemplate strategy) {
        logger.info("Added apply-once-at-end-of-cycle strategy {}", strategy);
        strategiesToApplyOnceAtEndOfCycle.add(strategy);
    }

    public boolean insertApplicableStrategies(List<ApplyStrategyDetails> applicable, boolean onlyApplyEndOfCycleStrategies) {
        boolean finalIteration = false;
        logger.debug("Checking {} strategies", strategiesToCheckEveryCycle.size() + strategiesToApplyOnceAtEndOfCycle.size());
        if (!onlyApplyEndOfCycleStrategies) {
            logger.debug("Checking {} mid-cycle strategies", strategiesToCheckEveryCycle.size());
            determineApplicableStrategies(applicable, strategiesToCheckEveryCycle);
        }
        if (applicable.size() == 0) {
            finalIteration = true;
            logger.debug("Checking {} end-of-cycle strategies", strategiesToApplyOnceAtEndOfCycle.size());
            determineApplicableStrategies(applicable, strategiesToApplyOnceAtEndOfCycle);
        }
        return finalIteration;
    }

    private void
    determineApplicableStrategies(List<ApplyStrategyDetails> applicableDetails,
                                  List<StrategyTemplate> strategiesToCheck) {
        for (StrategyTemplate strategy : strategiesToCheck) {
            try {
                ApplyStrategyDetailsCollection applyCollection
                        = new ApplyStrategyDetailsCollection(strategy);
                logger.trace("  checking preconditions for {} ", strategy);
                strategy.checkPreconditions(this, applyCollection);
                if (applyCollection.isApplicable()) {
                    if (logger.isTraceEnabled()) {
                        logger.trace("      applicable {}", applyCollection.toStringApplicable());
                    }
                    applicableDetails.addAll(applyCollection.split());
                } else {
                    logger.trace("  NOT applicable {}", applyCollection.toStringApplicable());
                }
            } catch (Exception e) {
                logger.error("Strategy {} threw an exception", strategy, e);
            }
        }
    }

    // endregion
    // ====================================================

    // ====================================================
    // region<Options>

    public Options getOptions() {
        return options;
    }

    public static Options.Builder optionsBuilder() {
        return new Options.Builder();
    }


    public static class Options extends OptionManager {
        private ApplyStrategyDetails applyDetails;
        private GoalMemoryOptions goalMemoryOptions;

        private Options() {
        }

        public static Builder builder() {
            return new Builder();
        }

        public ApplyStrategyDetails getApplyDetails() {
            return applyDetails;
        }

        public GoalMemoryOptions getGoalMemoryOptions() {
            return goalMemoryOptions;
        }

        public static class Builder {
            private final Options instance;

            private Builder() {
                instance = new Options();
                instance.applyDetails = ApplyStrategyDetails.DEFAULT_APPLY_OPTIONS;
            }

            public Builder defaultApplyDetails(ApplyStrategyDetails details) {
                instance.applyDetails = details;
                return Builder.this;
            }

            public Builder goalMemoryOptions(GoalMemoryOptions goalMemoryOptions) {
                instance.goalMemoryOptions = goalMemoryOptions;
                return Builder.this;
            }

            public Builder useDefaultGoalMemoryOptions() {
                instance.goalMemoryOptions = GoalMemoryOptions.builder()
                        .name("DefaultGoalMemory")
                        .build();
                return Builder.this;
            }

            public Options build() {
                if (instance.goalMemoryOptions == null) {
                    String msg = "You must specify a memory options.";
                    logger.error(msg);
                    throw new UnsupportedOperationException(msg);
                }
                return instance;
            }

        }
    }

    // endregion
    // ====================================================

}
