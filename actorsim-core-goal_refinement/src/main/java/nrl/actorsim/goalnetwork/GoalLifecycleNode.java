package nrl.actorsim.goalnetwork;

import ch.qos.logback.classic.Level;
import nrl.actorsim.domain.Bindings;
import nrl.actorsim.domain.PredicateStatement;
import nrl.actorsim.domain.Statement;
import nrl.actorsim.domain.WorldObjectAdapter;
import nrl.actorsim.goalrefinement.lifecycle.*;
import nrl.actorsim.goalrefinement.strategies.ApplyStrategyDetailsCollection;
import nrl.actorsim.goalrefinement.strategies.StrategyThread;
import nrl.actorsim.memory.GoalMemorySimple;
import nrl.actorsim.memory.WorkingMemory;
import nrl.actorsim.utils.Utils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;

import static nrl.actorsim.goalrefinement.lifecycle.GoalMode.*;
import static nrl.actorsim.goalrefinement.lifecycle.StrategyName.*;


@SuppressWarnings("SameReturnValue")
public class GoalLifecycleNode
        extends GoalNetworkNode<Statement>
        implements GoalLifecycle {
    protected final static Logger logger = LoggerFactory.getLogger(GoalLifecycleNode.class);

    protected final static Logger errorLogger = logger;

    public final static GoalLifecycleNode NULL_GOAL_NODE;
    static {
        NULL_GOAL_NODE = new GoalLifecycleNode(PredicateStatement.ALWAYS_TRUE_STATEMENT);
    }

    private final Object modeLock = this;
    protected Logger traceLogger = LoggerFactory.getLogger(GoalLifecycleNode.class);
    protected boolean isGoalTracingEnabled = false;
    protected GoalMemorySimple goalMemory;
    /**
     * A string that can be augmented with specialized details in subclasses
     */
    protected String statusString;

    protected boolean dropRequested = false;
    protected boolean dropInProgress = false;

    /*
     * The mode of this goalNode.  Only changed by calling a Strategy (thus private visibility).
     */
    private GoalMode mode = GoalMode.UNFORMED;

    /**
     * The number of times this goalNode has been progressed.
     */
    private int inertia = 0;
    /**
     * The allowed ResolveTypes for this goalNode.
     */
    private EnumSet<ResolveType> enabledResolveTypes = ResolveType.getBitmaskWithSingleTypeEnabled(ResolveType.CONTINUE);

    /**
     * The order to try the enabled ResolveTypes.
     */
    private List<ResolveType> resolveOrder = ResolveType.DEFAULT_RESOLVE_ORDER;

    // ====================================================
    // region<Constructors and Object Overrides>

    public GoalLifecycleNode(Statement statement) {
        super(statement);
    }

    public Cloner<? extends GoalLifecycleNode, ?> cloner() {
        return new Cloner<>(this);
    }


    @Override
    public String toString() {
        return this.stored.getShortName() + "[" + getMode().getShortName() + "]";
    }

    /**
     * @return a string to display in a GUI
     */
    public String toStringDetails() {
        return getName() + " [" + mode + "]";
    }

    // endregion
    // ====================================================

    // ====================================================
    // region<GoalNetworkNode Overrides>

    @Override
    public GoalNetworkNode<Statement> setComplete() {
        String msg = "setComplete() should not be called directly for a GoalLifecycleNode!" +
                " Instead, use node.finish() to progress node: " + this.toString();
        logger.error(msg);
        throw new UnsupportedOperationException(msg);
    }

    protected GoalNetworkNode<Statement> setCompleteSafely() {
        return super.setComplete();
    }

    // endregion
    // ====================================================

    // ====================================================
    // region<Accessors>

    public GoalMode getMode() {
        return mode;
    }

    private void setMode(GoalMode newMode) {
        synchronized (modeLock) {
            mode = newMode;
        }
    }

    public boolean modeMatches(GoalMode matchingMode) {
        return (matchingMode == ANY)
                || (this.getMode() == matchingMode);
    }

    public String getName() {
        return stored.toString();
    }

    public GoalMemorySimple getGoalMemory() {
        return goalMemory;
    }

    public String getStatusString() {
        return statusString;
    }

    public boolean isPrimitive() {
        return isPrimitive;
    }

    public boolean setAsPrimitive() {
        return isPrimitive = true;
    }

    public boolean isNull() {
        return this == NULL_GOAL_NODE;
    }

    /**
     * Increment inertia by one.
     */
    public void incrementInertia() {
        this.inertia++;
    }

    /**
     * Get the current value for inertia.
     */
    public int getInertia() {
        return inertia;
    }

    /**
     * A simple priority scheme to push goals through refinements.
     * Higher priority values take precedent over lower values.
     *
     * @return the combined priority value
     */
    public int getPriority() {
        return inertia;
    }

    public List<ResolveType> getResolveOrder() {
        return resolveOrder;
    }


    // endregion
    // ====================================================

    // ====================================================
    // region<Parent and Subgoal related>

    @Override
    public GoalLifecycleNode[] getParents() {
        if (network != null) {
            return ((GoalLifecycleNetwork) network)
                    .parentsOf(this).toArray(new GoalLifecycleNode[0]);
        }
        return new GoalLifecycleNode[0];
    }

    @Override
    public GoalLifecycleNode[] getSubgoals() {
        if (network != null) {
            return ((GoalLifecycleNetwork) network)
                    .childrenOf(this).toArray(new GoalLifecycleNode[0]);
        }
        return new GoalLifecycleNode[0];
    }

    // endregion
    // ====================================================

    // ====================================================
    // region<Logging>

    public void disableGoalTracing() {
        String className = this.getClass().getName();
        logger.info("Disabling tracing for goalNode {} ", className);
        traceLogger = LoggerFactory.getLogger(GoalLifecycleNode.class);
        isGoalTracingEnabled = false;
    }

    @SuppressWarnings("UnusedReturnValue")
    public GoalLifecycleNode enableGoalTracing() {
        return enableGoalTracing(Level.TRACE);
    }

    public GoalLifecycleNode enableGoalTracingAtDebug() {
        return enableGoalTracing(Level.DEBUG);
    }

    public GoalLifecycleNode enableGoalTracing(Level level) {
        String className = this.getClass().getName();
        logger.info("Enabling trace logging for goals of type {} ", className);
        traceLogger = LoggerFactory.getLogger(this.getClass());
        ch.qos.logback.classic.Logger logbackLogger = (ch.qos.logback.classic.Logger) LoggerFactory.getLogger(className);
        logbackLogger.setLevel(level);
        isGoalTracingEnabled = true;
        traceLogger.trace("Tracing is enabled for {}", className);
        return this;
    }

    /**
     * Returns the logger that is used to trace goals from a specific class.
     * Note that this logger defaults to default logging behavior unless
     * trace logging is enabled.
     */
    public Logger getTraceLogger() {
        return traceLogger;
    }

    public Logger getLogger() {
        return logger;
    }

    public boolean isGoalTracingEnabled() {
        return isGoalTracingEnabled;
    }

    // endregion
    // ====================================================

    // ====================================================
    // region<Strategies>

    // ====================================================
    // region<Formulate Strategy>
    @Override
    final public void formulate(GoalMemorySimple memoryToFormWithin) {
        traceLogger.trace("START FORMULATE ================== {}", this);

        if (memoryToFormWithin != null) {
            if (goalMemory == null) {
                if (checkTransitionAllowed(FORMULATE.getResultingMode())) {
                    goalMemory = memoryToFormWithin;

                    StrategyStatus status = StrategyStatus.getFailingResult();
                    try {
                        status = formulateInSubclass();
                    } catch (Exception e) {
                        String message = getName() + " - exception thrown in subclass method.  GoalLifecycleNode cannot transition...  exception:" + e + " exceptionMessage:" + e.getMessage();
                        traceLogger.error(message);
                        logStackTraceIfRequested(traceLogger, e);
                    }

                    if (status.isSuccess()) {
                        formulateImpl();
                    }
                } else {
                    String message = "The transition to FORMULATED was not allowed for " + toStringDetails();
                    logger.error(message);
                }
            } else {
                String message = toStringDetails() + ": This goalNode is already assigned to the memory " + goalMemory.getName() + ". Remove it from this memory before reforming it.";
                logger.error(message);
            }
        } else {
            String message = toStringDetails() + ": The memory is NULL for the StrategyDetails and parent goalNode; cannot formulate this goalNode without a specific memory.";
            logger.error(message);
        }
        traceLogger.trace("END   FORMULATE __________________ {}", this);
    }

    private void formulateImpl() {
        SimpleNetwork.AddStatus status = goalMemory.add(this);
        if (status.noError()) {
            setMode(FORMULATED);
            performStrategyFollowup(UNFORMED);
        } else {
            logger.error("There was a problem adding the node to memory, not formulating!");
        }
    }

    protected StrategyStatus formulateInSubclass() {
        return StrategyStatus.getSuccessResult();
    }

    // endregion<Formulate Strategy>
    // ====================================================

    // ====================================================
    // region<Select Strategy>

    @Override
    final public void select() {
        traceLogger.trace("START SELECT ================== {}", this);
        if (checkTransitionAllowed(SELECT.getResultingMode())) {
            StrategyStatus status = StrategyStatus.getFailingResult();
            try {
                status = selectInSubclass();
            } catch (Exception e) {
                String message = getName() + " - exception thrown in subclass method.  GoalLifecycleNode cannot transition...  exception:" + e + " exceptionMessage:" + e.getMessage();
                traceLogger.error(message);
                logStackTraceIfRequested(traceLogger, e);
            }

            if (status.isSuccess()) {
                selectImpl();
            }
        } else {
            String message = "The transition to SELECTED was not allowed for " + toStringDetails();
            logger.error(message);
        }
        traceLogger.trace("END   SELECT __________________ {}", this);
    }

    private void selectImpl() {
        GoalMode oldMode = mode;
        setMode(GoalMode.SELECTED);
        performStrategyFollowup(oldMode);
    }

    protected StrategyStatus selectInSubclass() {
        return StrategyStatus.getSuccessResult();
    }

    // endregion<Select Strategy>
    // ====================================================

    // ====================================================
    // region<Expand Strategy>

    @Override
    final public void expanding() {
        traceLogger.trace("START EXPANDING ================== {}", this);
        if (checkTransitionAllowed(EXPANDING)) {
            StrategyStatus status = StrategyStatus.getFailingResult();
            try {
                status = expandingInSubclass();
            } catch (Exception e) {
                String message = getName() + " - exception thrown in subclass method.  GoalLifecycleNode cannot transition...  exception:" + e + " exceptionMessage:" + e.getMessage();
                traceLogger.error(message);
                logStackTraceIfRequested(traceLogger, e);
            }

            if (status.isSuccess()) {
                expandingImpl();
            }
        } else {
            String message = "The transition to EXPANDING was not allowed for " + toStringDetails();
            logger.error(message);
        }
        traceLogger.trace("END   EXPANDING __________________ {}", this);
    }

    private void expandingImpl() {
        GoalMode oldMode = mode;
        setMode(GoalMode.EXPANDING);
        performStrategyFollowup(oldMode);
    }

    protected StrategyStatus expandingInSubclass() {
        return StrategyStatus.getSuccessResult();
    }

    @Override
    final public void expand() {
        traceLogger.trace("START EXPAND ================== {}", this);
        if (checkTransitionAllowed(EXPAND.getResultingMode())) {
            StrategyStatus status = StrategyStatus.getFailingResult();
            try {
                status = expandInSubclass();
            } catch (Exception e) {
                String message = getName() + " - exception thrown in subclass method.  GoalLifecycleNode cannot transition...  exception:" + e + " exceptionMessage:" + e.getMessage();
                traceLogger.error(message);
                logStackTraceIfRequested(traceLogger, e);
            }

            if (status.isSuccess()) {
                expandImpl();
            }
        } else {
            String message = "The transition to EXPANDED was not allowed for " + toStringDetails();
            logger.error(message);
        }
        traceLogger.trace("END   EXPAND __________________ {}", this);
    }

    private void expandImpl() {
        GoalMode oldMode = mode;
        setMode(GoalMode.EXPANDED);
        performStrategyFollowup(oldMode);
    }

    protected StrategyStatus expandInSubclass() {
        return StrategyStatus.getSuccessResult();
    }

    // endregion<Expand Strategy>
    // ====================================================

    // ====================================================
    // region<Commit Strategy>

    @Override
    final public StrategyStatus commit() {
        traceLogger.trace("START COMMIT ================== {}", this);
        StrategyStatus status = StrategyStatus.getFailingResult();
        if (checkTransitionAllowed(COMMIT.getResultingMode())) {
            try {
                status = commitInSubclass();
            } catch (Exception e) {
                String message = getName() + " - exception thrown in subclass method.  GoalLifecycleNode cannot transition...  exception:" + e + " exceptionMessage:" + e.getMessage();
                traceLogger.error("{}", message);
                logStackTraceIfRequested(traceLogger, e);
            }

            if (status.isSuccess()) {
                commitImpl();
            }
        } else {
            String message = "The transition to COMMITTED was not allowed for " + toStringDetails();
            logger.error(message);
        }
        traceLogger.trace("END   COMMIT __________________ {}", this);
        return status;
    }

    protected void logStackTraceIfRequested(Logger logger, Exception e) {
        StackTraceElement[] stack = e.getStackTrace();
        for (StackTraceElement element : stack) {
            logger.error(element.toString());
        }
    }

    private void commitImpl() {
        GoalMode oldMode = mode;
        setMode(GoalMode.COMMITTED);
        performStrategyFollowup(oldMode);
    }

    protected StrategyStatus commitInSubclass() {
        return StrategyStatus.getSuccessResult();
    }

    // endregion<Commit Strategy>
    // ====================================================

    // ====================================================
    // region<Dispatch Strategy>

    @Override
    final public void dispatch() {
        traceLogger.trace("START DISPATCH ================== {}", this);
        if (checkTransitionAllowed(DISPATCH.getResultingMode())) {
            StrategyStatus status = StrategyStatus.getFailingResult();
            try {
                status = dispatchInSubclass();
            } catch (Exception e) {
                String message = getName() + " - exception thrown in subclass method.  GoalLifecycleNode cannot transition...  exception:" + e + " exceptionMessage:" + e.getMessage();
                traceLogger.error(message);
                logStackTraceIfRequested(traceLogger, e);
            }

            if (status.isSuccess()) {
                dispatchImpl();
            }
        } else {
            String message = "The transition to DISPATCHED was not allowed for " + toStringDetails();
            logger.error(message);
        }
        traceLogger.trace("END   DISPATCH __________________ {}", this);
    }

    private void dispatchImpl() {
        GoalMode oldMode = mode;
        setMode(GoalMode.DISPATCHED);
//        notifyParentOfDispatch();
        performStrategyFollowup(oldMode);
    }

    protected StrategyStatus dispatchInSubclass() {
        return StrategyStatus.getSuccessResult();
    }

    // endregion<Dispatch Strategy>
    // ====================================================

    // ====================================================
    // region<Monitor Strategy>

    @Override
    final public void monitor() {
        traceLogger.trace("START MONITOR ================== {}", this);
//        if (checkTransitionAllowed(DISPATCHED) == true) {
//            if (monitorPolicy.isReadyToCallMonitor() == true) {
//                monitorPolicy.notifyMonitorRunning();
//                try {
//                    traceLogger.trace("Calling monitorInSubclass", this);
//                    StrategyStatus status = monitorInSubclass();
//                    if (status.isSuccess()) {
//                        monitorImpl();
//                        if (status.isFinishQuietlyEnabled() == true) {
//                            skipToFinishedQuietly();
//                        }
//                    }
//                } catch (Exception e) {
//                    logger.error("Exception caught: {}", e);
//                } finally {
//                    monitorPolicy.notifyMonitorFinished();
//                }
//            } else if (monitorPolicy.isWaitingToBeScheduled()) {
//                traceLogger.trace("monitorPolicy is waiting to be scheduled.  Requesting an update with the goalNode memory.");
//                memory.notifyChanged(this);
//            } else {
//                traceLogger.trace("monitorPolicy is not ready to be called.  Its state is:{}.", monitorPolicy.stateToString());
//            }
//        } else {
//            String message = toStringTypeAndDetails() + ": The transition to DISPATCHED was not allowed.";
//            logger.error(message);
//        }
        traceLogger.trace("END   MONITOR __________________ {}", this);
    }

    private void monitorImpl() {
        traceLogger.trace("start {}", this);

        GoalMode oldMode = mode;
        setMode(GoalMode.DISPATCHED);
        performStrategyFollowup(oldMode);
        traceLogger.trace("end {}", this);
    }

    protected StrategyStatus monitorInSubclass() {
        return StrategyStatus.getSuccessResult();
    }

    //    public void setMonitorPolicy(MonitorPolicy policy) {
//        this.monitorPolicy = policy;
//    }
//
//    public MonitorPolicy getMonitorPolicy() {
//        return this.monitorPolicy;
//    }
//

    // endregion<Monitory Strategy>
    // ====================================================

    // ====================================================
    // region<Evaluate Strategy>

    @Override
    final public void evaluate() {
        traceLogger.trace("START EVALUATE ================== {}", this);
        if (checkTransitionAllowed(EVALUATE.getResultingMode())) {
            StrategyStatus status = StrategyStatus.getFailingResult();
            try {
                status = evaluateInSubclass();
            } catch (Exception e) {
                String message = getName() + " - exception thrown in subclass method.  GoalLifecycleNode cannot transition...  exception:" + e + " exceptionMessage:" + e.getMessage();
                traceLogger.error(message);
                logStackTraceIfRequested(traceLogger, e);
            }

            if (status.isSuccess()) {
                evaluateImpl();
            }
        } else {
            String message = "The transition to EVALUATED was not allowed for " + toStringDetails();
            logger.error(message);
        }
        traceLogger.trace("END   EVALUATE __________________ {}", this);
    }

    private void evaluateImpl() {
        GoalMode oldMode = mode;
        setMode(GoalMode.EVALUATED);
        performStrategyFollowup(oldMode);
    }

    protected StrategyStatus evaluateInSubclass() {
        return StrategyStatus.getSuccessResult();
    }

    // endregion<Evaluate Strategy>
    // ====================================================

    // ====================================================
    // region<Resolve Strategy>

    @Override
    final public void resolve() {
        traceLogger.trace("START RESOLVE ================== {}", this);
        StrategyStatus status = StrategyStatus.getFailingResult();
        try {
            resolveSetupInSubclass();
        } catch (Exception e) {
            String message = getName() + " - exception thrown in subclass method for resolve setup....  exception:" + e + " exceptionMessage:" + e.getMessage();
            traceLogger.error(message);
            logStackTraceIfRequested(traceLogger, e);
        }

        for (ResolveType resolveType : resolveOrder) {
            if (enabledResolveTypes.contains(resolveType)) {
                status = resolveImpl(resolveType);
                if (status.isSuccess()) {
                    break;
                }
            }
        }
        if (status.isSuccess()) {
            GoalMode oldMode = mode;
            performStrategyFollowup(oldMode);
        }
        try {
            clearResolveSetupInSubclass();
        } catch (Exception e) {
            String message = getName() + " - exception thrown in subclass method for clearing resolve setup...  exception:" + e + " exceptionMessage:" + e.getMessage();
            traceLogger.error(message);
            logStackTraceIfRequested(traceLogger, e);
        }

        traceLogger.trace("END   RESOLVE __________________ {}", this);
    }

    /**
     * Allows a subclass to establish variables for a series of resolutions in a single method
     * that is called prior to specific resolve strategies.
     */
    @SuppressWarnings("EmptyMethod")
    protected void resolveSetupInSubclass() {
    }

    /**
     * Allows a subclass to clear variables for a series of resolutions in a single method
     * that is called after all allowed resolve strategies.
     */
    @SuppressWarnings("EmptyMethod")
    protected void clearResolveSetupInSubclass() {
    }

    private StrategyStatus resolveImpl(ResolveType resolveType) {
        StrategyStatus status = StrategyStatus.getFailingResult();
        switch (resolveType) {
            case ADJUST:
                status = resolveWithAdjust();
                break;
            case CONTINUE:
                status = resolveWithContinue();
                break;
            case REPAIR:
                status = resolveWithRepair();
                break;
            case REEXPAND:
                status = resolveWithReexpand();
                break;
            case DEFER:
                status = resolveWithDefer();
                break;
            case REFORMULATE:
                status = resolveWithReformulate();
                break;
            case DROP:
                status = resolveWithDrop();
                break;
            case TO_DISPATCHED:
                //noinspection DuplicateBranchesInSwitch
                status = resolveWithContinue();
                break;
            case TO_COMMITTED:
                //noinspection DuplicateBranchesInSwitch
                status = resolveWithRepair();
                break;
            case TO_EXPANDED:
                //noinspection DuplicateBranchesInSwitch
                status = resolveWithReexpand();
                break;
            case TO_SELECTED:
                status = resolveToSelected();
                break;
            case TO_FORMULATED:
                status = resolveToFormulated();
                break;
        }
        return status;
    }

    final protected StrategyStatus resolveWithAdjust() {
        StrategyStatus status = StrategyStatus.getFailingResult();
        traceLogger.trace("START RESOLVE WITH ADJUST ================== {}", this);
        if (checkTransitionAllowed(EVALUATED)) {
            try {
                status = resolveWithAdjustInSubclass();
            } catch (Exception e) {
                String message = getName() + " - exception thrown in subclass method.  GoalLifecycleNode cannot transition...  exception:" + e + " exceptionMessage:" + e.getMessage();
                traceLogger.error(message);
                logStackTraceIfRequested(traceLogger, e);
            }

            if (status.isSuccess()) {
                setMode(GoalMode.EVALUATED);
                //performStrategyFollowup() called by resolveImpl() to eliminate cycling during resolve
            }
        } else {
            String message = "The transition to DISPATCHED was not allowed for " + toStringDetails();
            logger.debug(message);
        }
        traceLogger.trace("END   RESOLVE WITH CONTINUE __________________ {}", this);
        return status;
    }

    protected StrategyStatus resolveWithAdjustInSubclass() {
        return StrategyStatus.getSuccessResult();
    }

    final protected StrategyStatus resolveWithContinue() {
        StrategyStatus status = StrategyStatus.getFailingResult();
        traceLogger.trace("START RESOLVE WITH CONTINUE ================== {}", this);
        if (checkTransitionAllowed(DISPATCHED)) {
            try {
                status = resolveWithContinueInSubclass();
            } catch (Exception e) {
                String message = getName() + " - exception thrown in subclass method.  GoalLifecycleNode cannot transition...  exception:" + e + " exceptionMessage:" + e.getMessage();
                traceLogger.error(message);
                logStackTraceIfRequested(traceLogger, e);
            }

            if (status.isSuccess()) {
                setMode(GoalMode.DISPATCHED);
                //performStrategyFollowup() called by resolveImpl() to eliminate cycling during resolve
            }
        } else {
            String message = "The transition to DISPATCHED was not allowed for " + toStringDetails();
            logger.debug(message);
        }
        traceLogger.trace("END   RESOLVE WITH CONTINUE __________________ {}", this);
        return status;
    }

    protected StrategyStatus resolveWithContinueInSubclass() {
        return StrategyStatus.getSuccessResult();
    }

    final protected StrategyStatus resolveWithRepair() {
        StrategyStatus status = StrategyStatus.getFailingResult();
        traceLogger.trace("START RESOLVE WITH REPAIR ================== {}", this);
        if (checkTransitionAllowed(COMMITTED)) {
            try {
                status = resolveWithRepairInSubclass();
            } catch (Exception e) {
                String message = getName() + " - exception thrown in subclass method.  GoalLifecycleNode cannot transition...  exception:" + e + " exceptionMessage:" + e.getMessage();
                traceLogger.error(message);
                logStackTraceIfRequested(traceLogger, e);
            }

            if (status.isSuccess()) {
                setMode(GoalMode.COMMITTED);
                //performStrategyFollowup() called by resolveImpl() to eliminate cycling during resolve
            }
        } else {
            String message = "The transition to COMMITTED was not allowed for " + toStringDetails();
            logger.debug(message);
        }
        traceLogger.trace("END   RESOLVE WITH REPAIR __________________ {}", this);
        return status;
    }

    protected StrategyStatus resolveWithRepairInSubclass() {
        return StrategyStatus.getSuccessResult();
    }

    /**
     * Resolves a goalNode in the \mode{EVALUATED} mode by transitioning it to \mode{EXPANDED}.
     * <p>
     * The default behavior of this method is to discard any existing expansions.
     * <p>
     * This method is final, so subclasses should override resolveWithReexpandInSubclass()
     * for specialized behavior.
     */
    final protected StrategyStatus resolveWithReexpand() {
        StrategyStatus status = StrategyStatus.getFailingResult();
        traceLogger.trace("START RESOLVE WITH REEXPAND ================== {}", this);
        if (checkTransitionAllowed(EXPANDED)) {
            traceLogger.trace("Can transition, calling any subclass methods");
            try {
                status = resolveWithReexpandInSubclass();
            } catch (Exception e) {
                String message = getName() + " - exception thrown in subclass method.  GoalLifecycleNode cannot transition...  exception:" + e + " exceptionMessage:" + e.getMessage();
                traceLogger.error(message);
                logStackTraceIfRequested(traceLogger, e);
            }

            if (status.isSuccess()) {
                setMode(GoalMode.EXPANDED);
                //performStrategyFollowup() called by resolveImpl() to eliminate cycling during resolve
            }
        } else {
            String message = "The transition to EXPANDED was not allowed for " + toStringDetails();
            logger.debug(message);
        }
        traceLogger.trace("END   RESOLVE WITH REEXPAND __________________ {}", this);
        return status;
    }

    protected StrategyStatus resolveWithReexpandInSubclass() {
        return StrategyStatus.getSuccessResult();
    }

    /**
     * Resolves a goalNode in the \mode{EVALUATED} mode by transitioning it to \mode{SELECTED}.
     * <p>
     * The default behavior of this method is to discard any existing expansions.
     * <p>
     * This method is final, so subclasses should override resolveWithDeferInSubclass()
     * for specialized behavior.
     */
    final protected StrategyStatus resolveWithDefer() {
        StrategyStatus status = StrategyStatus.getFailingResult();
        traceLogger.trace("START RESOLVE WITH DEFER ================== {}", this);
        if (checkTransitionAllowed(SELECTED)) {
            traceLogger.trace("Can transition, calling any subclass methods");
            try {
                status = resolveWithDeferInSubclass();
            } catch (Exception e) {
                String message = getName() + " - exception thrown in subclass method.  GoalLifecycleNode cannot transition...  exception:" + e + " exceptionMessage:" + e.getMessage();
                traceLogger.error(message);
                logStackTraceIfRequested(traceLogger, e);
            }

            if (status.isSuccess()) {
                setMode(GoalMode.SELECTED);
                //performStrategyFollowup() called by resolveImpl() to eliminate cycling during resolve
            }
        } else {
            String message = "The transition to SELECTED was not allowed for " + toStringDetails();
            logger.debug(message);
        }
        traceLogger.trace("END   RESOLVE WITH DEFER __________________ {}", this);

        return status;
    }

    protected StrategyStatus resolveWithDeferInSubclass() {
        return StrategyStatus.getSuccessResult();
    }

    /**
     * Resolves a goalNode in the \mode{EVALUATED} mode by transitioning it to \mode{UNFORMED}.
     * <p>
     * Since the base class will not know the details of reformulating a new goalNode, the
     * default behavior of this method is to drop this goalNode from memory.
     * <p>
     * This method is final, so subclasses should override resolveWithReformulateInSubclass()
     * for specialized behavior.
     */
    final protected StrategyStatus resolveWithReformulate() {
        StrategyStatus status = StrategyStatus.getFailingResult();
        traceLogger.trace("START RESOLVE WITH REFORMULATE ================== {}", this);
        if (checkTransitionAllowed(UNFORMED)) {
            traceLogger.trace("Can transition, calling any subclass methods");
            try {
                status = resolveWithReformulateInSubclass();
            } catch (Exception e) {
                String message = getName() + " - exception thrown in subclass method.  GoalLifecycleNode cannot transition...  exception:" + e + " exceptionMessage:" + e.getMessage();
                traceLogger.error(message);
                logStackTraceIfRequested(traceLogger, e);
            }

            if (status.isSuccess()) {
                drop();
                //performStrategyFollowup() called by resolveImpl() to eliminate cycling during resolve
            }
        } else {
            String message = "The transition to UNFORMED was not allowed for " + toStringDetails();
            logger.debug(message);
        }
        traceLogger.trace("END   RESOLVE WITH REFORMULATE __________________ {}", this);
        return status;
    }

    protected StrategyStatus resolveWithReformulateInSubclass() {
        return StrategyStatus.getSuccessResult();
    }

    final protected StrategyStatus resolveWithDrop() {
        throw new UnsupportedOperationException("This strategy is not yet implemented");
    }

    /**
     * Resolves a goalNode in any mode by transitioning it to \mode{SELECTED}.
     * <p>
     * The default behavior of this method is to discard any existing expansions,
     * including those that may have been dispatched.
     * <p>
     * This method is final, so subclasses should override resolveToSelectedInSubclass()
     * for specialized behavior.
     */
    final protected StrategyStatus resolveToSelected() {
        StrategyStatus status = StrategyStatus.getFailingResult();
        traceLogger.trace("START RESOLVE TO SELECTED ================== {}", this);
        if (checkTransitionAllowed(SELECTED)) {
            traceLogger.trace("Can transition, calling any subclass methods");
            try {
                status = resolveToSelectedInSubclass();
            } catch (Exception e) {
                String message = getName() + " - exception thrown in subclass method.  GoalLifecycleNode cannot transition...  exception:" + e + " exceptionMessage:" + e.getMessage();
                traceLogger.error(message);
                logStackTraceIfRequested(traceLogger, e);
            }

            if (status.isSuccess()) {
                setMode(GoalMode.SELECTED);
                //performStrategyFollowup() called by resolveImpl() to eliminate cycling during resolve
            }
        } else {
            String message = "The transition to SELECTED was not allowed for " + toStringDetails();
            logger.debug(message);
        }
        traceLogger.trace("END   RESOLVE TO SELECTED __________________ {}", this);

        return status;
    }

    protected StrategyStatus resolveToSelectedInSubclass() {
        return StrategyStatus.getSuccessResult();
    }

    /**
     * Resolves a goalNode in any mode by transitioning it to \mode{FORMULATED}.
     * <p>
     * The default behavior of this method is to discard any existing expansions,
     * including those that may have been dispatched.
     * <p>
     * This method is final, so subclasses should override resolveWithDeferInSubclass()
     * for specialized behavior.
     */
    final protected StrategyStatus resolveToFormulated() {
        StrategyStatus status = StrategyStatus.getFailingResult();
        traceLogger.trace("START RESOLVE TO FORMULATED ================== {}", this);
        if (checkTransitionAllowed(FORMULATED)) {
            traceLogger.trace("Can transition, calling any subclass methods");
            try {
                status = resolveToFormulatedInSubclass();
            } catch (Exception e) {
                String message = getName() + " - exception thrown in subclass method.  GoalLifecycleNode cannot transition...  exception:" + e + " exceptionMessage:" + e.getMessage();
                traceLogger.error(message);
                logStackTraceIfRequested(traceLogger, e);
            }

            if (status.isSuccess()) {
                setMode(FORMULATED);
                //performStrategyFollowup() called by resolveImpl() to eliminate cycling during resolve
            }
        } else {
            String message = "The transition to FORMULATED was not allowed for " + toStringDetails();
            logger.debug(message);
        }
        traceLogger.trace("END   RESOLVE TO FORMULATED __________________ {}", this);

        return status;
    }

    protected StrategyStatus resolveToFormulatedInSubclass() {
        return StrategyStatus.getSuccessResult();
    }

    /**
     * Allows subclasses to declare which resolve types are enabled.
     * <p>
     * Subclasses should usually also override resolveWith[Type]InSubclass()
     * for the enabled resolve types.
     */
    @Override
    public void setEnabledResolveTypes(EnumSet<ResolveType> resolveTypes) {
        this.enabledResolveTypes = resolveTypes;
    }

    @Override
    public void setResolveOrder(List<ResolveType> resolveOrder) {
        this.resolveOrder = resolveOrder;
    }

    // endregion<Resolve Strategy>
    // ====================================================

    // ====================================================
    // region<Finish Strategy>

    @Override
    final public void finish() {
        traceLogger.trace("START FINISH ================== {}", this);
        if (checkTransitionAllowed(FINISH.getResultingMode())) {
            StrategyStatus status = StrategyStatus.getFailingResult();
            if (canFinish()) {
                try {
                    status = finishInSubclass();
                } catch (Exception e) {
                    String message = getName() + " - exception thrown in subclass method.  GoalLifecycleNode cannot transition...  exception:" + e + " exceptionMessage:" + e.getMessage();
                    traceLogger.error(message);
                    logStackTraceIfRequested(traceLogger, e);
                }

                if (status.isSuccess()) {
                    finishImpl();
                }
            }
            if (status.isFailure()) {
                traceLogger.debug("Finish did not transition, so scheduling a call to resolve()");
                incrementInertia();
            }
        } else {
            String message = "The transition to FINISH was not allowed for " + toStringDetails();
            logger.debug(message);
        }
        traceLogger.trace("END   FINISH __________________ {}", this);
    }

    private void finishImpl() {
        GoalMode oldMode = mode;
        setMode(GoalMode.FINISHED);
        setCompleteSafely();
        performStrategyFollowup(oldMode);
        if (goalMemory.getOptions().dropOnFinish()) {
            drop();
        }
    }

    protected StrategyStatus finishInSubclass() {
        return StrategyStatus.getSuccessResult();
    }

    /**
     * In order to finish, a goalNode must:
     * - have no unfinished subgoals,
     * - be able to finish in its subclass (subclasses return true by default unless overridden).
     */
    @Override
    final public boolean canFinish() {
        traceLogger.trace("Start {}", this);
        boolean isSubtreeFinished = isSubtreeFinished();
        if (!isSubtreeFinished) {
            logger.debug("Cannot finish this goalNode because one or more children are not finished");
        }

        boolean canFinishInSubclass = false;
        try {
            canFinishInSubclass = canFinishInSubclass();
        } catch (Exception e) {
            String message = getName() + " - exception thrown in subclass method.  GoalLifecycleNode cannot transition...  exception:" + e + " exceptionMessage:" + e.getMessage();
            traceLogger.error(message);
            logStackTraceIfRequested(traceLogger, e);
        }

        boolean canFinish = false;
        if (isSubtreeFinished && canFinishInSubclass) {
            canFinish = true;
        }
        traceLogger.trace("End {} isSubtreeFinished:{} canFinishInSubclass:{}", this, isSubtreeFinished, canFinishInSubclass);
        return canFinish;
    }

    final protected boolean isSubtreeFinished() {
        for (GoalLifecycleNode subgoal :
                ((GoalLifecycleNetwork) getNetwork()).childrenOf(this)) {
            if (subgoal.mode != GoalMode.FINISHED) {
                return false;
            }
        }
        return true;
    }

    protected boolean canFinishInSubclass() {
        return true;
    }

    // endregion<Finish Strategy>
    // ====================================================

    // ====================================================
    // region<Drop Strategy>


    public void requestDrop() {
        dropRequested = true;
    }

    public void cancelDropRequest() {
        dropRequested = false;
    }

    public boolean isDropRequestedAndValid() {
        return dropRequested
                & mode != UNFORMED
                & goalMemory != null;
    }

    @Override
    public void wasReleased() {
        super.wasReleased();
        if (!dropInProgress) {
            drop();
        }
    }

    @Override
    final public void drop() {
        dropInProgress = true;
        traceLogger.trace("START DROP ================== {}", this);
        logger.debug("Dropping goalNode {}", this);
        if (checkTransitionAllowed(DROP.getResultingMode())) {
            if (!releaseInProgress) {
                //noinspection unchecked
                getNetwork().release(this);
            }
            GoalMode oldMode = mode;
            setMode(GoalMode.UNFORMED);
            performStrategyFollowup(oldMode);
            goalMemory.getHistory().goalReleased(goalMemory.getSimulationTime(), this, oldMode);
            goalMemory = null;
        } else {
            String message = "The transition to UNFORMED was not allowed for " + toStringDetails();
            logger.error(message);
        }
        traceLogger.trace("END   DROP __________________ {}", this);
        dropInProgress = false;
    }

    // endregion<Drop Strategy>
    // ====================================================

    // ====================================================
    // region<Strategy Helpers>

    @Override
    public void attemptStrategy(StrategyName name) {
        switch (name) {
            //NB: FORMULATE is missing here because you must declare a memory
            case SELECT:
                this.select();
                break;
            case EXPAND:
                this.expand();
                break;
            case COMMIT:
                this.commit();
                break;
            case DISPATCH:
                this.dispatch();
                break;
            case MONITOR:
                this.monitor();
                break;
            case EVALUATE:
                this.evaluate();
                break;
            case FINISH:
                this.finish();
                break;
            case RESOLVE:
                this.resolve();
                break;
            case DROP:
                this.drop();
                break;
            case NEXT:
                this.nextStrategy();
                break;
            default:
                logger.error("Attempting to perform strategy {} on {} is undefined.", name, this);
        }
    }

    @Override
    public void nextStrategy() {
        traceLogger.trace("START NEXT STRATEGY ================== {}", this);
        switch (mode) {
            case FORMULATED:
                select();
                break;
            case SELECTED:
                expand();
                break;
            case EXPANDED:
                commit();
                break;
            case COMMITTED:
                dispatch();
                break;
            case DISPATCHED:
                evaluate();
                break;
            case EVALUATED:
                finish();
                break;

            case FINISHED:
            case UNFORMED:
            default:
                break;
        }
        traceLogger.trace("END   NEXT STRATEGY __________________ {}", this);
    }

    protected boolean checkTransitionAllowed(GoalMode newMode) {
        if (newMode != FORMULATED) {
            traceLogger.trace("Checking whether memory is valid");
            if (this.goalMemory == null) {
                String message = "The current memory is not set for " + this.toString()
                        + "Progressing a goal node requires memory to be set via formulate(memory)."
                        + "(Hint: maybe this is a duplicate node?  If so, use memory.find() to progress the right node.)";
                logger.error(message);
                throw new UnsupportedOperationException(message);
            }

            traceLogger.trace("Checking whether this thread is a StrategyThread");
            Thread currentThread = Thread.currentThread();
            boolean isSafeThread = (currentThread instanceof StrategyThread);
            if (!isSafeThread) {
                String message = "The current thread " + currentThread + " is not an instance of a StrategyThread.  "
                        + "Calls to perform a strategy should be done through the memory.notify methods.";
                logger.error(message);
                throw new UnsupportedOperationException(message);
            }
        }

        traceLogger.trace("Checking whether transition to {} is allowed for {}", newMode, this);
        boolean isTransitionAllowed = GoalModeTransitions.isValidTransition(mode, newMode);

        return isTransitionAllowed;
    }

    private void performStrategyFollowup(GoalMode oldMode) {
        incrementInertia();
        if (mode == FORMULATED
                && oldMode == UNFORMED) {
            goalMemory.getHistory().goalFormulated(goalMemory.getSimulationTime(), this);
        } else {
            goalMemory.getHistory().goalProgressed(goalMemory.getSimulationTime(), this, oldMode);
        }
        logger.debug("updated: {}", this);
    }
    // endregion Strategy Helpers
    // ====================================================

    // endregion<Strategies>
    // ====================================================

    // ====================================================
    // region<Testing Functions>

    public void assertMode(GoalMode expected) {
        GoalMode actual = this.getMode();
        assert (actual == expected) : "Incorrect mode expected:" + expected + " actual:" + actual + " for " + this;
    }

    public class Cloner<V extends GoalLifecycleNode, B extends Cloner<V, B>>
            extends GoalNetworkNode<Statement>.Cloner<V, B> {
        private boolean isPrimitive;

        protected Cloner(GoalLifecycleNode other) {
            super(other);
        }

        @Override
        public V build() {
            if (stored == null) {
                String msg = "You cannot create an empty node!";
                logger.error(msg);
                throw new IllegalArgumentException(msg);
            }
            //noinspection unchecked
            return (V) new GoalLifecycleNode(stored);
        }
    }

    // endregion
    // ====================================================


    // ====================================================
    // region<WorldObjectAdapter>

    public AsWorldObject asWorldObject() {
        return new AsWorldObject(this);
    }

    public static class AsWorldObject extends WorldObjectAdapter<GoalLifecycleNode> {
        AsWorldObject(GoalLifecycleNode goal) {
            super(GOAL, goal, goal.getId().toString());
        }

        public boolean nameEquals(AsWorldObject other) {
            return item.stored.getTarget().nameEquals(other.item.stored.getTarget());
        }

        public boolean bindingsEqual(AsWorldObject other) {
            Bindings thisBindings = item.stored.findBindings();
            Bindings otherBindings = other.item.stored.findBindings();
            int bindingsCompare = Utils.compareToHelper(thisBindings, otherBindings);
            return bindingsCompare == 0;
        }
    }
    // endregion
    // ====================================================


    public void checkFormulateCondition(
            WorkingMemory memory,
            ApplyStrategyDetailsCollection applyDetails) {

    }


    public GoalWrapper wrapper() {
        return new GoalWrapper(this);
    }

}
