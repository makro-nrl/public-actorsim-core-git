package nrl.actorsim.goalnetwork.test;

import nrl.actorsim.goalnetwork.GoalLifecycleNode;
import nrl.actorsim.goalrefinement.strategies.StrategyThread;
import nrl.actorsim.memory.GoalMemorySimple;

/**
 * A helper class to wrap test calls with a Thread implementing
 * StrategyThread, which is required for any calls that progress
 * a GoalLifecycleNode.
 * <p>
 * This class helps avoid needing ot instantiate the CognitiveCycleWorker,
 * which would normally progress goals.
 */
public class StrategyThreadHelper {

    @SuppressWarnings("unused")
    public static void run(Runnable runnable) throws InterruptedException {
        Thread t = new HelperThread(runnable);
        t.start();
        t.join();
    }

    public static
    void formulate(GoalLifecycleNode node, GoalMemorySimple memory)
            throws InterruptedException
    {
        Thread t = new HelperThread(
                () -> node.formulate(memory) //on a separate line for easy breakpoint setting
        );
        t.setName(String.format("StrategyThread%s", t.getId()));
        t.start();
        t.join();
    }

    public static void select(GoalLifecycleNode node) throws InterruptedException {
        Thread t = new HelperThread(
                node::select //on a separate line for easy breakpoint setting
        );
        t.setName(String.format("StrategyThread%s", t.getId()));
        t.start();
        t.join();
    }

    public static void finish(GoalLifecycleNode node) throws InterruptedException {
        Thread t = new HelperThread(
                node::finish //on a separate line for easy breakpoint setting
        );
        t.start();
        t.join();
    }

    public static void drop(GoalLifecycleNode node) throws InterruptedException {
        Thread t = new HelperThread(
                node::drop //on a separate line for easy breakpoint setting
        );
        t.start();
        t.join();
    }

    public static boolean release(GoalLifecycleNode node)
            throws InterruptedException {
        final boolean[] result = new boolean[1];
        Thread t = new HelperThread(() ->
                result[0] = node.release());
        t.start();
        t.join();
        return result[0];
    }


    static class HelperThread extends Thread implements StrategyThread {
        public HelperThread(Runnable runnable) {
            super(runnable);
        }
    }
}
