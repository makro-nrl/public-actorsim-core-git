package nrl.actorsim.goalnetwork;


import nrl.actorsim.domain.Statement;
import nrl.actorsim.memory.LifecycleHistory;

/**
 * A GoalLifecycleNetwork (GLN) extends the GoalNetwork with concerns
 * of the the goal lifecycle.
 */
public class GoalLifecycleNetwork extends GoalNetworkBase<Statement, GoalLifecycleNode> {

    public GoalLifecycleNetwork() {
        super(GoalLifecycleNetwork.class);
        history = new LifecycleHistory();
    }

    LifecycleHistory getHistory() {
        return (LifecycleHistory) history;
    }

    @Override
    public GoalLifecycleNode buildNode(Statement stored) {
        return new GoalLifecycleNode(stored);
    }

    @SuppressWarnings("unchecked")
    @Override
    public <V extends GoalNetworkNode<Statement>> Class<V> getNodeClass() {
        return (Class<V>) GoalLifecycleNode.class;
    }

}
