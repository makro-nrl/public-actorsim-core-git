package nrl.actorsim.goalnetwork;

/**
 * This is a placeholder class for when TGNs are implemented
 * <p>
 * <p>
 * The TemporalGoalLifecycleNetwork extends the TemporalGoalNetwork
 * with concerns from the goal lifecycle.
 */
public class TemporalGoalLifecycleNetwork extends TemporalGoalNetwork {
}
