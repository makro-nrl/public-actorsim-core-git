package nrl.actorsim.goalnetwork;

import nrl.actorsim.goalrefinement.lifecycle.ResolveType;

import java.io.Serializable;
import java.util.List;

/**
 * GoalWrapper makes important parts of GoalLifecycleNode serializable.
 */
public class GoalWrapper implements Serializable {
    String name;
    private final long id;
    String statusString;
    String mode;
    String[] resolveOrder;
    int inertia;


    public GoalWrapper(GoalLifecycleNode goalNode) {
        name = goalNode.getName();
        id = goalNode.getStored().getId();
        statusString = goalNode.statusString;
        mode = goalNode.getMode().getName();
        inertia = goalNode.getInertia();

        List<ResolveType> UnSerResolveOrder = goalNode.getResolveOrder();
        resolveOrder = new String[UnSerResolveOrder.size()];
        for (int i = 0; i < UnSerResolveOrder.size(); i++) {
            resolveOrder[i] = UnSerResolveOrder.get(i).toString();
        }

    }

    public String getStatusString() {
        return statusString;
    }

    public String getMode() {
        return mode;
    }

    public String getName() {
        return name;
    }

    public int getInertia() {
        return inertia;
    }

    public String[] getResolveOrder() {
        return resolveOrder;
    }

    public long getId() {
        return id;
    }
}