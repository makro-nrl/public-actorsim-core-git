package nrl.actorsim.goalrefinement.strategies;

import nrl.actorsim.goalnetwork.GoalLifecycleNode;
import org.jetbrains.annotations.NotNull;

import java.util.Iterator;
import java.util.List;
import java.util.function.Consumer;
import java.util.function.Predicate;

public class ApplyStrategyDetails implements Cloneable {
    public static final ApplyStrategyDetails DEFAULT_APPLY_OPTIONS = new ApplyStrategyDetails();
    public final StrategyTemplate strategy;
    private final GoalLifecycleNode applicableGoal;
    public int attemptedApplications = 0;
    public int maxApplications = Integer.MAX_VALUE;
    public ApplyStrategyDetails(@NotNull StrategyTemplate strategy) {
        this.strategy = strategy;
        this.applicableGoal = GoalLifecycleNode.NULL_GOAL_NODE;
    }

    public ApplyStrategyDetails(@NotNull StrategyTemplate strategy, GoalLifecycleNode applicableGoal) {
        this.strategy = strategy;
        this.applicableGoal = applicableGoal;
    }

    private ApplyStrategyDetails() {
        this.strategy = null;
        this.applicableGoal = GoalLifecycleNode.NULL_GOAL_NODE;
    }

    @Override
    public String toString() {
        if (strategy != null) {
            return strategy.toString();
        }
        return "There is no associated strategy with this details.";
    }

    @Override
    public ApplyStrategyDetails clone() throws CloneNotSupportedException {
        super.clone();
        ApplyStrategyDetails newDetails = new ApplyStrategyDetails(this.strategy, this.applicableGoal);
        newDetails.attemptedApplications = this.attemptedApplications;
        newDetails.maxApplications = this.maxApplications;
        return newDetails;
    }

    public String toStringApplicable() {
        return String.format("%s applies to: %s", strategy, applicableGoal.toString());
    }

    public String toStringApplicableStart() {
        return String.format("%s applies to:", strategy);
    }

    public String toStringGoal() {
        return applicableGoal.toString();
    }

    public boolean isApplicable() {
        return applicableGoal != GoalLifecycleNode.NULL_GOAL_NODE;
    }

    public boolean applyNowAndIncrementAttempted() {
        boolean doApply = false;
        if (isApplicable()
                && (attemptedApplications < maxApplications)) {
            doApply = true;
            attemptedApplications++;
        }

        return doApply;
    }

    public boolean attemptApply(boolean test) {
        return test && applyNowAndIncrementAttempted();
    }

    public void attemptApply(Consumer<GoalLifecycleNode> consumer) {
        if (applyNowAndIncrementAttempted()) {
            consumer.accept(applicableGoal);
        }
    }

    public void attemptApply(Predicate<GoalLifecycleNode> predicate, Consumer<GoalLifecycleNode> consumer) {
        if (attemptApply(predicate.test(applicableGoal))) {
            consumer.accept(applicableGoal);
        }
    }

    public void attemptApplyOnce(List<? extends GoalLifecycleNode> goals, Consumer<GoalLifecycleNode> consumer) {
        Iterator<? extends GoalLifecycleNode> iter = goals.iterator();
        if (attemptApply(iter.hasNext())) {
            GoalLifecycleNode goal = iter.next();
            consumer.accept(goal);
        }
    }

    public
    <G extends GoalLifecycleNode>
    G getGoal() {
        return (G) applicableGoal;
    }

    public static class LogCompressor {
        ApplyStrategyDetails lastDetails = null;
        String startString = "";
        String goalsString = "";
        int count = 0;

        public void logAtDebug(org.slf4j.Logger logger, ApplyStrategyDetails details) {
            if (details.strategy != null
                    && details.strategy.compressLogging()) {
                if ((lastDetails != null) && (details.strategy == lastDetails.strategy)) {
                    addGoal(details.getGoal());
                } else {
                    restart(details, logger);
                }
            } else {
                printAndClear(logger);
                logger.debug("    {}", details.toStringApplicable());
            }
            lastDetails = details;
        }

        private void addGoal(GoalLifecycleNode goal) {
            goalsString += goal.toString();
            count++;
        }

        private void restart(ApplyStrategyDetails details, org.slf4j.Logger logger) {
            printAndClear(logger);
            startString += String.format("    %s", details.toStringApplicableStart());
            addGoal(details.getGoal());
        }

        public void printAndClear(org.slf4j.Logger logger) {
            if (!goalsString.equals("")) {
                logger.debug("{} {} goals: {}", startString, count, goalsString);
                lastDetails = null;
                startString = "";
                goalsString = "";
                count = 0;
            }
        }
    }
}

