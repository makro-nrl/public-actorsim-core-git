package nrl.actorsim.goalrefinement.strategies;

/***
 * An abstract Thread class for making unit tests easier.
 *
 * This is needed because only StrategyThreads can progress goals in GoalMemory,
 * which can help eliminate threading issues when properly used.
 *
 * To use, create an abstract thread:
 *    Thread strategyThread = new StrategyThreadAbstract() {
 *        @Override
 *        public void run() {
 *           // progress goals here
 *        }
 *     };
 *     strategyThread.start();
 *     strategyThread.join();  //pause the unit tests until the strategyThread dies.
 */
@SuppressWarnings("JavaDoc")
public abstract class StrategyThreadAbstract extends Thread implements StrategyThread {

}
