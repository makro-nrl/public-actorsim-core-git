package nrl.actorsim.goalrefinement.strategies;

import nrl.actorsim.goalnetwork.GoalLifecycleNode;
import nrl.actorsim.goalrefinement.lifecycle.GoalMode;
import nrl.actorsim.goalrefinement.lifecycle.StrategyName;
import nrl.actorsim.memory.WorkingMemory;

public class AlwaysFinishCompleted extends AlwaysStrategy {
    public AlwaysFinishCompleted(Class<? extends GoalLifecycleNode> matchingGoalClass) {
        super(matchingGoalClass, GoalMode.ANY, StrategyName.FINISH);
    }

    @Override
    public boolean matches(GoalLifecycleNode goalNode) {
        return super.matches(goalNode)
                && goalNode.isComplete();
    }

    @Override
    public void attemptApply(WorkingMemory memory, ApplyStrategyDetails applyDetails) {
        applyDetails.attemptApply(GoalLifecycleNode::finish);
    }
}
