package nrl.actorsim.goalrefinement.strategies;

import nrl.actorsim.goalnetwork.GoalLifecycleNode;

@SuppressWarnings("unused")
public abstract class StrategyTemplateFormulate implements StrategyTemplateClassSpecific {

    private final Class<? extends GoalLifecycleNode> goalClass;
    private final String shortName;

    public StrategyTemplateFormulate(Class<? extends GoalLifecycleNode> goalClass) {
        this.goalClass = goalClass;
        this.shortName = this.getClass().getSimpleName() + " for " + goalClass.getSimpleName();
    }

    public StrategyTemplateFormulate(GoalLifecycleNode goalNode) {
        this(goalNode.getClass());
    }

    @Override
    public String toString() {
        return getShortName();
    }

    @Override
    public String getShortName() {
        return shortName;
    }

    @Override
    public Class<? extends GoalLifecycleNode> getMatchingClass() {
        return goalClass;
    }
}
