package nrl.actorsim.goalrefinement.strategies;

import nrl.actorsim.goalnetwork.GoalLifecycleNode;
import nrl.actorsim.goalrefinement.lifecycle.GoalMode;
import nrl.actorsim.goalrefinement.lifecycle.StrategyName;

public class AlwaysDispatchStrategy extends AlwaysStrategy {
    public AlwaysDispatchStrategy(Class<? extends GoalLifecycleNode> matchingGoalClass) {
        super(matchingGoalClass, GoalMode.COMMITTED, StrategyName.DISPATCH);
    }

    public AlwaysDispatchStrategy(GoalLifecycleNode matchingGoalNode) {
        this(matchingGoalNode.getClass());
    }
}
