package nrl.actorsim.goalrefinement.strategies;

import nrl.actorsim.goalnetwork.GoalLifecycleNode;
import nrl.actorsim.memory.WorkingMemory;

public class DropRequestedGoalsStrategy implements StrategyTemplate {
    @Override
    public void checkPreconditions(WorkingMemory memory, ApplyStrategyDetailsCollection applyCollection) {
        insertMatchingGoals(memory, applyCollection, GoalLifecycleNode::isDropRequestedAndValid);
    }

    @Override
    public void attemptApply(WorkingMemory memory, ApplyStrategyDetails applyDetails) {
        applyDetails.attemptApply(GoalLifecycleNode::drop);
    }
}
