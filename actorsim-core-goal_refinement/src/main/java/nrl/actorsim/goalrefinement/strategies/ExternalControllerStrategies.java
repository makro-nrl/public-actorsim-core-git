package nrl.actorsim.goalrefinement.strategies;

import nrl.actorsim.domain.*;
import nrl.actorsim.goalnetwork.GoalLifecycleNode;
import nrl.actorsim.goalrefinement.goals.StateVariableGoal;
import nrl.actorsim.goalrefinement.lifecycle.GoalMode;
import nrl.actorsim.goalrefinement.lifecycle.GoalModeTransitions;
import nrl.actorsim.goalrefinement.lifecycle.StrategyGroup;
import nrl.actorsim.goalrefinement.lifecycle.StrategyName;
import nrl.actorsim.memory.WorkingMemory;
import nrl.actorsim.utils.PrintOptions;

import java.util.List;
import java.util.function.Predicate;

import static nrl.actorsim.domain.WorldObject.*;
import static nrl.actorsim.goalrefinement.lifecycle.GoalMode.*;
import static nrl.actorsim.goalrefinement.lifecycle.StrategyName.*;


public class ExternalControllerStrategies<G extends StateVariableGoal> extends StrategyGroup {
    final static org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(ExternalControllerStrategies.class);

    public static StateVariableTemplate EXTERNAL_STRATEGY_CALL;

    public static WorldType STATUS = new WorldType("STATUS");
    public static WorldObject REQUESTED = new WorldObject(STATUS, "REQUESTED");
    public static WorldObject IN_PROGRESS = new WorldObject(STATUS, "IN_PROGRESS");
    public static WorldObject SUCCEEDED = new WorldObject(STATUS, "SUCCEEDED");
    public static WorldObject FAILED = new WorldObject(STATUS, "FAILED");

    static {
        EXTERNAL_STRATEGY_CALL = StateVariableTemplate.builder()
                .name("external_strategy_call")
                .argTypes(GOAL_STRATEGY, GOAL)
                .valueTypes(STATUS)
                .build();
    }

    @SuppressWarnings({"rawtypes", "unchecked"})
    public static
    <G extends StateVariableGoal>
    PersistenceStatement createStatement(StrategyName strategy, G goal, WorldObject status) {
        PersistenceStatement statement = EXTERNAL_STRATEGY_CALL
                .persistenceStatement(strategy.asWorldObject(), goal.asWorldObject())
                .assign(status);

        PrintOptions newOptions = new PrintOptions();
        statement.setPrintOptions(newOptions);
        return statement;
    }

    public ExternalControllerStrategies(G goal, WorkingMemory memory) {
        this(goal, memory, FORMULATE, SELECT, EXPAND, COMMIT, DISPATCH, EVALUATE, FINISH);
    }

    public ExternalControllerStrategies(G goal, WorkingMemory memory, StrategyName... strategies) {
        super();
        for (StrategyName strategyName : strategies) {
            add(new ExternalModeChangeStrategy<>(goal, strategyName, memory));
        }
    }

    private static class TmpStatement extends HoldingStatement {
        protected TmpStatement(String statementToHold) {
            super(statementToHold);
        }
    }

    public static class ExternalModeChangeStrategy<G extends StateVariableGoal> extends StrategyTemplateFilterByGoalClassAndMode {
        final WorkingMemory memory;
        final StrategyName strategyName;

        public ExternalModeChangeStrategy(G goal, StrategyName strategyName, WorkingMemory memory) {
            super(goal.getClass(), GoalMode.ANY);
            this.strategyName = strategyName;
            this.memory = memory;
        }

        @Override
        public boolean matches(GoalLifecycleNode goalNode) {
            //noinspection unchecked
            return super.matches(goalNode)
                    && goalNode instanceof StateVariableGoal
                    && GoalModeTransitions.isValidTransition(goalNode.getMode(), strategyName)
                    && transitionIsRequested((G) goalNode);
        }

        @Override
        public void attemptApply(WorkingMemory memory, ApplyStrategyDetails applyDetails) {
            G goal = applyDetails.getGoal();
            PersistenceStatement<WorldType> memoryStatement = getMatchingStatement(goal);

            if (memoryStatement != null) {
                logger.debug("Attempting to apply {} to goal {}", strategyName, goal);
                memoryStatement.assign(IN_PROGRESS);
                goal.attemptStrategy(strategyName);
                memoryStatement.assign(SUCCEEDED);
            }
        }

        public boolean transitionIsRequested(G goal) {
            logger.debug("Looking for goal {}", goal);
            return getMatchingStatement(goal) != null;
        }

        @SuppressWarnings("rawtypes")
        public PersistenceStatement<WorldType> getMatchingStatement(G goal) {
            PersistenceStatement statement = createStatement(strategyName, goal, REQUESTED);
            List<Statement> matchingRequests =
                    memory.findMatches(new Predicate<>() {
                        @Override
                        public boolean test(Statement testStatement) {
                            return strategyMatches(testStatement, statement)
                                    && goalMatches(testStatement, statement);
                        }

                        private boolean strategyMatches(Statement lStatement, Statement rStatement) {
                            WorldObject lObject = lStatement.getBinding(GOAL_STRATEGY.asArgument().toString());
                            WorldObject rObject = rStatement.getBinding(GOAL_STRATEGY.asArgument().toString());
                            return lObject.equals(rObject);
                        }

                        private boolean goalMatches(Statement lStatement, Statement rStatement) {
                            WorldObject lObject = lStatement.getBinding(GOAL.asArgument().toString());
                            WorldObject rObject = rStatement.getBinding(GOAL.asArgument().toString());

                            if (lObject instanceof StateVariableGoal.AsWorldObject) {
                                if (rObject instanceof StateVariableGoal.AsWorldObject) {
                                    StateVariableGoal.AsWorldObject lGoal = (StateVariableGoal.AsWorldObject) lObject;
                                    StateVariableGoal.AsWorldObject rGoal = (StateVariableGoal.AsWorldObject) rObject;
                                    return lGoal.nameEquals(rGoal)
                                            && lGoal.bindingsEqual(rGoal);
                                }
                            }
                            return false;
                        }
                    });
            if (matchingRequests.size() == 1) {
                //noinspection unchecked
                return (PersistenceStatement) matchingRequests.get(0);
            }
            return null;
        }
    }


    public static class Expand<G extends StateVariableGoal> extends StrategyTemplateFilterByGoalClassAndMode {
        public Expand(G goal) {
            super(goal.getClass(), SELECTED, EXPANDING);
        }

        @Override
        public void attemptApply(WorkingMemory memory, ApplyStrategyDetails applyDetails) {
            G goal = applyDetails.getGoal();
            Statement statement = new TmpStatement("EXPANDED(goal)");
            if (memory.contains(statement)) {
                goal.expand();
            }
        }
    }




}
