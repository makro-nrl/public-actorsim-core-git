package nrl.actorsim.goalrefinement.strategies;

import nrl.actorsim.goalnetwork.GoalLifecycleNode;
import org.jetbrains.annotations.NotNull;

import java.util.*;

public class ApplyStrategyDetailsCollection implements Cloneable {
    final static org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(ApplyStrategyDetailsCollection.class);

    public static ApplyStrategyDetailsCollection DEFAULT_APPLY_OPTIONS = new ApplyStrategyDetailsCollection();

    public final StrategyTemplate strategy;
    public Set<GoalLifecycleNode> applicableGoals = Collections.emptySet();

    public ApplyStrategyDetailsCollection(StrategyTemplate strategy) {
        this.strategy = strategy;
    }

    private ApplyStrategyDetailsCollection() {
        this.strategy = null;
    }

    @Override
    public String toString() {
        if (strategy != null) {
            return strategy.toString();
        }
        return "There is no associated strategy with this details.";
    }

    @Override
    public ApplyStrategyDetailsCollection clone() throws CloneNotSupportedException {
        super.clone();
        ApplyStrategyDetailsCollection newOptions = new ApplyStrategyDetailsCollection(this.strategy);

        if (this.applicableGoals != Collections.EMPTY_SET) {
            newOptions.applicableGoals = new TreeSet<>(this.applicableGoals);
        }
        return newOptions;
    }

    public String toStringApplicable() {
        StringBuilder goalsString = new StringBuilder("[");
        String sep = "";
        int count = 0;
        for (GoalLifecycleNode goal : applicableGoals) {
            goalsString.append(sep).append(goal.toString());
            sep = ", ";
            count++;
            if (count > 3) {
                goalsString.append(" ... ");
                break;
            }
        }
        goalsString.append("]");
        return String.format("%s applies to %d goals: %s", strategy, applicableGoals.size(), goalsString.toString());
    }

    public boolean isApplicable() {
        return applicableGoals.size() > 0;
    }

    public boolean add(@NotNull GoalLifecycleNode goal) {
        if (goal != null) {
            if (applicableGoals == Collections.EMPTY_SET) {
                applicableGoals = new TreeSet<>();
            }
            applicableGoals.add(goal);
            return true;
        }
        logger.error("Cannot addOrUpdate a null goal");
        return false;
    }

    public void addAll(@NotNull Collection<GoalLifecycleNode> goals) {
        if (goals != null) {
            if (goals.size() > 0) {
                if (applicableGoals == Collections.EMPTY_SET) {
                    applicableGoals = new TreeSet<>();
                }
                applicableGoals.addAll(goals);
            }
        } else {
            logger.error("Cannot addOrUpdate a null list of goals");
        }
    }

    public List<ApplyStrategyDetails> split() {
        List<ApplyStrategyDetails> applicableList = new ArrayList<>();
        for (GoalLifecycleNode goal : this.applicableGoals) {
            applicableList.add(new ApplyStrategyDetails(this.strategy, goal));
        }
        return applicableList;
    }

    public int size() {
        return applicableGoals.size();
    }
}

