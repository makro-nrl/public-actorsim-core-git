package nrl.actorsim.goalrefinement.strategies;

import nrl.actorsim.goalnetwork.GoalLifecycleNode;
import nrl.actorsim.goalrefinement.lifecycle.GoalMode;

import java.util.Arrays;
import java.util.EnumSet;
import java.util.List;
import java.util.function.Predicate;

import static nrl.actorsim.goalrefinement.lifecycle.GoalMode.ANY;


/**
 * A subclass of StrategyTemplate that matches goals of a specific class and one or more matching modes.
 */
public abstract class StrategyTemplateFilterByGoalClassAndMode implements StrategyTemplateClassSpecific {
    protected String shortName;
    protected Predicate<GoalLifecycleNode> afterClassAndModeTest;
    private Class<? extends GoalLifecycleNode> matchingGoalClass;
    private EnumSet<GoalMode> matchingModes;


    public StrategyTemplateFilterByGoalClassAndMode(Class<? extends GoalLifecycleNode> matchingGoalClass, GoalMode... matchingModes) {
        init(matchingGoalClass, Arrays.asList(matchingModes));
    }

    public StrategyTemplateFilterByGoalClassAndMode(Class<? extends GoalLifecycleNode> matchingGoalClass,
                                                    GoalMode mode,
                                                    Predicate<GoalLifecycleNode> afterClassAndModeTest) {
        //noinspection ArraysAsListWithZeroOrOneArgument
        init(matchingGoalClass, Arrays.asList(mode));
        this.afterClassAndModeTest = afterClassAndModeTest;
    }

    public StrategyTemplateFilterByGoalClassAndMode(Class<? extends GoalLifecycleNode> matchingGoalClass, List<GoalMode> matchingModes) {
        init(matchingGoalClass, matchingModes);
    }

    public StrategyTemplateFilterByGoalClassAndMode(Class<? extends GoalLifecycleNode> matchingGoalClass,
                                                    List<GoalMode> matchingModes,
                                                    Predicate<GoalLifecycleNode> afterClassAndModeTest) {
        init(matchingGoalClass, matchingModes);
        this.afterClassAndModeTest = afterClassAndModeTest;
    }

    private StrategyTemplateFilterByGoalClassAndMode(GoalLifecycleNode goalNode, GoalMode... matchingModes) {
        init(goalNode.getClass(), Arrays.asList(matchingModes));
    }

    @SuppressWarnings({"unchecked", "rawtypes"})
    private void init(Class matchingGoalClass, List<GoalMode> matchingModes) {
        this.matchingGoalClass = matchingGoalClass;
        this.matchingModes = EnumSet.copyOf(matchingModes);
        this.shortName = this.getClass().getSimpleName();
    }

    @Override
    public Class<? extends GoalLifecycleNode> getMatchingClass() {
        return matchingGoalClass;
    }

    @Override
    public String getShortName() {
        return shortName;
    }

    @Override
    public String toString() {
        StringBuilder modeString = new StringBuilder("[");
        String sep = "";
        for (GoalMode mode : matchingModes) {
            modeString.append(sep).append(mode);
            sep = ", ";
        }
        modeString.append("]");
        return this.getClass().getSimpleName() + " for " + this.matchingGoalClass.getSimpleName() + modeString;
    }

    @Override
    public boolean matches(GoalLifecycleNode goalNode) {
        boolean result = false;
        if (matchingGoalClass.isInstance(goalNode)) {
            GoalMode modeToCheck = goalNode.getMode();
            if (matchingModes.contains(ANY)) {
                result = true;
            } else {
                if (matchingModes.contains(modeToCheck)) {
                    result = ((afterClassAndModeTest == null)
                            || (afterClassAndModeTest.test(goalNode)));
                }
            }
        }
        return result;
    }

}
