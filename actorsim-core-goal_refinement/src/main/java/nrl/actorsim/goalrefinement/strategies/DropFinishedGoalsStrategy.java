package nrl.actorsim.goalrefinement.strategies;

import nrl.actorsim.goalnetwork.GoalLifecycleNode;
import nrl.actorsim.memory.WorkingMemory;

import java.util.Arrays;

import static nrl.actorsim.goalrefinement.lifecycle.GoalMode.FINISHED;
import static nrl.actorsim.goalrefinement.lifecycle.GoalMode.UNFORMED;

public class DropFinishedGoalsStrategy implements StrategyTemplateClassSpecific {
    final Class<? extends GoalLifecycleNode> goalClass;
    private final String shortName;

    public DropFinishedGoalsStrategy(Class<? extends GoalLifecycleNode> goalClass) {
        this.goalClass = goalClass;
        shortName = this.getClass().getSimpleName() + " for " + goalClass.getSimpleName();
    }

    public DropFinishedGoalsStrategy(GoalLifecycleNode goalNode) {
        this(goalNode.getClass());
    }

    @Override
    public boolean matches(GoalLifecycleNode goalNode) {
        return (goalClass.isInstance(goalNode))
                && goalNode.getMode() == FINISHED;
    }

    @Override
    public void attemptApply(WorkingMemory memory, ApplyStrategyDetails applyDetails) {
        applyDetails.attemptApply(goal -> {
            goal.drop();

            GoalLifecycleNode[] parents = goal.getParents();
            for (GoalLifecycleNode parent : parents) {
                parent.evaluate();
                org.slf4j.Logger logger = parent.getLogger();
                if (logger.isDebugEnabled()) {
                    logger.debug("Checking parent {} for UNFORMED goals", parent);
                    Arrays.asList(parent.getSubgoals()).forEach(subgoal -> {
                        if (subgoal.getMode() == UNFORMED) {
                            logger.debug("  Found UNFORMED goalNode {} that probably should have been removed!", subgoal);
                        }
                    });
                }
            }
        });
    }

    @Override
    public String toString() {
        return shortName;
    }

    @Override
    public String getShortName() {
        return shortName;
    }

    @Override
    public Class<? extends GoalLifecycleNode> getMatchingClass() {
        return goalClass;
    }
}
