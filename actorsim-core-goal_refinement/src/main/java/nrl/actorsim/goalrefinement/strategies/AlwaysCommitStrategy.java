package nrl.actorsim.goalrefinement.strategies;

import nrl.actorsim.goalnetwork.GoalLifecycleNode;
import nrl.actorsim.goalrefinement.lifecycle.GoalMode;
import nrl.actorsim.goalrefinement.lifecycle.StrategyName;

public class AlwaysCommitStrategy extends AlwaysStrategy {
    public AlwaysCommitStrategy(Class<? extends GoalLifecycleNode> matchingGoalClass) {
        super(matchingGoalClass, GoalMode.EXPANDED, StrategyName.COMMIT);
    }

    public AlwaysCommitStrategy(GoalLifecycleNode matchingGoalNode) {
        this(matchingGoalNode.getClass());
    }
}
