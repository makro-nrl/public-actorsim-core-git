package nrl.actorsim.goalrefinement.strategies;

import nrl.actorsim.goalnetwork.GoalLifecycleNode;

public interface StrategyTemplateClassSpecific extends StrategyTemplate {
    Class<? extends GoalLifecycleNode> getMatchingClass();
}
