package nrl.actorsim.goalrefinement.strategies;

import nrl.actorsim.goalnetwork.GoalLifecycleNode;
import nrl.actorsim.memory.WorkingMemory;
import org.slf4j.Logger;

public class FormulateOnConditionStrategy<T extends GoalLifecycleNode> extends StrategyTemplateFormulate {
    private final T goalBase;

    public FormulateOnConditionStrategy(T goalBase) {
        super(goalBase.getClass());
        this.goalBase = goalBase;
    }

    @Override
    public void checkPreconditions(WorkingMemory memory, ApplyStrategyDetailsCollection applyCollection) {
        if(goalBase.isGoalTracingEnabled()) {
            Logger traceLogger = goalBase.getTraceLogger();
            traceLogger.trace("{}: Checking preconditions", goalBase);
        }
        goalBase.checkFormulateCondition(memory, applyCollection);
    }

    @Override
    public void attemptApply(WorkingMemory memory, ApplyStrategyDetails applyDetails) {
        applyDetails.attemptApply(baseGoal -> {
            if (goalBase.isGoalTracingEnabled()) {
                baseGoal.enableGoalTracing();
            }

            if(baseGoal.isGoalTracingEnabled()) {
                baseGoal.getTraceLogger().trace("Attempting to formulate");
            }

            baseGoal.formulate(memory.getGoalMemory());
        });
    }
}
