package nrl.actorsim.goalrefinement.strategies;

import nrl.actorsim.goalnetwork.GoalLifecycleNode;
import nrl.actorsim.goalrefinement.lifecycle.GoalMode;
import nrl.actorsim.goalrefinement.lifecycle.StrategyName;
import nrl.actorsim.memory.WorkingMemory;
import nrl.actorsim.utils.Utils;

public class AlwaysStrategy implements StrategyTemplateClassSpecific {
    private final Class<? extends GoalLifecycleNode> matchingGoalClass;
    private final GoalMode matchingMode;
    private final String shortName;
    private final StrategyName strategyToCall;

    public AlwaysStrategy(Class<? extends GoalLifecycleNode> matchingGoalClass, GoalMode matchingMode, StrategyName strategyToCall) {
        this.matchingGoalClass = matchingGoalClass;
        this.matchingMode = matchingMode;
        this.strategyToCall = strategyToCall;
        Class<?> cls = Utils.getOriginalClass(this);
        String simpleName = cls.getSimpleName();
        this.shortName = simpleName + " for " + this.matchingGoalClass.getSimpleName() + "[" + this.matchingMode + "]";
    }

    public AlwaysStrategy(GoalLifecycleNode matchingGoalNode, GoalMode matchingMode, StrategyName strategyToCall) {
        this(matchingGoalNode.getClass(), matchingMode, strategyToCall);
    }

    @Override
    public String toString() {
        return shortName;
    }

    @Override
    public String getShortName() {
        return shortName;
    }

    @Override
    public boolean matches(GoalLifecycleNode goalNode) {
        return matchingGoalClass.isInstance(goalNode)
                && goalNode.modeMatches(matchingMode);
    }

    @Override
    public void attemptApply(WorkingMemory memory, ApplyStrategyDetails applyDetails) {
        if (applyDetails.isApplicable()
                && applyDetails.getGoal() != GoalLifecycleNode.NULL_GOAL_NODE
                && matches(applyDetails.getGoal())) {
            if (applyDetails.applyNowAndIncrementAttempted()) {
                GoalLifecycleNode goal = applyDetails.getGoal();
                goal.attemptStrategy(strategyToCall);
            }
        }
    }

    @Override
    public Class<? extends GoalLifecycleNode> getMatchingClass() {
        return matchingGoalClass;
    }
}

