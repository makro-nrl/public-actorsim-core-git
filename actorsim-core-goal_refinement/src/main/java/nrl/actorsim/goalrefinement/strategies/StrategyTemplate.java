package nrl.actorsim.goalrefinement.strategies;

import nrl.actorsim.goalnetwork.GoalLifecycleNode;
import nrl.actorsim.goalrefinement.lifecycle.Strategy;
import nrl.actorsim.memory.WorkingMemory;
import nrl.actorsim.utils.Named;
import nrl.actorsim.utils.Utils;

import java.util.Set;
import java.util.function.Predicate;

public interface StrategyTemplate extends Strategy, Named {

    default boolean logApply() {
        return true;
    }

    @SuppressWarnings("SameReturnValue")
    default boolean compressLogging() {
        return true;
    }

    @Override
    default String getShortName() {
        Class<?> cls = Utils.getOriginalClass(this);
        return cls.getSimpleName();
    }

    /**
     * Determines if this strategy matches a particular goal.
     * <p>
     * Clients override this for specific behavior.
     *
     * @param node The GoalNode to test
     * @return whether the
     */
    default boolean matches(GoalLifecycleNode node) {
        //default to matching any goal
        return true;
    }

    /**
     * Finds goals in memory that match according to matchesGoal().
     * <p>
     * Clients can override this behavior to modify how matching goals are found,
     * though it is recommended to use the predicate form instead if unique behavior is desired.
     *
     * @param memory          the memory to check
     * @param applyCollection the details object that will hold matching goals
     */
    default void insertMatchingGoals(WorkingMemory memory, ApplyStrategyDetailsCollection applyCollection) {
        Set<GoalLifecycleNode> matchingGoals = memory.getGoalMemory().find(this::matches);
        applyCollection.addAll(matchingGoals);
    }

    /**
     * Finds goals in memory that match according to matchesGoal() _and_ the provided additional predicate test.
     * <p>
     * Clients can override this behavior to modify how matching goals are found,
     * though it is recommended to simply provide the appropriate predicate check.
     *
     * @param memory          the memory to check
     * @param applyCollection the details object that will hold matching goals
     */
    default void insertMatchingGoals(WorkingMemory memory, ApplyStrategyDetailsCollection applyCollection, Predicate<GoalLifecycleNode> additionalTest) {
        Set<GoalLifecycleNode> matchingGoals = memory.getGoalMemory().find(this::matches);
        matchingGoals.removeIf(node -> !additionalTest.test(node));
        if (matchingGoals.size() > 0) {
            applyCollection.addAll(matchingGoals);
        }
    }

    /**
     * Checks the preconditions of this strategy.
     * <p>
     * This method should be called at the beginning of each iteration of the cognitive loop.
     * The default implementation should call the simple version of insertMatchingGoals(memory, applyDetails).
     * Subclasses may override this to perform different checks, though it is recommended
     * to call one of the variants of insertMatchingGoals() at the beginning to leverage the framework.
     *
     * @param memory          the memory to check
     * @param applyCollection the details object that will hold matching goals
     */
    default void checkPreconditions(WorkingMemory memory, ApplyStrategyDetailsCollection applyCollection) {
        insertMatchingGoals(memory, applyCollection);
    }

    /**
     * Attempts to apply the strategy to applicable goals.
     * <p>
     * Subclasses override this to perform the specific behavior of the strategy.
     * <p>
     * It is important that subclasses follow the prescribed mechanism for strategy application
     * using applyDetails.applyNowAndIncrementAttempted() to determine if the cognitive cycle
     * will support more attempts.
     * <p>
     * Single applications should be performed:
     * <p>
     * Iterator<GoalLifecycleNode> iter = applyDetails.applicableGoalNodes.iterator();
     * if (iter.hasNext()
     * && applyDetails.applyNowAndIncrementAttempted()) {
     * SpecialGoal specialGoal = (SpecialGoal) iter.next();
     * // perform work here
     * }
     * <p>
     * while simple iterative applications should be performed:
     * <p>
     * Iterator<GoalLifecycleNode> iter = applyDetails.applicableGoalNodes.iterator();
     * while (iter.hasNext()
     * && applyDetails.applyNowAndIncrementAttempted()) {
     * SpecialGoal specialGoal = (SpecialGoal) iter.next();
     * // perform work here
     * }
     * <p>
     * and complex iterative applications should be performed:
     * <p>
     * Iterator<GoalLifecycleNode> iter = applyDetails.applicableGoalNodes.iterator();
     * if (iter.hasNext()) {
     * SpecialGoal specialGoal = (SpecialGoal) iter.next();
     * <p>
     * Iterator<WorldObject> specialIter = specialGoal.getMatching().iterator();
     * while (specialIter.hasNext()
     * && applyDetails.applyNowAndIncrementAttempted()) {
     * WorldObject match = specialIter.next();
     * // peform work here
     * }
     * }
     * <p>
     * <p>
     * Subclasses
     *
     * @param memory       the memory to check
     * @param applyDetails the details object that will hold matching goals
     */
    void attemptApply(WorkingMemory memory, ApplyStrategyDetails applyDetails);
}
