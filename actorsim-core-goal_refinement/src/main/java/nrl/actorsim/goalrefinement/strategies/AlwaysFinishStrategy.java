package nrl.actorsim.goalrefinement.strategies;

import nrl.actorsim.goalnetwork.GoalLifecycleNode;
import nrl.actorsim.goalrefinement.lifecycle.GoalMode;
import nrl.actorsim.goalrefinement.lifecycle.StrategyName;

public class AlwaysFinishStrategy extends AlwaysStrategy {
    public AlwaysFinishStrategy(Class<? extends GoalLifecycleNode> matchingGoalClass) {
        super(matchingGoalClass, GoalMode.EVALUATED, StrategyName.FINISH);
    }

    public AlwaysFinishStrategy(GoalLifecycleNode matchingGoalNode) {
        this(matchingGoalNode.getClass());
    }
}
