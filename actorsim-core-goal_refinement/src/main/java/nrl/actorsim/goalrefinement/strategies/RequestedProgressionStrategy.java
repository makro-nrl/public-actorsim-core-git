package nrl.actorsim.goalrefinement.strategies;

import nrl.actorsim.goalnetwork.GoalLifecycleNode;
import nrl.actorsim.memory.WorkingMemory;
import org.jetbrains.annotations.NotNull;

public class RequestedProgressionStrategy implements StrategyTemplate {
    public final GoalLifecycleNode node;
    final Runnable runnable;

    public RequestedProgressionStrategy(GoalLifecycleNode node, @NotNull Runnable runnable) {
        this.node = node;
        this.runnable = runnable;
    }

    @Override
    public void attemptApply(WorkingMemory memory, ApplyStrategyDetails applyDetails) {
        runnable.run();
    }
}
