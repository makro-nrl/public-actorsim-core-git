package nrl.actorsim.goalrefinement.strategies;

import nrl.actorsim.goalnetwork.GoalLifecycleNode;
import nrl.actorsim.goalrefinement.lifecycle.GoalMode;
import nrl.actorsim.goalrefinement.lifecycle.StrategyName;

public class AlwaysSelectStrategy extends AlwaysStrategy {
    public AlwaysSelectStrategy(Class<? extends GoalLifecycleNode> matchingGoalClass) {
        super(matchingGoalClass, GoalMode.FORMULATED, StrategyName.SELECT);
    }

    public AlwaysSelectStrategy(GoalLifecycleNode goalNodeObject) {
        this(goalNodeObject.getClass());
    }
}
