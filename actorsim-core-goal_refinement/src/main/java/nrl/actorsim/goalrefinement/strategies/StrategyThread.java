package nrl.actorsim.goalrefinement.strategies;


/**
 * An interface that helps enforce valid changes to GoalLifecycleNodes.
 * <p>
 * The GoalLifecycleNode checks to ensure that a modifying thread implements
 * this interface.
 */
public interface StrategyThread {

}
