package nrl.actorsim.goalrefinement.strategies;

import nrl.actorsim.goalnetwork.GoalLifecycleNode;
import nrl.actorsim.goalrefinement.lifecycle.GoalMode;
import nrl.actorsim.goalrefinement.lifecycle.StrategyName;

public class AlwaysExpandStrategy extends AlwaysStrategy {
    public AlwaysExpandStrategy(Class<? extends GoalLifecycleNode> matchingGoalClass) {
        super(matchingGoalClass, GoalMode.SELECTED, StrategyName.EXPAND);
    }

    public AlwaysExpandStrategy(GoalLifecycleNode matchingGoalNode) {
        this(matchingGoalNode.getClass());
    }
}
