package nrl.actorsim.goalrefinement.goals;

import nrl.actorsim.domain.*;
import nrl.actorsim.goalnetwork.GoalLifecycleNode;
import nrl.actorsim.goalrefinement.strategies.ApplyStrategyDetailsCollection;
import nrl.actorsim.memory.WorkingMemory;

import java.util.Collection;
import java.util.List;
import java.util.Set;

import static nrl.actorsim.goalrefinement.goals.StateVariableGoal.CompleteWhen.PRESENT_IN_MEMORY;

/**
 * A goal base that provides convenience functions to
 * track whether a goal specified as a {@link Statement}
 * needs to be formulated within memory.
 * This class only checks against the target of the statement
 * for consistency, which means that this class does **not**
 * perform temporal consistency checking against the GoalMemory.
 *
 * {@link CompleteWhen} specifies whether this
 * goal is completed, and a goal can be completed when
 * it is PRESENT_IN_MEMORY or MISSING_FROM_MEMORY.
 *
 * An optional formulateCondition, also provided as a {@link Statement},
 * can be provided to specify the condition under which
 * the goal should be formulated.
 *
 * In the absence of a provided formulateCondition, the
 * decision whether to formulate a goal depends only
 * on the CompleteWhen:
 * <ul>
 *   <li> If a goal is completed when it appears in memory
 *        (i.e., it has the form achieved-goal(args)) then the
 *        goal is created if its argument objects appear that are
 *        relevant for this goal and the goal is transitioned
 *        to finished when the statement is missing from memory.  </li>
 *   <li> Conversely, if a goal is completed when it is missing
 *        from memory (e.g., it has the form requires-work(args))
 *        then the goal is created with its argument objects appear
 *        in memory and the goal is finished when the statement
 *        is missing from memory.</li>
 * </ul>
 *
 */
public class StateVariableGoal extends GoalLifecycleNode {
    public static StateVariableGoal NULL_BASE_GOAL = new StateVariableGoal();

    protected final Statement formulateCondition;
    protected final CompleteWhen completeWhen;

    public enum CompleteWhen {
        PRESENT_IN_MEMORY,
        MISSING_FROM_MEMORY
    }

    // ==========================================================
    // region<Constructors>

    @SuppressWarnings("unused")
    public static StateVariableGoal createAchievementGoal(Statement goalStatement,
                                                          Statement formulateCondition) {
        return new StateVariableGoal(goalStatement, PRESENT_IN_MEMORY, formulateCondition);
    }

    private StateVariableGoal() {
        this(PredicateStatement.ALWAYS_TRUE_STATEMENT,
                PRESENT_IN_MEMORY,
                null);
    }

    public StateVariableGoal(Statement goalStatement,
                             CompleteWhen completeWhen) {
        this(goalStatement, completeWhen, null);
    }

    public StateVariableGoal(Statement goalStatement,
                             CompleteWhen completeWhen,
                             Statement formulateCondition) {
        super(goalStatement);
        this.completeWhen = completeWhen;
        this.formulateCondition = formulateCondition;
    }

    // endregion
    // ==========================================================

    // ==========================================================
    // region<Accessors>

    public StateVariable getGoalCondition() { return stored.getTarget(); }

    public StateVariable getFormulateCondition() { return formulateCondition.getTarget(); }

    // endregion
    // ==========================================================

    // ==========================================================
    // region<Condition Checking>

    /**
     * Determines if the completion condition matches memory for any of the applicable goals.
     */
    public void checkFormulateCondition(
            WorkingMemory memory,
            ApplyStrategyDetailsCollection applyDetails)
    {
        if (isGoalTracingEnabled()) {
            traceLogger.trace("Checking formulate condition...");
        }

        if (formulateCondition != null) {
            checkFormulateAndGoalConditions(memory, applyDetails);
        } else {
            insertGoalIfCompletionConditionFails(memory, applyDetails);
        }
    }

    private void checkFormulateAndGoalConditions(WorkingMemory memory, ApplyStrategyDetailsCollection applyDetails) {
        if (completeWhen == CompleteWhen.MISSING_FROM_MEMORY) {
            Collection<? extends Statement> matching
                    = memory.getAddedStatements(this::matchesFormulateConditionTargetName);
            for (Statement statement : matching) {
                traceLogger.trace("  Condition {} found in memory when not expected, ensuring an associated goal.", matching);
                //ACTORSIM-1006: get the world object here from the binding (RR code relies on this)
                Bindings bindings = new Bindings();
                insertGoalIfCompletionConditionFails(memory, applyDetails, bindings);
            }
        } else { //completeWhen == CompleteWhen.PRESENT_IN_MEMORY
            Collection<? extends Statement> matching
                    = memory.getAddedStatements(this::matchesFormulateConditionTargetName);
            for (Statement statement : matching) {
                Bindings bindings = statement.getTarget().findBindings();
                traceLogger.trace("  Formulate condition '{}' found in memory ensuring an associated goal for {}", this, bindings );
                insertGoalIfCompletionConditionFails(memory, applyDetails, bindings);
            }
        }
    }

    private void insertGoalIfCompletionConditionFails(WorkingMemory memory,
                                                      ApplyStrategyDetailsCollection applyDetails,
                                                      Bindings bindings) {
        if (completeWhen == CompleteWhen.MISSING_FROM_MEMORY) {
            Collection<? extends Statement> matching
                    = memory.getAddedStatements(this::matchesGoalConditionTargetName);
            for (Statement statement : matching) {
                //ACTORSIM-1006: somehow, we should be using the provided object here! (RR code relies on this)
                traceLogger.trace("  Condition {} found in memory when not expected, ensuring an associated goal.", matching);
                createAndInsertGoal(memory, statement, applyDetails);
            }
        } else { //completeWhen == CompleteWhen.PRESENT_IN_MEMORY
            Statement statement = stored.cloner().build();
            statement.bindArgs(bindings);
            statement.estimatedStartEqualsEnd();
            if(memory.missing(statement)) {
                traceLogger.trace("  Condition '{}' missing from memory when expected, ensuring an associated goal.", statement);
                createAndInsertGoal(memory, statement, applyDetails);
            }
        }
    }

    private void insertGoalIfCompletionConditionFails(WorkingMemory memory,
                                                      ApplyStrategyDetailsCollection applyDetails) {
        if (completeWhen == CompleteWhen.MISSING_FROM_MEMORY) {
            Collection<? extends Statement> matching
                    = memory.getAddedStatements(this::matchesGoalConditionTargetName);
            for (Statement statement : matching) {
                traceLogger.trace("  Condition {} found in memory when not expected, ensuring an associated goal.", matching);
                createAndInsertGoal(memory, statement, applyDetails);
            }
        } else { //completeWhen == CompleteWhen.PRESENT_IN_MEMORY
            Collection<? extends WorldObject> matching = memory.getAddedObjects(this::matchesAnyConditionArgument);
            for (WorldObject o : matching) {
                Statement statement = stored.cloner().build();
                statement.bindArgs(o);
                statement.estimatedStartEqualsEnd();
                if(memory.missing(statement)) {
                    traceLogger.trace("  Condition '{}' missing from memory when expected, ensuring an associated goal.", statement);
                    createAndInsertGoal(memory, statement, applyDetails);
                }
            }
        }
    }

    protected void createAndInsertGoal(WorkingMemory memory,
                                       Statement goalStatement,
                                       ApplyStrategyDetailsCollection applyDetails) {
        GoalLifecycleNode goal = this.instance(goalStatement);
        insertGoal(goal, applyDetails);
    }

    protected void insertGoal(GoalLifecycleNode goal,
                              ApplyStrategyDetailsCollection applyDetails) {
        if (applyDetails.add(goal)) {
            traceLogger.trace("  .. added goal {}", goal);
        } else {
            traceLogger.trace("  .. goal not added {}", goal);
        }
    }

    // endregion
    // ==========================================================

    // ====================================================
    // region<matching and memory checks>

    public boolean matchesAnyConditionArgument(WorldObject worldObject) {
        return matchesAnyConditionArgument(getGoalCondition(), worldObject);
    }

    public boolean matchesFormulateConditionWorldObject(WorldObject worldObject) {
        return matchesAnyConditionArgument(getFormulateCondition(), worldObject);
    }

    private boolean matchesAnyConditionArgument(StateVariable condition, WorldObject worldObject) {
        List<WorldArgument> args = condition.getArgs();
        for (WorldArgument arg : args) {
            if (worldObject.equalsOrInheritsFromType(arg)) {
                return true;
            }
        }
        return false;
    }

    public boolean matchesGoalConditionTargetName(Statement statement) {
        return matchesGoalConditionName(statement.getTarget());
    }

    public boolean matchesGoalConditionName(StateVariable sv) {
        return getGoalCondition().nameCompareTo(sv) == 0;
    }

    public boolean matchesFormulateConditionTargetName(Statement statement) {
        return matchesFormulateConditionName(statement.getTarget());
    }

    public boolean matchesFormulateConditionName(StateVariable sv) {
        return getFormulateCondition().nameCompareTo(sv) == 0;
    }

    public boolean goalMissing(WorkingMemory memory, Statement statementToCheck) {
        return ! hasGoal(memory, statementToCheck);
    }

    public boolean hasGoal(WorkingMemory memory, Statement statementToCheck) {
        Set<GoalLifecycleNode> foundGoals = memory.getGoalMemory().find(
                goal -> statementToCheck.equals(goal.getStored()));

        return foundGoals.size() > 0;
    }
    // endregion
    // ====================================================

    // ==========================================================
    // region<Goal Instance Creation>

    public StateVariableGoal instance(Statement statement) {
        if (getGoalCondition().nameCompareTo(statement.getTarget()) == 0) {
            StateVariable sv = statement.getTarget();
            Bindings bindings = sv.findBindings();
            if (bindings != null) {
                return instance(bindings);
            }
        }
        return NULL_BASE_GOAL;
    }

    /**
     * A method for subclasses to override or overload
     * to instantiate the correct goal type.
     *
     * @param bindings the object(s) for which to create an instance
     * @return the new goal or NULL_BASE_GOAL if creation fails
     */
    @SuppressWarnings("unused")
    public StateVariableGoal instance(Bindings bindings) {
        return NULL_BASE_GOAL;
    }


    // endregion
    // ==========================================================
}
