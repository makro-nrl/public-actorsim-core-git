/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package nrl.actorsim.goalrefinement.lifecycle;


/**
 * @author mroberts
 */
public interface DropStrategy extends Strategy {
    StrategyName NAME = StrategyName.DROP;
    GoalMode RESULT = GoalMode.UNFORMED;

    /**
     * Drops the goalNode
     */
    void drop();
}
