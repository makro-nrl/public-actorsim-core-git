package nrl.actorsim.goalrefinement.lifecycle;


import nrl.actorsim.goalrefinement.strategies.StrategyTemplate;
import nrl.actorsim.memory.WorkingMemory;

import java.util.ArrayList;
import java.util.List;

/**
 * A convenience class to group a set of related strategies together.
 * Usually, this would be done to group the strategies related to a goalNode
 * in a single place.
 */
public class StrategyGroup {
    protected final List<StrategyTemplate> strategiesToCheckEveryCycle = new ArrayList<>();
    final List<StrategyTemplate> strategiesToApplyOnceAtEndOfCycle = new ArrayList<>();

    public void add(StrategyTemplate strategyToCheckEveryCycle) {
        strategiesToCheckEveryCycle.add(strategyToCheckEveryCycle);
    }

    public void addAll(StrategyGroup group) {
        strategiesToCheckEveryCycle.addAll(group.strategiesToCheckEveryCycle);
        strategiesToApplyOnceAtEndOfCycle.addAll(group.strategiesToApplyOnceAtEndOfCycle);
    }

    public void addAll(List<StrategyTemplate> strategies) {
        this.strategiesToCheckEveryCycle.addAll(strategies);
    }

    public void addStrategies(WorkingMemory memory) {
        for (StrategyTemplate strategy : strategiesToCheckEveryCycle) {
            memory.addStrategyToCheckEveryCycle(strategy);
        }

        for (StrategyTemplate strategy : strategiesToApplyOnceAtEndOfCycle) {
            memory.addApplyOnceAtEndEachCycle(strategy);
        }
    }

    public void addApplyOnceAtEndEachCycle(StrategyTemplate strategy) {
        strategiesToApplyOnceAtEndOfCycle.add(strategy);
    }


}
