package nrl.actorsim.goalrefinement.lifecycle;

import org.slf4j.LoggerFactory;

import java.util.*;

import static nrl.actorsim.goalrefinement.lifecycle.GoalMode.*;

public class GoalModeTransitions {
    protected final static org.slf4j.Logger logger = LoggerFactory.getLogger(GoalModeTransitions.class);

    public final static Map<GoalMode, Set<GoalMode>> INCOMING_TRANSITION = new TreeMap<>();

    static {
        INCOMING_TRANSITION.put(FORMULATED, Set.of(UNFORMED, EVALUATED));
        INCOMING_TRANSITION.put(SELECTED, Set.of(FORMULATED, EVALUATED));
        INCOMING_TRANSITION.put(EXPANDING, Set.of(SELECTED, EVALUATED));
        INCOMING_TRANSITION.put(EXPANDED, Set.of(SELECTED, EXPANDING, EVALUATED, EXPANDED));
        INCOMING_TRANSITION.put(COMMITTED, Set.of(EXPANDED, EVALUATED));
        INCOMING_TRANSITION.put(DISPATCHED, Set.of(COMMITTED, EVALUATED, DISPATCHED));
        INCOMING_TRANSITION.put(EVALUATED, Set.of(DISPATCHED, EVALUATED));
        INCOMING_TRANSITION.put(FINISHED, Set.of(ANY));
        INCOMING_TRANSITION.put(UNFORMED, Set.of(ANY));
    }

    public static boolean isValidTransition(GoalMode from, StrategyName strategyName) {
        GoalMode resultingMode = resultingModeOf(strategyName);
        return isValidTransition(from, resultingMode);
    }

    public static boolean isValidTransition(GoalMode from, GoalMode to) {
        if (INCOMING_TRANSITION.containsKey(to)) {
            Set<GoalMode> allowedIncoming = INCOMING_TRANSITION.get(to);
            return allowedIncoming.contains(ANY)
                    || allowedIncoming.contains(from);
        }
        return false;
    }

    public static Collection<GoalMode> incomingEdgesOf(GoalMode mode) {
        Set<GoalMode> allowedIncoming = Collections.emptySet();
        if (INCOMING_TRANSITION.containsKey(mode)) {
            allowedIncoming = INCOMING_TRANSITION.get(mode);
        }
        return allowedIncoming;
    }

    public static GoalMode resultingModeOf(StrategyName name) {
        switch (name) {
            case FORMULATE:
            case REGOAL:
                return FORMULATED;
            case SELECT:
            case DEFER:
                return SELECTED;
            case EXPAND:
            case REEXPAND:
                return EXPANDED;
            case COMMIT:
            case REPAIR:
                return COMMITTED;
            case DISPATCH:
            case CONTINUE:
                return DISPATCHED;
            case EVALUATE:
            case ADJUST:
                return EVALUATED;
            case FINISH:
                return FINISHED;
            case DROP:
                return UNFORMED;
            case NEXT:
            case MONITOR:
            case RESOLVE:
            case UNKNOWN:
            default:
                logger.error("Unknown resulting mode for strategy {}.", name);
                return UNKNOWN;
        }
    }
}
