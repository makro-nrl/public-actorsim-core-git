package nrl.actorsim.goalrefinement.lifecycle;

import nrl.actorsim.goalnetwork.GoalLifecycleNode;

import java.util.Map;
import java.util.TreeMap;

/**
 * A MonitorPolicy used by the monitor() strategy.
 * <p>
 * The policy transitions through the states
 * WAITING, SCHEDULED, READY, and RUNNING.
 * <p>
 * It begins in the WAITING state.
 * <p>
 * When in the WAITING state, MaintainMemory places the policy's goalNode in the queue with the interval as the timeout and calls notifyScheduled().
 * <p>
 * Once the policy's goalNode is pulled off the queue for processing, MaintainMemory calls notifyReady() and calls monitor()
 * <p>
 * GoalLifecycleNode.monitor() calls notifyRunning() and completes the monitor().  At the end it calls notifyMonitorFinished()
 * <p>
 * notifyMonitorFinished() checks whether newMonitorCallRequested() is true.  If true, it sets the state to WAITING which
 * starts the cycle again.  If false, it sets the state to INVALID which ends the cycle.
 *
 * @author Mak Roberts
 */
public class MonitorPolicy {
    final static org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(MonitorPolicy.class);
    public static MonitorPolicy NULL_MONITOR_POLICY = new MonitorPolicy();
    /**
     * The goalNode to monitor.
     */
    protected GoalLifecycleNode goalNode = GoalLifecycleNode.NULL_GOAL_NODE;
    /**
     * Tracks the number of times each state was transition to; used mainly for testing
     */
    Map<MonitorState, Integer> transitionToCounts;
    /**
     * Tracks the number of times each state was transition from; used mainly for testing
     */
    Map<MonitorState, Integer> transitionFromCounts;
    /**
     * Tracks the state of this monitor object.
     */
    private MonitorState state = MonitorState.INVALID;
    /**
     * The wait time before pushing the goalNode through monitor.
     * For the base class this value is given in milliseconds.
     */
    private int minimumInterval = -1;
    /**
     * Tracks whether a new monitor call will be requested once the current call is fulfilled.  Change with enable[disable]FutureMonitorCalls()
     */
    private boolean isNewMonitorCallRequested;

    protected MonitorPolicy() {
    }

    /**
     * Constructs a new MonitorPolicy. Valid instances must have a non-null, non-empty goalNode and a positive interval.
     *
     * @param goalNodeToMonitor the node to monitor
     * @param minimumInterval the interval
     * @throws UnsupportedOperationException - if either condition is violated
     */
    public MonitorPolicy(GoalLifecycleNode goalNodeToMonitor, int minimumInterval) throws UnsupportedOperationException {
        if (goalNodeToMonitor == GoalLifecycleNode.NULL_GOAL_NODE) {
            String message = "The goalNode must be valid (you passed GoalLifecycleNode.NULL_GOAL_NODE)";
            logger.error(message);
            throw new UnsupportedOperationException(message);
        }
        if (goalNodeToMonitor == null) {
            String message = "The goalNode must be valid (you passed null).  Please refer to the NullObject pattern to avoid passing null.";
            logger.error(message);
            throw new UnsupportedOperationException(message);
        }
        this.goalNode = goalNodeToMonitor;

        if (minimumInterval <= 0) {
            String message = "The minimum interval must be a positive.";
            logger.error(message);
            throw new UnsupportedOperationException(message);
        }
        this.minimumInterval = minimumInterval;
//        goalNode.setMonitorPolicy(this);
        init();
        transitionTo(MonitorState.WAITING);
    }

    private void init() {
        transitionToCounts = new TreeMap<>();
        transitionFromCounts = new TreeMap<>();
        enableFutureMonitorCalls();
    }

    public int getMinimumInterval() {
        return minimumInterval;
    }

    public void setMinimumInterval(int interval) {
        minimumInterval = interval;
    }

    private void transitionTo(MonitorState newState) {
        MonitorState oldState = state;
        if (oldState != newState) {
            state = newState;

            int fromCount = 0;
            if (transitionFromCounts.containsKey(oldState)) {
                fromCount = transitionFromCounts.get(oldState);
            }
            transitionFromCounts.put(oldState, fromCount + 1);

            int toCount = 0;
            if (transitionToCounts.containsKey(newState)) {
                toCount = transitionToCounts.get(newState);
            }
            transitionToCounts.put(newState, toCount + 1);

            logger.debug("Monitor transitioning old:{} new:{} for {}.", oldState, newState, goalNode);
        } else {
            logger.debug("Monitor already in requested state so transition skipped.");
        }
    }

    public boolean isValid() {
        boolean isValid = false;
        if (state != MonitorState.INVALID) {
            isValid = true;
        }
        return isValid;
    }

    public boolean isWaitingToBeScheduled() {
        boolean isWaitingToBeScheduled = false;
        if (state == MonitorState.WAITING) {
            isWaitingToBeScheduled = true;
        }
        return isWaitingToBeScheduled;
    }

    public boolean isScheduled() {
        boolean isScheduled = false;
        if (state == MonitorState.SCHEDULED) {
            isScheduled = true;
        }
        return isScheduled;
    }

    public boolean isReadyToCallMonitor() {
        boolean isReady = false;
        if (state == MonitorState.READY) {
            isReady = true;
        }
        return isReady;
    }

    public void notifyScheduled() {
        transitionTo(MonitorState.SCHEDULED);
    }

    public void notifyReady() {
        transitionTo(MonitorState.READY);
    }

    public void notifyMonitorRunning() {
        transitionTo(MonitorState.RUNNING);
    }

    public void notifyMonitorFinished() {
        if (newMonitorCallRequested()) {
            transitionTo(MonitorState.WAITING);
        } else {
            transitionTo(MonitorState.INVALID);
        }
    }

    /**
     * Checks whether a new monitor will be needed in the future.
     * By default, a new monitor is requested after each call to monitor.
     *
     * @return whether a new monitor is requested
     */
    private boolean newMonitorCallRequested() {
        return isNewMonitorCallRequested;
    }

    protected void disableFutureMonitorCalls() {
        isNewMonitorCallRequested = false;
    }

    protected void enableFutureMonitorCalls() {
        isNewMonitorCallRequested = true;
    }

    public void cancelScheduledMonitor() {
        transitionTo(MonitorState.WAITING);
    }

    public void cancelMonitor() {
        transitionTo(MonitorState.INVALID);
    }

    public Object stateToString() {
        return state.toString();
    }

    /**
     * A state enum for the monitor.
     */
    private enum MonitorState {INVALID, WAITING, SCHEDULED, READY, RUNNING}

}
