/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package nrl.actorsim.goalrefinement.lifecycle;


/**
 * @author mroberts
 */
public interface CommitStrategy extends Strategy {
    StrategyName NAME = StrategyName.COMMIT;
    GoalMode RESULT = GoalMode.COMMITTED;

    /**
     * Expands the goalNode
     *
     * @return the status of calling commit
     */
    @SuppressWarnings("UnusedReturnValue")
    StrategyStatus commit();
}
