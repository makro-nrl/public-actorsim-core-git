/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package nrl.actorsim.goalrefinement.lifecycle;


/**
 * @author mroberts
 */
public interface EvaluateStrategy extends Strategy {
    StrategyName NAME = StrategyName.EVALUATE;
    GoalMode RESULT = GoalMode.EVALUATED;

    /**
     * Evaluates the goalNode
     */
    void evaluate();
}
