package nrl.actorsim.goalrefinement.lifecycle;

public class StrategyStatus {

    StrategyResult result = StrategyResult.FAIL;
    boolean proceedToFinishQuietly = false;
    private StrategyStatus() {
    }

    public static StrategyStatus getSuccessResult() {
        StrategyStatus status = new StrategyStatus();
        status.result = StrategyResult.SUCCESS;
        return status;
    }

    public static StrategyStatus getFailingResult() {
        StrategyStatus status = new StrategyStatus();
        status.result = StrategyResult.FAIL;
        return status;
    }

    public void setSuccessResult() {
        this.result = StrategyResult.SUCCESS;
    }

    public void setFailResult() {
        this.result = StrategyResult.FAIL;
    }

    public boolean isSuccess() {
        return result == StrategyResult.SUCCESS;
    }

    public boolean isFailure() {
        return result == StrategyResult.FAIL;
    }

    public void enableFinishQuietly() {
        proceedToFinishQuietly = true;
    }

    public void disableFinishQuietly() {
        proceedToFinishQuietly = false;
    }

    public boolean isFinishQuietlyEnabled() {
        return proceedToFinishQuietly;
    }

    /**
     * The result from calling a strategy.
     * <p>
     * To cancel a strategy in a subclass, return FAIL.
     */
    public enum StrategyResult {
        SUCCESS,
        SUCCESS_BUT_CONTINUE_TO_NEXT_RESOLUTION,
        FAIL
    }
}

