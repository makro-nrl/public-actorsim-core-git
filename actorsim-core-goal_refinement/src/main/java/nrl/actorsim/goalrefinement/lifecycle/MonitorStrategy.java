/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package nrl.actorsim.goalrefinement.lifecycle;


/**
 * @author mroberts
 */
public interface MonitorStrategy extends Strategy {
    StrategyName NAME = StrategyName.MONITOR;
    GoalMode RESULT = GoalMode.DISPATCHED;

    /**
     * Expands the goalNode
     */
    void monitor();
}
