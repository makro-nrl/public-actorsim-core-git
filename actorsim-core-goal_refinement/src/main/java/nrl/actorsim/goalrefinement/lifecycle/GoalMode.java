/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package nrl.actorsim.goalrefinement.lifecycle;

import nrl.actorsim.domain.WorldObject;
import nrl.actorsim.domain.WorldObjectAdapter;

/**
 * @author mroberts
 */
public enum GoalMode {
    UNFORMED("UNFORMED", "UNFMD"),

    FORMULATED("FORMED", "FORMD"),

    SELECTED("SELECTED", "SELTD"),

    EXPANDING("EXPANDING", "EXPNG"),
    EXPANDED("EXPANDED", "EXPND"),

    COMMITTED("COMMITTED", "COMTD"),

    DISPATCHED("DISPATCHED", "DISPD"),

    EVALUATED("EVALUATED", "EVALD"),

    FINISHED("FINISHED", "FIN_D"),

    ANY("ANY", "ANY"),
    UNKNOWN("UNKNOWN", "UNKN");

    private final String name;
    private final String shortName;

    GoalMode(String name, String shortName) {
        this.name = name;
        this.shortName = shortName;
    }

    public String getName() {
        return name;
    }

    public String getShortName() {
        return shortName;
    }

    // ====================================================
    // region<WorldObjectAdapter>

    AsWorldObject asWorldObject() {
        return new AsWorldObject(this);
    }

    public static class AsWorldObject extends WorldObjectAdapter<GoalMode> {
        protected AsWorldObject(GoalMode item) {
            super(WorldObject.GOAL_MODE, item, item.getName());
        }
    }

    // endregion
    // ====================================================
}
