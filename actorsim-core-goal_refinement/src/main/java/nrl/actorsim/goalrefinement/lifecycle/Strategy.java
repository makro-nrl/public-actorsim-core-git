package nrl.actorsim.goalrefinement.lifecycle;

public interface Strategy {


    static StrategyName getAssociatedStrategyName(GoalMode mode) {
        switch (mode) {
            case FORMULATED:
                return StrategyName.FORMULATE;
            case SELECTED:
                return StrategyName.SELECT;
            case EXPANDED:
                return StrategyName.EXPAND;
            case COMMITTED:
                return StrategyName.COMMIT;
            case DISPATCHED:
                return StrategyName.DISPATCH;
            case EVALUATED:
                return StrategyName.EVALUATE;
            case FINISHED:
                return StrategyName.FINISH;
            case UNFORMED:
                return StrategyName.DROP;
            default:
                return StrategyName.UNKNOWN;
        }
    }

    static GoalMode getAssociatedMode(StrategyName name) {
        switch (name) {
            case FORMULATE:
                return GoalMode.FORMULATED;
            case SELECT:
                return GoalMode.SELECTED;
            case EXPAND:
                return GoalMode.EXPANDED;
            case COMMIT:
                return GoalMode.COMMITTED;
            case DISPATCH:
                return GoalMode.DISPATCHED;
            case EVALUATE:
                return GoalMode.EVALUATED;
            case FINISH:
                return GoalMode.FINISHED;
            case RESOLVE:
                return GoalMode.ANY;
            case DROP:
                return GoalMode.UNFORMED;
            default:
                return GoalMode.UNKNOWN;
        }
    }

}
