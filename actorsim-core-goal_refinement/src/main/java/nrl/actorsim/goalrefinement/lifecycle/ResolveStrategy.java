/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package nrl.actorsim.goalrefinement.lifecycle;

import java.util.EnumSet;
import java.util.List;


/**
 * @author mroberts
 */
public interface ResolveStrategy extends Strategy {
    StrategyName NAME = StrategyName.RESOLVE;
    GoalMode RESULT = GoalMode.ANY;

    /**
     * Resolves the goalNode
     */
    void resolve();

    /**
     * The allowed ResolveTypes for this goalNode.
     *
     * @param resolveTypes the enabled resolve types
     */
    void setEnabledResolveTypes(EnumSet<ResolveType> resolveTypes);

    /**
     * The order to try the enabled ResolveTypes.
     *
     * @param resolveOrder the resolve order
     */
    void setResolveOrder(List<ResolveType> resolveOrder);

}

