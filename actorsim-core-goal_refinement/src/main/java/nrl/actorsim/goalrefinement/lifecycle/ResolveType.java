package nrl.actorsim.goalrefinement.lifecycle;

import java.util.ArrayList;
import java.util.Collections;
import java.util.EnumSet;
import java.util.List;

public enum ResolveType {
    //this first set is for 2014/2015 semantics
    ADJUST,
    CONTINUE,
    REPAIR,
    REEXPAND,
    DEFER,
    REFORMULATE,
    DROP,

    //this second set is for 2016+ semantics 
    TO_DISPATCHED,
    TO_COMMITTED,
    TO_EXPANDED,
    TO_SELECTED,
    TO_FORMULATED;

    public static final List<ResolveType> DEFAULT_RESOLVE_ORDER = getDefaultOrder();
    public static EnumSet<ResolveType> EMTPY_RESOLVE = getEmptyBitmask();
    public static EnumSet<ResolveType> DEFAULT_RESOLVE_WITH_CONTINUE = getBitmaskWithSingleTypeEnabled(CONTINUE);

    public static List<ResolveType> getDefaultOrder() {
        List<ResolveType> order = new ArrayList<>();
        order.add(ADJUST);
        order.add(CONTINUE);
        order.add(REPAIR);
        order.add(REEXPAND);
        order.add(DEFER);
        order.add(TO_FORMULATED);
        order.add(REFORMULATE);
        order.add(DROP);
        return order;
    }

    public static EnumSet<ResolveType> getEmptyBitmask() {
        return getEmptyBitmask(false);
    }

    public static EnumSet<ResolveType> getEmptyBitmaskSynchronized() {
        return getEmptyBitmask(true);
    }

    private static EnumSet<ResolveType> getEmptyBitmask(boolean constructSynchronized) {
        EnumSet<ResolveType> enumSet;
        if (constructSynchronized) {
            enumSet = (EnumSet<ResolveType>) Collections.synchronizedSet(EnumSet.noneOf(ResolveType.class));
        } else {
            enumSet = EnumSet.noneOf(ResolveType.class);
        }
        return enumSet;
    }

    public static EnumSet<ResolveType> getBitmaskWithSingleTypeEnabled(ResolveType type) {
        EnumSet<ResolveType> bitmask = getEmptyBitmask();
        bitmask.add(type);
        return bitmask;
    }

    public static EnumSet<ResolveType> getBitmaskWithContinueEnabled() {
        EnumSet<ResolveType> bitmask = getEmptyBitmask();
        bitmask.add(ResolveType.CONTINUE);
        return bitmask;
    }

    public static EnumSet<ResolveType> getBitmask(ResolveType one, ResolveType two) {
        EnumSet<ResolveType> bitmask = getEmptyBitmask();
        bitmask.add(one);
        bitmask.add(two);
        return bitmask;
    }
}

