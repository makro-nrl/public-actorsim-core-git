/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package nrl.actorsim.goalrefinement.lifecycle;


/**
 * @author mroberts
 */
public interface SelectStrategy extends Strategy {
    StrategyName NAME = StrategyName.SELECT;
    GoalMode RESULT = GoalMode.SELECTED;

    /**
     * Selects the goalNode
     */
    void select();
}
