/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package nrl.actorsim.goalrefinement.lifecycle;


import nrl.actorsim.memory.GoalMemorySimple;

/**
 * @author mroberts
 */
public interface FormulateStrategy extends Strategy {
    StrategyName NAME = StrategyName.FORMULATE;
    GoalMode RESULT = GoalMode.FORMULATED;

    /**
     * Formulates a goalNode from the subclass.
     */
    void formulate(GoalMemorySimple memoryToFormWithin);
}
