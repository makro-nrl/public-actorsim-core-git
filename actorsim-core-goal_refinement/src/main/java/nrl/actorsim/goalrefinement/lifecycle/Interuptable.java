package nrl.actorsim.goalrefinement.lifecycle;

/**
 * Provides a mechanism for a goalNode to declare that it can be interrupted if DISPATCHED.
 */
public interface Interuptable {
    boolean isCurrentlyAssigned();

    void interrupt();
}
