/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package nrl.actorsim.goalrefinement.lifecycle;


/**
 * A testing "strategy" used to push goals through the lifecycle by calling GoalLifecycleNode.nextStrategy().
 * It is not an actual strategy of the goalNode lifecycle.
 *
 * @author mroberts
 */
public interface NextStrategy extends Strategy {
    StrategyName NAME = StrategyName.NEXT;
    GoalMode RESULT = GoalMode.ANY;

    /**
     * Attempt to push the goalNode through the next applicable strategy.
     */
    void nextStrategy();
}
