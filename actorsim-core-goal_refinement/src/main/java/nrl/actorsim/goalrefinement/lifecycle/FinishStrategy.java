/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package nrl.actorsim.goalrefinement.lifecycle;


/**
 * @author mroberts
 */
public interface FinishStrategy extends Strategy {
    StrategyName NAME = StrategyName.FINISH;
    GoalMode RESULT = GoalMode.FINISHED;

    boolean canFinish();

    /**
     * Finishes the goalNode
     */
    void finish();
}
