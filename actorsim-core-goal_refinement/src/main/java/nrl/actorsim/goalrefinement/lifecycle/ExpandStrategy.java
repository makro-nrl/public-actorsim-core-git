/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package nrl.actorsim.goalrefinement.lifecycle;


/**
 * @author mroberts
 */
public interface ExpandStrategy extends Strategy {
    StrategyName NAME = StrategyName.EXPAND;
    GoalMode RESULT = GoalMode.EXPANDED;

    /**
     * Expands the goalNode
     */
    void expanding();

    /**
     * Expands the goalNode
     */
    void expand();
}


