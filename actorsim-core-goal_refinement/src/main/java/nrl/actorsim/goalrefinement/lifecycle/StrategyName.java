package nrl.actorsim.goalrefinement.lifecycle;

import nrl.actorsim.domain.WorldObject;
import nrl.actorsim.domain.WorldObjectAdapter;

public enum StrategyName {
    FORMULATE("Formulate", "Form"),
    SELECT("Select", "Sel"),
    EXPAND("Expand", "Expd"),
    COMMIT("Commit", "Comt"),
    DISPATCH("Dispatch", "Disp"),
    MONITOR("Monitor", "Mon"),
    EVALUATE("Evaluate", "Eval"),
    FINISH("Finish", "Fin"),
    DROP("Drop", "Drop"),

    RESOLVE("Resolve", "Res"),
    ADJUST("Adjust", "Adj"),
    CONTINUE("Continue", "Cont"),
    REPAIR("Repair", "Repr"),
    REEXPAND("Reexpand", "Reex"),
    DEFER("Defer", "Defr"),
    REGOAL("Regoal", "Regl"),

    NEXT("Next", "Next"),
    UNKNOWN("UNKOWN", "UNK");
    private final String name;
    private final String shortName;

    StrategyName(String name, String shortName) {
        this.name = name;
        this.shortName = shortName;
    }

    public GoalMode getResultingMode() {
        return GoalModeTransitions.resultingModeOf(get(name));
    }

    private static StrategyName get(String command) {
        if (command.equals(FORMULATE.name)) {
            return FORMULATE;
        } else if (command.equals(SELECT.name)) {
            return SELECT;
        } else if (command.equals(EXPAND.name)) {
            return EXPAND;
        } else if (command.equals(COMMIT.name)) {
            return COMMIT;
        } else if (command.equals(DISPATCH.name)) {
            return DISPATCH;
        } else if (command.equals(MONITOR.name)) {
            return MONITOR;
        } else if (command.equals(EVALUATE.name)) {
            return EVALUATE;
        } else if (command.equals(FINISH.name)) {
            return FINISH;
        } else if (command.equals(DROP.name)) {
            return DROP;
        } else if (command.equals(RESOLVE.name)) {
            return RESOLVE;
        } else if (command.equals(NEXT.name)) {
            return NEXT;
        } else {
            return UNKNOWN;
        }
    }

    private static StrategyName contains(String command) {
        if (command.contains(FORMULATE.name)) {
            return FORMULATE;
        } else if (command.contains(SELECT.name)) {
            return SELECT;
        } else if (command.contains(EXPAND.name)) {
            return EXPAND;
        } else if (command.contains(COMMIT.name)) {
            return COMMIT;
        } else if (command.contains(DISPATCH.name)) {
            return DISPATCH;
        } else if (command.contains(MONITOR.name)) {
            return MONITOR;
        } else if (command.contains(EVALUATE.name)) {
            return EVALUATE;
        } else if (command.contains(FINISH.name)) {
            return FINISH;
        } else if (command.contains(RESOLVE.name)) {
            return RESOLVE;
        } else if (command.contains(DROP.name)) {
            return DROP;
        } else if (command.contains(NEXT.name)) {
            return NEXT;
        } else if (command.contains(FORMULATE.shortName)) {
            return FORMULATE;
        } else if (command.contains(SELECT.shortName)) {
            return SELECT;
        } else if (command.contains(EXPAND.shortName)) {
            return EXPAND;
        } else if (command.contains(COMMIT.shortName)) {
            return COMMIT;
        } else if (command.contains(DISPATCH.shortName)) {
            return DISPATCH;
        } else if (command.contains(MONITOR.shortName)) {
            return MONITOR;
        } else if (command.contains(EVALUATE.shortName)) {
            return EVALUATE;
        } else if (command.contains(FINISH.shortName)) {
            return FINISH;
        } else if (command.contains(RESOLVE.shortName)) {
            return RESOLVE;
        } else if (command.contains(DROP.shortName)) {
            return DROP;
        } else if (command.contains(NEXT.shortName)) {
            return NEXT;
        } else {
            return UNKNOWN;
        }
    }

    public static StrategyName getMatching(String command) {
        return contains(command);
    }

    public String getName() {
        return name;
    }

    public String getShortName() {
        return shortName;
    }

    // ====================================================
    // region<WorldObjectAdapter>

    public AsWorldObject asWorldObject() {
        return new AsWorldObject(this);
    }

    public static class AsWorldObject extends WorldObjectAdapter<StrategyName> {
        protected AsWorldObject(StrategyName item) {
            super(WorldObject.GOAL_STRATEGY, item, item.getName());
        }
    }

    // endregion
    // ====================================================
}
