package nrl.actorsim.planner;

import nrl.actorsim.domain.PlanningDomain;
import nrl.actorsim.memory.WorkingMemory;

public class GTPyhopPlanRequestWithMemory extends PlanRequestWithMemory {

    public GTPyhopPlanRequestWithMemory(WorkingMemory memory, PlanningDomain domain) {
        super(memory, domain);
    }
}
