package nrl.actorsim.planner;

import nrl.actorsim.memcached.CommandInterface;
import nrl.actorsim.memcached.MemcachedConnector;
import nrl.actorsim.memcached.MemcachedOptions;

import java.time.Duration;
import java.time.Instant;

/**
 * Generates plans by converting the contents of a {@link PlanRequest}
 * to a json object and submitting the request through memcached.
 */
public class GTPyhopPlannerWorker extends PlannerWorkerBase<GTPyhopPlanRequestWithMemory> {
    final static org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(GTPyhopPlannerWorker.class);
    public static String GTPYHOP_COMMAND_KEY = "gtpyhop_command";
    public static String GTPYHOP_RESULT_KEY = "gtpyhop_result";

    final MemcachedConnector memcached;

    public GTPyhopPlannerWorker(PlannerOptions options) {
        super(options);
        MemcachedOptions memcachedOptions = MemcachedOptions.builder()
                .commandKey(GTPYHOP_COMMAND_KEY)
                .resultKey(GTPYHOP_RESULT_KEY)
                .commandClass(GTPyhopCommand.class)
                .build();
        memcached = new MemcachedConnector(memcachedOptions);
    }

    @Override
    protected void runPlanner() {
        memcached.initMemcacheAndKeysIfNeeded();
        int timeoutInSeconds = plannerOptions.plannerTimeoutInSeconds.getValueAsInt();
        logger.debug("Running planner (timeout={}secs)...", timeoutInSeconds);

        currentRequest.generateDomainAndProblem();
        GTPyhopCommand command = GTPyhopCommand.planTest(currentRequest);
        memcached.sendCommand(command);
        Instant timeout = Instant.now().plus(Duration.ofSeconds(timeoutInSeconds));
        Instant now = Instant.now();
        CommandInterface result = memcached.readResult();
        while (result == null
                && now.isBefore(timeout)) {
            result = memcached.readResult();
            try {
                //noinspection BusyWait
                sleep(251); //sleep for about a quarter of a second, waiting for a new plan
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            now = Instant.now();
        }
        logger.debug("Received result {}", result);
        if (result != null) {
            currentRequest.resetPlan();
            String[] tokens = ((GTPyhopCommand) result).decomposition.split("\n");
            for (String line : tokens) {
                logger.debug("plan-line(" + line + "): {}", line);
                currentRequest.addPlanLine(line);
            }
        }
    }

    // endregion
    // ====================================================



}
