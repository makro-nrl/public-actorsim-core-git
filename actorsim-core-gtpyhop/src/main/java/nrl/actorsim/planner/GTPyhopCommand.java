package nrl.actorsim.planner;

import nrl.actorsim.domain.Statement;
import nrl.actorsim.goals.RequestGoal;
import nrl.actorsim.memcached.CommandInterface;

public class GTPyhopCommand extends CommandInterface {


    public enum ActionName {
        UNSPECIFIED,
        PLAN_TEST,
        PLAN_FOR,
        PLAN_KILL
    }

    public ActionName action;
    public String goal_list;
    public String plan;
    public String decomposition;
    public String planner_output;

    public boolean capture_planner_output;
    public int planner_timeout_in_seconds;

    public GTPyhopCommand() {
        this(ActionName.UNSPECIFIED);
    }

    public GTPyhopCommand(ActionName action) {
        this.action = action;
    }

    public static GTPyhopCommand planFor(String goal_list) {
        GTPyhopCommand command = new GTPyhopCommand(ActionName.PLAN_FOR);
        command.goal_list = goal_list;
        return command;
    }

    public static GTPyhopCommand planTest(GTPyhopPlanRequestWithMemory request) {
        GTPyhopCommand command = new GTPyhopCommand(ActionName.PLAN_TEST);
        RequestGoal goalBase = request.getMainGoal();
        Statement statement = goalBase.getStored();
        command.goal_list = statementToString(statement);
        return command;
    }

    private static String statementToString(Statement statement) {
        String targetString = statement.getTarget().getName();
        String argsString;
        if (statement.hasBindings()) {
            argsString = statement.toStringBindings();
        } else {
            argsString = statement.toStringArgs();
        }
        String valueString = statement.toStringValue();
        String result = String.format("%s%s%s", targetString, argsString, valueString);
        result = result
                .replace("<<","(" ).replace(">>", ")")
                .replace("<","(" ).replace(">", ")")
                .replace("minecraftitem==","");

        return result;

    }


}
